//
//  PRAppDelegate.h
//  Plectrum Relauncher
//
//  Created by Petros Loukareas on 01/08/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PRAppDelegate : NSObject <NSApplicationDelegate>


@end

