//
//  PRAppDelegate.m
//  Plectrum Relauncher
//
//  Created by Petros Loukareas on 01/08/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import "PRAppDelegate.h"


@interface PRAppDelegate()

@property (strong) NSTimer *timer;

@end


@implementation PRAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.Plectrum"];
    if ([appArray count] > 0) {
        [(NSRunningApplication *)[appArray objectAtIndex:0] terminate];
    }
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(checkAppTermination) userInfo:nil repeats:YES];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {

}

- (void)checkAppTermination {
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.Plectrum"];
    if ([appArray count] == 0) {
        NSString *path = [[[[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
        [[NSWorkspace sharedWorkspace] launchApplication:path];
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        [NSApp terminate:nil];
    }
}


@end
