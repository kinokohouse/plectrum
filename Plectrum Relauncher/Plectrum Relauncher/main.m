//
//  main.m
//  Plectrum Relauncher
//
//  Created by Petros Loukareas on 01/08/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
