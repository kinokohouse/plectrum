# Plectrum #
Previously known as Triggerfinger. An app to show song changes and to control iTunes, just like GrowlTunes, but things got a bit out of hand and now we have all this.

* also controls Swinsian and Spotify (and Apple Music instead of iTunes under 10.15 and higher): play, pause, next song, previous song, what have you;
* song change notification:
    * can use Notifications to show song changes (improving on what iTunes offers itself);
    * ...can also use Growl instead if installed and running, if you prefer;
    * ...and has its own internal notification solution if none of the above do it for you.
* has its own internet radio streamer based on [ `FreeStreamer`](https://muhku.github.io), from which you can also record;
    * you can add an arbitrary number of internet radio favorites...
    * I've provided 10 of my personal favorites (don't judge me)...
    * IMPORTANT: None of these stations endorse this piece of software, I just like what they're broadcasting. :) Artwork may or may not be from their official channels. If you are the actual claimant and want it removed, drop me a line.
    * ...and by all means, add some good ones from yourself, by hand...
    * ...or pick a few from the thousands supplied by [www.radio-browser.info](https://www.radio-browser.info)!
    * ...of which you can include as many as you like for selection from the status menu...
    * ...they can display live artwork while playing (where available - some tinkering required)...
    * ...and you can preview the stream when editing/adding a new favorite;
* you can also open streams by URL without the need to add a favorite.
* you can save the track info of the currently playing track to a text file with a single keystroke. Hear a song on the radio and want to check the album out later? Hit the keystroke and it will be added to the text file.
* new feature, tell it in AppleScript 'now playing', it will return a compound string of the artist, the playing track and the currently playing application, some radio stations without metadata will just return 'Internet broadcast or relay'.
* ...and much more!

(*) Actually from a separate stream, see below also.

Requires 10.12+. Ready to use binary (unsigned, so launch it with right-click-open when running it for the first time) from the `Downloads` section. MIT License.

Icons courtesy of www.flaticon.com; artist is www.freepik.com.


Known limitations
-----------------
* After reverting back to FreeStreamer, there're still some bugs in there, for example, if you lose the connection, there's a fair chance that Plectrum will crash. The issue is currently under investigation, as are some other bugs.
* The 'Liked' property from Spotify does not work correctly (a known bug in Spotify), therefore the 'rating' that shows up in notifications (the row of hearts and dots) is calculated from the 'popularity' value.
* Plectrum cannot open Windows Media based streams.
* Recording is limited to exact stream copy, so no recording in raw PCM.
* Recording is basically downloading from a separate stream, so things to keep in mind:
    * It uses twice the bandwidth of a regular radio stream;
    * There might be a couple of seconds of discrepancy at the beginning and end of the recording, compared to what you were hearing when you made the recording.
    * You'll also always get a extra few seconds' worth of recording. :)
* As for pausing the stream, you have two options, between which you can choose in the settings (except if you have a stream currently running and paused, in which case the pertinent controls will be disabled) - and, you are advised that the latter option is the way to go:
    * pausing will actually pause the stream, and recommence playing it on unpause (i.e. the unplayed part of the stream will build up in the buffer - and also, metadata will probably get unsynched during this pausing;
    * pausing will mute the stream, and unmute on unpause (default) - metadata changes (song switches will stay in sync).
* If you're recording and decide to edit your radio favorites, expect some, well, unexpected stuff (superfluous labels in your Audacity label file, or your recording being stopped if you edit the currently recording favorite or if you remove the currently recording favorite from the menu). It's a bit like the old days - don't mess with the VHS while it is recording.
* HACK ALERT: Radio notifications are delayed 1 second when fetching artwork is enabled for a radio favorite, to ensure the artwork network grab is correct (most of the time). Even if there is a misgrab, another corrective grab will take place after 5 seconds, but the first onscreen notification may still show the wrong artwork. If you use the new (since 3.25) feature to defer notifications until new artwork is available, it will retry fetching new artwork 8 times for every 3 seconds, after which it will send a notification anyway with generic artwork. With this option enabled, you may notice that in some cases the song information has already changed (when manually showing current song info) before the artwork has changed, as song metadata will be immediately update even if the new song notification is deferred.
* If you start playing audio in one application that's under control of Plectrum, while audio from another such application is playing, the audio already playing should stop or pause. There can be some lag in stopping the audio, depending on the circumstances. Version 3.68 takes some precautions to at least immediately stop the radio playing.
* Not really a limitation of Plectrum, but anyway... The FIP stream does not contain metadata, but if you're still on 10.14 or below and like to know the title of the cool song they played just now, you might like to give [this Dashboard widget](https://bitbucket.org/kinokohouse/fip-dashboard-widget/src/master/) a whirl.

![Something Something Surf Music [h/t Piet]](https://bitbucket.org/kinokohouse/plectrum/raw/298d74adf6deab7ab76a29fd41e6bee62ac5cbf1/vox-support.jpg "Something Something Surf Music [h/t Piet]")

#### Any other questions?


Building
--------
Project is made in Xcode 10.3, but should open in anything from 8 up. The Growl framework (2.0.1) has been included. If you have a need to see the full source, or if you need to add a fresh copy for some reason, you can find it here: 

* https://web.archive.org/web/20200414120446/http://growl.info/downloads

_(Growl has been retired some time ago, but all stuff is still available through the archives)_

Building under 10.14 is recommended - debugging under macOS Vista (h/t [Jeff Johnson](https://twitter.com/lapcatsoftware)) is all but impossible, at least for me. If anyone has hints on how to debug SBApplications under 10.15/XCode 12.4, please send them along.




### Version History

**3.69 (build 135, current version):**

* Only fixing some documentation wording and bugs, therefore just bumping the build number.


**3.69 (build 134):**

* New feature, tell it in AppleScript 'now playing', it will return a compound string of the artist, the playing track and the currently playing application, some radio stations without metadata will just return 'Internet broadcast or relay'.


**3.68 (build 132):**

* For some reason the artwork in the menu refused to update immediately - should be fixed now.
* Also starting application in one application would not immediately stop the audio in another application. It never occured before because it usually only happens when your computer is really busy. This is a known limitation of how controlling the applications work. At least we've taken some precautions to try and stop the radio from playing in a bit more immediate way. Oh, and sneaked (just bumped the build number) a fix for another regression, all of a sudden it wasn't possible anymore to vote for favorites that you've added from [www.radio-browser.info](https://www.radio-browser.info). I'll probably notices similar regressions over the next few days, and fix em as I find em.



**3.67 (build 130):**

* Left one bug in - if you had entered an alternative source for track info, it wouldn't work any more (too eager with the copy and paste) - now resolved.



**3.66 (build 129):**

* The crashing bug when the stream got interupted has been resolved. Also some sundry bugs with new track notifications (that were actually quite old but never noticed earlier) were also resolved, thereby positively contributing to the overall stability of the app.



**3.65 (build 128):**

* Plectrum is now once again FreeStreamer-based (Nightwave Plaza, the station so good it caused a partial rollback of this piece of software). Also, Nightwave Plaza has been returned as a default preset. All other goodness that has been added between Triggerfinger and Plectrum has been preserved.



**3.62 (build 127, current version):**

* For now, Nightwave Plaza has been removed as a default preset until the situation stabilizes (connecting is hit-and-miss, no metadata being received) - even its own web app has problems connecting to the stream - which is a bit of a shame because as far as I'm concerned Nightwave Plaza was the raison de être for this app. I also cannot vouch for the reliability of recording the stream. This might cause a partial rewrite of this app (or, if you will, a new 'ffmpeg' flavor of it). If you want to stably connect to Nightwave Plaza, and see the regular metadata (not cover art, but I'm sure there's a trick for that also, but you'll have to figure that one out yourself), use VLC in the meantime (and point it to http://radio.plaza.one/mp3).

    This is the last version to be based on AVPlayer and is still available from the downloads section, along with the latest version (see above) which is based on FreeStreamer all over again. :) The two versions are usable interchangeably (there are no incompatibilies between their file formats). Just make sure if you are using some app cleaning tool that you don't dispose of the contents of the Library folder



**3.61 (build 126):**

Some small bug fixes:

* When playing from an URL, it should now be possible to add a favorite from the currently playing URL (menu option was always disabled - should now be fixed).
* When playing from a file from any source but Plectrum Radio, looking for track info on the web or Bandcamp was disabled. I figured, why should it be? You might have found some obscure track as an mp3, and would like to know more about it.



**3.60 (build 125):**

Mainly maintenance stuff:

* A bug made it possible to save info for the same track multiple times, if at least info for another track had been saved in between; this should be fixed now.
* The track info field in the menu would not expand beyond two lines, this has also been fixed.
* The hot key engine has been updated to something slightly more modern and maintainable. Most of this stays under the hood, but you will encounter some difference in user experience:
* You will notice some changes in the hot key registry thingamajigs, notably the cancel button only appearing when appropriate.
* It is no longer possible to 'undo' newly set hot key combinations by pressing the `Cancel` button at the bottom of the hot key registry sheet. That button is no longer there (only the `Done` button to dismiss the sheet), and newly registered hot key combinations take immediate effect.
* Hot keys are no longer stored in the preference file, but in its own separate file in the `~/Application Support/Plectrum` folder. Your old hot keys are transparently converted to the new scheme.



**3.50 (build 120):**

A bigger update than it appears to be. Please make a backup of your radio favorites before upgrading, just to be sure!

* Added a new way to fetch album art by transforming the track name in the stream-supplied metadata. Currently this is only supported for Jazz de Ville, but more can/will be added when new cases are found.
* You can now also throw away recordings by pressing `backspace` in the recording bin when recordings are currently selected.



**3.44 (build 119):**

Another one of those maintenance updates, mainly focused on giving a consistent user experience: 

* If a recording preview is playing, the recording preview slider now updates correctly while a contextual menu is open on the recording bin.
* Double-clicking on a recording in the recording bin will now preview it (if it is not the program that is currently recording or a missing file).



**3.43 (build 118):**

Maintenance update: 

* Mucked around with autolayout a bit more, and now've gotten the status menu track display to update lots more smoothly (and really automatically, i.e. getting autolayout to really autolayout everything instead).



**3.42 (build 117):**

Maintenance update: 

* Documentation bug: forgot to mention in the help text that you have to hold option down to drag an image from the status menu to the Finder.



**3.41 (build 116):**

Maintenance update: 

* Fixed a bug where `Save URL as Favorite` (new name for menu option by the way) would be accessible while already editing a favorite.
* Fixed wording of some status menu options (see above bullet point).



**3.40 (build 115):**

Again, a small version bump that doesn't do justice to some big bug fixes and feature additions.

* Fixed a bug where radio track updates were blocked by the Plectrum status menu being open.
* Speaking of the status menu, track titles that are too wide for the status menu no longer are truncated but now wrap over maximally 3 lines; same goes for artist names and album names (2 lines each). The track display menu item will grow larger as needed together with the info to be displayed.
* You can now drag images out of most image wells (not only the Plectrum radio image well). A somewhat obscure feature...
* Speaking of obscure features, there is now a `Plectrum Help` menu option that will explain these obscure features. The help feature is now also unified; the help for fetching radio art can also be found here. It is also very dark mode aware (try switching modes while the window is open).
* If you've started playing the radio from an URL (instead of a favorite), you will now find a menu option to add that URL as a favorite (when playing that URL is succesful).
* When playing previews of recordings in the recording bin, you will find that the scrubbing feature feels more 'live'.
* If you are unfortunate enough to encounter an error on loading favorites or of the contents of the recording bin, you will now be offered to either retry loading, restart Plectrum or reset the attendant file to zero as a last resort if the problems persist - a solution that's a little more subtle than the flying mallet approach of earlier versions. Since errors of this kind occurring is (luckily) very rare, this feature is still very much untested.



**3.39 (build 114):**

* The bug where the Plectrum status menu was immediately closing after being opened was back with a vengeange, but I believe it is now so flattened it will not rear its ugly head again.
* Made a few clarifications to the help file for artwork fetching.



**3.38 (build 113):**

* Fixed a bug where configuring the art panel when listening to a stream that provides artwork could result in the generic artwork for that channel to be displayed until the end of the current song.
* All preferences, track info lists, favorites etc. were saved whenever they are updated AND at application shutdown. The save on shutdown may cause corruption of preference files if a shutdown for some reason does not allow this save operation to properly complete, beside from it being redundant, so that operation was removed.
* Credit roll now properly mentions the app's purpose, its license, and where to get the source.



**3.37 (build 112):**

* Maintenance update: some cosmetic fixes. 


**3.36 (build 111):**

* Percent-encoding percent-encodes everything, except the ampersand apparently. :/ This would lead to searches for a track on the web or Bandcamp to be broken off at the '&' sign. Should be fixed now.
* Some preset favorites had died (Katsushika - most Japanese LeanStreams seem to have been blocked for listening outside of Japan, although a VPN might work), or had their URLs changed; they were adapted so you have a working base set again.
* Fixed a rather big bug: if you had edited a favorite that you got from [www.radio-browser.info](https://www.radio-browser.info) and then created a new favorite, you then weren't able to edit the URL for that favorite. This problem has now been resolved.
* Remembered to put in a disclaimer in the credits about Bandcamp (not being affiliated or endorsed, etc.)


**3.35 (build 109):**

The innocuous version number bump belies a substantial update, which includes:

* You can now search for the currently playing track on the web or on Bandcamp:
* You can select a choice of search engines for web searches, selectable from the settings window.
* These two options are available from the Plectrum status menu, but only for radio services (Plectrum's or otherwise).
* The track info window has been completely revamped:
* The track list can now be edited to some extent (i.e. you can remove tracks; use the contextual right-click menu for this option).
* You can also search for each track in the list on the web or on Bandcamp (using the contextual menu or the supplied icons).
* A couple of visual and logic bugs and annoyances have been fixed:
* The track display in the status menu is now correctly updated even if the status menu is currently open.
* Switching playback to Plectrum will no longer result in old artwork from the previous source being displayed while the stream starts up (most of the time at least ;)).
* If you had multiple applications open and then opened a stream from Plectrum, then again stopped the Plectrum stream, the Plectrum controls would then correctly identify which of the paused applications would receive control (along a priority scheme: iTunes/Music -> Spotify -> Swinsian), but would then not update the track info in the status menu correctly (it would display that of the last played application, instead of the controlled application). This should now be fixed.
* Wording of some labels and menu options changed to something somewhat more sensible.
* All windows that matter (not only the notifications) can by default join *all* desktops if you have more than one.
* If you switched between dark and light mode while recording (which nobody would probably think of trying, but anyway) your menu icon would be prone to disappearing - fixed.

I snuck build 108 past you, sorry about that. :)


**3.32 (build 107):**

* Maintenance update: BPM Inspector didn't show up in Music, now it does.


**3.31 (build 106):**

* Maintenance update: moved Plectrum notification window up to screen saver level.


**3.3 (build 105):**

* Changed the way the current track data is retrieved from Apple Music - it's a bit more sluggish now maybe, but this should fix all the mysterious (and frequent) crashes that seemed to happen only on selected albums or playlists.
* Some other small bugs fixed.


**3.26 (build 103):**

* A bug prevented fetching of artwork (and stalling of the artwork requeueing mechanism) when stopping and starting a favorite - should be fixed now.


**3.25 (build 102):**

* Added a feature that might be useful for cases where the new artwork to fetch lags behind metadata: you can now defer notifications until there's new artwork (ensuring that new song notification and accompanying artwork are always in sync). It will retry 8 times every 3 seconds (just checking if the artwork URL has changed, so trying to keep server hammering to a minimum), after which it will give up and return generic artwork.
* You can now also suppress superfluous notifications (i.e. new metadata fires, but it's the same data during the same song).


**3.21 (build 101):**

* Metadata display (if there is any to display) is now correct also for streams that are not favorites.


**3.2 (build 100):**

The fact that Swinsian's half-stars were not properly dealt with irked me, so I added support for this feature. I've also added support for half-stars in iTunes and Music, if that (hidden) feature has been enabled. For iTunes, type this in the terminal:

`defaults write com.apple.iTunes allow-half-stars -bool TRUE`

...or alternatively for Music, type this:

`defaults write com.apple.Music allow-half-stars -bool TRUE`

To turn support for half-stars off, retype and change `TRUE` to `FALSE`.

The half-stars feature (at least in iTunes) has enough quirks to see that Apple turned if off by default for a reason, though, so caveat emptor as always, and if you're not comfortable with using the terminal to change preferences, best leave this bit of iTunes/Music untouched.

* It is now also possible to ♥️ tracks in iTunes and Music. Tracks still can't be ♥️ in Spotify though - see the first item under *Known Limitations* above.
* Huge bug: never noticed that the in-menu volume slider didn't work for Swinsian... Should be fixed now!


_[earlier history truncated]_
