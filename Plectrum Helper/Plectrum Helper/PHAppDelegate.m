//
//  PHAppDelegate.m
//  Plectrum Helper
//
//  Created by Petros Loukareas on 22-04-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PHAppDelegate.h"

@interface PHAppDelegate ()

@end

@implementation PHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSString *path = [[[[[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    [[NSWorkspace sharedWorkspace] launchApplication:path];
    [NSApp terminate:nil];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {

}


@end
