//
//  main.m
//  Plectrum Helper
//
//  Created by Petros Loukareas on 22-04-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
