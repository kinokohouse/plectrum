//
//  PLNowPlaying.m
//  Plectrum
//
//  Created by Petros Loukareas on 18/02/2023.
//  Copyright © 2023 Petros Loukareas. All rights reserved.
//

#import "PLNowPlaying.h"
#import "AppDelegate.h"

@implementation PLNowPlaying : NSScriptCommand

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = [[NSApplication sharedApplication] delegate];
    NSString *infoString;
    if ([[appDelegate trackInfoSource] isEqualToString:@"iTunesRadio"] || [[appDelegate trackInfoSource] isEqualToString:@"MusicRadio"]) {
        infoString = [NSString stringWithFormat:@"%@ from internet radio", [appDelegate trackArtist]];
    } else if ([[appDelegate trackInfoSource] isEqualToString:@"Plectrum"]) {
        [appDelegate getTrackInfo];
        infoString = [NSString stringWithFormat:@"%@ from Plectrum", [appDelegate trackTitle]];
    } else {
        infoString = [NSString stringWithFormat:@"%@ - %@ from %@", [appDelegate trackArtist], [appDelegate trackTitle], [appDelegate trackInfoSource]];
    }
    return infoString;
}

@end
