//
//  PLNowPlaying.h
//  Plectrum
//
//  Created by Petros Loukareas on 18/02/2023.
//  Copyright © 2023 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLNowPlaying : NSScriptCommand

@end

NS_ASSUME_NONNULL_END
