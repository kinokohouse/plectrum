//
//  NSImage+Blur.h
//  Plectrum
//
//  Created by Petros Loukareas on 13/10/2019.
//  Based on earlier work by Nuoxici (c) 2019, MIT Licence.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSImage (Blur)

- (NSImage *)blurEffectWithRadius:(CGFloat)aRadius;

@end

NS_ASSUME_NONNULL_END
