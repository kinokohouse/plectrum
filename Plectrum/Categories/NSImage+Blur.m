//
//  NSImage+Blur.h
//  Plectrum
//
//  Created by Petros Loukareas on 13/10/2019.
//  Based on earlier work by Nuoxici (c) 2019, MIT Licence.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "NSImage+Blur.h"
#import <CoreImage/CIFilter.h>

@implementation NSImage (Blur)

- (NSImage *)blurEffectWithRadius:(CGFloat)aRadius {
    NSImage *image = self;
    [image lockFocus];
    CIImage *baseImage = [[CIImage alloc] initWithData:[image TIFFRepresentation]];
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur" keysAndValues:kCIInputImageKey, baseImage, @"inputRadius", @(aRadius), nil];
    CIImage *outImage = [filter valueForKey:@"outputImage"];
    NSRect rect = NSMakeRect(0, 0, self.size.width, self.size.height);
    NSRect sourceRect = NSMakeRect(0, 0, self.size.width, self.size.height);
    [outImage drawInRect:rect fromRect:sourceRect operation:NSCompositingOperationSourceOver fraction:10];
    [image unlockFocus];
    return image;
}

@end
