//
//  PLRadioBrowserToolbox.m
//  Plectrum
//
//  Created by Petros Loukareas on 13/07/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import "PLRadioBrowserToolbox.h"
#import "AppDelegate.h"

NSString *const radioBrowser = @"all.api.radio-browser.info";
NSString *const defaultHost = @"de1.api.radio-browser.info";


@interface PLRadioBrowserToolbox() <NSTableViewDelegate, NSTableViewDataSource, NSTextFieldDelegate>

@property (strong) NSMenu *countryMenu;
@property (assign) NSTimeInterval lastcountryPopUpButtonFetch;
@property (assign) BOOL limitResults;
@property (assign) NSInteger popUpSelection;

@end


@implementation PLRadioBrowserToolbox


#pragma mark - Awake From Nib

- (void)awakeFromNib {
    [super awakeFromNib];
    [self prepareForFirstUse];
}


#pragma mark - Window Methods

- (void)setFirstResponder {
    [_browserSheet makeFirstResponder:_searchField];
}

- (void)setOtherFirstResponder {
    [_browserSheet makeFirstResponder:_tableView];
}

- (void)showBrowserSheetOnWindow:(NSWindow *)window {
    if (_lastcountryPopUpButtonFetch == 0 || [NSDate timeIntervalSinceReferenceDate] - _lastcountryPopUpButtonFetch > 10800.0f) {
        [self getcountryPopUpButton];
        [self performSelector:@selector(continueShowingSheet:) withObject:window afterDelay:0.5f];
    } else {
        [self continueShowingSheet:window];
    }
}

- (void)continueShowingSheet:(NSWindow *)window {
    [_addButton setEnabled:NO];
    [_playButton setEnabled:NO];
    [_closeButton setEnabled:YES];
    [_browserSheet makeFirstResponder:_searchField];
    [window setTitle:@"Search for New Radio Favorites"];
    [window beginSheet:_browserSheet completionHandler:^(NSModalResponse returnCode) {
        // do nothing, everything already taken care of.
    }];
}


#pragma mark - Textfield Delegate Methods (Filter Field)

- (void)controlTextDidChange:(NSNotification *)obj {
    [self checkFilterField];
}

- (void)checkFilterField {
    if ([[_filterField stringValue] isEqualToString:@""]) {
        _filteringInProgress = NO;
        if (_tableContents) {
            if ([_tableContents count] > 0) {
                NSString *multiple = @"s";
                if ([self->_tableContents count] == 1) multiple = @"";
                [_errorField setStringValue:[NSString stringWithFormat:@"Found %lu station%@.", [_tableContents count], multiple]];
            }
        }
    } else {
        _filteringInProgress = YES;
        [self filterTableContents];
        if (_tableContents) {
            if ([_tableContents count] > 0) {
                NSString *multiple = @"s";
                if ([self->_tableContents count] == 1) multiple = @"";
                [_errorField setStringValue:[NSString stringWithFormat:@"Showing %lu of %lu station%@.", [_filteredTableContents count], [_tableContents count], multiple]];
            }
        }
    }
    [_tableView reloadData];
}


#pragma mark - Table View Data Source Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    if (_filteringInProgress) {
        return [_filteredTableContents count];
    } else {
        return [_tableContents count];
    }
}


#pragma mark - Table View Delegate Methods

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *view;
    NSDictionary *dictForLine;
    NSString *ident = [tableColumn identifier];
    if (_filteringInProgress) {
        if ([_filteredTableContents count] > row) {
            dictForLine = [_filteredTableContents objectAtIndex:row];
        } else {
            return nil;
        }
    } else {
        if ([_tableContents count] > row) {
            dictForLine = [_tableContents objectAtIndex:row];
        } else {
            return nil;
        }
    }
    if ([ident isEqualToString:@"stationname"]) {
        view = [_tableView makeViewWithIdentifier:@"radioname" owner:self];
        NSString *name = [dictForLine valueForKey:@"name"];
        NSString *url = [dictForLine valueForKey:@"url"];
        if (name == nil || [name isEqualToString:@""]) name = @"Unknown Station";
        NSString *compoString = [NSString stringWithFormat:@"%@\n%@", name, url];
        [[view textField] setStringValue:name];
        [[view textField] setToolTip:compoString];
    } else if ([ident isEqualToString:@"country"]) {
        view = [_tableView makeViewWithIdentifier:@"place" owner:self];
        NSString *country = [dictForLine valueForKey:@"country"];
        if (country == nil || [country isEqualToString:@""]) country = @"Planet Earth";
        [[view textField] setStringValue:country];
        [[view textField] setToolTip:country];
    } else if ([ident isEqualToString:@"format"]) {
        view = [_tableView makeViewWithIdentifier:@"type" owner:self];
        [[view textField] setStringValue:[dictForLine valueForKey:@"codec"]];
    } else {
        view = [_tableView makeViewWithIdentifier:@"bitrate" owner:self];
        NSInteger bitrate = [[dictForLine valueForKey:@"bitrate"] integerValue];
        NSString *bitString = [NSString stringWithFormat:@"%ld", (long)bitrate];
        if (bitrate == 0) bitString = @"n/a";
        [[view textField] setStringValue:bitString];
    }
    return view;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSUInteger sel = [[_tableView selectedRowIndexes] count];
    switch(sel) {
        case 0:
            [_playButton setEnabled:NO];
            [_addButton setEnabled:NO];
            break;
        case 1:
            [_playButton setEnabled:YES];
            [_addButton setEnabled:YES];
            break;
        default:
            [_playButton setEnabled:NO];
            [_addButton setEnabled:YES];
    }
}


#pragma mark - Class Methods

+ (NSArray <NSString *> *)IPv4AddressesForHostname:(nullable NSString *)host {
    if (host == nil || [host isEqualToString:@""]) host = radioBrowser;
    CFHostRef hostRef = CFHostCreateWithName(kCFAllocatorDefault, (__bridge CFStringRef)host);
    Boolean success = CFHostStartInfoResolution(hostRef, kCFHostAddresses, NULL);
    if (!success) {
        CFRelease(hostRef);
        return @[];
    }
    CFArrayRef addressArray = CFHostGetAddressing(hostRef, NULL);
    NSArray *addresses = [(__bridge NSArray*)addressArray copy];
    CFRelease(hostRef);
    return [PLRadioBrowserToolbox dataToIPv4Addresses:addresses];
}

+ (NSArray *)hostnamesForIPv4Address:(NSString *)address {
    struct addrinfo *result = NULL;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_NUMERICHOST;
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    int errorStatus = getaddrinfo([address cStringUsingEncoding:NSASCIIStringEncoding], NULL, &hints, &result);
    if (errorStatus != 0) return @[];
    CFDataRef addressRef = CFDataCreate(NULL, (UInt8 *)result->ai_addr, result->ai_addrlen);
    if (addressRef == nil) return @[];
    freeaddrinfo(result);
    CFHostRef hostRef = CFHostCreateWithAddress(kCFAllocatorDefault, addressRef);
    if (hostRef == nil) return nil;
    CFRelease(addressRef);
    BOOL success = CFHostStartInfoResolution(hostRef, kCFHostNames, NULL);
    if (!success) {
        CFRelease(hostRef);
        return @[];
    }
    CFArrayRef hostnamesRef = CFHostGetNames(hostRef, NULL);
    NSArray *hostnames = [(__bridge NSArray*)hostnamesRef copy];
    CFRelease(hostRef);
    return hostnames;
}

+ (NSMutableArray <NSString *> *)dataToIPv4Addresses:(NSArray *)array {
    NSMutableArray <NSString *> *newAddresses = [[NSMutableArray alloc] init];
    NSData *data;
    char buffer[4];
    NSInteger c = [array count];
    for (int i = 0; i < c; i++) {
        data = [array objectAtIndex:i];
        [data getBytes:buffer range:NSMakeRange(4, 4)];
        NSString *dataString = [NSString stringWithFormat:@"%d.%d.%d.%d", (uint8)buffer[0], (uint8)buffer[1], (uint8)buffer[2], (uint8)buffer[3]];
        [newAddresses addObject:[dataString copy]];
    }
    return newAddresses;
}

+ (NSString *)buildUserAgentString {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
    if (appName == nil) appName = @"Radio";
    NSString *appVersion = [bundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (appVersion == nil) appVersion = @"0.0";
    NSString *appBuild = [bundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    if (appBuild == nil) appBuild = @"0";
    NSString *appPlatform = [(NSArray *)[bundle objectForInfoDictionaryKey:@"CFBundleSupportedPlatforms"] objectAtIndex:0];
    if (appPlatform == nil) appPlatform = @"Mac";
    return [NSString stringWithFormat:@"%@ Reverted_to_FreeStreamer Flavor %@/%@ Build %@", appName, appPlatform, appVersion, appBuild];
}


#pragma mark - Instance Utilities

- (void)determineBaseURL {
    NSArray *array = [PLRadioBrowserToolbox IPv4AddressesForHostname:nil];
    if ([array count] == 0) {
        _baseURL = defaultHost;
        return;
    }
    int idx = arc4random_uniform((unsigned int)[array count] - 1);
    NSArray *urlArray = [PLRadioBrowserToolbox hostnamesForIPv4Address:[array objectAtIndex:idx]];
    if ([urlArray count] == 0) {
        _baseURL = defaultHost;
    } else {
        _baseURL = [urlArray objectAtIndex:0];
    }
}

- (void)prepareForFirstUse {
    [self determineBaseURL];
    _userAgentString = [PLRadioBrowserToolbox buildUserAgentString];
}

- (void)resetWindowControls {
    [self wrapUpSendingRequest];
    [_errorField setStringValue:@""];
    [_searchField setStringValue:@""];
    [_filterField setStringValue:@""];
    _filteringInProgress = NO;
    _tableContents = [@[] mutableCopy];
    _filteredTableContents = [@[] mutableCopy];
    [_tableView reloadData];
}

- (void)prepareForSendingRequest {
    [_spinningWheel startAnimation:nil];
    [_searchField setEnabled:NO];
    [_searchButton setEnabled:NO];
    [_countryPopUpButton setEnabled:NO];
    [_filterField setEnabled:NO];
    [_choicePopUpButton setEnabled:NO];
    [_tableView setEnabled:NO];
}

- (void)wrapUpSendingRequest {
    [_searchField setEnabled:YES];
    [_searchButton setEnabled:YES];
    [_countryPopUpButton setEnabled:YES];
    [_filterField setEnabled:YES];
    [_choicePopUpButton setEnabled:YES];
    [_tableView setEnabled:YES];
    [_spinningWheel stopAnimation:nil];
}

- (void)filterTableContents {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@ OR country contains[cd] %@ OR state contains[cd] %@ OR tags contains[cd] %@", [_filterField stringValue], [_filterField stringValue], [_filterField stringValue], [_filterField stringValue]];
    _filteredTableContents = [_tableContents filteredArrayUsingPredicate:predicate];
}


#pragma mark - Radio Browser Server Communication

- (void)sendRequest:(nonnull NSString *)requestParams withCallback:(SEL)selector onTarget:(nonnull id)target {
    if (_userAgentString == nil || _baseURL == nil) [self prepareForFirstUse];
    NSString *urlString = [NSString stringWithFormat:@"http:/%@/json/%@", _baseURL, requestParams];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"User-Agent":_userAgentString};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[@"hidebroken=true" dataUsingEncoding:NSUTF8StringEncoding]];
    NSDictionary __block *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        error = nil;
        if (!data) {
            self->_returnedData = nil;
            self->_dataError = [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:@{NSLocalizedDescriptionKey:@"HTTP request yielded no data."}];
            IMP imp = [target methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(target, selector);
            return;
        }
        @try {
            dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        }
        @catch (NSError *e) {
            self->_returnedData = nil;
            self->_dataError = e;
            IMP imp = [target methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(target, selector);
            return;
        }
        if (error) {
            self->_returnedData = nil;
            self->_dataError = error;
            IMP imp = [target methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(target, selector);
            return;
        }
        if (!dict || [dict count] == 0) {
            self->_returnedData = nil;
            self->_dataError = [NSError errorWithDomain:NSURLErrorDomain code:2 userInfo:@{NSLocalizedDescriptionKey:@"HTTP request yielded no or empty JSON object."}];
            IMP imp = [target methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(target, selector);
            return;
        }
        self->_returnedData = dict;
        self->_dataError = nil;
        IMP imp = [target methodForSelector:selector];
        void (*func)(id, SEL) = (void *)imp;
        func(target, selector);
        return;
    }];
    [dataTask resume];
}

- (void)clickStationWithUUID:(nonnull NSString *)stationUUID {
    if (_userAgentString == nil || _baseURL == nil) [self prepareForFirstUse];
    NSString *urlString = [NSString stringWithFormat:@"http:/%@/json/url/%@", _baseURL, stationUUID];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"User-Agent":_userAgentString};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSDictionary __block *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        error = nil;
        @try {
            dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        @catch (NSError *e) {
            self->_lastClickWasSuccessful = NO;
            return;
        }
        if (error) {
            self->_lastClickWasSuccessful = NO;
            return;
        }
        if (!dict || [dict count] == 0) {
            self->_lastClickWasSuccessful = NO;
            return;
        }
        self->_lastClickWasSuccessful = YES;
        return;
    }];
    [dataTask resume];
}

- (void)voteForStationWithUUID:(nonnull NSString *)stationUUID {
    if (_userAgentString == nil || _baseURL == nil) [self prepareForFirstUse];
    NSString *urlString = [NSString stringWithFormat:@"http:/%@/json/vote/%@", _baseURL, stationUUID];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"User-Agent":_userAgentString};
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSDictionary __block *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data == nil) {
            self->_lastVoteWasSuccessful = NO;
            return;
        }
        error = nil;
        @try {
            dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        }
        @catch (NSError *e) {
            self->_lastVoteWasSuccessful = NO;
            dispatch_async(dispatch_get_main_queue(),^(void) {
                [self votingFailed];
            });
            return;
        }
        if (error) {
            self->_lastVoteWasSuccessful = NO;
            dispatch_async(dispatch_get_main_queue(),^(void) {
                [self votingFailed];
            });
            return;
        }
        if (!dict || [dict count] == 0) {
            self->_lastVoteWasSuccessful = NO;
            dispatch_async(dispatch_get_main_queue(),^(void) {
                [self votingFailed];
            });
            return;
        }
        self->_lastVoteWasSuccessful = YES;
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self votingSucceeded];
        });
        return;
    }];
    [dataTask resume];
}

- (void)votingSucceeded {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSUserNotification *userNotification = [[NSUserNotification alloc] init];
    [userNotification setTitle:@"Thank you for voting!"];
    [userNotification setSubtitle:@"Your vote was successfully sent in for:"];
    [userNotification setInformativeText:[[appDelegate currentFav] name]];
    [userNotification setSoundName:nil];
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:userNotification];
}

- (void)votingFailed {
    NSUserNotification *userNotification = [[NSUserNotification alloc] init];
    [userNotification setTitle:@"Mmm."];
    [userNotification setSubtitle:@"Apparently your vote didn't register."];
    [userNotification setInformativeText:@"Please try later again."];
    [userNotification setSoundName:nil];
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:userNotification];
}


#pragma mark - Build/Refresh Country Menu

- (void)getcountryPopUpButton {
    [_errorField setTextColor:[NSColor labelColor]];
    [_errorField setStringValue:@"Loading country list..."];
    [self sendRequest:@"countries" withCallback:@selector(buildcountryPopUpButton) onTarget:self];
}

- (void)buildcountryPopUpButton {
    if (!_returnedData) {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_errorField setTextColor:[NSColor redColor]];
            [self->_errorField setStringValue:@"Failed to load country list."];
        });
        return;
    }
    dispatch_async(dispatch_get_main_queue(),^(void) {
        [self prepareForSendingRequest];
    });
    if ([_returnedData respondsToSelector:@selector(objectAtIndex:)]) {
        _countryMenu = [[NSMenu alloc] init];
        for (int i = 0; i < [self->_returnedData count]; i++) {
            NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:[(NSDictionary *)[self->_returnedData objectAtIndex:i] valueForKey:@"name"] action:nil keyEquivalent:@""];
            [self->_countryMenu addItem:item];
        }
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_countryPopUpButton setMenu:self->_countryMenu];
            [self->_countryPopUpButton selectItemAtIndex:0];
            self->_lastcountryPopUpButtonFetch = [NSDate timeIntervalSinceReferenceDate];
            [self->_errorField setStringValue:@""];
            [self wrapUpSendingRequest];
            [self performSelector:@selector(setFirstResponder) withObject:nil afterDelay:0.05f];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_errorField setTextColor:[NSColor redColor]];
            [self->_errorField setStringValue:@"Failed to load country list."];
            [self wrapUpSendingRequest];
            [self performSelector:@selector(setFirstResponder) withObject:nil afterDelay:0.05f];
        });
    }
}


#pragma mark - IBActions

- (IBAction)showCountryPopUpButton:(id)sender {
    [_countryPopUpButton setHidden:NO];
    [_popularityPopUpButton setHidden:YES];
    [_searchField setHidden:YES];
}

- (IBAction)showPopularityPopUpButton:(id)sender {
    [_countryPopUpButton setHidden:YES];
    [_popularityPopUpButton setHidden:NO];
    [_searchField setHidden:YES];
}

- (IBAction)hideSecondaryPopUpButtons:(id)sender {
    [_countryPopUpButton setHidden:YES];
    [_popularityPopUpButton setHidden:YES];
    [_searchField setHidden:NO];
}

- (IBAction)addToFavorites:(id)sender {
    [_closeButton setEnabled:NO];
    NSString *oldErrorString = [_errorField stringValue];
    [_errorField setTextColor:[NSColor labelColor]];
    [_errorField setStringValue:@"Adding favorite(s)..."];
    [self performSelector:@selector(continueAddingToFavorites:) withObject:oldErrorString afterDelay:0.10f];
}
    
- (void)continueAddingToFavorites:(NSString *)oldErrorString {
    NSArray *array;
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ([selection count] == 0) {
        [_closeButton setEnabled:YES];
        return;
    }
    if (!_filteringInProgress) {
        array = [_tableContents objectsAtIndexes:selection];
    } else {
        array = [_filteredTableContents objectsAtIndexes:selection];
    }
    [_favTableView addFavoritesFromRadioBrowser:array];
    [_errorField setStringValue:oldErrorString];
    [_closeButton setEnabled:YES];
}

- (IBAction)justPlay:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSDictionary *dict;
    if ([[_tableView selectedRowIndexes] count] == 0) return;
    NSInteger selection = [[_tableView selectedRowIndexes] firstIndex];
    if (_filteringInProgress) {
        dict = [_filteredTableContents objectAtIndex:selection];
    } else {
        dict = [_tableContents objectAtIndex:selection];
    }
    [[appDelegate urlTextField] setStringValue:[dict valueForKey:@"url"]];
    [self closeWindow:self];
    [appDelegate openURLOK:self];
    [self clickStationWithUUID:[dict valueForKey:@"stationuuid"]];
}

- (IBAction)closeWindow:(id)sender {
    [self resetWindowControls];
    [[_browserSheet sheetParent] setTitle:@"Edit Radio Favorites"];
    [[_browserSheet sheetParent] endSheet:_browserSheet];
}

- (IBAction)clearFilterField:(id)sender {
    [_filterField setStringValue:@""];
    [self checkFilterField];
}

- (IBAction)startSearch:(id)sender {
    _limitResults = NO;
    [_addButton setEnabled:NO];
    [_playButton setEnabled:NO];
    _popUpSelection = [_choicePopUpButton indexOfSelectedItem];
    NSString *searchTerm;
    NSString *commandString = @"";
    NSInteger popUpIndex;
    switch(_popUpSelection) {
        case 0:
            searchTerm = [_searchField stringValue];
            if ([searchTerm length] < 3) {
                [_browserSheet makeFirstResponder:_searchField];
                [self->_errorField setTextColor:[NSColor redColor]];
                [self->_errorField setStringValue:@"Enter 3 or more characters."];
                NSBeep();
                return;
            }
            searchTerm = [searchTerm stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            commandString = [NSString stringWithFormat:@"stations/byname/%@", searchTerm];
            break;
        case 1:
            searchTerm = [_searchField stringValue];
            if ([searchTerm length] < 3) {
                [_browserSheet makeFirstResponder:_searchField];
                [self->_errorField setTextColor:[NSColor redColor]];
                [self->_errorField setStringValue:@"Enter 3 or more characters."];
                NSBeep();
                return;
            }
            searchTerm = [searchTerm stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            commandString = [NSString stringWithFormat:@"stations/bytag/%@", searchTerm];
            break;
        case 2:
            searchTerm = [[_countryPopUpButton selectedItem] title];
            searchTerm = [searchTerm stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            commandString = [NSString stringWithFormat:@"stations/bycountry/%@", searchTerm];
            break;
        case 3:
            _limitResults = YES;
            popUpIndex = [_popularityPopUpButton indexOfSelectedItem];
            if (popUpIndex == 0) {
                commandString = [NSString stringWithFormat:@"stations/topclick/300"];
            } else if (popUpIndex == 2) {
                commandString = [NSString stringWithFormat:@"stations/topvote/300"];
            } else {
                commandString = [NSString stringWithFormat:@"stations/lastclick/300"];
            }
            break;
    }
    [self prepareForSendingRequest];
    [_errorField setTextColor:[NSColor labelColor]];
    [_errorField setStringValue:@"Searching..."];
    [self sendRequest:commandString withCallback:@selector(theSearchGoesOn) onTarget:self];
}

- (void)theSearchGoesOn {
    if (!_returnedData) {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_errorField setTextColor:[NSColor labelColor]];
            [self->_errorField setStringValue:@"No results for search."];
            self->_filteringInProgress = NO;
            self->_tableContents = @[];
            self->_filteredTableContents = @[];
            [self wrapUpSendingRequest];
            [self->_tableView reloadData];
        });
        return;
    }
    if (_popUpSelection == 2) {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [[self->_tableView tableColumnWithIdentifier:@"country"] setHidden:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [[self->_tableView tableColumnWithIdentifier:@"country"] setHidden:NO];
        });
    }
    if ([_returnedData respondsToSelector:@selector(getObjects:andKeys:count:)]) {
        _returnedData = @[(NSDictionary *)_returnedData];
    }
    if ([_returnedData respondsToSelector:@selector(objectAtIndex:)]) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"codec ==[cd] %@ OR codec ==[cd] %@ OR codec ==[cd] %@", @"MP3", @"AAC", @"AAC+"];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"bitrate <= 320"];
        NSCompoundPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
        NSArray *filteredData = [_returnedData filteredArrayUsingPredicate:predicate];
        if (_limitResults) {
            if ([filteredData count] > 250) {
                _tableContents = [filteredData objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 250)]];
            } else {
                _tableContents = filteredData;
            }
        } else {
            _tableContents = filteredData;
        }
        _limitResults = NO;
        dispatch_async(dispatch_get_main_queue(),^(void) {
            self->_filteringInProgress = NO;
            [self->_filterField setStringValue:@""];
            [self wrapUpSendingRequest];
            NSString *multiple = @"s";
            if ([self->_tableContents count] == 1) multiple = @"";
            [self->_errorField setStringValue:[NSString stringWithFormat:@"Found %lu station%@.", [self->_tableContents count], multiple]];
            [self->_tableView reloadData];
            [self performSelector:@selector(setOtherFirstResponder) withObject:nil afterDelay:0.05f];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_errorField setTextColor:[NSColor redColor]];
            [self->_errorField setStringValue:@"Error fetching results."];
            [self wrapUpSendingRequest];
        });
    }
}

@end
