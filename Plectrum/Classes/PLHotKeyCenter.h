//
//  PLHotKeyCenter.h
//  Plectrum
//
//  Created by Petros on 14/12/22.
//  Copyright (c) 2017-2022 Kinoko House. MIT License.
//


#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>
#import "PLHotKey.h"
#import "PLHotKeyTextView.h"


@interface PLHotKeyCenter : NSObject

@property NSMutableArray <PLHotKey *> *hotKeys;
@property NSDictionary *keyEquivDict;

@property (assign) BOOL isPaused;


- (instancetype)init;
- (void)dealloc;

- (BOOL)registerHotKey:(unsigned long)keyCode withModMask:(unsigned long)modMask andKeyString:(NSString *)keyString withKeyID:(int)keyID;
- (void)unregisterHotKey:(PLHotKey *)hotKey;

- (PLHotKey *)getHotKeyWithKeyString:(NSString *)keyString;
- (PLHotKey *)getHotKeyWithKeyID:(int)keyID;

- (void)loadHotKeys;
- (void)saveHotKeys;

- (void)unregisterHotKeys;
- (void)initHotKeys;

- (void)setHotKeyTextViews;

- (NSString *)hotKeyToMenuKeyEquivalent:(int)keycode;


@end


