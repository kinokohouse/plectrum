//
//  PLUtilities.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 08-01-19.
//  Copyright © 2019-2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


@interface PLUtilities : NSObject


#pragma mark - PLUtilities Class Methods

+ (NSString *)createUUID;
+ (void)showAlertWithMessageText:(NSString *)topText informativeText:(NSString *)mainText;
+ (NSImage *)iconWithSystemReference:(NSString *)string;
+ (NSString *)getNameForCopy:(NSString *)theName;
+ (NSString *)sortableDateString;
+ (NSImage *)iconScaledToMenuSize:(NSImage *)image;
+ (NSString *)getPathToAppSupportFolder;

@end
