//
//  PLHotKeyCenter.m
//  Plectrum
//
//  Created by Petros on 14/12/22.
//  Copyright (c) 2017-2022 Kinoko House. MIT License.
//


#import "PLHotKeyCenter.h"
#import "AppDelegate.h"


@interface PLHotKeyCenter()

@end


@implementation PLHotKeyCenter

#pragma mark - Instance Creation and Disposal

- (instancetype)init {
    if (self = [super init]) {
        _hotKeys = [[NSMutableArray alloc] init];
        EventTypeSpec eventType;
        eventType.eventClass = kEventClassKeyboard;
        eventType.eventKind = kEventHotKeyPressed;
        InstallApplicationEventHandler(&HotKeyEventHandler, 1, &eventType, NULL, NULL);
        _keyEquivDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      @0x00, @"a",
                                      @0x0B, @"b",
                                      @0x08, @"c",
                                      @0x02, @"d",
                                      @0x0E, @"e",
                                      @0x03, @"f",
                                      @0x05, @"g",
                                      @0x04, @"h",
                                      @0x22, @"i",
                                      @0x26, @"j",
                                      @0x28, @"k",
                                      @0x25, @"l",
                                      @0x2E, @"m",
                                      @0x2D, @"n",
                                      @0x1F, @"o",
                                      @0x23, @"p",
                                      @0x0C, @"q",
                                      @0x0F, @"r",
                                      @0x01, @"s",
                                      @0x11, @"t",
                                      @0x20, @"u",
                                      @0x09, @"v",
                                      @0x0D, @"w",
                                      @0x07, @"x",
                                      @0x10, @"y",
                                      @0x06, @"z",
                                      @0x1D, @"0",
                                      @0x12, @"1",
                                      @0x13, @"2",
                                      @0x14, @"3",
                                      @0x15, @"4",
                                      @0x17, @"5",
                                      @0x16, @"6",
                                      @0x1A, @"7",
                                      @0x1C, @"8",
                                      @0x19, @"9",
                                      @0x32, @"`",
                                      @0x0A, @"§",
                                      @0x2B, @",",
                                      @0x2F, @".",
                                      @0x1B, @"-",
                                      @0x18, @"=",
                                      @0x2C, @"/",
                                      @0x2A, @"\\",
                                      @0x27, @"'",
                                      @0x29, @";",
                                      @0x1E, @"]",
                                      @0x21, @"[",
                                      @0x35, @"⎋",
                                      @0x24, @"↩",
                                      @0x4C, @"⌤",
                                      @0x30, @"⇥",
                                      @0x33, @"⌫",
                                      @0x7B, @"←",
                                      @0x7C, @"→",
                                      @0x7D, @"↓",
                                      @0x7E, @"↑",
                                      nil];
    }
    return self;
}

- (void) dealloc {
    [self unregisterHotKeys];
    OSStatus RemoveEventHandler(EventHandlerRef HotKeyEventHandler);
}


#pragma mark - Hotkey Registration

- (BOOL)registerHotKey:(unsigned long)keyCode withModMask:(unsigned long)modMask andKeyString:(NSString *)keyString withKeyID:(int)keyID {
    PLHotKey *oldHotKey = [self getHotKeyWithKeyID:keyID];
    if (oldHotKey) [self unregisterHotKey:oldHotKey];
    PLHotKey *hotKey = [[PLHotKey alloc] initWithKeyCode:keyCode modMask:modMask keyString:keyString];
    [hotKey setKeyID:keyID];
    UInt32 modflags = [self modMaskToModFlags:modMask];
    EventHotKeyRef gMyHotKeyRef;
    EventHotKeyID gMyHotKeyID;
    OSType myType = NSHFSTypeCodeFromFileType(@"PiKk");
    gMyHotKeyID.signature = myType;
    gMyHotKeyID.id = (UInt32)[hotKey keyID];
    RegisterEventHotKey((UInt32)keyCode, modflags, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    NSValue *refStruct = [NSValue valueWithBytes:&gMyHotKeyRef objCType:@encode(EventHotKeyRef)];
    NSValue *htkStruct = [NSValue valueWithBytes:&gMyHotKeyRef objCType:@encode(EventHotKeyID)];
    [hotKey setRefStruct:refStruct];
    [hotKey setHtkStruct:htkStruct];
    [_hotKeys addObject:hotKey];
    [self saveHotKeys];
    return YES;
}


#pragma mark - Unregistering HotKeys

- (void)unregisterHotKey:(PLHotKey *)hotKey {
    EventHotKeyRef gMyHotKeyRef;
    NSValue *myValue = [hotKey refStruct];
    [myValue getValue:&gMyHotKeyRef];
    UnregisterEventHotKey(gMyHotKeyRef);
    [_hotKeys removeObject:hotKey];
    [self saveHotKeys];
}

- (void)unregisterHotKeys {
    for (PLHotKey *hotKey in _hotKeys) {
        [self unregisterHotKey:hotKey];
    }
}


#pragma mark - Finding HotKeys by Property

- (PLHotKey *)getHotKeyWithKeyString:(NSString *)keyString {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyString == %@", keyString];
    NSArray <PLHotKey *> *array = [_hotKeys filteredArrayUsingPredicate:predicate];
    if ([array count] > 0) {
        return [array firstObject];
    }
    return nil;
}

- (PLHotKey *)getHotKeyWithKeyID:(int)keyID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"keyID == %d", keyID];
    NSArray <PLHotKey *> *array = [_hotKeys filteredArrayUsingPredicate:predicate];
    if ([array count] > 0) {
        return [array firstObject];
    }
    return nil;
}


#pragma mark - Loading, Saving and Initializing

- (void)loadHotKeys {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *hotKeyPath = [[PLUtilities getPathToAppSupportFolder] stringByAppendingPathComponent:@"Plectrum.hotkeys"];
    if (![fm fileExistsAtPath:hotKeyPath]) {
        [self initHotKeys];
        [self saveHotKeys];
    } else {
        @try {
            _hotKeys = [[NSMutableArray alloc] init];
            _hotKeys = [NSKeyedUnarchiver unarchiveObjectWithFile:hotKeyPath];
            if (!_hotKeys) {
                NSLog(@"STUFFBOX: HotKeys do exist but could not be loaded");
                NSLog(@"STUFFBOX: Creating standard hotkeys...");
                [self initHotKeys];
                return;
            }
            [self setHotKeyTextViews];
            [self activateLoadedHotKeys];
        }
        @catch(NSException *e) {
            NSLog(@"STUFFBOX: Exception when loading HotKeys");
            NSLog(@"STUFFBOX: Creating standard hotkeys...");
            [self initHotKeys];
        }
    }
}

- (void)saveHotKeys {
    NSString *hotKeyPath = [[PLUtilities getPathToAppSupportFolder] stringByAppendingPathComponent:@"Plectrum.hotkeys"];
    BOOL success = [NSKeyedArchiver archiveRootObject:_hotKeys toFile:hotKeyPath];
    if (!success) {
        NSLog(@"STUFFBOX: Didn't succeed in saving HotKeys");
        NSLog(@"STUFFBOX: Something more informative and remedial should go here");
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"hotkeys"] forFile:hotKeyPath options:0];
    }
}

- (void)initHotKeys {
    if (!_hotKeys) _hotKeys = [[NSMutableArray alloc] init];
    [self registerHotKey:0x18 withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃=" withKeyID:0];
    [self registerHotKey:0x1B withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃-" withKeyID:1];
    [self registerHotKey:0x22 withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃I" withKeyID:2];
    [self registerHotKey:0x31 withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃Space" withKeyID:3];
    [self registerHotKey:0x2F withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃." withKeyID:4];
    [self registerHotKey:0x2B withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃," withKeyID:5];
    [self registerHotKey:0x24 withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃↩" withKeyID:6];
    [self registerHotKey:0x2C withModMask:(NSEventModifierFlagControl) andKeyString:@"⌃/" withKeyID:7];
    [self setHotKeyTextViews];
    [self saveHotKeys];
}

- (void)activateLoadedHotKeys {
    NSArray *array = [_hotKeys copy];
    _hotKeys = [[NSMutableArray alloc] init];
    for (PLHotKey *hotKey in array) {
        int keyID = [hotKey keyID];
        [self registerHotKey:[hotKey keyCode] withModMask:[hotKey modMask] andKeyString:[hotKey keyString] withKeyID:keyID];
    }
}

- (void)setHotKeyTextViews {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    PLHotKey *hotKey;
    hotKey = [self getHotKeyWithKeyID:0];
    if (hotKey) {
        [[appDelegate hkVolumeUp] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:1];
    if (hotKey) {
        [[appDelegate hkVolumeDown] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:2];
    if (hotKey) {
        [[appDelegate hkNowPlaying] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:3];
    if (hotKey) {
        [[appDelegate hkPlayPause] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:4];
    if (hotKey) {
        [[appDelegate hkNextTrack] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:5];
    if (hotKey) {
        [[appDelegate hkPreviousTrack] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:6];
    if (hotKey) {
        [[appDelegate hkActivate] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
    hotKey = [self getHotKeyWithKeyID:7];
    if (hotKey) {
        [[appDelegate hkCopy] setHotKeyTextViewWithKeyCode:[hotKey keyCode] modMask:[hotKey modMask] keyString:[hotKey keyString]];
    }
}


#pragma mark - Utilities

- (NSString *)hotKeyToMenuKeyEquivalent:(int)keycode {
    NSString *keyEquiv;
    unichar c;
    NSArray *keyArray;
    switch (keycode) {
        case 0x7A:
            c = NSF1FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x78:
            c = NSF2FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x63:
            c = NSF3FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x76:
            c = NSF4FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x60:
            c = NSF5FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x61:
            c = NSF6FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x62:
            c = NSF7FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x64:
            c = NSF8FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x65:
            c = NSF9FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6D:
            c = NSF10FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x67:
            c = NSF11FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6F:
            c = NSF12FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x69:
            c = NSF13FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6B:
            c = NSF14FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x71:
            c = NSF15FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x31:
            keyEquiv = @" ";
            break;
        default:
            keyArray = [_keyEquivDict allKeysForObject:[NSNumber numberWithInt:keycode]];
            if (keyArray == nil) {
                NSLog(@"Error converting hotkey to menu key equivalent");
                keyEquiv = @"§";
                break;
            }
            keyEquiv = [keyArray objectAtIndex:0];
        }
    return keyEquiv;
}

- (UInt32)modMaskToModFlags:(unsigned long)modMask {
    UInt32 modflags = 0;
    if ((modMask & NSEventModifierFlagShift) > 0) {
        modflags += shiftKey;
    }
    if ((modMask & NSEventModifierFlagControl) > 0) {
        modflags += controlKey;
    }
    if ((modMask & NSEventModifierFlagOption) > 0) {
        modflags += optionKey;
    }
    if ((modMask & NSEventModifierFlagCommand) > 0) {
        modflags += cmdKey;
    }
    return modflags;
}


#pragma mark - Keyboard Event Handler

OSStatus HotKeyEventHandler(EventHandlerCallRef nextHandler,EventRef theEvent, void *userData) {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    PLHotKeyCenter *hotKeyCenter = [appDelegate hotKeyCenter];
    if ([hotKeyCenter isPaused]) {
        NSBeep();
        return noErr;
    }
    EventHotKeyID hkRef;
    GetEventParameter(theEvent,kEventParamDirectObject,typeEventHotKeyID,NULL,sizeof(hkRef),NULL,&hkRef);
    NSNotification *notification = [[NSNotification alloc] initWithName:@"PLHotKeyPressed" object:hotKeyCenter userInfo:@{@"keyID":[NSNumber numberWithInt:(int)hkRef.id]}];
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    return noErr;
}


@end
