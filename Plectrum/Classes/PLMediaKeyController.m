//
//  PLMediaKeyController.m
//  Plectrum
//
//  Created by Petros Loukareas on 22/07/2021.
//  Based on Gaurav Khanna's example at https://gist.github.com/gvkhna
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import "PLMediaKeyController.h"
#import "AppDelegate.h"

NSString * const MediaKeyPlayPauseNotification = @"MediaKeyPlayPauseNotification";
NSString * const MediaKeyNextNotification = @"MediaKeyNextNotification";
NSString * const MediaKeyPreviousNotification = @"MediaKeyPreviousNotification";
//NSString * const MediaKeyMuteNotification = @"MediaKeyMuteNotification";
NSString * const MediaKeyVolumeDownNotification = @"MediaKeyVolumeDownNotification";
NSString * const MediaKeyVolumeUpNotification = @"MediaKeyVolumeUpNotification";

#define NX_KEYSTATE_DOWN    0x0A
#define NX_KEYSTATE_UP      0x0B

@implementation PLMediaKeyController

MAKE_SINGLETON(PLMediaKeyController, sharedController)

CGEventRef tapEventCallback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void *refcon) {

    if (type == kCGEventTapDisabledByTimeout) {
        CGEventTapEnable([[PLMediaKeyController sharedController] eventPort], TRUE);
    }
    
    if (type != NX_SYSDEFINED) {
        return event;
    }
    
    NSEvent *keyEvent = [NSEvent eventWithCGEvent:event];
    
    if ([keyEvent subtype] != 8) {
        return event;
    }
    
    int data = (int)[keyEvent data1];
    int keyCode = (data & 0xFFFF0000) >> 16;
    int keyFlags = (data & 0xFFFF);
    int keyState = (keyFlags & 0xFF00) >> 8;
    BOOL keyIsRepeat = (keyFlags & 0x1) > 0;
    long modFlags = ([keyEvent modifierFlags] & 0xFFFF0000) >> 16;
    
    if (keyIsRepeat) return event;
    
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (![appDelegate useMediaKeys]) return event;

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    switch (keyCode) {
        case NX_KEYTYPE_PLAY:
            if (keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyPlayPauseNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if(keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;
        case NX_KEYTYPE_FAST:
            if (keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyNextNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if (keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;
        case NX_KEYTYPE_REWIND:
            if(keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyPreviousNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if (keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;
        /*case NX_KEYTYPE_MUTE:
            if (modFlags != 4) return event;
            if (keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyMuteNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if (keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;*/
        case NX_KEYTYPE_SOUND_DOWN:
            if (modFlags != 4) return event;
            if (keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyVolumeDownNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if (keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;
        case NX_KEYTYPE_SOUND_UP:
            if (modFlags != 4) return event;
            if (keyState == NX_KEYSTATE_DOWN) {
                [center postNotificationName:MediaKeyVolumeUpNotification object:(__bridge PLMediaKeyController *)refcon];
            }
            if (keyState == NX_KEYSTATE_UP || keyState == NX_KEYSTATE_DOWN) return NULL;
        break;
    }
    return event;
}

- (id)init {
    if (self = [super init]) {
        CFRunLoopRef runLoop;
        _eventPort = CGEventTapCreate(kCGSessionEventTap,
                                      kCGHeadInsertEventTap,
                                      kCGEventTapOptionDefault,
                                      CGEventMaskBit(NX_SYSDEFINED),
                                      tapEventCallback,
                                      (__bridge void * _Nullable)(self));

        NSLog(@"_eventPort: %@", _eventPort);
        if (_eventPort == NULL) {
            NSLog(@"PLECTRUM: Event Tap for Media Keys could not be created");
            return self;
        }

        _runLoopSource = CFMachPortCreateRunLoopSource(kCFAllocatorSystemDefault, _eventPort, 0);

        if (_runLoopSource == NULL) {
            NSLog(@"PLECTRUM: Run Loop Source could not be created");
            return self;
        }

        runLoop = CFRunLoopGetCurrent();

        if (runLoop == NULL) {
            NSLog(@"PLECTRUM: Couldn't get current thread Run Loop");
            return self;
        }

        CFRunLoopAddSource(runLoop, _runLoopSource, kCFRunLoopCommonModes);
    }
    return self;
}

- (void)releaseTap {
    if (_eventPort) CFRelease(_eventPort);
    if (_runLoopSource) CFRelease(_runLoopSource);
}

@end
