//
//  PLNotificationCenter.m
//  Plectrum
//
//  Created by Petros Loukareas on 19/09/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLNotificationCenter.h"
#import "AppDelegate.h"

static const CGFloat timer = 5.0f;

@interface PLNotificationCenter()

@property (weak) IBOutlet NSImageView *cover;
@property (weak) IBOutlet NSTextField *title;
@property (weak) IBOutlet NSTextField *artist;
@property (weak) IBOutlet NSTextField *album;
@property (weak) IBOutlet NSTextField *time;
@property (weak) IBOutlet NSTextField *rating;
@property (weak) IBOutlet NSButton *close;

@property (weak) IBOutlet NSImageView *stationArt;
@property (weak) IBOutlet NSTextField *stationName;
@property (weak) IBOutlet NSTextField *nowPlaying;
@property (weak) IBOutlet NSTextField *stationURL;
@property (weak) IBOutlet NSButton *radioClose;

@property (strong) NSViewAnimation *viewAnimation;
@property (strong) NSTrackingArea *trackingArea;
@property (strong) NSTimer *timer;
@property (assign) BOOL radioNotificationIsVisible;
@property (assign) BOOL sourceIsRadio;
@property (assign) BOOL demoMode;

@end

@implementation PLNotificationCenter

- (void)fadeIn {
    NSPanel *notification;
    if (_sourceIsRadio) {
        notification = _radioNotification;
    } else {
        notification = _notification;
    }
    NSDictionary *animDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              notification, NSViewAnimationTargetKey,
                              NSViewAnimationFadeInEffect, NSViewAnimationEffectKey, nil];
    NSArray *animArray = [NSArray arrayWithObjects:animDict, nil];
    [notification setAlphaValue:0.0f];
    [notification makeKeyAndOrderFront:nil];
    _viewAnimation = [[NSViewAnimation alloc] initWithViewAnimations:animArray];
    [_viewAnimation startAnimation];
    _notificationIsVisible = YES;
    [self invalidateTimer];
    _timer = [NSTimer timerWithTimeInterval:0.1f target:self selector:@selector(becameVisible) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)fadeOut {
    NSPanel *notification;
    if (_sourceIsRadio) {
        notification = _radioNotification;
    } else {
        notification = _notification;
    }
    NSDictionary *animDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              notification, NSViewAnimationTargetKey,
                              NSViewAnimationFadeOutEffect, NSViewAnimationEffectKey, nil];
    NSArray *animArray = [NSArray arrayWithObjects:animDict, nil];
    _viewAnimation = [[NSViewAnimation alloc] initWithViewAnimations:animArray];
    [_viewAnimation startAnimation];
    [self invalidateTimer];
    _timer = [NSTimer timerWithTimeInterval:0.5f target:self selector:@selector(becameInvisible) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)becameVisible {
    _notificationIsVisible = YES;
    [self deleteNotification];
}

- (void)becameInvisible {
    _notificationIsVisible = NO;
    if (_sourceIsRadio) {
        [_radioNotification orderOut:nil];
    } else {
        [_notification orderOut:nil];
    }
}

- (void)showNotificationWithTitle:(NSString *)trackTitle artwork:(NSImage *)artwork artist:(NSString *)artistName album:(NSString *)albumName time:(NSString *)timeStamp rating:(NSString *)ratingString {
    AppDelegate *appDelegate = [NSApp delegate];
    if (_demoMode) return;
    if (timeStamp == nil) {
        _sourceIsRadio = NO;
        [self showRadioNotificationWithStation:trackTitle nowPlaying:artistName stationURL:albumName stationArt:[NSImage imageNamed:@"stream"]];
        return;
    }
    if (_sourceIsRadio) {
        if (_notificationIsVisible) {
            [self closeNotification:nil];
        }
    }
    [self invalidateTimer];
    _sourceIsRadio = NO;
    [_box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_title setStringValue:trackTitle];
    [_cover setImage:artwork];
    [[appDelegate sivArt] setImage:artwork];
    [_artist setStringValue:artistName];
    [_album setStringValue:albumName];
    if (timeStamp) {
        [_time setStringValue:timeStamp];
    } else {
        [_time setStringValue:@""];
    }
    [_rating setStringValue:ratingString];
    _timer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueBuildingNotification) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)showRadioNotificationWithStation:(NSString *)stationName nowPlaying:(NSString *)nowPlaying stationURL:(NSString *)stationURL stationArt:(NSImage *)stationArt {
    if (_demoMode) return;
    if (!_sourceIsRadio) {
        if (_notificationIsVisible) {
            [self closeNotification:nil];
        }
    }
    [self invalidateTimer];
    _sourceIsRadio = YES;
    [_radioBox setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_stationName setStringValue:stationName];
    [_nowPlaying setStringValue:nowPlaying];
    [_stationURL setStringValue:stationURL];
    if (!stationArt) stationArt = [NSImage imageNamed:@"radio"];
    [_stationArt setImage:stationArt];
    _timer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueBuildingRadioNotification) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)continueBuildingNotification {
    NSPoint newOrigin = [self getOrigin:_corner];
    NSRect trackingArea = NSMakeRect(0, 0, [_notification contentView].frame.size.width, [_notification contentView].frame.size.height);
    [_box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_notification setLevel:NSScreenSaverWindowLevel];
    [_notification setFrameOrigin:newOrigin];
    if (_trackingArea) {
        [[_notification contentView] removeTrackingArea:_trackingArea];
    }
    _trackingArea = [[NSTrackingArea alloc] initWithRect:trackingArea options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[_notification contentView] addTrackingArea:_trackingArea];
    if (![_viewAnimation isAnimating]) {
        if (!_notificationIsVisible) {
            [self fadeIn];
        } else {
            [_viewAnimation stopAnimation];
            [_notification setAlphaValue:1.0f];
            [_notification makeKeyAndOrderFront:nil];
            [self becameVisible];
        }
    } else {
        [_viewAnimation stopAnimation];
        [_notification setAlphaValue:1.0f];
        [_notification makeKeyAndOrderFront:nil];
        [self becameVisible];
    }
}

- (void)continueBuildingRadioNotification {
    NSPoint newOrigin = [self getOrigin:_corner];
    NSRect trackingArea = NSMakeRect(0, 0, [_radioNotification contentView].frame.size.width, [_radioNotification contentView].frame.size.height);
    [_radioBox setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_radioNotification setLevel:NSScreenSaverWindowLevel];
    [_radioNotification setFrameOrigin:newOrigin];
    if (_trackingArea) {
        [[_radioNotification contentView] removeTrackingArea:_trackingArea];
    }
    _trackingArea = [[NSTrackingArea alloc] initWithRect:trackingArea options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[_radioNotification contentView] addTrackingArea:_trackingArea];
    if (![_viewAnimation isAnimating]) {
        if (!_notificationIsVisible) {
            [self fadeIn];
        } else {
            [_viewAnimation stopAnimation];
            [_radioNotification setAlphaValue:1.0f];
            [_radioNotification makeKeyAndOrderFront:nil];
            [self becameVisible];
        }
    } else {
        [_viewAnimation stopAnimation];
        [_radioNotification setAlphaValue:1.0f];
        [_radioNotification makeKeyAndOrderFront:nil];
        [self becameVisible];
    }
}

- (void)deleteNotification {
    [self invalidateTimer];
    _timer = [NSTimer timerWithTimeInterval:(timer + 0.4f) target:self selector:@selector(fadeOut) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)initiatePreview {
    if ([_viewAnimation isAnimating]) {
        [_viewAnimation stopAnimation];
    }
    [self invalidateTimer];
    _demoMode = YES;
    if (_sourceIsRadio) {
        [_radioNotification orderOut:nil];
    }
    [_notification setAlphaValue:0.0f];
    [_notification orderOut:nil];
    [_box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:1.0f]];
    [_title setStringValue:@"Notification Preview"];
    [_cover setImage:[NSImage imageNamed:@"kitteh"]];
    [_artist setStringValue:@"This is how it would look"];
    [_album setStringValue:@"along with its position on the screen"];
    [_time setStringValue:@"3:34 (currently at 1:14)"];
    [_rating setStringValue:@"❤︎❤︎❤︎❤︎❤︎"];
    _timer = [NSTimer timerWithTimeInterval:0.05f target:self selector:@selector(continueInitiatingPreview) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)continueInitiatingPreview {
    NSPoint newOrigin = [self getOrigin:_corner];
    [_box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_notification setAlphaValue:1.0f];
    [_notification setLevel:NSScreenSaverWindowLevel];
    [_notification setFrameOrigin:newOrigin];
    [_notification makeKeyAndOrderFront:nil];
    if (_trackingArea) [[_notification contentView] removeTrackingArea:_trackingArea];
    NSSize trackingSize = [_notification contentView].frame.size;
    NSRect newTrackingArea = NSMakeRect(0, 0, trackingSize.width, trackingSize.height);
    _trackingArea = [[NSTrackingArea alloc] initWithRect:newTrackingArea options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[_notification contentView] addTrackingArea:_trackingArea];
}

- (void)updatePreviewLocation {
    NSPoint newOrigin = [self getOrigin:_corner];
    [_notification setFrameOrigin:newOrigin];
}

- (void)updatePreviewOpacity {
    [_box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    [_box setNeedsDisplay:YES];
}

- (void)endPreview {
    [_notification orderOut:nil];
    _demoMode = NO;
    if (_trackingArea) {
        [[_notification contentView] removeTrackingArea:_trackingArea];
    }
}

- (void)invalidateTimer {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (NSPoint)getOrigin:(NSInteger)corner {
    NSPanel *notification;
    NSBox *box;
    if (!_sourceIsRadio || _demoMode) {
        notification = _notification;
        box = _box;
    } else {
        notification = _radioNotification;
        box = _radioBox;
    }
    NSPoint newPoint = NSMakePoint(0.0f, 0.0f);
    NSRect screenSize = [NSScreen mainScreen].visibleFrame;
    CGFloat height = box.frame.size.height + 20;
    CGFloat width = notification.frame.size.width;
    switch (corner) {
        case 0:
            newPoint.x = (screenSize.origin.x + screenSize.size.width) - width;
            newPoint.y = screenSize.origin.y;
            break;
            
        case 1:
            newPoint.x = (screenSize.origin.x + screenSize.size.width) - width;
            newPoint.y = (screenSize.origin.y + screenSize.size.height) - height;
            break;
            
        case 2:
            newPoint.x = screenSize.origin.x;
            newPoint.y = (screenSize.origin.y + screenSize.size.height) - height;
            break;
            
        case 3:
            newPoint.x = screenSize.origin.x;
            newPoint.y = screenSize.origin.y;
            break;
            
        default:
            newPoint.x = (screenSize.origin.x + screenSize.size.width) - width;
            newPoint.y = screenSize.origin.y;
    }
    return newPoint;
}

- (IBAction)closeNotification:(id)sender {
    [self invalidateTimer];
    if (_sourceIsRadio) {
        [_radioNotification orderOut:nil];
    } else {
        [_notification orderOut:nil];
    }
    _notificationIsVisible = NO;
}

- (void)mouseEntered:(NSEvent *)event {
    NSPanel *notification;
    NSBox *box;
    NSButton *close;
    if (!_sourceIsRadio || _demoMode) {
        notification = _notification;
        box = _box;
        close = _close;
    } else {
        notification = _radioNotification;
        box = _radioBox;
        close = _radioClose;
    }
    [box setBorderColor:[NSColor whiteColor]];
    [box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:1.0f]];
    if (!_demoMode) {
        if (_viewAnimation) {
            if ([_viewAnimation isAnimating]) {
                [_viewAnimation stopAnimation];
                [notification setAlphaValue:1.0f];
            }
        }
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        [close setHidden:NO];
    }
}

- (void)mouseExited:(NSEvent *)event {
    NSBox *box;
    NSButton *close;
    if (!_sourceIsRadio || _demoMode) {
        box = _box;
        close = _close;
    } else {
        box = _radioBox;
        close = _radioClose;
    }
    [box setBorderColor:[NSColor clearColor]];
    [box setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:_opacity]];
    if (!_demoMode) {
        [close setHidden:YES];
        [self deleteNotification];
    }
}

@end
