//
//  AppDelegate.m
//  Plectrum
//
//  Created by Petros Loukareas on 07-04-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//


#import "AppDelegate.h"

#import "iTunes.h"
#import "Music.h"
#import "Swinsian.h"
#import "Spotify.h"

#import "PLImageView.h"
#import "PLUtilities.h"
#import "PLLeftTableViewController.h"
#import "PLRightTableViewController.h"
#import "PLRecordingTableViewController.h"
#import "PLAudioPreviewBox.h"
#import "PLRadioBrowserToolbox.h"
#import "PLMediaKeyController.h"


const float TIMEOUT_RANGE = 20.0f;
const float MAX_ARTWORK_DEFERMENT_RETRY_COUNT = 8;

@interface AppDelegate () <NSApplicationDelegate, NSUserNotificationCenterDelegate, NSMenuDelegate, NSAlertDelegate, NSWindowDelegate, NSOpenSavePanelDelegate, NSTableViewDelegate, NSTableViewDataSource, GrowlApplicationBridgeDelegate>

@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet PLCoverArtWindow *coverArtWindow;
@property (weak) IBOutlet NSTextView *helpTextView;
@property (weak) IBOutlet NSScrollView *helpScrollView;
@property (weak) IBOutlet NSPopUpButton *helpPopUpButton;
@property (weak) IBOutlet NSButton *radioButtonNotificationCenter;
@property (weak) IBOutlet NSButton *radioButtonGrowl;
@property (weak) IBOutlet NSButton *radioButtonInternalSolution;
@property (weak) IBOutlet NSButton *configNotificationButton;
@property (weak) IBOutlet NSButton *radioButtonNothing;
@property (weak) IBOutlet NSButton *startOnLoginCheckBox;
@property (weak) IBOutlet NSButton *artWindowEnabledCheckBox;
@property (weak) IBOutlet NSButton *artWindowConfigWindowButton;
@property (weak) IBOutlet NSButton *triggerOnStartCheckBox;
@property (weak) IBOutlet NSButton *showAlsoIfFrontmostCheckBox;
@property (weak) IBOutlet NSButton *askForConfirmationCheckBox;
@property (weak) IBOutlet NSButton *showMenuCheckBox;
@property (weak) IBOutlet NSButton *muteInsteadOfPauseCheckBox;
@property (weak) IBOutlet NSButton *blockNextAndPreviousForRadioCheckBox;
@property (weak) IBOutlet NSButton *useMediaKeysCheckBox;
@property (weak) IBOutlet NSPopUpButton *searchEnginePopUpButton;
@property (weak) IBOutlet NSMenu *statusMenu;
@property (weak) IBOutlet NSMenuItem *miRadioFavorites;
@property (weak) IBOutlet NSMenuItem *miNowPlaying;
@property (weak) IBOutlet NSMenuItem *miVoteForFav;
@property (weak) IBOutlet NSMenuItem *miMakeFav;
@property (weak) IBOutlet NSMenuItem *miPlayPause;
@property (weak) IBOutlet NSMenuItem *miNextTrack;
@property (weak) IBOutlet NSMenuItem *miPreviousTrack;
@property (weak) IBOutlet NSMenuItem *miActivate;
@property (weak) IBOutlet NSMenuItem *miCopy;
@property (weak) IBOutlet NSMenuItem *miSearchWeb;
@property (weak) IBOutlet NSMenuItem *miSearchBC;
@property (weak) IBOutlet NSMenuItem *miUpperSeparator;
@property (weak) IBOutlet NSMenuItem *miSlider;
@property (weak) IBOutlet NSMenuItem *miStars;
@property (weak) IBOutlet NSMenuItem *miHeart;
@property (weak) IBOutlet NSMenuItem *miSeparator;
@property (weak) IBOutlet NSMenuItem *miBPMInspector;
@property (weak) IBOutlet NSMenuItem *miBPMSeparator;
@property (weak) IBOutlet NSView *sliderView;
@property (weak) IBOutlet NSSlider *slider;
@property (weak) IBOutlet NSTextField *sliderLabel;
@property (weak) IBOutlet NSView *starView;
@property (weak) IBOutlet NSView *heartView;
@property (weak) IBOutlet NSButton *preStar;
@property (weak) IBOutlet NSButton *postStar;
@property (weak) IBOutlet NSImageView *star1;
@property (weak) IBOutlet NSImageView *star2;
@property (weak) IBOutlet NSImageView *star3;
@property (weak) IBOutlet NSImageView *star4;
@property (weak) IBOutlet NSImageView *star5;
@property (weak) IBOutlet NSButton *heart;

@property (weak) IBOutlet NSMenuItem *miPlaying;
@property (weak) IBOutlet NSMenuItem *miTrackView;

@property (weak) IBOutlet NSView *tiv;
@property (weak) IBOutlet NSImageView *tivArt;
@property (weak) IBOutlet NSTextField *tivTitle;
@property (weak) IBOutlet NSTextField *tivArtist;
@property (weak) IBOutlet NSTextField *tivAlbum;
@property (weak) IBOutlet NSTextField *tivTimeRating;

@property (weak) IBOutlet NSView *riv;
@property (weak) IBOutlet NSTextField *rivStation;
@property (weak) IBOutlet NSTextField *rivSong;
@property (weak) IBOutlet NSTextField *rivURL;
@property (weak) IBOutlet NSImageView *rivArt;

@property (weak) IBOutlet NSView *siv;
@property (weak) IBOutlet NSTextField *sivStation;
@property (weak) IBOutlet NSTextField *sivSong;
@property (weak) IBOutlet NSTextField *sivURL;
@property (weak) IBOutlet NSButton *playPause;
@property (weak) IBOutlet NSButton *stop;
@property (weak) IBOutlet NSButton *record;

@property (weak) IBOutlet NSWindow *trackInfoWindow;
@property (weak) IBOutlet NSTableView *trackInfoTableView;
@property (weak) IBOutlet NSButton *trackInfoClearButton;
@property (weak) IBOutlet NSButton *trackInfoExportButton;
@property (strong) NSMutableString *trackInfoContents;
@property (assign) BOOL trackInfoWindowIsAlreadyOpen;

@property (weak) IBOutlet PLNotificationCenter *PLNotificationCenter;
@property (weak) IBOutlet NSWindow *notificationConfigSheet;
@property (weak) IBOutlet NSBox *notificationCornerBox;
@property (weak) IBOutlet NSImageView *bgImage;
@property (weak) IBOutlet NSButton *bottomRight;
@property (weak) IBOutlet NSButton *topRight;
@property (weak) IBOutlet NSButton *topLeft;
@property (weak) IBOutlet NSButton *bottomLeft;
@property (weak) IBOutlet NSSlider *opacitySlider;

@property (weak) IBOutlet NSWindow *artWindowConfigSheet;
@property (weak) IBOutlet NSPopUpButton *showSongDetailsPopUpButton;
@property (weak) IBOutlet NSMenuItem *showSongDetailsOnMouseOverMenuItem;
@property (weak) IBOutlet NSButton *lockArtWindowCheckBox;
@property (weak) IBOutlet NSButton *showControlsOnMouseOverCheckBox;
@property (weak) IBOutlet NSSlider *artWindowOpacitySlider;

@property (weak) IBOutlet NSWindow *controlWindow;
@property (weak) IBOutlet PLLeftTableViewController *favTableView;
@property (weak) IBOutlet PLRightTableViewController *menuTableView;
@property (weak) IBOutlet PLRadioBrowserToolbox *toolbox;
@property (weak) IBOutlet NSTextField *lowerField;
@property (weak) IBOutlet PLAudioPreviewBox *previewBox;
@property (assign) BOOL controlWindowIsAlreadyOpen;

@property (weak) IBOutlet NSWindow *recordingWindow;
@property (weak) IBOutlet PLRecordingTableViewController *recordingTableView;
@property (assign) BOOL recordingWindowIsAlreadyOpen;

@property (weak) IBOutlet NSBox *rewBox;
@property (weak) IBOutlet NSBox *playBox;
@property (weak) IBOutlet NSBox *ffBox;

@property (assign) BOOL lastArtWindowLevelMenuSelectionWasTopItem;
@property (assign) BOOL lastShowControlsOnMouseOverState;
@property (assign) BOOL lastLockArtWindowState;

@property (retain) NSStatusItem *statusItem;
@property (assign) BOOL menuIsOpen;
@property (assign) BOOL collectingIsBlocked;
@property (strong) NSTimer *blockTimer;
@property (strong) NSTimer *twoShot;

@property (strong) iTunesApplication *iTunes;
@property (strong) MusicApplication *Music;
@property (strong) SwinsianApplication *Swinsian;
@property (strong) SpotifyApplication *Spotify;
@property (strong) NSString *playingApplication;
@property (assign) BOOL appleAppHalfStarsEnabled;

@property (strong) NSString *previousImageURL;
@property (strong) NSString *currentImageURL;
@property (assign) NSInteger defermentRetryCount;
@property (assign) SEL passedSelector;

@property (assign) BOOL growlRunning;

@property (strong) NSNotificationCenter *nc;
@property (strong) NSDistributedNotificationCenter *dnc;
@property (strong) NSNotificationCenter *appTrafficNotice;
@property (strong) NSTimer *notificationDelayTimer;
@property (strong) NSNotification *delayedNotification;
@property (strong) NSTimer *oneShot;

@property (assign) NSInteger retryCount;

@property (strong) NSArray <NSMenuItem *> *menuItemCollection;
@property (strong) NSArray <NSButton *> *starCollection;
@property (assign) float sliderPosition;

@property (strong) PLMediaKeyController *keyController;

@property (strong) NSString *outputPath;

@property (strong) NSArray *tuneRating;
@property (strong) NSArray *spotRating;

@property (assign) BOOL startOnLogin;
@property (assign) BOOL triggerOnStart;
@property (assign) BOOL showAlsoIfFrontmost;
@property (assign) BOOL showMenu;
@property (assign) BOOL muteInsteadOfPause;
@property (assign) BOOL blockNextAndPreviousForRadio;
@property (assign) NSInteger radioButton;
@property (assign) BOOL prefsHaveBeenReset;
@property (assign) BOOL signalCameFromMediaKeys;
@property (assign) NSInteger searchEngineChoiceIndex;

@property (assign) NSUInteger oldCorner;
@property (assign) CGFloat oldOpacity;

@property (strong) NSMutableDictionary *origPrefs;


#pragma mark - BPM Inspector properties

@property (weak) IBOutlet NSWindow *bpmInspector;
@property (weak) IBOutlet NSButton *theBigButton;
@property (weak) IBOutlet NSImageView *leftLamp;
@property (weak) IBOutlet NSImageView *rightLamp;
@property (weak) IBOutlet NSTextField *bpmMeter;
@property (weak) IBOutlet NSButton *minButton;
@property (weak) IBOutlet NSButton *plusButton;

@property (assign) BOOL picToggle;
@property (assign) unsigned int beatsPerMinute;
@property (assign) double timeInterval;
@property (assign) double lastInterval;
@property (assign) double currentAverage;
@property (assign) double precisionSize;
@property (assign) unsigned int maxTimeList;
@property (assign) unsigned int colorIndex;

@property (strong) NSMutableArray *timeList;
@property (strong) NSMutableArray *theColorTable;
@property (strong) NSMutableArray *theLiteColorTable;
@property (strong) NSMutableArray *theDarkColorTable;

@property (strong) NSTimer *blinkTimer;
@property (strong) NSTimer *delayTimer;

@property (strong) NSImage *regDrumImage;
@property (strong) NSImage *altDrumImage;
@property (strong) NSImage *lampOnImage;
@property (strong) NSImage *lampOffImage;


#pragma mark - Radio IBOutlets

@property (weak) IBOutlet NSWindow *urlEntryWindow;

#pragma mark - Radio properties

@property (strong) NSMutableArray <PLRadioFavorite *> *favList;
@property (strong) NSMutableArray <PLRadioFavorite *> *menuList;
@property (strong) NSString *alternativeTitle;
@property (strong) NSString *lastURL;
@property (strong) NSString *plLastStreamTitle;
@property (strong) NSString *plLastStationName;
@property (strong) NSString *plLastDisplayedURL;
@property (strong) NSImage *plDisplayArt;
@property (strong) NSMenuItem *lastFavoriteMenuSelection;
@property (strong) NSMenuItem *favoriteForRestart;
@property (assign) BOOL fetchesArtwork;
@property (assign) BOOL ignoreEmptyBuffer;
@property (strong) NSMenu *favSubMenu;
@property (strong) NSObject *suppressedApp;
@property (strong) NSTimer *timeOutTimer;
@property (strong) NSTimer *firstNotificationCatchTimer;
@property (strong) NSTimer *bufferExpirationTimer;
@property (strong) NSTimer *recordingBlinkTimer;
@property (strong) NSTimer *recordingUpdateTimer;
@property (strong) NSDate *startTime;
@property (strong) NSDate *lastTimeStamp;
@property (strong) NSString *dateString;
@property (strong) NSString *swinsianUUID;
@property (strong) NSTimer *swinsianNotificationDelayTimer;
@property (strong) NSNotification *swinsianNotification;
@property (assign) NSTimeInterval recordingStartedAtThisTime;

@property (assign) NSInteger favIndex;
@property (assign) NSInteger noOfObjects;
@property (assign) float suppressedVolume;
@property (assign) BOOL recordingBlinkState;
@property (strong) NSFileHandle *labelFileHandle;
@property (assign) BOOL recordingDisabled;


@end


@implementation AppDelegate


#pragma mark - App Delegate Methods

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _retryCount = 0;
    _previousImageURL = @"";
    _currentImageURL = @"";
    _prefsHaveBeenReset = NO;
    _collectingIsBlocked = NO;
    NSMutableArray *appList = [[NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.Plectrum"] mutableCopy];
    if ([appList count] > 1) [NSApp terminate:nil];
    _signalCameFromMediaKeys = NO;
    _menuIsOpen = NO;
    _growlRunning = NO;
    _isPlaying = NO;
    _isPaused = NO;
    _isStopped = YES;
    _isRecording = NO;
    _isStarting = NO;
    _isGuardingAgainstRecordingModeChange = NO;
    _hasPlayedBefore = NO;
    _recordingUUID = @"";
    _trackInfoSource = @"None";
    _currentPlayerStatus = @"Stopped";
    _tuneRating = @[@"• • • • •", @"½ • • • •", @"★ • • • •", @"★½ • • •", @"★★ • • •", @"★★½ • •", @"★★★ • •", @"★★★½ •", @"★★★★ •", @"★★★★½", @"★★★★★"];
    _spotRating = @[@"• • • • •", @"❤︎ • • • •", @"❤︎❤︎ • • •", @"❤︎❤︎❤︎ • •", @"❤︎❤︎❤︎❤︎ •", @"❤︎❤︎❤︎❤︎❤︎"];
    _menuItemCollection = [NSArray arrayWithObjects:_miNowPlaying, _miPlayPause, _miNextTrack, _miPreviousTrack, _miActivate, _miCopy, nil];
    _starCollection = [NSArray arrayWithObjects:_star1, _star2, _star3, _star4, _star5, nil];
    for (int i = 0; i < 5; i++) {
        [[[_starCollection objectAtIndex:i] image] setTemplate:YES];
    }
    [[_heart image] setTemplate:YES];
    [[[_searchEnginePopUpButton menu] itemAtIndex:0] setRepresentedObject:@"https://www.bing.com/search?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:1] setRepresentedObject:@"https://search.brave.com/search?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:2] setRepresentedObject:@"https://duckduckgo.com/?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:3] setRepresentedObject:@"https://www.ecosia.org/search?method=index&q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:4] setRepresentedObject:@"https://freespoke.com/search/web?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:5] setRepresentedObject:@"https://www.google.com/search?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:6] setRepresentedObject:@"https://www.qwant.com/?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:7] setRepresentedObject:@"https://www.startpage.com/sp/search?q="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:8] setRepresentedObject:@"https://search.yahoo.com/search?p="];
    [[[_searchEnginePopUpButton menu] itemAtIndex:9] setRepresentedObject:@"https://yandex.com/search/?text="];
    NSImage *globeImage = [NSImage imageNamed:@"button-www"];
    [globeImage setSize:NSMakeSize(14.0f, 14.0f)];
    [_miSearchWeb setImage:globeImage];
    NSImage *bcImage = [NSImage imageNamed:@"button-bandcamp"];
    [bcImage setSize:NSMakeSize(14.0f, 14.0f)];
    [_miSearchBC setImage:bcImage];
    _firstOpen = YES;
    [self appleAppHasHalfStarsEnabled];
    NSImage *playImg = [NSImage imageNamed:@"s_play"];
    [playImg setTemplate:YES];
    [_playPause setImage:playImg];
    NSImage *stopImg = [NSImage imageNamed:@"s_stop"];
    [stopImg setTemplate:YES];
    [_stop setImage:stopImg];
    NSMutableAttributedString *atString = [[NSMutableAttributedString alloc] initWithString:[_lowerField stringValue]];
    NSDictionary *obliqAttrib = @{NSFontAttributeName:[NSFont systemFontOfSize:9.0f], NSObliquenessAttributeName:@0.25f};
    [atString setAttributes:obliqAttrib range:NSMakeRange(0, [[_lowerField stringValue] length])];
    [_lowerField setAttributedStringValue:atString];
    [self getPrefs];
    if (@available(macOS 10.15, *)) {
        _iTunes = nil;
        _Music = [SBApplication applicationWithBundleIdentifier:@"com.apple.Music"];
    } else {
        _iTunes = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];
        _Music = nil;
    }
    _Swinsian = [SBApplication applicationWithBundleIdentifier:@"com.swinsian.Swinsian"];
    _Spotify = [SBApplication applicationWithBundleIdentifier:@"com.spotify.client"];
    _dnc = [NSDistributedNotificationCenter defaultCenter];
    _nc = [NSNotificationCenter defaultCenter];
    _notificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];
    [_notificationCenter setDelegate:self];

    // Observers: iTunes & Music
    if (@available(macOS 10.15, *)) {
        [_dnc addObserver:self selector:@selector(musicUpdateTrackInfo:) name:@"com.apple.Music.playerInfo" object:nil];
        [_dnc addObserver:self selector:@selector(musicTrackInfoChanged:) name:@"com.apple.Music.sourceSaved" object:nil];
    } else {
        [_dnc addObserver:self selector:@selector(iTunesUpdateTrackInfo:) name:@"com.apple.iTunes.playerInfo" object:nil];
        [_dnc addObserver:self selector:@selector(iTunesTrackInfoChanged:) name:@"com.apple.iTunes.sourceSaved" object:nil];
    }
    
    // Observers: Swinsian
    [_dnc addObserver:self selector:@selector(swinsianUpdateTrackInfo:) name:@"com.swinsian.Swinsian-Track-Playing" object:nil];
    [_dnc addObserver:self selector:@selector(swinsianUpdateTrackInfo:) name:@"com.swinsian.Swinsian-Track-Paused" object:nil];
    [_dnc addObserver:self selector:@selector(swinsianUpdateTrackInfo:) name:@"com.swinsian.Swinsian-Track-Stopped" object:nil];

    // Observer: Spotify
    [_dnc addObserver:self selector:@selector(spotifyUpdateTrackInfo:) name:@"com.spotify.client.PlaybackStateChanged" object:nil];

    // Observers: Plectrum
    [_nc addObserver:self selector:@selector(plectrumPreupdateTrackInfo:) name:FSAudioStreamMetaDataNotification object:nil];
    [_nc addObserver:self selector:@selector(plectrumPlayerStatusChanged:) name:FSAudioStreamStateChangeNotification object:nil];
    [_nc addObserver:self selector:@selector(plectrumErrorOccured:) name:FSAudioStreamErrorNotification object:nil];

    // Observers: Media Keys
    [_nc addObserver:self selector:@selector(performPreviousFromMediaKeys) name:MediaKeyPreviousNotification object:nil];
    [_nc addObserver:self selector:@selector(performPlayPauseFromMediaKeys) name:MediaKeyPlayPauseNotification object:nil];
    [_nc addObserver:self selector:@selector(performNextFromMediaKeys) name:MediaKeyNextNotification object:nil];
    [_nc addObserver:self selector:@selector(performVolumeDownFromMediaKeys) name:MediaKeyVolumeDownNotification object:nil];
    [_nc addObserver:self selector:@selector(performVolumeUpFromMediaKeys) name:MediaKeyVolumeUpNotification object:nil];

    // Observers: Apple Interface Theme
    [_dnc addObserver:self selector:@selector(checkDark) name:@"AppleInterfaceThemeChangedNotification" object:nil];

    // Observer: Supported App Termination
    _appTrafficNotice = [[NSWorkspace sharedWorkspace] notificationCenter];
    [_appTrafficNotice addObserver:self selector:@selector(appWasTerminated:) name:NSWorkspaceDidTerminateApplicationNotification object:nil];
    [_appTrafficNotice addObserver:self selector:@selector(appWasLaunched:) name:NSWorkspaceDidLaunchApplicationNotification object:nil];
    
    // Observer: HotKeyCenter
    [_nc addObserver:self selector:@selector(hotKeyFired:) name:@"PLHotKeyPressed" object:_hotKeyCenter];

    _statusItem = nil;
    _trackInfoContents = [NSMutableString stringWithString:@""];
    _trackInfoArray = [[NSMutableArray alloc] init];
    [_tivTitle setMaximumNumberOfLines:3];
    [_tivArtist setMaximumNumberOfLines:2];
    [_tivAlbum setMaximumNumberOfLines:2];
    [_rivStation setMaximumNumberOfLines:2];
    [_rivSong setMaximumNumberOfLines:3];
    [_rivURL setMaximumNumberOfLines:2];
    [_sivStation setMaximumNumberOfLines:3];
    [_sivSong setMaximumNumberOfLines:2];
    [_sivURL setMaximumNumberOfLines:2];
    [self installStatusMenu];
    [self loadRadioFavorites];
    [self addFavoritesToMenu];
    [self loadTrackInfo];
    [self loadRadioRecordings];
    [_helpPanel setHidesOnDeactivate:NO];
    [_helpPopUpButton selectItemAtIndex:0];
    NSBundle * myMainBundle = [NSBundle mainBundle];
    [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
    NSString *helpFileName;
    if ([self darkModeIsActive]) {
        helpFileName = @"tipsandtricks-dm";
    } else {
        helpFileName = @"tipsandtricks-lm";
    }
    NSString * helpFilePath = [myMainBundle pathForResource:helpFileName ofType:@"rtfd"];
    [_helpTextView readRTFDFromFile:helpFilePath];
    [_helpTextView setNeedsDisplay:YES];
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *path = [mainBundle privateFrameworksPath];
    path = [path stringByAppendingPathComponent:@"Growl.framework"];
    NSBundle *growlFramework = [NSBundle bundleWithPath:path];
    if ([growlFramework load]) {
        Class GAB = NSClassFromString(@"GrowlApplicationBridge");
        if ([GAB respondsToSelector:@selector(setGrowlDelegate:)]) {
            [GAB performSelector:@selector(setGrowlDelegate:) withObject:self];
        }
    }
    [_miStars setView:nil];
    [_miStars setHidden:YES];
    [_miHeart setView:nil];
    [_miHeart setHidden:YES];
    [_miSeparator setHidden:YES];
    NSString *whichIsRunning = [self getPlayingApplication];
    if ([whichIsRunning isEqualToString:@"iTunes"]) {
        if (iTunesEPlSPlaying == [_iTunes playerState] || iTunesEPlSPaused == [_iTunes playerState]) {
            [_miStars setView:_starView];
            [_miStars setHidden:NO];
            [_miSeparator setHidden:NO];
        }
    }
    if ([whichIsRunning isEqualToString:@"Music"]) {
        if (MusicEPlSPlaying == [_Music playerState] || MusicEPlSPaused == [_Music playerState]) {
            [_miStars setView:_starView];
            [_miStars setHidden:NO];
            [_miSeparator setHidden:NO];
        }
    }
    if ([whichIsRunning isEqualToString:@"Swinsian"]) {
        if (MusicEPlSPlaying == [_Music playerState] || MusicEPlSPaused == [_Music playerState]) {
            [_miStars setView:_starView];
            [_miStars setHidden:NO];
            [_miSeparator setHidden:NO];
        }
    }
    [self getTrackInfo];
    if (_trackTitle == nil) {
        [_miTrackView setView:nil];
        [_miPlaying setHidden:NO];
        [_miTrackView setHidden:YES];
    } else {
        NSString *app = [self getPlayingApplication];
        if ([app containsString:@"Radio"]) {
            [_miTrackView setView:_riv];
        } else {
            [_miTrackView setView:_tiv];
        }
        [self collectTrackInfoForMenu];
        [_miPlaying setHidden:YES];
        [_miTrackView setHidden:NO];
    }
    if (_triggerOnStart) [self showNowPlayingInfo];
    [[_PLNotificationCenter notification] setBackgroundColor:[NSColor clearColor]];
    [[_PLNotificationCenter box] setBorderColor:[NSColor clearColor]];
    [[_PLNotificationCenter box] setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:[_PLNotificationCenter opacity]]];
    [[_PLNotificationCenter radioNotification] setBackgroundColor:[NSColor clearColor]];
    [[_PLNotificationCenter radioBox] setBorderColor:[NSColor clearColor]];
    [[_PLNotificationCenter radioBox] setFillColor:[NSColor colorWithCalibratedRed:0.1f green:0.1f blue:0.1f alpha:[_PLNotificationCenter opacity]]];
    [_coverArtWindow prepareForFirstUse];
    [_coverArtWindow makeKeyAndOrderFront:nil];
    if ([_coverArtWindow artWindowEnabled] && ([[self getNewStatus] isNotEqualTo:@"Stopped"])) {
        [_coverArtWindow fadeIn];
    }
    _timeInterval = 1;
    _maxTimeList = 16;
    _currentAverage = 0.0;
    _beatsPerMinute = 60;
    _precisionSize = 0.05;
    _timeList = [NSMutableArray arrayWithCapacity:_maxTimeList];
    _theLiteColorTable = [[NSMutableArray alloc] init];
    _theDarkColorTable = [[NSMutableArray alloc] init];
    _picToggle = NO;
    _blinkTimer = nil;
    _delayTimer = nil;
    _regDrumImage = [NSImage imageNamed:@"drum-left"];
    [_regDrumImage setTemplate:YES];  // <-- optional refreshment
    _altDrumImage = [NSImage imageNamed:@"drum-right"];
    [_altDrumImage setTemplate:YES];  // <-- optional refreshment
    _lampOnImage = [NSImage imageNamed:@"lamp-on"];
    _lampOffImage = [NSImage imageNamed:@"lamp-off"];
    [_theBigButton sendActionOn:NSLeftMouseDownMask];
    NSImage *minImage = [NSImage imageNamed:@"minusicon"];
    [minImage setTemplate:YES];
    [_minButton setImage:minImage];
    NSImage *plusImage = [NSImage imageNamed:@"plusicon"];
    [plusImage setTemplate:YES];
    [_plusButton setImage:plusImage];
    [self buildColorTable];
    [_bpmInspector setLevel:NSFloatingWindowLevel];
    _trackInfoWindowIsAlreadyOpen = NO;
    _controlWindowIsAlreadyOpen = NO;
    [self checkDark];
    if (_prefsHaveBeenReset) {
        _prefsHaveBeenReset = NO;
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Settings and hotkeys have been reset" informativeText:@"Inconsistencies were found in your settings and they have been reset, along with your hotkeys. Your radio favorites, saved track info and recording bin are not affected. Please open the Settings to again set them to your desired values. We apologize for the inconvenience."];
    }
    [self handleTrust];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    if (@available(macOS 10.15, *)) {
        [_dnc removeObserver:self name:@"com.apple.Music.playerInfo" object:nil];
        [_dnc removeObserver:self name:@"com.apple.Music.sourceSaved" object:nil];
    } else {
        [_dnc removeObserver:self name:@"com.apple.iTunes.playerInfo" object:nil];
        [_dnc removeObserver:self name:@"com.apple.iTunes.sourceSaved" object:nil];
    }
    [_dnc removeObserver:self name:@"com.swinsian.Swinsian-Track-Playing" object:nil];
    [_dnc removeObserver:self name:@"com.swinsian.Swinsian-Track-Paused" object:nil];
    [_dnc removeObserver:self name:@"com.swinsian.Swinsian-Track-Stopped" object:nil];
    [_dnc removeObserver:self name:@"com.spotify.client.PlaybackStateChanged" object:nil];
    [_dnc removeObserver:self name:@"AppleInterfaceThemeChangedNotification" object:nil];
    [_appTrafficNotice removeObserver:self name:NSWorkspaceDidTerminateApplicationNotification object:nil];
    [_dnc removeObserver:self name:FSAudioStreamMetaDataNotification object:nil];
    [_dnc removeObserver:self name:FSAudioStreamStateChangeNotification object:nil];
    [_dnc removeObserver:self name:FSAudioStreamErrorNotification object:nil];
    [_dnc removeObserver:self name:MediaKeyPreviousNotification object:nil];
    [_dnc removeObserver:self name:MediaKeyPlayPauseNotification object:nil];
    [_dnc removeObserver:self name:MediaKeyNextNotification object:nil];
    [_dnc removeObserver:self name:MediaKeyVolumeDownNotification object:nil];
    [_dnc removeObserver:self name:MediaKeyVolumeUpNotification object:nil];
    [_nc removeObserver:self name:@"PLHotKeyPressed" object:_hotKeyCenter];
    [_nc removeObserver:self name:FSAudioStreamMetaDataNotification object:nil];
    [_nc removeObserver:self name:FSAudioStreamStateChangeNotification object:nil];
    [_nc removeObserver:self name:FSAudioStreamErrorNotification object:nil];
    [_nc removeObserver:self name:MediaKeyPlayPauseNotification object:nil];
    [_nc removeObserver:self name:MediaKeyNextNotification object:nil];
    [_nc removeObserver:self name:MediaKeyVolumeDownNotification object:nil];
    [_nc removeObserver:self name:MediaKeyVolumeUpNotification object:nil];
    [self discardStatusMenu];
    if (_isMuted) _plVolume = _oldPlVolume;
    //[_hotKeyCenter unregisterHotKeys];
    if (_keyController) [_keyController releaseTap];
    [_window orderOut:self];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    if (!flag || (![_window isVisible])) {
        [self storePrefs];
        [self preparePrefsWindow];
        [_window makeKeyAndOrderFront:self];
    }
    return YES;
}


#pragma mark - Notification Center Delegate Methods

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification {
    return YES;
}

- (void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification {
    if ([_trackInfoSource isEqualToString:@"iTunes"]) {
        [_iTunes activate];
    } else if ([_trackInfoSource isEqualToString:@"Music"]) {
        [_Music activate];
    } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
        [_Swinsian activate];
    } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
        [_Spotify activate];
    }
}


#pragma mark - Growl Delegate Methods

- (NSString *) applicationNameForGrowl {
    return @"Plectrum";
}

- (NSData *) applicationIconDataForGrowl {
    return [[self applicationIconForGrowl] TIFFRepresentation];
}

- (NSImage *)applicationIconForGrowl {
    return [NSImage imageNamed:@"appicon"];
}

- (void)growlIsReady {
    _growlRunning = YES;
}

- (void)growlNotificationWasClicked:(id)clickContext {
    if ([_trackInfoSource isEqualToString:@"iTunes"]) {
        [_iTunes activate];
    } else if ([_trackInfoSource isEqualToString:@"Music"]) {
        [_Music activate];
    } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
        [_Swinsian activate];
    } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
        [_Spotify activate];
    }
}

- (void)windowDidResize:(NSNotification *)notification {
    if ([[notification object] isEqual:_coverArtWindow]) {
        [_coverArtWindow windowDidResize:notification];
    }
}

#pragma mark - Hotkey Methods

- (void)hotKeyFired:(NSNotification *)notification {
    int command = [[[notification userInfo] valueForKey:@"keyID"] intValue];
    [self respondToHotkey:command];
}

- (void)respondToHotkey:(int)keyID {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    NSString *infoString;
    NSString *whichIsRunning = [self getPlayingApplication];
    switch (keyID) {
        case 0: // volume up
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                NSInteger iTunesVolume = [_iTunes soundVolume];
                iTunesVolume += 10;
                if (iTunesVolume > 100) iTunesVolume = 100;
                [_iTunes setSoundVolume:iTunesVolume];
                [_slider setFloatValue:(float)iTunesVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                NSInteger musicVolume = [_Music soundVolume];
                musicVolume += 10;
                if (musicVolume > 100) musicVolume = 100;
                [_Music setSoundVolume:musicVolume];
                [_slider setFloatValue:(float)musicVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                NSInteger musicVolume = [[_Swinsian soundVolume] intValue];
                musicVolume += 10;
                if (musicVolume > 100) musicVolume = 100;
                [_Swinsian setSoundVolume:[NSNumber numberWithInt:(int)musicVolume]];
                [_slider setFloatValue:(float)musicVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                NSInteger spotifyVolume = [_Spotify soundVolume];
                spotifyVolume += 10;
                if (spotifyVolume > 100) spotifyVolume = 100;
                [_Spotify setSoundVolume:spotifyVolume];
                [_slider setFloatValue:(float)spotifyVolume];
            } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
                 if (_isMuted && _isPaused) {
                    [self playpause:self];
                }
                _plVolume += 0.1f;
                if (_plVolume > 1) _plVolume = 1;
                [_radioController setVolume:_plVolume];
                [_slider setFloatValue:(_plVolume * 100)];
            }
            break;
            
        case 1: // volume down
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                NSInteger iTunesVolume = [_iTunes soundVolume];
                iTunesVolume -= 10;
                if (iTunesVolume < 0) iTunesVolume = 0;
                [_iTunes setSoundVolume:iTunesVolume];
                [_slider setFloatValue:(float)iTunesVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                NSInteger musicVolume = [_Music soundVolume];
                musicVolume -= 10;
                if (musicVolume < 0) musicVolume = 0;
                [_Music setSoundVolume:musicVolume];
                [_slider setFloatValue:(float)musicVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                NSInteger musicVolume = [[_Swinsian soundVolume] intValue];
                musicVolume -= 10;
                if (musicVolume < 0) musicVolume = 0;
                [_Swinsian setSoundVolume:[NSNumber numberWithInt:(int)musicVolume]];
                [_slider setFloatValue:(float)musicVolume];
                return;
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                NSInteger spotifyVolume = [_Spotify soundVolume];
                spotifyVolume -= 10;
                if (spotifyVolume < 0) spotifyVolume = 0;
                [_Spotify setSoundVolume:spotifyVolume];
                [_slider setFloatValue:(float)spotifyVolume];
            } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
                if (_isMuted && _isPaused) {
                    [self playpause:self];
                }
                _plVolume -= 0.1f;
                if (_plVolume < 0) _plVolume = 0;
                [_radioController setVolume:_plVolume];
                [_slider setFloatValue:(_plVolume * 100)];
            }
            break;
            
        case 2: // now playing
            [self showNowPlayingInfo];
            break;
            
        case 3: // play / pause
            if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                if ([_iTunes isRunning]) {
                    [_iTunes playpause];
                    if ([_iTunes playerState] == iTunesEPlSPaused) {
                        [_coverArtWindow receiveStatus:@"Paused"];
                    } else {
                        [_coverArtWindow receiveStatus:@"Playing"];
                    }
                }
            } else if ([_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"MusicRadio"]) {
                if ([_Music isRunning]) {
                    [_Music playpause];
                    if ([_Music playerState] == MusicEPlSPaused) {
                        [_coverArtWindow receiveStatus:@"Paused"];
                    } else {
                        [_coverArtWindow receiveStatus:@"Playing"];
                    }
                }
            } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
                if ([_Swinsian isRunning]) {
                    [_Swinsian playpause];
                    if ([_Swinsian playerState] == SwinsianPlayerStatePaused) {
                        [_coverArtWindow receiveStatus:@"Paused"];
                    } else {
                        [_coverArtWindow receiveStatus:@"Playing"];
                    }
                }
            } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
                if ([_Spotify isRunning]) {
                    [_Spotify playpause];
                    if ([_Spotify playerState] == SpotifyEPlSPaused) {
                        [_coverArtWindow receiveStatus:@"Paused"];
                    } else {
                        [_coverArtWindow receiveStatus:@"Playing"];
                    }
                }
                
            } else if (_isPlaying) {
                [self playpause:self];
                if (_isPaused) {
                    _currentPlayerStatus = @"Paused";
                } else {
                    _currentPlayerStatus = @"Playing";
                }
            }
            break;
            
        case 4: // next track
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                [_iTunes nextTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                [_Music nextTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                [_Swinsian nextTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                [_Spotify nextTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
                if (_favIndex == NSNotFound) return;
                if (!_signalCameFromMediaKeys) {
                    if (_blockNextAndPreviousForRadio) return;
                }
                if (_playingFromURL) return;
                if ([_favList count] < 2) return;
                switch(_radioButton) {
                    case 0:
                        [_notificationCenter removeDeliveredNotification:_notification];
                        break;
                        
                    case 2:
                        [_PLNotificationCenter closeNotification:self];
                        break;
                        
                    default:
                        break;
                }
                if (_timeOutTimer) {
                    [_timeOutTimer invalidate];
                    _timeOutTimer = nil;
                }
                if (_firstNotificationCatchTimer) {
                    [_firstNotificationCatchTimer invalidate];
                    _firstNotificationCatchTimer = nil;
                }
                _favIndex++;
                if (_favIndex > (_noOfObjects - 1)) _favIndex = 0;
                [self playFavoriteByIndex:_favIndex];
            }
            break;
            
        case 5: // previous track
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                [_iTunes previousTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                [_Music previousTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                [_Swinsian previousTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                [_Spotify previousTrack];
                return;
            } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
                if (_favIndex == NSNotFound) return;
                if (!_signalCameFromMediaKeys) {
                    if (_blockNextAndPreviousForRadio) return;
                }
                if  (_playingFromURL) return;
                if ([_favList count] < 2) return;
                switch(_radioButton) {
                    case 0:
                        [_notificationCenter removeDeliveredNotification:_notification];
                        break;
                        
                    case 2:
                        [_PLNotificationCenter closeNotification:self];
                        break;
                        
                    default:
                        break;
                }
                if (_timeOutTimer) {
                    [_timeOutTimer invalidate];
                    _timeOutTimer = nil;
                }
                if (_firstNotificationCatchTimer) {
                    [_firstNotificationCatchTimer invalidate];
                    _firstNotificationCatchTimer = nil;
                }
                _favIndex--;
                if (_favIndex < 0) _favIndex = (_noOfObjects - 1);
                [self playFavoriteByIndex:_favIndex];
            }
            break;
            
        case 6: // activate
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                [_iTunes activate];
                return;
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                [_Music activate];
                return;
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                [_Swinsian activate];
                return;
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                [_Spotify activate];
                return;
            }
            break;
            
        case 7: // copy tune info to textview
            if ([whichIsRunning isEqualToString:@"iTunes"]) {
                [self getTrackInfo];
                if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                    infoString = [NSString stringWithFormat:@"%@", _trackArtist];
                } else {
                    infoString = [NSString stringWithFormat:@"%@ - %@", _trackArtist, _trackTitle];
                }
            } else if ([whichIsRunning isEqualToString:@"Music"]) {
                [self getTrackInfo];
                if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                    infoString = [NSString stringWithFormat:@"%@", _trackArtist];
                } else {
                    infoString = [NSString stringWithFormat:@"%@ - %@", _trackArtist, _trackTitle];
                }
            } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
                [self getTrackInfo];
                infoString = [NSString stringWithFormat:@"%@ - %@", _trackArtist, _trackTitle];
            } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
                [self getTrackInfo];
                infoString = [NSString stringWithFormat:@"%@ - %@", _trackArtist, _trackTitle];
            } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
                [self getTrackInfo];
                infoString = [NSString stringWithFormat:@"%@", _trackTitle];
            }
            if (_isPaused) return;
            if ([infoString isEqualToString:@"Opening stream..."]) return;
            if (_notification) {
                if ([[_notification identifier] hasPrefix:@"TRANSIENT-"]) {
                    [_notificationCenter removeDeliveredNotification:_notification];
                }
             }
            _notification = [[NSUserNotification alloc] init];
            if (infoString == nil || [infoString isEqualToString:@"(null)"] || [infoString isEqualToString:@"Internet radio or relay"]) {
                if ([_radioController isPlaying]) {
                    [_notification setTitle:@"No station metadata"];
                    [_notification setSubtitle:@"The current track info could not be saved,"];
                    [_notification setInformativeText:@"as there is no metadata present in the stream."];
                    [_notification setSoundName:nil];
                    NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
                    [_notification setIdentifier:notificationUUID];
                    [_notificationCenter deliverNotification:_notification];
                }
                return;
            }
            if (![_trackInfoArray containsObject:infoString]) {
                [_trackInfoArray addObject:infoString];
                [_trackInfoTableView reloadData];
                [_trackInfoTableView scrollRowToVisible:[_trackInfoArray count] - 1];
                [self saveTrackInfo];
                [_trackInfoClearButton setEnabled:YES];
                [_trackInfoExportButton setEnabled:YES];
                [_notification setTitle:@"Track info saved"];
                [_notification setSubtitle:@"Info has been saved for track:"];
                [_notification setInformativeText:infoString];
                [_notification setSoundName:nil];
                NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
                [_notification setIdentifier:notificationUUID];
                [_notificationCenter deliverNotification:_notification];
            } else {
                [_notification setTitle:@"Track info NOT saved"];
                [_notification setSubtitle:@"This track was already on the list:"];
                [_notification setInformativeText:infoString];
                [_notification setSoundName:nil];
                NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
                [_notification setIdentifier:notificationUUID];
                [_notificationCenter deliverNotification:_notification];
            }
            break;
    }
}

- (void)performNextFromMediaKeys {
    _signalCameFromMediaKeys = YES;
    [self respondToHotkey:4];
    _signalCameFromMediaKeys = NO;
}

- (void)performPlayPauseFromMediaKeys {
    [self respondToHotkey:3];
}

- (void)performPreviousFromMediaKeys {
    _signalCameFromMediaKeys = YES;
    [self respondToHotkey:5];
    _signalCameFromMediaKeys = NO;
}

- (void)performVolumeDownFromMediaKeys {
    [self respondToHotkey:1];
}

- (void)performVolumeUpFromMediaKeys {
    [self respondToHotkey:0];
}

- (IBAction)handleMenuSelection:(id)sender {
    [self respondToHotkey:[[sender identifier] intValue]];
}

- (void)performMenuSelectionByIdentifier:(int)identifier {
    [self respondToHotkey:identifier];
}

#pragma mark - Hotkey IBActions

- (IBAction)setHotkeysToDefault:(id)sender {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Reset Hotkeys to Default"];
    [alert setInformativeText:@"You are about to reset the hotkeys to their default setting. Is this really what you want to do?"];
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert setAlertStyle:NSAlertStyleWarning];
    [alert beginSheetModalForWindow:_window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSAlertFirstButtonReturn) {
            [self->_hotKeyCenter unregisterHotKeys];
            [self->_hotKeyCenter initHotKeys];
            [self setMenuKeyEquivalents];
        }
    }];
}


#pragma mark - Radio Browser IBActions

- (IBAction)showRadioBrowserSheet:(id)sender {
    [_toolbox showBrowserSheetOnWindow:_controlWindow];
}

- (IBAction)voteForCurrentFavorite:(id)sender {
    NSString *uuid = [_currentFav stationuuid];
    if (uuid && [uuid isNotEqualTo:@""]) [_toolbox voteForStationWithUUID:uuid];
}


#pragma mark - Miscellaneous IBActions

- (IBAction)showHelpWindow:(id)sender {
    NSBundle * myMainBundle = [NSBundle mainBundle];
    NSString *helpFileName, *helpFilePath;
    [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
    [_helpPopUpButton selectItemAtIndex:0];
    if ([self darkModeIsActive]) {
        helpFileName = @"tipsandtricks-dm";
    } else {
        helpFileName = @"tipsandtricks-lm";
    }
    helpFilePath = [myMainBundle pathForResource:helpFileName ofType:@"rtfd"];
    [_helpTextView readRTFDFromFile:helpFilePath];
    [[_helpScrollView documentView] scrollPoint:NSMakePoint(0.0f, 0.0f)];
    [_helpTextView setNeedsDisplay:YES];
    [_helpPanel makeKeyAndOrderFront:nil];
    [NSApp activateIgnoringOtherApps:YES];
}

- (IBAction)helpPopUpButtonChanged:(id)sender {
    NSString *helpFileName, *helpFilePath;
    NSInteger choice = [_helpPopUpButton indexOfSelectedItem];
    NSBundle * myMainBundle = [NSBundle mainBundle];
    [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
    if (choice == 0) {
        if ([self darkModeIsActive]) {
            helpFileName = @"tipsandtricks-dm";
        } else {
            helpFileName = @"tipsandtricks-lm";
        }
        helpFilePath = [myMainBundle pathForResource:helpFileName ofType:@"rtfd"];
    } else {
        helpFilePath = [myMainBundle pathForResource:@"fetcharthelp" ofType:@"rtf"];
    }
    [_helpTextView readRTFDFromFile:helpFilePath];
    [[_helpScrollView documentView] scrollPoint:NSMakePoint(0.0f, 0.0f)];
    [_helpTextView setNeedsDisplay:YES];
}

#pragma mark - Status Menu (Delegate) Methods

- (void)installStatusMenu {
    if (_statusItem == nil) {
        NSStatusBar *statusBar = [NSStatusBar systemStatusBar];
        if (_showMenu) {
            _statusItem = [statusBar statusItemWithLength:24];
        } else {
            _statusItem = [statusBar statusItemWithLength:0];
        }
        [self setMenuKeyEquivalents];
        if (@available(macOS 10.12, *)) {
            [_statusItem setAutosaveName:@"PlectrumStatusItem"];
            [_statusItem setVisible:YES];
        }
        [self installPick];
        [_statusMenu setMinimumWidth:305.0f];
        [_statusItem setMenu:_statusMenu];
    }
}

- (void)installPick {
    NSImage *image = [NSImage imageNamed:@"pick"];
    [image setSize:NSMakeSize(16, 16)];
    [image setTemplate:YES];
    [_statusItem setImage:image];
}

- (void)installRaydio {
    NSImage *image;
    NSImage *altImage;
    if (!_isRecording) {
        image = [NSImage imageNamed:@"ray-dee-yo"];
        [image setTemplate:YES];
    } else {
        if ([self darkModeIsActive]) {
            image = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
            altImage = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
        } else {
            image = [NSImage imageNamed:@"ray-dee-yo-dark-rec"];
            altImage = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
        }
    }
    [image setSize:NSMakeSize(16, 16)];
    [altImage setSize:NSMakeSize(16, 16)];
    [_statusItem setImage:image];
    [_statusItem setAlternateImage:altImage];
}

- (void)darkModeSwitchDetected {
    NSImage *image;
    NSImage *altImage;
    if (_isRecording) {
        if ([self darkModeIsActive]) {
            image = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
            altImage = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
        } else {
            image = [NSImage imageNamed:@"ray-dee-yo-dark-rec"];
            altImage = [NSImage imageNamed:@"ray-dee-yo-light-rec"];
        }
        [image setSize:NSMakeSize(16, 16)];
        [altImage setSize:NSMakeSize(16, 16)];
        [_statusItem setImage:image];
        [_statusItem setAlternateImage:altImage];
    }
}

- (void)discardStatusMenu {
    if (_statusItem) {
        [[NSStatusBar systemStatusBar] removeStatusItem: _statusItem];
        _statusItem = nil;
    }
}

- (void)hideStatusMenu {
    [_statusItem setLength:0];
}

- (void)showStatusMenu {
    [_statusItem setLength:24];
}

- (void)setMenuKeyEquivalents {
    [_miNowPlaying setKeyEquivalent:@""];
    [_miNowPlaying setKeyEquivalentModifierMask:0];
    [_miPlayPause setKeyEquivalent:@""];
    [_miPlayPause setKeyEquivalentModifierMask:0];
    [_miNextTrack setKeyEquivalent:@""];
    [_miNextTrack setKeyEquivalentModifierMask:0];
    [_miPreviousTrack setKeyEquivalent:@""];
    [_miPreviousTrack setKeyEquivalentModifierMask:0];
    [_miActivate setKeyEquivalent:@""];
    [_miActivate setKeyEquivalentModifierMask:0];
    for (PLHotKey *hotKey in [_hotKeyCenter hotKeys]) {
        NSString *keyEquiv = [_hotKeyCenter hotKeyToMenuKeyEquivalent:(int)[hotKey keyCode]];
        switch([hotKey keyID]) {
            case 2: {
                [_miNowPlaying setKeyEquivalent:keyEquiv];
                [_miNowPlaying setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
            case 3: {
                [_miPlayPause setKeyEquivalent:keyEquiv];
                [_miPlayPause setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
                
            case 4: {
                [_miNextTrack setKeyEquivalent:keyEquiv];
                [_miNextTrack setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
                
            case 5: {
                [_miPreviousTrack setKeyEquivalent:keyEquiv];
                [_miPreviousTrack setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
                
            case 6: {
                [_miActivate setKeyEquivalent:keyEquiv];
                [_miActivate setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
            
            case 7: {
                [_miCopy setKeyEquivalent:keyEquiv];
                [_miCopy setKeyEquivalentModifierMask:[hotKey modMask]];
                break;
            }
                
            default: {
                break;
            }
        }
    }
}

- (void)menuWillOpen:(NSMenu *)menu {
    _menuIsOpen = YES;
    NSString *whichIsRunning = [self getPlayingApplication];
    [_miSearchWeb setHidden:YES];
    [_miSearchWeb setEnabled:NO];
    [_miSearchBC setHidden:YES];
    [_miSearchBC setEnabled:NO];
    if ([_iTunes isRunning] || [_Music isRunning]) {
        [_miBPMInspector setHidden:NO];
        [_miBPMSeparator setHidden:NO];
        [_miSearchWeb setHidden:NO];
        [_miSearchWeb setEnabled:YES];
        [_miSearchBC setHidden:NO];
        [_miSearchBC setEnabled:YES];
    } else {
        if (![_bpmInspector isVisible]) {
            [_miBPMInspector setHidden:YES];
            if ([_miArtWindow isHidden]) {
                [_miBPMSeparator setHidden:YES];
            } else {
                [_miBPMSeparator setHidden:NO];
            }
        } else {
            [_miBPMInspector setHidden:NO];
            [_miBPMSeparator setHidden:NO];
        }
    }
    if ([whichIsRunning isEqualToString:@"iTunes"]) {
        [_miMakeFav setHidden:YES];
        if (iTunesEPlSPlaying == [_iTunes playerState] || iTunesEPlSPaused == [_iTunes playerState]) {
            [_miMakeFav setHidden:YES];
            if ([_trackInfoSource isEqualToString:@"None"]) {
                [self voidTrackInfoForMenu];
            } else {
                [self getTrackInfo];
            }
            [_miUpperSeparator setHidden:NO];
            [_miSlider setView:_sliderView];
            [_miSlider setHidden:NO];
            [_slider setFloatValue:(float)[_iTunes soundVolume]];
            for (int i = 0; i < [_menuItemCollection count]; i++) {
                [[_menuItemCollection objectAtIndex:i] setEnabled:YES];
            }
            [_miActivate setHidden:NO];
            [_miNextTrack setTitle:@"Next Track"];
            [_miPreviousTrack setTitle:@"Previous Track"];
            [_miActivate setTitle:@"Activate iTunes"];
            if (iTunesEPlSPlaying == [_iTunes playerState] || iTunesEPlSPaused == [_iTunes playerState]) {
                if ([_trackInfoSource isEqualToString:@"iTunes"]) {
                    [_miTrackView setView:_tiv];
                    [self collectTrackInfoForMenu];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miStars setView:_starView];
                    [_miStars setHidden:NO];
                    [_miHeart setView:_heartView];
                    [_miHeart setHidden:NO];
                    [_miSeparator setHidden:NO];
                    [_miSearchWeb setHidden:NO];
                    [_miSearchWeb setEnabled:YES];
                    [_miSearchBC setHidden:NO];
                    [_miSearchBC setEnabled:YES];
                } else {
                    [_miTrackView setView: _riv];
                    [self collectTrackInfoForMenu];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miStars setView:nil];
                    [_miStars setHidden:YES];
                    [_miHeart setView:nil];
                    [_miHeart setHidden:YES];
                    [_miSeparator setHidden:YES];
                }
            } else {
                [_miTrackView setView:nil];
                [_miPlaying setHidden:NO];
                [_miTrackView setHidden:YES];
                [_miStars setView:nil];
                [_miStars setHidden:YES];
                [_miHeart setView:nil];
                [_miHeart setHidden:YES];
                [_miSeparator setHidden:YES];
            }
            return;
        }
    } else if ([whichIsRunning isEqualToString:@"Music"]) {
        [_miMakeFav setHidden:YES];
        if (MusicEPlSPlaying == [_Music playerState] || MusicEPlSPaused == [_Music playerState]) {
            if ([_trackInfoSource isEqualToString:@"None"]) {
                [self voidTrackInfoForMenu];
            } else {
                [self getTrackInfo];
            }
            [_miUpperSeparator setHidden:NO];
            [_miSlider setView:_sliderView];
            [_miSlider setHidden:NO];
            [_slider setFloatValue:(float)[_Music soundVolume]];
            for (int i = 0; i < [_menuItemCollection count]; i++) {
                [[_menuItemCollection objectAtIndex:i] setEnabled:YES];
            }
            [_miActivate setHidden:NO];
            [_miNextTrack setTitle:@"Next Track"];
            [_miPreviousTrack setTitle:@"Previous Track"];
            [_miActivate setTitle:@"Activate Music"];
            if (MusicEPlSPlaying == [_Music playerState] || MusicEPlSPaused == [_Music playerState]) {
                if ([_trackInfoSource isEqualToString:@"Music"]) {
                    [_miTrackView setView:_tiv];
                    [self collectTrackInfoForMenu];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miStars setView:_starView];
                    [_miStars setHidden:NO];
                    [_miHeart setView:_heartView];
                    [_miHeart setHidden:NO];
                    [_miSeparator setHidden:NO];
                    [_miSearchWeb setHidden:NO];
                    [_miSearchWeb setEnabled:YES];
                    [_miSearchBC setHidden:NO];
                    [_miSearchBC setEnabled:YES];
                } else {
                    [_miTrackView setView: _riv];
                    [self collectTrackInfoForMenu];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miStars setView:nil];
                    [_miStars setHidden:YES];
                    [_miHeart setView:nil];
                    [_miHeart setHidden:YES];
                    [_miSeparator setHidden:YES];
                }
            } else {
                [_miTrackView setView:nil];
                [_miPlaying setHidden:NO];
                [_miTrackView setHidden:YES];
                [_miStars setView:nil];
                [_miStars setHidden:YES];
                [_miHeart setView:nil];
                [_miHeart setHidden:YES];
                [_miSeparator setHidden:YES];
            }
            return;
        }
    } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
        [_miMakeFav setHidden:YES];
        if (SwinsianPlayerStatePlaying == [_Swinsian playerState] || SwinsianPlayerStatePaused == [_Swinsian playerState]) {
            if ([_trackInfoSource isEqualToString:@"None"]) {
                [self voidTrackInfoForMenu];
            } else {
                [self getTrackInfo];
            }
            [_miHeart setView:nil];
            [_miHeart setHidden:YES];
            [_miUpperSeparator setHidden:NO];
            [_miSlider setView:_sliderView];
            [_miSlider setHidden:NO];
            [_slider setFloatValue:(float)[[_Swinsian soundVolume] intValue]];
            for (int i = 0; i < [_menuItemCollection count]; i++) {
                [[_menuItemCollection objectAtIndex:i] setEnabled:YES];
            }
            [_miActivate setHidden:NO];
            [_miNextTrack setTitle:@"Next Track"];
            [_miPreviousTrack setTitle:@"Previous Track"];
            [_miActivate setTitle:@"Activate Swinsian"];
            if (SwinsianPlayerStatePlaying == [_Swinsian playerState] || SwinsianPlayerStatePaused == [_Swinsian playerState]) {
                if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
                    [_miTrackView setView:_tiv];
                    [self collectTrackInfoForMenu];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miStars setView:_starView];
                    [_miStars setHidden:NO];
                    [_miSeparator setHidden:NO];
                    [_miSearchWeb setHidden:NO];
                    [_miSearchWeb setEnabled:YES];
                    [_miSearchBC setHidden:NO];
                    [_miSearchBC setEnabled:YES];
                }
            } else {
                [_miTrackView setView:nil];
                [_miPlaying setHidden:NO];
                [_miTrackView setHidden:YES];
                [_miStars setView:nil];
                [_miStars setHidden:YES];
                [_miSeparator setHidden:YES];
            }
            return;
        }
    } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
        [_miMakeFav setHidden:YES];
        if (SpotifyEPlSPlaying == [_Spotify playerState] || SpotifyEPlSPaused == [_Spotify playerState]) {
            if ([_trackInfoSource isEqualToString:@"None"]) {
                [self voidTrackInfoForMenu];
            } else {
                [self getTrackInfo];
            }
            [_miStars setView:nil];
            [_miStars setHidden:YES];
            [_miHeart setView:nil];
            [_miHeart setHidden:YES];
            [_miSeparator setHidden:YES];
            [_miUpperSeparator setHidden:NO];
            [_miSlider setView:_sliderView];
            [_miSlider setHidden:NO];
            [_slider setFloatValue:(float)[_Spotify soundVolume]];
            for (int i = 0; i < [_menuItemCollection count]; i++) {
                [[_menuItemCollection objectAtIndex:i] setEnabled:YES];
            }
            [_miActivate setHidden:NO];
            [_miNextTrack setTitle:@"Next Track"];
            [_miPreviousTrack setTitle:@"Previous Track"];
            [_miActivate setTitle:@"Activate Spotify"];
            if (SpotifyEPlSPlaying == [_Spotify playerState] || SpotifyEPlSPaused == [_Spotify playerState]) {
                [self collectTrackInfoForMenu];
                if ([_trackTitle isEqualToString:@"Nothing playing..."] && [_trackInfoSource isNotEqualTo:@"Spotify"]) {
                    [self voidTrackInfoForMenuLite];
                    [_miTrackView setView:nil];
                    [_miPlaying setHidden:NO];
                    [_miTrackView setHidden:YES];
                } else {
                    [_miTrackView setView:_tiv];
                    [_miPlaying setHidden:YES];
                    [_miTrackView setHidden:NO];
                    [_miSearchWeb setHidden:NO];
                    [_miSearchWeb setEnabled:YES];
                    [_miSearchBC setHidden:NO];
                    [_miSearchBC setEnabled:YES];
                }
            } else {
                [self voidTrackInfoForMenuLite];
                [_miTrackView setView:nil];
                [_miPlaying setHidden:NO];
                [_miTrackView setHidden:YES];
            }
            return;
        }
    } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
        if (_isPlaying) {
            if (_playingFromFav) [_miMakeFav setHidden:YES];
            if (_playingFromURL) [_miMakeFav setHidden:NO];
            if ([_trackInfoSource isEqualToString:@"None"]) {
                [self voidTrackInfoForMenu];
            } else {
                [self getTrackInfo];
            }
            [_miStars setView:nil];
            [_miStars setHidden:YES];
            [_miHeart setView:nil];
            [_miHeart setHidden:YES];
            [_miSeparator setHidden:YES];
            [_miUpperSeparator setHidden:NO];
            [_miSlider setView:_sliderView];
            [_miSlider setHidden:NO];
            [_slider setFloatValue:([_radioController volume] * 100)];
            for (int i = 0; i < [_menuItemCollection count]; i++) {
                [[_menuItemCollection objectAtIndex:i] setEnabled:YES];
            }
            [_miActivate setHidden:YES];
            [_miNextTrack setTitle:@"Next Favorite"];
            [_miPreviousTrack setTitle:@"Previous Favorite"];
            [_miActivate setTitle:@"Activate Plectrum"];
            if (_isPlaying) {
                [_miTrackView setView: _siv];
                [self collectTrackInfoForMenu];
                [_miPlaying setHidden:YES];
                [_miTrackView setHidden:NO];
                [_miSearchWeb setHidden:NO];
                [_miSearchWeb setEnabled:YES];
                [_miSearchBC setHidden:NO];
                [_miSearchBC setEnabled:YES];
            } else {
                [_miTrackView setView:nil];
                [_miPlaying setHidden:NO];
                [_miTrackView setHidden:YES];
            }
            return;
        }
    } else {
        [_miMakeFav setHidden:YES];
        [_miUpperSeparator setHidden:YES];
        [_miSlider setView:nil];
        [_miSlider setHidden:YES];
        for (int i = 0; i < [_menuItemCollection count]; i++) {
            [[_menuItemCollection objectAtIndex:i] setEnabled:NO];
        }
        [_miNextTrack setTitle:@"Next Track"];
        [_miPreviousTrack setTitle:@"Previous Track"];
        [_miActivate setTitle:@"Activate Player"];
        [_miActivate setHidden:NO];
        [self voidTrackInfoForMenuLite];
        [_miTrackView setView:nil];
        [_miPlaying setHidden:NO];
        [_miTrackView setHidden:YES];
        [_miStars setView:nil];
        [_miStars setHidden:YES];
        [_miSeparator setHidden:YES];
    }
}

- (void)menuDidClose:(NSMenu *)menu {
    _menuIsOpen = NO;
    NSString *whichIsRunning = [self getPlayingApplication];
    if (_isStopped) {
        _favoriteForRestart = nil;
        _currentFav = nil;
        [self installPick];
        if ([whichIsRunning isNotEqualTo:@"None"]) {
            [self collectTrackInfoForMenu];
            [self menuWillOpen:_statusMenu];
        }
    }
    if ([whichIsRunning isEqualToString:@"None"]) {
        if ([_coverArtWindow artWindowOnScreen]) {
            [_coverArtWindow fadeOut];
        }
    }
}

- (void)insertBreathingTime {
    return;
}

- (void)nullifyBlockTimer {
    if (_blockTimer) {
        [_blockTimer invalidate];
        _blockTimer = nil;
    }
    _collectingIsBlocked = NO;
}

- (void)collectTrackInfoForMenu {
    if (_collectingIsBlocked) return;
    if (_trackTitle == nil) {
        [self voidTrackInfoForMenu];
        return;
    }
    if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"Swinsian"] || [_trackInfoSource isEqualToString:@"Spotify"]) {
        if (_trackArt == nil) {
            _trackArt = [NSImage imageNamed:@"appicon"];
        }
        [_tivArt setImage:_trackArt];
        [_tivTitle setStringValue:_trackTitle];
        [_tivArtist setStringValue:_trackArtist];
        [_tivAlbum setStringValue:_trackAlbum];
        NSString *newTrackRating = _trackRating;
        if (_trackLoved) newTrackRating = [NSString stringWithFormat:@"%@   ❤︎", newTrackRating];
        [_tivTimeRating setStringValue:[NSString stringWithFormat:@"%@   %@", _trackLength, newTrackRating]];
        [self performSelector:@selector(insertBreathingTime) withObject:nil afterDelay:0.02f];
    } else if ([_trackInfoSource isEqualToString:@"iTunesRadio"] || [_trackInfoSource isEqualToString:@"MusicRadio"] || [_trackInfoSource isEqualToString:@"None"]) {
        if (_trackTitle == nil) {
            _trackTitle = @"Radio";
        }
        [_rivStation setStringValue:_trackTitle];
        if (_trackArtist == nil) {
            _trackArtist = @"Internet broadcast or relay";
        }
        [_rivSong setStringValue:_trackArtist];
        if (_trackAlbum == nil) {
            _trackAlbum = @"--";
        }
        [_rivURL setStringValue:_trackAlbum];
    } else {
        if (!_plDisplayArt) {
            _plDisplayArt = [NSImage imageNamed:@"radio"];
        }
        if (_radioController) {
            if (_playingFromFav && _fetchesArtwork) {
                _trackArt = [[_coverArtWindow coverArt] image];
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = _plDisplayArt;
        }
        NSImage *dispArt = _trackArt;
        if (_isStarting && !_hasPlayedBefore) {
            _trackArt = nil;
            _isStarting = NO;
        }
        if ([_trackArt isEqualTo:[NSImage imageNamed:@"stream"]] || _trackArt == nil) {
            if (_playingFromFav) {
                dispArt = [_currentFav image];
            } else {
                dispArt = [NSImage imageNamed:@"radio"];
            }
        }
        [_sivArt setImage:dispArt];
        if (_trackTitle == nil) {
            _trackTitle = @"Plectrum Radio";
        }
        [_sivStation setStringValue:_trackTitle];
        if (_trackArtist == nil) {
            _trackArtist = @"Internet broadcast or relay";
        }
        [_sivSong setStringValue:_trackArtist];
        if (_trackAlbum == nil) {
            _trackAlbum = @"--";
        }
        [_sivURL setStringValue:_trackAlbum];
    }
    _collectingIsBlocked = YES;
    if (_blockTimer) {
        [_blockTimer invalidate];
        _blockTimer = nil;
    }
    _blockTimer = [NSTimer timerWithTimeInterval:0.25f target:self selector:@selector(nullifyBlockTimer) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:_blockTimer forMode:NSRunLoopCommonModes];
}

- (void)voidTrackInfoForMenu {
    [self voidTrackInfoForMenuLite];
    [_miTrackView setView:nil];
    [_miPlaying setHidden:NO];
    [_miTrackView setHidden:YES];
    [_miStars setView:nil];
    [_miStars setHidden:YES];
    [_miHeart setView:nil];
    [_miHeart setHidden:YES];
    [_miSlider setView:nil];
    [_miSlider setHidden:YES];
    [_miSeparator setHidden:YES];
    [_miUpperSeparator setHidden:YES];
}

- (void)voidTrackInfoForMenuLite {
    _trackLength = @"0:00";
    _trackTitle = @"Nothing playing...";
    _trackArtist = @"---";
    _trackAlbum = @"---";
    _trackRating = [_tuneRating objectAtIndex:0];
    _trackArt = [NSImage imageNamed:@"appicon"];
    [_sivSong setToolTip:nil];
    [_sivStation setToolTip:nil];
    [_rivSong setToolTip:nil];
    [_rivStation setToolTip:nil];
    [_tivTitle setToolTip:nil];
    [_tivArtist setToolTip:nil];
}

- (IBAction)starsClicked:(id)sender {
    BOOL halfFlag = NO;
    int i = [[sender identifier] intValue];
    float swinsianRating = (float)i / 2;
    int appleRating = i * 10;
    int starTab = (int)(appleRating / 20);
    if (i % 2 == 1) {
        halfFlag = YES;
        if (!_appleAppHalfStarsEnabled) {
            appleRating += 10;
        }
    }
    _trackRating = [_tuneRating objectAtIndex:i];
    BOOL playerStatus = NO;
    if (@available(macOS 10.15, *)) {
        if ([_Music isRunning]) {
            if ([_playingApplication isEqualToString:@"Music"]) {
                playerStatus = ([[self getStringFromMusicWithParameter:@"player state"] isEqualToString:@"kPSS"]);
            }
        }
        if ([_Swinsian isRunning]) {
            if ([_playingApplication isEqualToString:@"Swinsian"]) {
                playerStatus = (SwinsianPlayerStateStopped == [_Swinsian playerState]);
            }
        }
    } else {
        if ([_iTunes isRunning]) {
            if ([_playingApplication isEqualToString:@"iTunes"]) {
                playerStatus = (iTunesEPlSStopped == [_iTunes playerState]);
            }
        }
        if ([_Swinsian isRunning]) {
            if ([_playingApplication isEqualToString:@"Swinsian"]) {
                playerStatus = (SwinsianPlayerStateStopped == [_Swinsian playerState]);
            }
        }
    }
    if (playerStatus) {
        return;
    } else {
        if (starTab == 0) {
            for (NSButton *button in _starCollection) {
                [button setImage:[NSImage imageNamed:@"star-open"]];
                [[button image] setTemplate:YES];
            }
        } else {
            for (int l = 0; l < starTab; l++) {
                [[_starCollection objectAtIndex:l] setImage:[NSImage imageNamed:@"star-black"]];
                [[[_starCollection objectAtIndex:l] image] setTemplate:YES];
            }
        }
        if (starTab > 0) {
            for (int m = 0; m < (5 - starTab); m++) {
                [[_starCollection objectAtIndex:(m + starTab)] setImage:[NSImage imageNamed:@"star-open"]];
                [[[_starCollection objectAtIndex:(m + starTab)] image] setTemplate:YES];
            }
        }
        if (@available(macOS 10.15, *)) {
            if ([_Music isRunning]) {
                if ([_playingApplication isEqualToString:@"Music"]) {
                    [self executeAppleScriptCommandInMusic:[NSString stringWithFormat:@"set rating of current track to %d", appleRating]];
                    if (appleRating == 0) {
                        NSInteger newAppleRating = [self getIntegerFromMusicWithParameter:@"rating fo current track"];
                        if (newAppleRating != 0) [self executeAppleScriptCommandInMusic:@"set rating of current track to 1"];
                    }
                    if (halfFlag) {
                        if (_appleAppHalfStarsEnabled) {
                            [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                            [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                        } else {
                            [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-black"]];
                            [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                        }
                    }
                }
            }
            if ([_Swinsian isRunning]) {
                if ([_playingApplication isEqualToString:@"Swinsian"]) {
                    [[_Swinsian currentTrack] setRating:[NSNumber numberWithFloat:swinsianRating]];
                    if (halfFlag) {
                        [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                        [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                    }
                }
            }
        } else {
            if ([_iTunes isRunning]) {
                if ([_playingApplication isEqualToString:@"iTunes"]) {
                    [[_iTunes currentTrack] setRating:appleRating];
                    if (appleRating == 0) {
                        NSInteger newAppleRating = [[_iTunes currentTrack] rating];
                        if (newAppleRating != 0) [[_iTunes currentTrack] setRating:1];
                    }
                    if (halfFlag) {
                        if (_appleAppHalfStarsEnabled) {
                            [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                            [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                        } else {
                            [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-black"]];
                            [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                        }
                    }
                }
            }
            if ([_Swinsian isRunning]) {
                if ([_playingApplication isEqualToString:@"Swinsian"]) {
                    [[_Swinsian currentTrack] setRating:[NSNumber numberWithFloat:swinsianRating]];
                    if (halfFlag) {
                        [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                        [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                    }
                }
            }
        }
        [self collectTrackInfoForMenu];
    }
}

- (void)clickStarsWithoutReallyClicking:(NSInteger)numberOfStars {
    BOOL halfFlag = NO;
    BOOL appleFlag = NO;
    int i = (int)numberOfStars;
    int appleRating = i * 10;
    int starTab = (int)(appleRating / 20);
    if (i % 2 == 1) {
        halfFlag = YES;
        /*if (!_appleAppHalfStarsEnabled) {
            appleRating += 10;
        }*/
    }
    _trackRating = [_tuneRating objectAtIndex:i];
    BOOL playerStatus = NO;
    if (@available(macOS 10.15, *)) {
        if ([_Music isRunning]) {
            if ([_playingApplication isEqualToString:@"Music"]) {
                playerStatus = ([[self getStringFromMusicWithParameter:@"player state"] isEqualToString:@"kPSS"]);
                appleFlag = YES;
            }
        }
        if ([_Swinsian isRunning]) {
            if ([_playingApplication isEqualToString:@"Swinsian"]) {
                playerStatus = (SwinsianPlayerStateStopped == [_Swinsian playerState]);
            }
        }
    } else {
        if ([_iTunes isRunning]) {
            if ([_playingApplication isEqualToString:@"iTunes"]) {
                playerStatus = (iTunesEPlSStopped == [_iTunes playerState]);
                appleFlag = YES;
            }
        }
        if ([_Swinsian isRunning]) {
            if ([_playingApplication isEqualToString:@"Swinsian"]) {
                playerStatus = (SwinsianPlayerStateStopped == [_Swinsian playerState]);
            }
        }
    }
    if (playerStatus) {
        return;
    } else {
        if (starTab == 0) {
            for (NSButton *button in _starCollection) {
                [button setImage:[NSImage imageNamed:@"star-open"]];
                [[button image] setTemplate:YES];
            }
        } else {
            for (int l = 0; l < starTab; l++) {
                [[_starCollection objectAtIndex:l] setImage:[NSImage imageNamed:@"star-black"]];
                [[[_starCollection objectAtIndex:l] image] setTemplate:YES];
            }
        }
        if (starTab > 0) {
            for (int m = 0; m < (5 - starTab); m++) {
                [[_starCollection objectAtIndex:(m + starTab)] setImage:[NSImage imageNamed:@"star-open"]];
                [[[_starCollection objectAtIndex:(m + starTab)] image] setTemplate:YES];
            }
        }
        if (halfFlag) {
            if (appleFlag) {
                if (_appleAppHalfStarsEnabled) {
                    [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                    [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
                }
            } else {
                [[_starCollection objectAtIndex:starTab] setImage:[NSImage imageNamed:@"star-half"]];
                [[[_starCollection objectAtIndex:starTab] image] setTemplate:YES];
            }
        }
        [self collectTrackInfoForMenu];
    }
}

- (void)setStars:(NSInteger)numberOfStars {
    if (numberOfStars > 10) numberOfStars = 10;
    if (numberOfStars < 0) numberOfStars = 0;
    if (numberOfStars == 0) {
        [self clickStarsWithoutReallyClicking:0];
    } else {
        [self clickStarsWithoutReallyClicking:numberOfStars];
    }
}

- (IBAction)heartClicked:(id)sender {
    BOOL playerStatus = NO;
    if (@available(macOS 10.15, *)) {
        if ([_Music isRunning]) {
            if ([_playingApplication isEqualToString:@"Music"]) {
                playerStatus = ([[self getStringFromMusicWithParameter:@"player state"] isEqualToString:@"kPSS"]);
            }
        }
    } else {
        if ([_iTunes isRunning]) {
            if ([_playingApplication isEqualToString:@"iTunes"]) {
                playerStatus = (iTunesEPlSStopped == [_iTunes playerState]);
            }
        }
    }
    if (!playerStatus) {
        _trackLoved = !_trackLoved;
        if (@available(macOS 10.15, *)) {
            if ([_Music isRunning]) {
                if ([_playingApplication isEqualToString:@"Music"]) {
                    NSString *command;
                    if (!_trackLoved) {
                        command = @"set loved of current track to false";
                    } else {
                        command = @"set loved of current track to true";
                    }
                    [self executeAppleScriptCommandInMusic:command];
                    [self setLovedness:_trackLoved];
                    [self collectTrackInfoForMenu];
                    [self fakeMusicNotification];
                }
            }
        } else {
            if ([_iTunes isRunning]) {
                if ([_playingApplication isEqualToString:@"iTunes"]) {
                    [[_iTunes currentTrack] setLoved:_trackLoved];
                    [self setLovedness:_trackLoved];
                    [self collectTrackInfoForMenu];
                    [self fakeiTunesNotification];
                }
            }
        }
    }
}

- (void)setLovedness:(BOOL)loved {
    if (loved) {
        if (@available(macOS 10.15, *)) {
            [_heart setImage:[NSImage imageNamed:@"heart-full"]];
            [[_heart image] setTemplate:YES];
        } else {
            [_heart setImage:[NSImage imageNamed:@"heart-full"]];
            [_heart setAlternateImage:[NSImage imageNamed:@"heart-open"]];
            [[_heart image] setTemplate:YES];
            [[_heart alternateImage] setTemplate:YES];
        }
    } else {
        if (@available(macOS 10.15, *)) {
            [_heart setImage:[NSImage imageNamed:@"heart-open"]];
            [[_heart image] setTemplate:YES];
        } else {
            [_heart setImage:[NSImage imageNamed:@"heart-open"]];
            [_heart setAlternateImage:[NSImage imageNamed:@"heart-full"]];
            [[_heart image] setTemplate:YES];
            [[_heart alternateImage] setTemplate:YES];
        }
    }
}

- (void)fakeiTunesNotification {
    if ([_iTunes isRunning]) {
        if ([_playingApplication isEqualToString:@"iTunes"]) {
            NSString *playerState = @"Stopped";
            if ([_iTunes playerState] == iTunesEPlSPlaying) playerState = @"Playing";
            if ([_iTunes playerState] == iTunesEPlSPaused) playerState = @"Paused";
            if ([_iTunes playerState] == iTunesEPlSStopped) playerState = @"Stopped";
            NSDictionary *dict = [NSDictionary dictionaryWithObject:playerState forKey:@"Player State"];
            [self iTunesTrackInfoChanged:[NSNotification notificationWithName:@"dummyiTunesNotification" object:nil userInfo:dict]];
        }
    }
}

- (void)fakeMusicNotification {
    if ([_Music isRunning]) {
        if ([_playingApplication isEqualToString:@"Music"]) {
            NSString *playerState = @"Stopped";
            if ([_Music playerState] == MusicEPlSPlaying) playerState = @"Playing";
            if ([_Music playerState] == MusicEPlSPaused) playerState = @"Paused";
            if ([_Music playerState] == MusicEPlSStopped) playerState = @"Stopped";
            NSDictionary *dict = [NSDictionary dictionaryWithObject:playerState forKey:@"Player State"];
            [self musicTrackInfoChanged:[NSNotification notificationWithName:@"dummyMusicNotification" object:nil userInfo:dict]];
        }
    }
}


#pragma mark - Managing Radio Favorites

- (void)removeFavoritesFromMenu {
    int i;
    _favSubMenu = nil;
    NSUInteger menuCount = [[_statusItem menu] numberOfItems];
    for (i = 6; i < menuCount; i++) {
        if ([[[[_statusItem menu] itemAtIndex:i] title] isEqualToString:@""]) {
            menuCount = (i - 6);
            break;
        }
    }
    if (menuCount > 0) {
        for (i = (5 + (int)menuCount); i >= 6; i--) {
            [[_statusItem menu] removeItemAtIndex:i];
        }
    }
}

- (void)addFavoritesToMenu {
    int i;
    NSImage *img;
    NSMenuItem *menuItem;
    NSString *imgPath;
    NSBundle *bundle = [NSBundle mainBundle];
    if ([_menuList count] == 0) {
        [_miRadioFavorites setTitle:@"No Radio Favorites"];
    } else {
        [_miRadioFavorites setTitle:@"Radio Favorites"];
    }
    if ([_menuList count] > 4) {
        for (i = 0; i < 3; i++) {
            menuItem = [[NSMenuItem alloc] initWithTitle:[[_menuList objectAtIndex:i] name] action:@selector(favoriteSelected:) keyEquivalent:@""];
            [menuItem setRepresentedObject:[_menuList objectAtIndex:i]];
            img = [[[_menuList objectAtIndex:i] image] copy];
            if (!img) {
                imgPath = [bundle pathForResource:@"stream" ofType:@"jpg"];
                img = [[NSImage alloc] initWithContentsOfFile:imgPath];
            }
            [menuItem setImage:[PLUtilities iconScaledToMenuSize:img]];
            [[_statusItem menu] insertItem:[menuItem copy] atIndex:(6 + i)];
        }
        NSMenuItem *subMenuItem = [[NSMenuItem alloc] initWithTitle:@"More Favorites" action:nil keyEquivalent:@""];
        NSMenu *subMenu =[[NSMenu alloc] init];
        [[_statusItem menu] insertItem:subMenuItem atIndex:9];
        [subMenuItem setSubmenu:subMenu];
        for (i = 3; i < [_menuList count]; i++) {
            menuItem = [[NSMenuItem alloc] initWithTitle:[[_menuList objectAtIndex:i] name] action:@selector(favoriteSelected:) keyEquivalent:@""];
            img = [[[_menuList objectAtIndex:i] image] copy];
            if (!img) {
                imgPath = [bundle pathForResource:@"stream" ofType:@"jpg"];
                img = [[NSImage alloc] initWithContentsOfFile:imgPath];
            }
            [menuItem setImage:[PLUtilities iconScaledToMenuSize:img]];
            [menuItem setRepresentedObject:[_menuList objectAtIndex:i]];
            [subMenu insertItem:[menuItem copy] atIndex:(i - 3)];
        }
        _favSubMenu = subMenu;
    } else {
        for (i = 0; i < [_menuList count]; i++) {
            menuItem = [[NSMenuItem alloc] initWithTitle:[[_menuList objectAtIndex:i] name] action:@selector(favoriteSelected:) keyEquivalent:@""];
            img = [[[_menuList objectAtIndex:i] image] copy];
            if (!img) {
                imgPath = [bundle pathForResource:@"stream" ofType:@"jpg"];
                img = [[NSImage alloc] initWithContentsOfFile:imgPath];
            }
            [menuItem setImage:[PLUtilities iconScaledToMenuSize:img]];
            [menuItem setRepresentedObject:[_menuList objectAtIndex:i]];
            [[_statusItem menu] insertItem:[menuItem copy] atIndex:(6 + i)];
        }
        _favSubMenu = nil;
    }
}

- (IBAction)addURLAsFavoriteFromMenu:(id)sender {
    [self editFavorites:self];
    [_favTableView addFavorite:sender];
}

- (IBAction)editFavorites:(id)sender {
    [_miMakeFav setEnabled:NO];
    if (_controlWindowIsAlreadyOpen) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        return;
    }
    if (_askForConfirmation) {
        [[_favTableView removeButton] setTitle:@"Remove..."];
    } else {
        [[_favTableView removeButton] setTitle:@"Remove"];
    }
    [_favTableView setFavList:[[NSMutableArray alloc] initWithArray:_favList copyItems:YES]];
    [_favTableView setHasBeenEdited:NO];
    [_favTableView sortFavList];
    [_favTableView selectNothing];
    [[_favTableView tableView] reloadData];
    [_menuTableView setMenuList:[[NSMutableArray alloc] initWithArray:_menuList copyItems:YES]];
    [_menuTableView selectNothing];
    [[_menuTableView tableView] reloadData];
    [[_favTableView controlWindow] makeKeyAndOrderFront:nil];
    _controlWindowIsAlreadyOpen = YES;
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
}

- (IBAction)editFavoritesOK:(id)sender {
    _favList = [[_favTableView favList] mutableCopy];
    _menuList = [[_menuTableView menuList] mutableCopy];
    [self removeFavoritesFromMenu];
    [self addFavoritesToMenu];
    [self saveRadioFavorites];
    [[_favTableView controlWindow] orderOut:nil];
    _controlWindowIsAlreadyOpen = NO;
    if (_playingFromURL) {
        [_miMakeFav setEnabled:YES];
        return;
    }
    [self getIndexForFavoriteWithURL:[_radioController url].absoluteString];
    if (_favIndex != NSNotFound) {
        if ((_isPlaying || _isPaused) && _playingFromFav) {
            PLRadioFavorite *fav = [_menuList objectAtIndex:_favIndex];
            if ([[fav uuid] isNotEqualTo:[_currentFav uuid]]) {
                [self stop:self];
                _playingFromURL = NO;
                _playingFromFav = NO;
                _fetchesArtwork = NO;
                [self installPick];
                NSString *whichIsRunning = [self getPlayingApplication];
                if ([whichIsRunning isEqualToString:@"None"]) {
                    if ([_coverArtWindow artWindowOnScreen]) {
                        [_coverArtWindow fadeOut];
                    }
                }
            }
        }
        _lastFavoriteMenuSelection = [self getFavoriteMenuItemForIndex:_favIndex];
        if (_isPlaying) {
            _plDisplayArt = [[_menuList objectAtIndex:_favIndex] image];
            [self reloadCurrentFav];
            if (!_plDisplayArt) _plDisplayArt = [NSImage imageNamed:@"stream"];
            if (_playingFromFav && _fetchesArtwork) {
                [_coverArtWindow setImageOnly:_trackArt];
            } else {
                [_coverArtWindow setImageOnly:_plDisplayArt];
            }
            [_lastFavoriteMenuSelection setState:NSOnState];
        }
    } else {
        [self stop:self];
        _playingFromURL = NO;
        _playingFromFav = NO;
        _fetchesArtwork = NO;
        [self installPick];
        NSString *whichIsRunning = [self getPlayingApplication];
        if ([whichIsRunning isEqualToString:@"None"]) {
            if ([_coverArtWindow artWindowOnScreen]) {
                [_coverArtWindow fadeOut];
            }
        }
    }
    [_miMakeFav setEnabled:YES];
}

- (IBAction)editFavoritesCancelled:(id)sender {
    if ([_favTableView hasBeenEdited]) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Edits and additions will be lost"];
        [alert setInformativeText:@"You appear to have added or edited favorites, and those changes will be lost if you cancel now. Are you sure you want to cancel and revert your changes?"];
        [alert addButtonWithTitle:@"No"];
        [alert addButtonWithTitle:@"Yes"];
        [alert beginSheetModalForWindow:_controlWindow completionHandler:^(NSModalResponse returnCode) {
            if (returnCode == 1000) {
                return;
            } else {
                [[self->_favTableView controlWindow] orderOut:nil];
                self->_controlWindowIsAlreadyOpen = NO;
            }
        }];
    } else {
        [[_favTableView controlWindow] orderOut:nil];
        _controlWindowIsAlreadyOpen = NO;
    }
    [_miMakeFav setEnabled:YES];
}

- (IBAction)showTrackInfoWindow:(id)sender {
    if (_trackInfoWindowIsAlreadyOpen) {
        [_trackInfoWindow makeKeyAndOrderFront:nil];
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        return;
    }
    if ([_trackInfoArray count] == 0) {
        [_trackInfoClearButton setEnabled:NO];
        [_trackInfoExportButton setEnabled:NO];
    } else {
        [_trackInfoClearButton setEnabled:YES];
        [_trackInfoExportButton setEnabled:YES];
    }
    _trackInfoWindowIsAlreadyOpen = YES;
    [_trackInfoWindow makeKeyAndOrderFront:nil];
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
}

- (void)reloadCurrentFav {
    if (!_playingFromFav) return;
    NSString *uuid = [_currentFav uuid];
    for (PLRadioFavorite *fav in _favList) {
        if ([[fav uuid] isEqualToString:uuid]) {
            _currentFav = fav;
            _fetchesArtwork = [_currentFav fetchArt];
            if (_fetchesArtwork) {
                [self plectrumPreupdateTrackInfo:[self rebuildNotification]];
            } else {
                _plDisplayArt = [fav image];
                [[_coverArtWindow coverArt] setImage:_plDisplayArt];
            }
            break;
        }
    }
}

- (void)getIndexForFavoriteWithURL:(NSString *)urlRepresentation {
    if ([_menuList count] == 0 || !_menuList) {
        _noOfObjects = 0;
        _favIndex = NSNotFound;
    }
    NSInteger u = NSNotFound;
    for (int i = 0; i < [_menuList count]; i++) {
        if ([[[_menuList objectAtIndex:i] url] isEqualToString:urlRepresentation]) {
            u = i;
            break;
        }
    }
    _favIndex = u;
    _noOfObjects = [_menuList count];
}


#pragma mark - Listen To The Radio

- (IBAction)favoriteSelected:(id)sender {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    _hasPlayedBefore = NO;
    [_rivArt setImage:[NSImage imageNamed:@"radio"]];
    if ([sender state] == NSMixedState || [sender state] == NSOnState) return;
    [_miSearchWeb setEnabled:NO];
    [_miSearchBC setEnabled:NO];
    _currentFav = (PLRadioFavorite *)[sender representedObject];
    NSString *urlString = [_currentFav url];
    _alternativeTitle = [_currentFav name];
    NSURL *theURL = [NSURL URLWithString:urlString];
    if (theURL && [theURL scheme] && [theURL host]) {
        if (_radioController != nil) {
            [self installPick];
            [self stop:self];
            if (_lastFavoriteMenuSelection) {
                [_lastFavoriteMenuSelection setState:NSOffState];
            }
        }
        _radioController = [[FSAudioController alloc] initWithUrl:theURL];
        [_radioController setVolume:_plVolume];
        [self play:self];
        _playingFromFav = YES;
        if (![[_currentFav stationuuid] isEqualToString:@""]) {
            [_miVoteForFav setHidden:NO];
            [_miVoteForFav setEnabled:YES];
        }
        _fetchesArtwork = [_currentFav fetchArt];
        _playingFromURL = NO;
        [self getIndexForFavoriteWithURL:urlString];
        _plDisplayArt = [[_menuList objectAtIndex:_favIndex] image];
        if (!_plDisplayArt) {
            _plDisplayArt = [NSImage imageNamed:@"stream"];
        }
        _lastFavoriteMenuSelection = sender;
        [_lastFavoriteMenuSelection setState:NSMixedState];
        if (_timeOutTimer) {
            [_timeOutTimer invalidate];
            _timeOutTimer = nil;
        }
        _timeOutTimer = [NSTimer timerWithTimeInterval:TIMEOUT_RANGE target:self selector:@selector(favoriteOrURLTimedOut) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:_timeOutTimer forMode:NSRunLoopCommonModes];
    } else {
        if (_notification) {
            if ([[_notification identifier] hasPrefix:@"TRANSIENT-"]) {
                [_notificationCenter removeDeliveredNotification:_notification];
            }
         }
        _notification = [[NSUserNotification alloc] init];
        [_notification setTitle:@"Could not connect to station"];
        [_notification setSubtitle:@"Please check URL for favorite:"];
        [_notification setInformativeText:[sender title]];
        [_notification setSoundName:nil];
        NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
        [_notification setIdentifier:notificationUUID];
        [_notificationCenter deliverNotification:_notification];
    }
}

- (void)playFavoriteByIndex:(NSInteger)index {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    _hasPlayedBefore = NO;
    _isStarting = YES;
    _currentFav = [_menuList objectAtIndex:index];
    NSString *urlString = [_currentFav url];
    _alternativeTitle = [_currentFav name];
    _plDisplayArt = [_currentFav image];
    if (!_plDisplayArt) {
        _plDisplayArt = [NSImage imageNamed:@"stream"];
    }
    NSURL *theURL = [NSURL URLWithString:urlString];
    if (theURL != nil) {
        if (_radioController != nil) {
            [self installPick];
            [self stop:self];
        }
        if (_lastFavoriteMenuSelection) {
            [_lastFavoriteMenuSelection setState:NSOffState];
        }
        _radioController = [[FSAudioController alloc] initWithUrl:theURL];
        [_radioController setVolume:_plVolume];
        [self play:self];
        _playingFromFav = YES;
        _fetchesArtwork = [_currentFav fetchArt];
        _playingFromURL = NO;
         _favIndex = index;
        _lastFavoriteMenuSelection = [self getFavoriteMenuItemForIndex:index];
        [_lastFavoriteMenuSelection setState:NSMixedState];
        if (_timeOutTimer) {
            [_timeOutTimer invalidate];
            _timeOutTimer = nil;
        }
        _timeOutTimer = [NSTimer timerWithTimeInterval:TIMEOUT_RANGE target:self selector:@selector(favoriteOrURLTimedOut) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:_timeOutTimer forMode:NSRunLoopCommonModes];
    } else {
        if (_notification) {
            if ([[_notification identifier] hasPrefix:@"TRANSIENT-"]) {
                [_notificationCenter removeDeliveredNotification:_notification];
            }
         }
        _notification = [[NSUserNotification alloc] init];
        [_notification setTitle:@"Could not connect to station"];
        [_notification setSubtitle:@"Please check URL for favorite:"];
        [_notification setInformativeText:[[_menuList objectAtIndex:index] name]];
        [_notification setSoundName:nil];
        NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
        [_notification setIdentifier:notificationUUID];
        [_notificationCenter deliverNotification:_notification];
    }
}

- (NSMenuItem *)getFavoriteMenuItemForIndex:(NSInteger)index {
    if ([_menuList count] == 0 || !_menuList) return nil;
    if (index > ([_menuList count] - 1)) return nil;
    if (index > 2 && [_menuList count] > 4) {
        if (!_favSubMenu) return nil;
        return [_favSubMenu itemAtIndex:(index - 3)];
    } else {
        return [_statusMenu itemAtIndex:(index + 6)];
    }
    return nil;
}

- (IBAction)openURL:(id)sender {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    [_openURLWarning setStringValue:@""];
    [_urlTextField setStringValue:_playingURL];
    [_urlEntryWindow makeKeyAndOrderFront:nil];
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
}

- (IBAction)openURLOK:(id)sender {
    _plDisplayArt = [NSImage imageNamed:@"stream"];
    NSString *urlString = [_urlTextField stringValue];
    NSURL *theURL = [NSURL URLWithString:urlString];
    if (theURL && [theURL scheme] && [theURL host]) {
        [_openURLWarning setStringValue:@""];
        [_urlEntryWindow orderOut:nil];
        if (_radioController != nil) {
            [self installPick];
            [self stop:self];
        }
        _alternativeTitle = @"";
        _hasPlayedBefore = NO;
        _radioController = [[FSAudioController alloc] initWithUrl:theURL];
        [_radioController setVolume:_plVolume];
        if (_lastFavoriteMenuSelection) [_lastFavoriteMenuSelection setState:NSOffState];
        _lastFavoriteMenuSelection = nil;
        _favoriteForRestart = nil;
        [self play:self];
        _playingURL = [theURL absoluteString];
        _playingFromURL = YES;
        [_miMakeFav setEnabled:YES];
        _playingFromFav = NO;
        _fetchesArtwork = NO;
        if (_timeOutTimer) {
            [_timeOutTimer invalidate];
            _timeOutTimer = nil;
        }
        _timeOutTimer = [NSTimer timerWithTimeInterval:TIMEOUT_RANGE target:self selector:@selector(favoriteOrURLTimedOut) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:_timeOutTimer forMode:NSRunLoopCommonModes];
    } else {
        [_openURLWarning setStringValue:@"URL is not valid."];
        NSBeep();
    }
}

- (void)playFromURL:(NSURL *)url {
    if (_radioController != nil) {
        [self installPick];
        [self stop:self];
        [_miSearchWeb setEnabled:NO];
        [_miSearchBC setEnabled:NO];
    }
    _alternativeTitle = @"";
    _hasPlayedBefore = NO;
    _isStarting = YES;
    _radioController = [[FSAudioController alloc] initWithUrl:url];
    [_radioController setVolume:_plVolume];
    if (_lastFavoriteMenuSelection) [_lastFavoriteMenuSelection setState:NSOffState];
    _lastFavoriteMenuSelection = nil;
    _favoriteForRestart = nil;
    [self play:self];
    _playingURL = [url absoluteString];
    _playingFromURL = YES;
    [_miMakeFav setEnabled:YES];
    _playingFromFav = NO;
    _fetchesArtwork = NO;
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    _timeOutTimer = [NSTimer timerWithTimeInterval:TIMEOUT_RANGE target:self selector:@selector(favoriteOrURLTimedOut) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:_timeOutTimer forMode:NSRunLoopCommonModes];
}

- (IBAction)openURLCancelled:(id)sender {
    [_openURLWarning setStringValue:@""];
    [_urlEntryWindow orderOut:nil];
}

- (IBAction)play:(id)sender {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    if (_radioController) {
        if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"iTunesRadio"]) {
            if ([_iTunes isRunning]) {
                [_iTunes pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"MusicRadio"]) {
            if ([_Music isRunning]) {
                [_Music pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
            if ([_Swinsian isRunning]) {
                [_Swinsian pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
            if ([_Spotify isRunning]) {
                [_Spotify pause];
            }
        }
        /*} else if ([_Spotify isRunning]) {
            [_Spotify pause];
        }*/
        if ([[[_radioController url] absoluteString] hasSuffix:@"m3u8"]) {
            [self disableRecording];
        } else {
            [self enableRecording];
        }
        [_radioController play];
    } else {
        if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"iTunesRadio"]) {
            if ([_iTunes isRunning]) {
                [_iTunes pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"MusicRadio"]) {
            if ([_Music isRunning]) {
                [_Music pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
            if ([_Swinsian isRunning]) {
                [_Swinsian pause];
            }
        } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
            if ([_Spotify isRunning]) {
                [_Spotify pause];
            }
        }
        /*} else if ([_Spotify isRunning]) {
            [_Spotify pause];
        }*/
        _radioController = [[FSAudioController alloc] init];
        [_radioController setVolume:_plVolume];
        if (_favoriteForRestart) {
            [self getIndexForFavoriteWithURL:[_currentFav url]];
             if (_favIndex != NSNotFound) {
                [self playFavoriteByIndex:_favIndex];
                if ([[_currentFav url] hasSuffix:@"m3u8"]) {
                    [self disableRecording];
                } else {
                    [self enableRecording];
                }
            }
        } else {
            [_radioController setUrl:[NSURL URLWithString:_lastURL]];
            if ([_lastURL hasSuffix:@"m3u8"]) {
                [self disableRecording];
            } else {
                [self enableRecording];
            }
            [_radioController play];
            _playingFromFav = NO;
            _playingFromURL = YES;
            [_miMakeFav setEnabled:YES];
        }
    }
    NSImage *pauseImg = [NSImage imageNamed:@"s_pause"];
    [pauseImg setTemplate:YES];
    [_playPause setImage:pauseImg];
    _plLastStreamTitle = @"Opening stream...";
    _plLastStationName = @"Please stand by, contacting site...";
    _plLastDisplayedURL = @"Plectrum Radio";
    _trackInfoSource = @"Plectrum";
    _isStarting = YES;
    [_coverArtWindow receiveStatus:@"Playing"];
    _lastTimeStamp = [NSDate date];
    _isPlaying = YES;
    _isPaused = NO;
    _isStopped = NO;
    _ignoreEmptyBuffer = YES;
    if (_bufferExpirationTimer) {
        [_bufferExpirationTimer invalidate];
        _bufferExpirationTimer = nil;
    }
    _bufferExpirationTimer = [NSTimer scheduledTimerWithTimeInterval:TIMEOUT_RANGE target:self selector:@selector(unignoreEmptyBuffer) userInfo:nil repeats:NO];

}

- (void)unignoreEmptyBuffer {
    _ignoreEmptyBuffer = NO;
}

- (void)pause {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    if (!_muteInsteadOfPause) {
        _wasPlayingFromFav = _playingFromFav;
        _wasPlayingFromURL = _playingFromURL;
        [_radioController stop];
    }
    _isPlaying = YES;
    _isPaused = !_isPaused;
    if (_isPaused) {
        [_muteInsteadOfPauseCheckBox setEnabled:NO];
        [_muteInsteadOfPauseCheckBox setToolTip:@"This option cannot be changed when the player is paused"];
    } else {
        [_muteInsteadOfPauseCheckBox setEnabled:YES];
        [_muteInsteadOfPauseCheckBox setToolTip:nil];
    }
    if (!_isPaused && !_muteInsteadOfPause) {
        _playingFromFav = _wasPlayingFromFav;
        _playingFromURL = _wasPlayingFromURL;
        if (_wasPlayingFromFav) {
            _radioController = [[FSAudioController alloc] init];
            [_radioController setVolume:_plVolume];
            [self getIndexForFavoriteWithURL:[_currentFav url]];
             if (_favIndex != NSNotFound) {
                [self playFavoriteByIndex:_favIndex];
                if ([[_currentFav url] hasSuffix:@"m3u8"]) {
                    [self disableRecording];
                } else {
                    [self enableRecording];
                }
            }
            [_radioController play];
        } else {
            [self playFromURL:[NSURL URLWithString:[_urlTextField stringValue]]];
            if ([[_urlTextField stringValue] hasSuffix:@"m3u8"]) {
                [self disableRecording];
            } else {
                [self enableRecording];
            }

        }
    }
}

- (IBAction)playpause:(id)sender {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    if (!_radioController) {
        [self play:self];
        return;
    }
    if (_isPlaying && !_isPaused) {
        if (_muteInsteadOfPause) {
            _isMuted = YES;
            _oldPlVolume = _plVolume;
            _plVolume = 0.0f;
            [_radioController setVolume:_plVolume];
            [_slider setFloatValue:0.0f];
        }
        [self pause];
        NSImage *playImg = [NSImage imageNamed:@"s_play"];
        [playImg setTemplate:YES];
        [_playPause setImage:playImg];
        [_coverArtWindow receiveStatus:@"Paused"];
        _currentPlayerStatus = @"Paused";
        if (_isRecording) {
            [self stopRecording];
        }
    } else if (_isPlaying && _isPaused) {
        if (_muteInsteadOfPause) {
            _isMuted = NO;
            _plVolume = _oldPlVolume;
            [_radioController setVolume:_plVolume];
            [_slider setFloatValue:_plVolume * 100];
        }
        [self pause];
        NSImage *pauseImg = [NSImage imageNamed:@"s_pause"];
        [pauseImg setTemplate:YES];
        [_playPause setImage:pauseImg];
        [_coverArtWindow receiveStatus:@"Playing"];
        _lastTimeStamp = [NSDate date];
        _currentPlayerStatus = @"Playing";
        [self showNowPlayingInfo];
    } else {
        return;
    }
}

- (IBAction)stop:(id)sender {
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    [self checkToStopRecording];
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    _lastURL = [_radioController url].absoluteString;
    [_radioController stop];
    _radioController = nil;
    if (_isMuted) {
        _isMuted = NO;
        _plVolume = _oldPlVolume;
        [_slider setFloatValue:_plVolume * 100];
    }
    if (_lastFavoriteMenuSelection) {
        [_lastFavoriteMenuSelection setState:NSOffState];
        _favoriteForRestart = _lastFavoriteMenuSelection;
        _lastFavoriteMenuSelection = nil;
    }
    NSImage *playImg = [NSImage imageNamed:@"s_play"];
    [playImg setTemplate:YES];
    [_playPause setImage:playImg];
    _isPlaying = NO;
    _isPaused = NO;
    _isStopped = YES;
    _playingFromURL = NO;
    [_miVoteForFav setHidden:YES];
    [_muteInsteadOfPauseCheckBox setEnabled:YES];
    [_miSearchWeb setEnabled:YES];
    [_miSearchBC setEnabled:YES];
    _currentPlayerStatus = @"Stopped";
    _previousImageURL = @"";
    _currentImageURL = @"";
    _defermentRetryCount = 0;
    _trackTitle = @"Loading stream...";
}

- (void)favoriteOrURLTimedOut {
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    [self stop:self];
    if ([_coverArtWindow artWindowOnScreen]) {
        [_coverArtWindow fadeOut];
    }
    _playingFromURL = NO;
    _playingFromFav = NO;
    _fetchesArtwork = NO;
    [_miVoteForFav setHidden:YES];
    [_miSearchWeb setEnabled:NO];
    [_miSearchBC setEnabled:NO];
    _hasPlayedBefore = NO;
    if (_notification) {
        if ([[_notification identifier] hasPrefix:@"TRANSIENT-"]) {
            [_notificationCenter removeDeliveredNotification:_notification];
        }
     }
    _notification = [[NSUserNotification alloc] init];
    [_notification setTitle:@"Stream timed out"];
    [_notification setSubtitle:@"It took too long to make a connection."];
    [_notification setInformativeText:@"Check your network, or try again later."];
    [_notification setSoundName:nil];
    NSString *notificationUUID = [NSString stringWithFormat:@"TRANSIENT-%@", [PLUtilities createUUID]];
    [_notification setIdentifier:notificationUUID];
    [_notificationCenter deliverNotification:_notification];
    if (_menuIsOpen) [self menuWillOpen:_statusMenu];
}

- (void)plectrumPlayerStatusChanged:(NSNotification *) notification {
    if ([_notifyingObject isEqual:[_favTableView tryoutController]]) {
        return;
    }
    NSDictionary *userInfo = [notification userInfo];
    int playerState = [[userInfo valueForKey:FSAudioStreamNotificationKey_State] intValue];
    switch (playerState) {
        case kFsAudioStreamRetrievingURL:
            break;
        case kFsAudioStreamStopped:
            break;
        case kFsAudioStreamBuffering:
            break;
        case kFsAudioStreamSeeking:
            break;
        case kFsAudioStreamPlaying:
            _retryCount = 0;
            break;
        case kFsAudioStreamFailed:
            /* let error observer handle this (see below) */
            break;
    }
}

- (void)createSimpleTrackChangeNotification {
    if (_firstNotificationCatchTimer) {
        [_firstNotificationCatchTimer invalidate];
        _firstNotificationCatchTimer = nil;
        [_miSearchWeb setHidden:YES];
        [_miSearchWeb setEnabled:NO];
        [_miSearchBC setHidden:YES];
        [_miSearchBC setEnabled:NO];
    }
    NSDictionary *metaDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @{@"alttitle":@"Internet radio or relay", @"SimpleNotification":@YES},
                              FSAudioStreamNotificationKey_MetaData, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:FSAudioStreamMetaDataNotification object:NSStringFromClass([self class]) userInfo:metaDict];
}

- (void)plectrumErrorOccured:(NSNotification *)notification {
    if ([_notifyingObject isEqual:[_favTableView tryoutController]]) {
        [_favTableView erroredOut];
        return;
    }
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:_lastTimeStamp];
    NSDictionary *userInfo = [notification userInfo];
    int errorCode = [[userInfo valueForKey:FSAudioStreamNotificationKey_Error] intValue];
    NSString *messageText, *subText, *infoText;
    switch (errorCode) {
        case kFsAudioStreamErrorOpen:
            messageText = @"Cannot open the audio stream";
            subText = @"The audio stream cannot be accessed.";
            infoText = @"Please check the URL and try again.";
            break;
        case kFsAudioStreamErrorStreamParse:
            messageText = @"Cannot read the audio stream";
            subText = @"Problem parsing the audio data.";
            infoText = @"Please try again later.";
            break;
        case kFsAudioStreamErrorNetwork:
            messageText = @"Network failure";
            subText = @"There was a problem accessing the server.";
            infoText = @"Please try again later.";
            break;
        case kFsAudioStreamErrorUnsupportedFormat:
            messageText = @"Unsupported format";
            subText = @"The stream is in an unsupported format.";
            infoText = @"Please enter MP3, AAC or M3U stream URL.";
            break;
        case kFsAudioStreamErrorStreamBouncing:
            if (interval < 10) {
                messageText = @"Stream ran out of data";
                subText = @"Stream stalled within 10 seconds.";
                infoText = @"Giving up for now - try again later.";
            } else {
                [_sivSong setStringValue:@"Station stalled"];
                [_sivSong setToolTip:nil];
                [_sivStation setStringValue:@"Reloading, please wait..."];
                [_sivStation setToolTip:nil];
                [self stop:self];
                [self play:self];
                return;
            }
            break;
        default:
            messageText = @"Unknown error";
            subText = @"No idea what went wrong here.";
            infoText = @"Please try again and hope for the best.";
            break;
    }
    _retryCount++;
    if (_retryCount < 3) return;
    _retryCount = 0;
    [self stop:self];
    _playingFromURL = NO;
    _playingFromFav = NO;
    _fetchesArtwork = NO;
    _playingApplication = [self getPlayingApplication];
    if ([_playingApplication isEqualToString:@"None"]) {
        if ([_coverArtWindow artWindowOnScreen]) {
            [_coverArtWindow fadeOut];
        }
    }
    [self installPick];
    NSUserNotification *userNotification = [[NSUserNotification alloc] init];
    [userNotification setTitle:messageText];
    [userNotification setSubtitle:subText];
    [userNotification setInformativeText:infoText];
    [userNotification setSoundName:nil];
    [_notificationCenter deliverNotification:userNotification];
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
}

- (IBAction)clearTrackInfo:(id)sender {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Clear collected track info"];
    [alert setInformativeText:@"You are about to clear the track info you have collected up to now. Are you sure this is what you want to do?"];
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert setAlertStyle:NSAlertStyleCritical];
    [alert beginSheetModalForWindow:_trackInfoWindow completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSAlertFirstButtonReturn) {
            [self->_trackInfoArray removeAllObjects];
            [self->_trackInfoTableView reloadData];
            [self->_trackInfoClearButton setEnabled:NO];
            [self->_trackInfoExportButton setEnabled:NO];
        }
    }];
}

- (IBAction)exportTrackInfo:(id)sender {
    __block NSString *thePath;
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:@"Save track info"];
    [savePanel setMessage:@"Enter name and location for saving:"];
    NSDate *theDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
    NSString *dateString = [formatter stringFromDate:theDate];
    [savePanel setNameFieldStringValue:[NSString stringWithFormat:@"Track Info %@.txt", dateString]];
    [savePanel setAccessoryView:nil];
    [savePanel setDelegate:nil];
    if (_firstOpen) {
        [savePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    [savePanel beginSheetModalForWindow:_trackInfoWindow completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseCancel) return;
        if ([savePanel URL] != nil) {
            self->_firstOpen = NO;
            thePath = [[savePanel URL] path];
        } else {
            return;
        }
        NSError *error;
        NSString *trackInfoContents = [self->_trackInfoArray componentsJoinedByString:@"\n"];
        BOOL success = [trackInfoContents writeToFile:thePath atomically:NO encoding:NSUTF8StringEncoding error:&error];
        if (!success) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            [PLUtilities showAlertWithMessageText:@"Could not export track info" informativeText:[NSString stringWithFormat:@"A problem was encountered when exporting the track info. This may be a problem with permissions; try saving the file elsewhere, or try copying and pasting it to another text file. Please contact the developer if the problem persists. We are sorry for the inconvenience.\n\nError code: %ld\n\nError message: %@", (long)[error code], [error localizedDescription]]];
        }
    }];
}


#pragma mark - Fetching Radio Artwork

- (void)updateRadioArtworkAsynchronouslyAndContinueWithSelector:(nullable SEL)selector {
    if (![_currentFav deferNotification]) {
        if (_oneShot) {
            [_oneShot invalidate];
            _oneShot = nil;
        }
        _oneShot = [NSTimer timerWithTimeInterval:5.0f target:self selector:selector userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:_oneShot forMode:NSRunLoopCommonModes];
    }
    [self fetchRadioArtworkAsynchronouslyWithCompletionHandler:^(NSImage *image) {
        if ([self->_currentFav deferNotification]) {
            if ([self->_previousImageURL isEqualToString:self->_currentImageURL]) {
                self->_passedSelector = selector;
                self->_defermentRetryCount++;
                if (self->_defermentRetryCount < MAX_ARTWORK_DEFERMENT_RETRY_COUNT) {
                    dispatch_async(dispatch_get_main_queue(),^(void) {
                        if (self->_twoShot) {
                            [self->_twoShot invalidate];
                            self->_twoShot = nil;
                        }
                        self->_twoShot = [NSTimer timerWithTimeInterval:3.0f target:self selector:@selector(requeueArtworkUpdate) userInfo:nil repeats:NO];
                        [[NSRunLoop currentRunLoop] addTimer:self->_twoShot forMode:NSRunLoopCommonModes];
                    });
                } else {
                    self->_defermentRetryCount = 0;
                    if (image) {
                        self->_trackArt = image;
                        dispatch_async(dispatch_get_main_queue(),^(void) {
                            [self continuePlectrumUpdateTrackInfo];
                        });
                    }
                }
            } else {
                self->_previousImageURL = self->_currentImageURL;
                if (image) {
                    self->_trackArt = image;
                    dispatch_async(dispatch_get_main_queue(),^(void) {
                        [self continuePlectrumUpdateTrackInfo];
                    });
                }
            }
        } else {
            if (image) {
                self->_trackArt = image;
                dispatch_async(dispatch_get_main_queue(),^(void) {
                    [self continuePlectrumUpdateTrackInfo];
                });
            }
        }
    }];
}

- (void)requeueArtworkUpdate {
    [self updateRadioArtworkAsynchronouslyAndContinueWithSelector:_passedSelector];
}

- (void)fetchRadioArtworkAsynchronouslyWithCompletionHandler:(void(^)(NSImage *image))completion {
    _currentImageURL = [PLUtilities createUUID];
    if ([_playingApplication isNotEqualTo:@"Plectrum"]) {
        completion([self getAlternativeImage]);
        return;
    }
    if (!_playingFromFav || !_fetchesArtwork) completion([_currentFav image]);
    __block NSURLSession *session0, *session1, *session2;
    __block NSURLRequest *request0, *request1, *request2;
    __block NSURLSessionDataTask *task0, *task1, *task2;
    if ([_currentFav textTransform]) {
        NSString *transformedString = [self transformString:_trackTitle withTransformation:[_currentFav textTransformType]];
        NSString *urlString = [NSString stringWithFormat:@"%@%@.%@", [_currentFav artUrl], transformedString, [_currentFav prefix]];
        NSURL *artURL = [NSURL URLWithString:urlString];
        session0 = [NSURLSession sharedSession];
        request0 = [[NSURLRequest alloc] initWithURL:artURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
        task0 = [session0 dataTaskWithRequest:request0 completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
            if (!networkError) {
                NSImage *image = [[NSImage alloc] initWithData:data];
                if (image) {
                    completion(image);
                    return;
                } else {
                    completion([self getAlternativeImage]);
                    return;
                }
            } else {
                completion([self getAlternativeImage]);
                return;
            }
        }];
        [task0 resume];
    } else if (![_currentFav fetchType]) {
        NSURL *url = [NSURL URLWithString:[_currentFav artUrl]];
        if (!url) {
            NSLog(@"PLECTRUM: Empty URL returned on favorite.");
            NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
            completion([self getAlternativeImage]);
            return;
        }
        NSString *keyPath = [_currentFav secondaryString];
        session1 = [NSURLSession sharedSession];
        request1 = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
        task1 = [session1 dataTaskWithRequest:request1 completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
            if (!networkError) {
                if (!data) {
                    dispatch_async(dispatch_get_main_queue(),^(void) {
                        NSLog(@"PLECTRUM: Empty data retrieved from status URL.");
                        NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                    });
                    completion([self getAlternativeImage]);
                    return;
                }
                NSDictionary *dict;
                NSError *theError = nil;
                @try {
                    dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&theError];
                }
                @catch (NSException *e) {
                    dispatch_async(dispatch_get_main_queue(),^(void) {
                        NSLog(@"PLECTRUM: Empty data uncaught by checkpoint.");
                        NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                    });
                    completion([self getAlternativeImage]);
                    return;
                }
                if (theError == nil) {
                    if (!data) {
                        dispatch_async(dispatch_get_main_queue(),^(void) {
                            NSLog(@"PLECTRUM: No data from JSON structure.");
                            NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                        });
                        completion([self getAlternativeImage]);
                        return;
                    }
                    NSURL *artURL;
                    id retrievedKeyValue = [dict valueForKeyPath:keyPath];
                    if (retrievedKeyValue == nil || [[retrievedKeyValue class] isEqual:[NSNull class]]) {
                        dispatch_async(dispatch_get_main_queue(),^(void) {
                            NSLog(@"PLECTRUM: Key empty or key retrieval failed.");
                            NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                        });
                        self->_currentImageURL = [PLUtilities createUUID];
                        completion([self getAlternativeImage]);
                        return;
                    }
                    self->_currentImageURL = retrievedKeyValue;
                    if ([self->_currentFav deferNotification]) {
                        if ([self->_currentImageURL isEqualToString:self->_previousImageURL]) {
                            completion([self getOldArt]);
                            return;
                        }
                    }
                    artURL = [NSURL URLWithString:retrievedKeyValue];
                    session2 = [NSURLSession sharedSession];
                    request2 = [[NSURLRequest alloc] initWithURL:artURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
                    task2 = [session2 dataTaskWithRequest:request2 completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
                        if (!networkError) {
                            NSImage *image = [[NSImage alloc] initWithData:data];
                            if (image) {
                                completion(image);
                                return;
                            } else {
                                completion([self getAlternativeImage]);
                                return;
                            }
                        } else {
                            completion([self getAlternativeImage]);
                            return;
                        }
                    }];
                    [task2 resume];
                } else {
                    completion([self getAlternativeImage]);
                    return;
                }
            } else {
                completion([self getAlternativeImage]);
                return;
            }
        }];
        [task1 resume];
    } else if ([_currentFav fetchType]) {
        NSURL *theURL = [NSURL URLWithString:[_currentFav artUrl]];
        session1 = [NSURLSession sharedSession];
        request1 = [[NSURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
        task1 = [session1 dataTaskWithRequest:request1 completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
            if (!networkError) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
                    uint32_t encoding[10] = {NSUTF8StringEncoding, NSASCIIStringEncoding, NSShiftJISStringEncoding, kCFStringEncodingDOSGreek, NSISOLatin1StringEncoding, NSISOLatin2StringEncoding, kTextEncodingISO_2022_JP, kCFStringEncodingUTF7, NSUTF16StringEncoding,  NSUTF32StringEncoding};
                    __block NSString *string;
                    int i = 0;
                    do {
                        string = [[NSString alloc] initWithData:data encoding:encoding[i]];
                        i++;
                        if (i == 10) {
                            string = @"";
                            break;
                        }
                    } while (string == nil);
                    NSString *grepString = [self->_currentFav secondaryString];
                    NSArray <NSString *> *array = [grepString componentsSeparatedByString:@"*"];
                    NSString *firstBit = [[array firstObject] stringByReplacingOccurrencesOfString:@"?" withString:@".*?"];
                    firstBit = [firstBit stringByReplacingOccurrencesOfString:@"\\.*?" withString:@".*?"];
                    NSString *secondBit = [[array lastObject] stringByReplacingOccurrencesOfString:@"?" withString:@".*?"];
                    secondBit = [secondBit stringByReplacingOccurrencesOfString:@"\\.*?" withString:@".*?"];
                    grepString = [NSString stringWithFormat:@"%@(.*?)%@", firstBit, secondBit];
                    NSArray *matches;
                    NSRegularExpression *regex;
                    NSError *regError;
                    @try {
                        regex = [[NSRegularExpression alloc] initWithPattern:grepString options:NSRegularExpressionCaseInsensitive error:&regError];
                        if (!regError) {
                            matches = [regex matchesInString:string options:0 range:NSMakeRange(0, [string length])];
                        } else {
                            completion([self getAlternativeImage]);
                            return;
                        }
                    }
                    @catch (NSException *e) {
                        NSLog(@"PLECTRUM: Exception while performing regex.");
                        if (e) {
                            NSLog(@"PLECTRUM: Reason:         %@", [e reason]);
                        } else {
                            NSLog(@"PLECTRUM: Reason:         Unknown");
                        }
                        NSLog(@"PLECTRUM: Input string:   %@", string);
                        NSLog(@"PLECTRUM: Search pattern: %@", grepString);
                        NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork");
                        completion([self getAlternativeImage]);
                        return;
                    }
                    if ([matches count] == 0 || matches == nil) {
                        completion([self getAlternativeImage]);
                        return;
                    }
                    NSTextCheckingResult *firstMatch;
                    NSRange matchRange;
                    NSString *matchString;
                    @try {
                        firstMatch = [matches objectAtIndex:0];
                        if ([firstMatch numberOfRanges] < 2) completion(self->_plDisplayArt);
                        matchRange = [firstMatch rangeAtIndex:1];
                        matchString = [string substringWithRange:matchRange];
                    }
                    @catch (NSException *e) {
                        NSLog(@"PLECTRUM: Failure finding RegEx Match.");
                        NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                        completion([self getAlternativeImage]);
                        return;
                    }
                    if (matchString == nil || [matchString isEqualToString:@""]) completion([self->_currentFav image]);
                    NSString *artURLString = [NSString stringWithFormat:@"%@%@", [self->_currentFav prefix], matchString];
                    NSURL *artURL = [NSURL URLWithString:artURLString];
                    if (!artURL) {
                        NSLog(@"PLECTRUM: Failure retrieving artwork URL.");
                        NSLog(@"PLECTRUM: Abandoning fetch - reverting to default artwork.");
                        completion([self getAlternativeImage]);
                        return;
                    }
                    self->_currentImageURL = artURLString;
                    if ([self->_currentFav deferNotification]) {
                        if ([self->_currentImageURL isEqualToString:self->_previousImageURL]) {
                            completion([self getOldArt]);
                            return;
                        }
                    }
                    session2 = [NSURLSession sharedSession];
                    request2 = [[NSURLRequest alloc] initWithURL:artURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
                    task2 = [session2 dataTaskWithRequest:request2 completionHandler:^(NSData *data, NSURLResponse *response, NSError *networkError) {
                        if (!networkError) {
                            NSImage *image = [[NSImage alloc] initWithData:data];
                            if (image) {
                                completion(image);
                                return;
                            } else {
                                completion([self getAlternativeImage]);
                                return;
                            }
                        } else {
                            completion([self getAlternativeImage]);
                            return;
                        }
                    }];
                    [task2 resume];
                });
            } else {
                completion([self getAlternativeImage]);
                return;
            }
        }];
        [task1 resume];
    } else {
        completion([self getAlternativeImage]);
    }
}

- (NSString *)transformString:(NSString *)string withTransformation:(NSInteger)transformation {
    NSString *transformedString;
    NSCharacterSet *charset = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz0123456789-_"] invertedSet];
    if (transformation == 0) {
        transformedString = [string lowercaseString];
        transformedString = [transformedString stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
        transformedString = [transformedString stringByReplacingOccurrencesOfString:@" - " withString:@"-"];
        transformedString = [transformedString stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        transformedString = [[transformedString componentsSeparatedByCharactersInSet:charset] componentsJoinedByString:@""];
        if ([transformedString hasPrefix:@"jazz_de_ville-"]) {
            transformedString = @"jazz_de_ville-jazz_de_ville"; // this will fail, invoking alt image
        }
        //NSLog(@"Transformed string:\n%@", transformedString);
        return transformedString;
    } else { // only 'Jazz de Ville' available for now at/since 3.5
        return string;
    }
}

- (NSImage *)getAlternativeImage {
    if ([_currentFav image] != nil) {
        return [_currentFav image];
    } else {
        return [NSImage imageNamed:@"stream"];
    }
}

- (NSImage *)getOldArt {
    return _trackArt;
}

- (void)reupdateRadioArtwork {
    [self fetchRadioArtworkAsynchronouslyWithCompletionHandler:^(NSImage *image) {
        if (image) {
            self->_plDisplayArt = image;
        } else {
            if ([self->_currentFav image] != nil) {
                self->_plDisplayArt = [self->_currentFav image];
            } else {
                self->_plDisplayArt = [NSImage imageNamed:@"stream"];
            }
        }
        dispatch_async(dispatch_get_main_queue(),^(void) {
            [self->_coverArtWindow setImageOnly:self->_plDisplayArt];
            if ([self->_PLNotificationCenter notificationIsVisible]) {
                [self->_rivArt setImage:self->_plDisplayArt];
            }
        });
    }];
}

- (void)refetchRadioArtwork {
    [self fetchRadioArtworkAsynchronouslyWithCompletionHandler:^(NSImage *image) {
        if (image) {
            self->_trackArt = image;
        } else {
            if ([self->_currentFav image] != nil) {
                self->_trackArt = [self->_currentFav image];
            } else {
                self->_trackArt = [NSImage imageNamed:@"stream"];
            }
        }
    }];
}

- (IBAction)showFetchArtworkHelpWindow:(id)sender {
    [NSApp activateIgnoringOtherApps:YES];
    [_helpPopUpButton selectItemAtIndex:1];
    NSBundle * myMainBundle = [NSBundle mainBundle];
    [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
    NSString *helpFilePath = [myMainBundle pathForResource:@"fetcharthelp" ofType:@"rtf"];
    [_helpTextView readRTFDFromFile:helpFilePath];
    [[_helpScrollView documentView] scrollPoint:NSMakePoint(0.0f, 0.0f)];
    [_helpTextView setNeedsDisplay:YES];
    [_helpPanel makeKeyAndOrderFront:nil];
}

- (NSNotification *)rebuildNotification {
    id object;
    if (_radioController) {
        object = _radioController;
    } else {
        object = nil;
    }
    NSMutableDictionary *metadict = [[NSMutableDictionary alloc] init];
    [metadict setObject:_trackTitle forKey:FSAudioStreamNotificationKey_MetaData];
    NSNotification *notification = [NSNotification notificationWithName:FSAudioStreamMetaDataNotification object:object userInfo:metadict];
    return notification;
}


#pragma mark - Recording Audio

- (IBAction)recordRadio:(id)sender {
    if (_isPaused) return;
    if ([_favTableView isTryingOut]) return;
    if ([_previewBox isPlaying]) return;
    if ([_trackArtist isEqualToString:@"Please stand by, contacting site..."]) return;
    if (_isRecording || _isGuardingAgainstRecordingModeChange || !_radioController) return;
    _isGuardingAgainstRecordingModeChange = YES;
    _isRecording = YES;
    NSDate *theDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
    _dateString = [formatter stringFromDate:theDate];
    NSString *theArtist = _trackArtist;
    [formatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
    NSString *dateString = [formatter stringFromDate:theDate];
    NSString *fileName = [NSString stringWithFormat:@"%@ %@", _trackArtist, dateString];
    NSString *filePath = [NSString stringWithFormat:@"%@.%@", fileName, [[_radioController activeStream] suggestedFileExtension]];
    filePath = [NSString stringWithFormat:@"%@/%@", [_recordingTableView getUserFolderPath], filePath];
    NSString *originURL = _trackAlbum;
    NSString *fileType = [[[_radioController activeStream] suggestedFileExtension] uppercaseString];
    _outputPath = filePath;
    PLRadioRecording *newRecording = [[PLRadioRecording alloc] initWithName:fileName path:_outputPath type:fileType size:@"0 KB" byteSize:0 time:@"0m00s" url:originURL];
    _recordingUUID = [newRecording uuid];
    _currentRecording = newRecording;
    //[[_recordingTableView recordings] addObject:newRecording];
    if ([theArtist isEqualToString:@""]) theArtist = @"Unknown Station";
    if (_playingFromURL) theArtist = @"URL Stream";
    NSURL *fileURL = [NSURL fileURLWithPath:_outputPath];
    [_radioController activeStream].outputFile = fileURL;
    //NSLog(@"PATH: %@", [fileURL path]);
    [self registerRecording];
}

- (void)registerRecording {
    NSLog(@"Registering recording");
    NSString *originURL = _trackAlbum;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
    NSString *theArtist = _trackArtist;
    if ([theArtist isEqualToString:@""]) theArtist = @"Unknown Station";
    if (_playingFromURL) theArtist = @"URL Stream";
    NSString *fileName = [NSString stringWithFormat:@"%@ %@.%@", theArtist, _dateString, [[[_radioController activeStream] suggestedFileExtension] lowercaseString]];
    NSString *fileType = [[[_radioController activeStream] suggestedFileExtension] uppercaseString];
    PLRadioRecording *newRecording = [[PLRadioRecording alloc] initWithName:fileName path:_outputPath type:fileType size:@"0 KB" byteSize:0 time:@"0:00" url:originURL];
    _recordingUUID = [newRecording uuid];
    _currentRecording = newRecording;
    [[_recordingTableView recordings] addObject:newRecording];
    while ([_recordingTableView doNotDisturb]);
    if (_recordingWindowIsAlreadyOpen) {
        [_recordingTableView saveCurrentSelection];
        [_recordingTableView sortRecordings];
        [[_recordingTableView tableView] reloadData];
        [_recordingTableView restorePreviousSelection];
    }
    _startTime = [NSDate date];
    _recordingBlinkState = NO;
    if (_recordingBlinkTimer) {
        [_recordingBlinkTimer invalidate];
        _recordingBlinkTimer = nil;
    }
    _recordingBlinkTimer = [NSTimer timerWithTimeInterval:0.5f target:self selector:@selector(blinkRecordingIndicator) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_recordingBlinkTimer forMode:NSRunLoopCommonModes];
    dispatch_async(dispatch_get_main_queue(),^(void) {
        [self->_record setAction:@selector(stopRecording:)];
    });
    [self installRaydio];
    [self startUpdatingCurrentRecording];
    NSString *labelPath = [[[_currentRecording filePath] stringByDeletingPathExtension] stringByAppendingPathExtension:@"txt"];
    NSLog(@"LABELS: %@", labelPath);
    _isGuardingAgainstRecordingModeChange = NO;
    
    [[NSFileManager defaultManager] createFileAtPath:labelPath contents:nil attributes:nil];
    _labelFileHandle = [NSFileHandle fileHandleForWritingAtPath:labelPath];
    NSLog(@"%@", _labelFileHandle);
    _recordingStartedAtThisTime = [NSDate timeIntervalSinceReferenceDate];
    [self writeAudacityLabel];
}

- (void)writeAudacityLabel {
    if (_isRecording) {
        if ([_trackTitle isEqualToString:@"Opening stream..."]) return;
        [_labelFileHandle seekToEndOfFile];
        NSTimeInterval interval = [NSDate timeIntervalSinceReferenceDate] - _recordingStartedAtThisTime;
        NSString *string = [NSString stringWithFormat:@"%f\t%f\t%@\n", interval, interval, _trackTitle];
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        [_labelFileHandle writeData:data];
        [_labelFileHandle synchronizeFile];
    }
}

- (float)getTrueDurationOfRecording:(NSString *)filePath {
    NSURL *audioFileURL = [NSURL fileURLWithPath:filePath];
    AVURLAsset *audioAsset = [AVURLAsset URLAssetWithURL:audioFileURL options:nil];
    CMTime audioDuration = audioAsset.duration;
    return CMTimeGetSeconds(audioDuration);
}

- (IBAction)stopRecording:(id)sender {
    [self stopRecording];
}

- (void)stopRecording {
    if (!_isRecording || _isGuardingAgainstRecordingModeChange) {
        return;
    }
    _isGuardingAgainstRecordingModeChange = YES;
    _isRecording = NO;
    [self stopUpdatingCurrentRecording];
    if (_recordingBlinkTimer) {
        [_recordingBlinkTimer invalidate];
        _recordingBlinkTimer = nil;
    }
    [_record setImage:[NSImage imageNamed:@"record"]];
    if (_radioController) {
        [[_radioController activeStream] setOutputFile:nil];
    }
    NSDate *endTime = [NSDate date];
    NSTimeInterval interval = [endTime timeIntervalSinceDate:_startTime];
    int minutes = (int)(interval / 60);
    int seconds = (int)(interval - (minutes * 60));
    NSString *fileTime = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    NSDictionary *fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:[_currentRecording filePath] error:nil];
    NSString *fileSize = [NSByteCountFormatter stringFromByteCount:[fileAttribs fileSize] countStyle:NSByteCountFormatterCountStyleFile];
    NSString *byteSize = [NSString stringWithFormat:@"%032lld", [fileAttribs fileSize]];
    [_currentRecording setFileSize:fileSize];
    [_currentRecording setFileSizeInBytes:byteSize];
    [_currentRecording setFileTime:fileTime];
    _recordingUUID = @"";
    while ([_recordingTableView doNotDisturb]);
    if (_recordingWindowIsAlreadyOpen) {
        [_recordingTableView saveCurrentSelection];
        [_recordingTableView sortRecordings];
        [[_recordingTableView tableView] reloadData];
        [_recordingTableView restorePreviousSelection];
    }
    [self saveRadioRecordings];
    [_record setAction:@selector(recordRadio:)];
    [self installRaydio];
    _isGuardingAgainstRecordingModeChange = NO;
}

- (void)blinkRecordingIndicator {
    if (_recordingBlinkState) {
        [_record setImage:[NSImage imageNamed:@"record"]];
    } else {
        [_record setImage:[NSImage imageNamed:@"recording"]];
    }
    _recordingBlinkState = !_recordingBlinkState;
}

- (void)checkToStopRecording {
    if (_isRecording) {
        [self stopRecording];
    }
}

- (void)disableRecording {
    _recordingDisabled = YES;
    [_record setToolTip:@"Plectrum cannot record this type of stream."];
    [_record setEnabled:NO];
}

- (void)enableRecording {
    _recordingDisabled = NO;
    [_record setToolTip:nil];
    [_record setEnabled:YES];
}

- (IBAction)manageRecordings:(id)sender {
    [_recordingTableView initializeForWindowShow];
    [_recordingWindow makeKeyAndOrderFront:nil];
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
    _recordingWindowIsAlreadyOpen = YES;
}

- (IBAction)managingRecordingsDone:(id)sender {
    if ([[_recordingTableView previewBox] isPlaying]) [[_recordingTableView previewBox] stop];
    [_recordingWindow orderOut:nil];
    _recordingWindowIsAlreadyOpen = NO;
}

- (void)startUpdatingCurrentRecording {
    [self invalidateCurrentRecordingUpdateTimer];
    _recordingUpdateTimer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(updateCurrentRecording) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_recordingUpdateTimer forMode:NSRunLoopCommonModes];
}

- (void)updateCurrentRecording {
    NSDate *endTime = [NSDate date];
    NSTimeInterval interval = [endTime timeIntervalSinceDate:_startTime];
    NSString *fileTime;
    int hours = (int)(interval / 3600);
    int minutes = (int)(interval / 60) - (hours * 60);
    int seconds = (int)(interval - (hours * 3600) - (minutes * 60));
    if (hours == 0) {
        fileTime = [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    } else {
        fileTime = [NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes, seconds];
    }
    NSDictionary *fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:[_currentRecording filePath] error:nil];
    NSString *fileSize = [NSByteCountFormatter stringFromByteCount:[fileAttribs fileSize] countStyle:NSByteCountFormatterCountStyleFile];
    NSString *byteSize = [NSString stringWithFormat:@"%032lld", [fileAttribs fileSize]];
    [_currentRecording setFileSize:fileSize];
    [_currentRecording setFileSizeInBytes:byteSize];
    [_currentRecording setFileTime:fileTime];
    if ([_recordingTableView doNotDisturb]) return;
    if (_recordingWindowIsAlreadyOpen) {
        [_recordingTableView reloadCurrentlyRecordingRowOnly];
    }
}

- (void)stopUpdatingCurrentRecording {
    [self invalidateCurrentRecordingUpdateTimer];
}

- (void)invalidateCurrentRecordingUpdateTimer {
    if (_recordingUpdateTimer) {
        [_recordingUpdateTimer invalidate];
        _recordingUpdateTimer = nil;
    }
}


#pragma mark - Main Window IBActions

- (IBAction)showMainWindow:(id)sender {
    [self storePrefs];
    [self preparePrefsWindow];
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
    [_window makeKeyAndOrderFront:self];
}

- (IBAction)showInfoWindow:(id)sender {
    [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
    [NSApp orderFrontStandardAboutPanel:self];
}

- (IBAction)radioButtonClicked:(id)sender {
    _radioButton = [[sender identifier] integerValue];
    [self setWhatRadioButton:_radioButton];
}

- (IBAction)cancelButtonClicked:(id)sender {
    [self restorePrefs];
    [_window orderOut:nil];
}

- (IBAction)saveButtonClicked:(id)sender {
    [self savePrefs];
    [_window orderOut:self];
}

- (IBAction)startupAtLoginCheckBoxClicked:(id)sender {
    _startOnLogin = [_startOnLoginCheckBox state];
    [self addToStartupItems:_startOnLogin];
}

- (IBAction)artWindowEnabledCheckBoxClicked:(id)sender {
    [_coverArtWindow setArtWindowEnabled:[_artWindowEnabledCheckBox state]];
    if ([_coverArtWindow artWindowEnabled]) {
        [_artWindowConfigWindowButton setEnabled:YES];
    } else {
        [_artWindowConfigWindowButton setEnabled:NO];
    }
}

- (IBAction)triggerOnStartCheckBoxClicked:(id)sender {
    _triggerOnStart = [_triggerOnStartCheckBox state];
}

- (IBAction)showMenuCheckBoxClicked:(id)sender {
    _showMenu = [_showMenuCheckBox state];
    if (_showMenu) [self showStatusMenu]; else [self hideStatusMenu];
}

- (IBAction)muteInsteadOfPauseCheckBoxClicked:(id)sender {
    if (_muteInsteadOfPause) {
        if (_isPaused) {
            [self playpause:self];
        }
    }
    _muteInsteadOfPause = [_muteInsteadOfPauseCheckBox state];
}

- (IBAction)blockNextAndPreviousForRadioCheckBoxClicked:(id)sender {
    _blockNextAndPreviousForRadio = [_blockNextAndPreviousForRadioCheckBox state];
}

- (IBAction)useMediaKeysCheckBoxClicked:(id)sender {
    _useMediaKeys = [_useMediaKeysCheckBox state];
    [self handleTrust];
}

- (IBAction)setShowAlsoIfFrontmostCheckBoxClicked:(id)sender {
    _showAlsoIfFrontmost = [_showAlsoIfFrontmostCheckBox state];
}

- (IBAction)setAskForConfirmationCheckBoxClicked:(id)sender {
    _askForConfirmation = [_askForConfirmationCheckBox state];
    if (_askForConfirmation) {
        [[_favTableView removeButton] setTitle:@"Remove..."];
    } else {
        [[_favTableView removeButton] setTitle:@"Remove"];
    }
}

- (IBAction)hotkeyButtonClicked:(id)sender {
    [_window beginSheet:_hotKeySheet completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == 0) {
            [self setMenuKeyEquivalents];
        }
    }];
}

- (void)alertDidEndAgain:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == 0) {
        [self setMenuKeyEquivalents];
    }
}

- (IBAction)hotKeySheetButtonClicked:(id)sender {
    [_hotKeySheet makeFirstResponder:[_hkVolumeUp clearButton]];
    [_window endSheet:_hotKeySheet returnCode:[[sender identifier] integerValue]];
}

- (IBAction)sliderUpdated:(id)sender {
    NSString *whichIsRunning = [self getPlayingApplication];
    if ([whichIsRunning isEqualToString:@"iTunes"]) {
        [_iTunes setSoundVolume:(NSInteger)[_slider floatValue]];
        return;
    } else if ([whichIsRunning isEqualToString:@"Music"]) {
        [_Music setSoundVolume:(NSInteger)[_slider floatValue]];
        return;
    } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
        [_Spotify setSoundVolume:(NSInteger)[_slider floatValue]];
        return;
    } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
        NSNumber *soundVolume = [NSNumber numberWithInt:(int)[_slider floatValue]];
        [_Swinsian setSoundVolume:soundVolume];
        return;
    } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
        if (_radioController) {
            if (_isMuted) {
                _plVolume = 0.0f;
                _oldPlVolume = 0.0f;
                [self playpause:self];
            }
            _plVolume = (float)([_slider floatValue] / 100);
            [_radioController setVolume:_plVolume];
        }
    }
}


#pragma mark - Utilities

- (void)appWasLaunched:(NSNotification *)notification {
    NSString *launchedApp = [[notification userInfo] objectForKey:@"NSApplicationBundleIdentifier"];
    if ([launchedApp isEqualToString:@"com.apple.iTunes"] || [launchedApp isEqualToString:@"com.apple.Music"]) {
        [self appleAppHasHalfStarsEnabled];
    }
}

- (void)appWasTerminated:(NSNotification *)notification {
    NSString *playerState = @"Stopped";
    NSString *terminatedApp = [[notification userInfo] objectForKey:@"NSApplicationBundleIdentifier"];
    if ([terminatedApp isEqualToString:@"com.apple.iTunes"]) {
        if ([_Swinsian isRunning]) {
            if ([_Swinsian playerState] == SwinsianPlayerStatePlaying) {
                playerState = @"Playing";
            } else if ([_Swinsian playerState] == SwinsianPlayerStatePaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Spotify isRunning]) {
            if ([_Spotify playerState] == SpotifyEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_Spotify playerState] == SpotifyEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if (_isPlaying || _isPaused) {
            return;
        } else {
            if ([_coverArtWindow artWindowOnScreen]) {
                [_coverArtWindow fadeOut];
            }
            return;
        }
    } else if ([terminatedApp isEqualToString:@"com.apple.Music"]) {
        if ([_Swinsian isRunning]) {
            if ([_Swinsian playerState] == SwinsianPlayerStatePlaying) {
                playerState = @"Playing";
            } else if ([_Swinsian playerState] == SwinsianPlayerStatePaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Spotify isRunning]) {
            if ([_Spotify playerState] == SpotifyEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_Spotify playerState] == SpotifyEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if (_isPlaying || _isPaused) {
            return;
        }
    } else if ([terminatedApp isEqualToString:@"com.swinsian.Swinsian"]) {
        if ([_iTunes isRunning]) {
            if ([_iTunes playerState] == iTunesEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_iTunes playerState] == iTunesEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromiTunes];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromiTunes];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Music isRunning]) {
            if ([_Music playerState] == MusicEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_Music playerState] == MusicEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromMusic];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromMusic];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Spotify isRunning]) {
            if ([_Spotify playerState] == SpotifyEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_Spotify playerState] == SpotifyEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if (_isPlaying || _isPaused) {
            return;
        }
    } else if ([terminatedApp isEqualToString:@"com.spotify.client"]) {
        if ([_iTunes isRunning]) {
            if ([_iTunes playerState] == iTunesEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_iTunes playerState] == iTunesEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromiTunes];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromiTunes];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Music isRunning]) {
            if ([_Music playerState] == MusicEPlSPlaying) {
                playerState = @"Playing";
            } else if ([_Music playerState] == MusicEPlSPaused) {
                playerState = @"Paused";
            }
            if ([playerState isEqualToString:@"Playing"]) {
                [self getTrackInfoFromMusic];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else if (_isPlaying || _isPaused) {
                return;
            }  else if ([playerState isEqualToString:@"Paused"]) {
                [self getTrackInfoFromMusic];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                return;
            } else {
                if ([_coverArtWindow artWindowOnScreen]) {
                    [_coverArtWindow fadeOut];
                }
                return;
            }
        } else if ([_Swinsian isRunning]) {
           if ([_Swinsian playerState] == SwinsianPlayerStatePlaying) {
               playerState = @"Playing";
           } else if ([_Swinsian playerState] == SwinsianPlayerStatePaused) {
               playerState = @"Paused";
           }
           if ([playerState isEqualToString:@"Playing"]) {
               [self getTrackInfoFromSwinsian];
               [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
               return;
           } else if (_isPlaying || _isPaused) {
               return;
           }  else if ([playerState isEqualToString:@"Paused"]) {
               [self getTrackInfoFromSwinsian];
               [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
               return;
           } else {
               if ([_coverArtWindow artWindowOnScreen]) {
                   [_coverArtWindow fadeOut];
               }
               return;
           }
       } else if (_isPlaying || _isPaused) {
           return;
       } else {
            if ([_coverArtWindow artWindowOnScreen]) {
                [_coverArtWindow fadeOut];
            }
        }
    }
}

- (NSString *)getPlayingApplication {
    BOOL plectrumPlaying = NO;
    BOOL iTunesPlaying;
    BOOL musicPlaying;
    BOOL swinsianPlaying;
    BOOL SpotifyPlaying;
    BOOL plectrumPaused = NO;
    BOOL iTunesPaused = NO;
    BOOL musicPaused = NO;
    BOOL swinsianPaused = NO;
    BOOL SpotifyPaused = NO;
    if ([_iTunes isRunning]) {
        if (iTunesEPlSPlaying == [_iTunes playerState]) {
            iTunesPlaying = YES;
        } else {
            iTunesPlaying = NO;
        }
        if (iTunesEPlSPaused == [_iTunes playerState]) {
            iTunesPaused = YES;
        } else {
            iTunesPaused = NO;
        }
    } else {
        iTunesPlaying = NO;
    }
    if ([_Music isRunning]) {
        if (MusicEPlSPlaying == [_Music playerState]) {
            musicPlaying = YES;
        } else {
            musicPlaying = NO;
        }
        if (MusicEPlSPaused == [_Music playerState]) {
            musicPaused = YES;
        } else {
            musicPaused = NO;
        }
    } else {
        musicPlaying = NO;
    }
    if ([_Swinsian isRunning]) {
        if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
            swinsianPlaying = YES;
        } else {
            swinsianPlaying = NO;
        }
        if (SwinsianPlayerStatePaused == [_Swinsian playerState]) {
            swinsianPaused = YES;
        } else {
            swinsianPaused = NO;
        }
    } else {
        swinsianPlaying = NO;
    }
    if ([_Spotify isRunning]) {
        if (SpotifyEPlSPlaying == [_Spotify playerState]) {
            SpotifyPlaying = YES;
        } else {
            SpotifyPlaying = NO;
        }
        if (SpotifyEPlSPaused == [_Spotify playerState]) {
            SpotifyPaused = YES;
        } else {
            SpotifyPaused = NO;
        }
    } else {
        SpotifyPlaying = NO;
    }
    if (_isPlaying) {
        if (!_isPaused) {
            plectrumPlaying = YES;
        } else {
            plectrumPlaying = NO;
        }
    }
    if (iTunesPlaying) {
        _playingApplication = @"iTunes";
        return _playingApplication;
    } else if (musicPlaying) {
        _playingApplication = @"Music";
        return _playingApplication;
    } else if (swinsianPlaying) {
        _playingApplication = @"Swinsian";
        return _playingApplication;
    } else if (SpotifyPlaying) {
        _playingApplication = @"Spotify";
        return _playingApplication;
    } else if (plectrumPlaying) {
        _playingApplication = @"Plectrum";
        return _playingApplication;
    }
    if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"iTunesRadio"]) {
        if ([_iTunes isRunning]) {
            if ([_iTunes playerState] == iTunesEPlSPaused) {
                [self getTrackInfoFromiTunes];
                _playingApplication = @"iTunes";
                return _playingApplication;
            } else {
                
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"MusicRadio"]) {
        if ([_Music isRunning]) {
            if ([_Music playerState] == MusicEPlSPaused) {
                [self getTrackInfoFromMusic];
                _playingApplication = @"Music";
                return _playingApplication;
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Swinsian"]) {
        if ([_Swinsian isRunning]) {
            if ([_Swinsian playerState] == SwinsianPlayerStatePaused) {
                [self getTrackInfoFromSwinsian];
                _playingApplication = @"Swinsian";
                return _playingApplication;
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
        if ([_Spotify isRunning]) {
            if (SpotifyEPlSPaused == [_Spotify playerState]) {
                [self getTrackInfoFromSpotify];
                _playingApplication = @"Spotify";
                return _playingApplication;
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
        if (_isPlaying) {
            if (_isPaused) {
                [self getTrackInfoFromPlectrum];
                _playingApplication = @"Plectrum";
                return _playingApplication;
            }
        }
    }
    if (iTunesPaused) {
        [self getTrackInfoFromiTunes];
        NSImage *artwork = _trackArt;
        if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
            artwork = [NSImage imageNamed:@"stream"];
        }
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"iTunes";
        return _playingApplication;
    } else if (musicPaused) {
        [self getTrackInfoFromMusic];
        NSImage *artwork = _trackArt;
        if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
            artwork = [NSImage imageNamed:@"stream"];
        }
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"Music";
        return _playingApplication;
    } else if (iTunesPaused) {
        [self getTrackInfoFromSwinsian];
        NSImage *artwork = _trackArt;
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"Swinsian";
        return _playingApplication;
    } else if (SpotifyPaused) {
        [self getTrackInfoFromSpotify];
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"Spotify";
        return _playingApplication;
    } else if (swinsianPaused) {
        [self getTrackInfoFromSwinsian];
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"Swinsian";
        return _playingApplication;
    } else if (plectrumPaused) {
        [self getTrackInfoFromPlectrum];
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
        [_coverArtWindow receiveStatus:@"Paused"];
        [self getTrackInfo];
        _playingApplication = @"Plectrum";
        return _playingApplication;
    }
    return @"None";
}

- (NSImage *)getDesktopPicture {
    NSMutableArray *windowList = (NSMutableArray *)CFBridgingRelease(CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID));
    for (NSDictionary *window in windowList) {
        NSString *owner = [window objectForKey:@"kCGWindowOwnerName"];
        NSString *name = [window objectForKey:@"kCGWindowName"];
        if ([owner isEqualToString:@"Dock"]) {
            if ([name hasPrefix:@"Desktop Picture"]) {
                CGWindowID winId = [[window objectForKey:@"kCGWindowNumber"] shortValue];
                CGImageRef theCGImage = CGWindowListCreateImage(CGRectNull, kCGWindowListOptionIncludingWindow, winId, kCGWindowImageDefault);
                if (theCGImage) {
                    NSImage *image = [[NSImage alloc] initWithCGImage:theCGImage size:[NSScreen mainScreen].frame.size];
                    CFRelease(theCGImage);
                    return image;
                }
            }
        }
    }
    return nil;
}

- (NSString *)getStatus {
    return _currentPlayerStatus;
}

- (NSString *)getNewStatus {
    NSString *pState = @"Stopped";
    if ([_trackInfoSource isEqualToString:@"iTunes"] || [_trackInfoSource isEqualToString:@"iTunesRadio"]) {
        if ([_iTunes isRunning]) {
            if (iTunesEPlSPlaying == [_iTunes playerState]) {
                pState = @"Playing";
            } else if (iTunesEPlSPaused == [_iTunes playerState]) {
                pState = @"Paused";
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Music"] || [_trackInfoSource isEqualToString:@"MusicRadio"]) {
        if ([_Music isRunning]) {
            if (MusicEPlSPlaying == [_Music playerState]) {
                pState = @"Playing";
            } else if (MusicEPlSPaused == [_Music playerState]) {
                pState = @"Paused";
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Spotify"]) {
        if ([_Spotify isRunning]) {
            if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                pState = @"Playing";
            } else if (SpotifyEPlSPaused == [_Spotify playerState]) {
               pState = @"Paused";
            }
        }
    } else if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
        if (_isPlaying) {
            if (!_isPaused) {
                pState = @"Playing";
            } else {
                pState = @"Paused";
            }
        }
    }
    _currentPlayerStatus = [pState copy];
    return _currentPlayerStatus;
}

- (void)suppressVolumeOfPlayingApplication {
    if ([_iTunes isRunning]) {
        if ([_iTunes playerState] == iTunesEPlSPlaying) {
            _suppressedVolume = (float)[_iTunes soundVolume];
            _suppressedApp = _iTunes;
            [_iTunes setSoundVolume:0];
        }
    }
    if ([_Music isRunning]) {
        if ([_Music playerState] == MusicEPlSPlaying) {
            _suppressedVolume = (float)[_Music soundVolume];
            _suppressedApp = _Music;
            [_Music setSoundVolume:0];
        }
    }
    if ([_Spotify isRunning]) {
        if ([_Spotify playerState] == SpotifyEPlSPlaying) {
            _suppressedVolume = (float)[_Spotify soundVolume];
            _suppressedApp = _Spotify;
            [_Spotify setSoundVolume:0];
        }
    }
    if (_isPlaying) {
        if (!_isPaused) {
            if (_radioController) {
                _suppressedVolume = (float)[_radioController volume];
                _suppressedApp = self;
                [_radioController setVolume:0.0f];
            } else {
                _suppressedApp = nil;
            }
        }
    }
}

- (void)restoreVolumeOfPlayingApplication {
    if (_suppressedApp == _iTunes) {
        if ([_iTunes isRunning]) {
            [_iTunes setSoundVolume:(NSInteger)_suppressedVolume];
        }
    } else if (_suppressedApp == _Music) {
        if ([_Music isRunning]) {
            [_Music setSoundVolume:(NSInteger)_suppressedVolume];
        }
    } else if (_suppressedApp == _Spotify) {
        if ([_Spotify isRunning]) {
            [_Spotify setSoundVolume:(NSInteger)_suppressedVolume];
        }
    } else if (_suppressedApp == self) {
        if (_radioController) {
            [_radioController setVolume:_suppressedVolume];
        }
    }
}

- (void)appleAppHasHalfStarsEnabled {
    NSString *appleApp;
    NSPipe *pipe = [NSPipe pipe];
    NSFileHandle *file = pipe.fileHandleForReading;
    NSTask *task = [[NSTask alloc] init];
    task.launchPath = @"/usr/bin/defaults";
    if (@available(macOS 10.15, *)) {
        appleApp = @"com.apple.Music";
    } else {
        appleApp = @"com.apple.iTunes";
    }
    task.arguments = @[@"read", appleApp, @"allow-half-stars"];
    task.standardOutput = pipe;
    [task launch];
    NSData *data = [file readDataToEndOfFile];
    [file closeFile];
    NSString *output = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    if ([output hasPrefix:@"1"]) {
        _appleAppHalfStarsEnabled = YES;
    } else {
        _appleAppHalfStarsEnabled = NO;
    }
}


#pragma mark - Preferences

- (void)getPrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSPoint thePoint = [self getDefaultFloaterPosition];
    NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
    NSString *artWindowRectString = NSStringFromRect(NSMakeRect(screenRect.origin.x + 10, screenRect.origin.y + 10, 300, 300));
    long floaterXPos = thePoint.x;
    long floaterYPos = thePoint.y;
    NSDictionary *basicDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @2,                          @"radioButton",
                                   @NO,                         @"startOnLogin",
                                   @YES,                        @"triggerOnStart",
                                   @YES,                        @"showAlsoIfFrontmost",
                                   @YES,                        @"askForConfirmation",
                                   @YES,                        @"showMenu",
                                   @YES,                        @"muteInsteadOfPause",
                                   @NO,                         @"blockNextAndPreviousForRadio",
                                   @NO,                         @"useMediaKeys",
                                   @0.75f,                      @"plVolume",
                                   @0,                          @"notificationCorner",
                                   @0.5f,                       @"notificationOpacity",
                                   @NO,                         @"artWindowEnabled",
                                   @NO,                         @"artWindowVisible",
                                   @1,                          @"showSongDetailsSelection",
                                   @0,                          @"artWindowLevel",
                                   @YES,                        @"showControlsOnMouseOver",
                                   artWindowRectString,         @"artWindowRect",
                                   @NO,                         @"artWindowLocked",
                                   @1.0f,                       @"artWindowOpacity",
                                   @5,                          @"searchEngine",
                                   [NSNumber numberWithLong:floaterXPos], @"floaterX",
                                   [NSNumber numberWithLong:floaterYPos], @"floaterY",
                                   nil];
    [userDefaults registerDefaults:basicDefaults];
    NSArray *hotKeys = [userDefaults arrayForKey:@"hotKeys"];
    if (hotKeys) {
        [self convertHotKeys:hotKeys];
        [_hotKeyCenter saveHotKeys];
        [userDefaults removeObjectForKey:@"hotKeys"];
    } else {
        [_hotKeyCenter loadHotKeys];
    }
    _radioButton = [userDefaults integerForKey:@"radioButton"];
    [self setWhatRadioButton:_radioButton];
    _startOnLogin = [userDefaults boolForKey:@"startOnLogin"];
    [_startOnLoginCheckBox setState:_startOnLogin];
    _triggerOnStart = [userDefaults boolForKey:@"triggerOnStart"];
    [_triggerOnStartCheckBox setState:_triggerOnStart];
    _showAlsoIfFrontmost = [userDefaults boolForKey:@"showAlsoIfFrontmost"];
    [_showAlsoIfFrontmostCheckBox setState:_showAlsoIfFrontmost];
    _askForConfirmation = [userDefaults boolForKey:@"askForConfirmation"];
    [_askForConfirmationCheckBox setState:_askForConfirmation];
    if (_askForConfirmation) {
        [[_favTableView removeButton] setTitle:@"Remove..."];
    } else {
        [[_favTableView removeButton] setTitle:@"Remove"];
    }
    _showMenu = [userDefaults boolForKey:@"showMenu"];
    [_showMenuCheckBox setState:_showMenu];
    _muteInsteadOfPause = [userDefaults boolForKey:@"muteInsteadOfPause"];
    [_muteInsteadOfPauseCheckBox setState:_muteInsteadOfPause];
    _blockNextAndPreviousForRadio = [userDefaults boolForKey:@"blockNextAndPreviousForRadio"];
    [_blockNextAndPreviousForRadioCheckBox setState:_blockNextAndPreviousForRadio];
    _useMediaKeys = [userDefaults boolForKey:@"useMediaKeys"];
    [_useMediaKeysCheckBox setState:_useMediaKeys];
    _plVolume = [userDefaults floatForKey:@"plVolume"];
    [_PLNotificationCenter setCorner:[userDefaults integerForKey:@"notificationCorner"]];
    [self clickCornerButton:[_PLNotificationCenter corner]];
    [_PLNotificationCenter setOpacity:[userDefaults floatForKey:@"notificationOpacity"]];
    [_opacitySlider setFloatValue:[_PLNotificationCenter opacity]];
    [_coverArtWindow setArtWindowEnabled:[userDefaults boolForKey:@"artWindowEnabled"]];
    [_artWindowEnabledCheckBox setState:[_coverArtWindow artWindowEnabled]];
    [_miArtWindow setHidden:![_coverArtWindow artWindowEnabled]];
    [_coverArtWindow setArtWindowVisible:[userDefaults boolForKey:@"artWindowVisible"]];
    if ([_coverArtWindow artWindowEnabled]) {
        if ([_coverArtWindow artWindowVisible]) {
            [_miArtWindow setTitle:@"Hide Art Panel"];
        } else {
            [_miArtWindow setTitle:@"Show Art Panel"];
        }
    }
    NSInteger showSongDetailsSelection = [userDefaults integerForKey:@"showSongDetailsSelection"];
    [_coverArtWindow setShowSongDetailsSelection:showSongDetailsSelection];
    [_showSongDetailsPopUpButton selectItemAtIndex:showSongDetailsSelection];
    NSString *artWindowRect = [userDefaults stringForKey:@"artWindowRect"];
    [_coverArtWindow setFrame:NSRectFromString(artWindowRect) display:YES];
    NSInteger artWindowLevel = [userDefaults integerForKey:@"artWindowLevel"];
    [_coverArtWindow setArtWindowLevel:artWindowLevel];
    if (artWindowLevel == 0) {
        [_artWindowLevelPopUpButton selectItemAtIndex:artWindowLevel];
        [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    } else {
        [_artWindowLevelPopUpButton selectItemAtIndex:artWindowLevel + 1];
        [_showSongDetailsOnMouseOverMenuItem setEnabled:YES];
    }
    [_coverArtWindow updateWindowLevel];
    [_coverArtWindow setShowControlsOnMouseOver:[userDefaults boolForKey:@"showControlsOnMouseOver"]];
    [_showControlsOnMouseOverCheckBox setState:[_coverArtWindow showControlsOnMouseOver]];
    [_coverArtWindow setIsLocked:[userDefaults boolForKey:@"artWindowLocked"]];
    [_coverArtWindow reflectLockStatus];
    if ([_coverArtWindow artWindowLevel] == 0) {
        _lastShowControlsOnMouseOverState = [_coverArtWindow showControlsOnMouseOver];
        _lastLockArtWindowState = [_coverArtWindow isLocked];
        [_lockArtWindowCheckBox setState:YES];
        [_lockArtWindowCheckBox setEnabled:NO];
        [_showControlsOnMouseOverCheckBox setState:NO];
        [_showControlsOnMouseOverCheckBox setEnabled:NO];
        if (showSongDetailsSelection == 1) {
            [_showSongDetailsPopUpButton selectItemAtIndex:2];
            [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
        }
    }
    [_coverArtWindow setOpacity:[userDefaults floatForKey:@"artWindowOpacity"]];
    [_artWindowOpacitySlider setFloatValue:[_coverArtWindow opacity]];
    _searchEngineChoiceIndex = [userDefaults integerForKey:@"searchEngine"];
    [_searchEnginePopUpButton selectItemAtIndex:_searchEngineChoiceIndex];
    long myX = [userDefaults integerForKey:@"floaterX"];
    long myY = [userDefaults integerForKey:@"floaterY"];
    [_bpmInspector setFrameOrigin:NSMakePoint(myX, myY)];
}

- (void)convertHotKeys:(NSArray *)oldHotKeys {
    NSArray *intermediateObject;
    for (int i = 0; i < [oldHotKeys count]; i++) {
        intermediateObject = [oldHotKeys objectAtIndex:i];
        if (![intermediateObject isEqual:[NSNull null]]) {
            UInt32 keycode = (UInt32)[[intermediateObject objectAtIndex:0] unsignedLongValue];
            UInt32 modmask = (UInt32)[[intermediateObject objectAtIndex:1] unsignedLongValue];
            NSString *keystring = [intermediateObject objectAtIndex:2];
            [_hotKeyCenter registerHotKey:keycode withModMask:modmask andKeyString:keystring withKeyID:i];
        }
    }
    [_hotKeyCenter setHotKeyTextViews];
}

- (void)resetPrefs {
    NSPoint thePoint = [self getDefaultFloaterPosition];
    NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
    NSString *artWindowRectString = NSStringFromRect(NSMakeRect(screenRect.origin.x + 10, screenRect.origin.y + 10, 300, 300));
    long floaterXPos = thePoint.x;
    long floaterYPos = thePoint.y;
    _radioButton = 2;
    [self setWhatRadioButton:_radioButton];
    _startOnLogin = NO;
    [_startOnLoginCheckBox setState:_startOnLogin];
    _triggerOnStart = YES;
    [_triggerOnStartCheckBox setState:_triggerOnStart];
    _showAlsoIfFrontmost = YES;
    [_showAlsoIfFrontmostCheckBox setState:_showAlsoIfFrontmost];
    _askForConfirmation = YES;
    [_askForConfirmationCheckBox setState:_askForConfirmation];
    [[_favTableView removeButton] setTitle:@"Remove..."];
    _showMenu = YES;
    [_showMenuCheckBox setState:_showMenu];
    _muteInsteadOfPause = YES;
    [_muteInsteadOfPauseCheckBox setState:_muteInsteadOfPause];
    _blockNextAndPreviousForRadio = NO;
    [_blockNextAndPreviousForRadioCheckBox setState:_blockNextAndPreviousForRadio];
    _useMediaKeys = NO;
    [_useMediaKeysCheckBox setState:_useMediaKeys];
    _plVolume = 0.75f;
    [_PLNotificationCenter setCorner:0];
    [self clickCornerButton:[_PLNotificationCenter corner]];
    [_PLNotificationCenter setOpacity:0.5f];
    [_opacitySlider setFloatValue:[_PLNotificationCenter opacity]];
    [_coverArtWindow setArtWindowEnabled:YES];
    [_artWindowEnabledCheckBox setState:[_coverArtWindow artWindowEnabled]];
    [_miArtWindow setHidden:![_coverArtWindow artWindowEnabled]];
    [_coverArtWindow setArtWindowVisible:NO];
    [_miArtWindow setTitle:@"Show Art Panel"];
    NSInteger showSongDetailsSelection = 1;
    [_coverArtWindow setShowSongDetailsSelection:showSongDetailsSelection];
    [_showSongDetailsPopUpButton selectItemAtIndex:showSongDetailsSelection];
    [_coverArtWindow setFrame:NSRectFromString(artWindowRectString) display:YES];
    NSInteger artWindowLevel = 0;
    [_coverArtWindow setArtWindowLevel:artWindowLevel];
    [_artWindowLevelPopUpButton selectItemAtIndex:artWindowLevel];
    [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    [_coverArtWindow updateWindowLevel];
    [_coverArtWindow setShowControlsOnMouseOver:YES];
    [_showControlsOnMouseOverCheckBox setState:[_coverArtWindow showControlsOnMouseOver]];
    [_coverArtWindow setIsLocked:NO];
    [_coverArtWindow reflectLockStatus];
    _lastShowControlsOnMouseOverState = [_coverArtWindow showControlsOnMouseOver];
    _lastLockArtWindowState = [_coverArtWindow isLocked];
    [_lockArtWindowCheckBox setState:YES];
    [_lockArtWindowCheckBox setEnabled:NO];
    [_showControlsOnMouseOverCheckBox setState:NO];
    [_showControlsOnMouseOverCheckBox setEnabled:NO];
    [_showSongDetailsPopUpButton selectItemAtIndex:2];
    [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    [_coverArtWindow setOpacity:1.0f];
    [_artWindowOpacitySlider setFloatValue:[_coverArtWindow opacity]];
    [_bpmInspector setFrameOrigin:NSMakePoint(floaterXPos, floaterYPos)];
    _searchEngineChoiceIndex = 5;
    [_searchEnginePopUpButton selectItemAtIndex:_searchEngineChoiceIndex];
    [self savePrefs];
    _prefsHaveBeenReset = YES;
}

- (void)setWhatRadioButton:(NSInteger)theButton {
    switch(theButton) {
        case 0:
            [_radioButtonNotificationCenter setState:YES];
            [_configNotificationButton setEnabled:NO];
            [_miArtWindow setHidden:YES];
            if (![_miBPMInspector isHidden]) {
                [_miBPMInspector setHidden:YES];
            }
            break;
            
        case 1:
            [_radioButtonGrowl setState:YES];
            [_configNotificationButton setEnabled:NO];
            break;
            
        case 2:
            [_radioButtonInternalSolution setState:YES];
            [_configNotificationButton setEnabled:YES];
            break;
            
        case 3:
            [_radioButtonNothing setState:YES];
            [_configNotificationButton setEnabled:NO];
            break;
            
        default:
            [_radioButtonNotificationCenter setState:YES];
            [_configNotificationButton setEnabled:NO];
    }
}

- (void)storePrefs {
    _origPrefs = [[NSMutableDictionary alloc] init];
    [_origPrefs setValue:[NSNumber numberWithBool:_startOnLogin] forKey:@"startOnLogin"];
    [_origPrefs setValue:[NSNumber numberWithInteger:_radioButton] forKey:@"radioButton"];
    [_origPrefs setValue:[NSNumber numberWithBool:_triggerOnStart] forKey:@"triggerOnStart"];
    [_origPrefs setValue:[NSNumber numberWithBool:_showAlsoIfFrontmost] forKey:@"showAlsoIfFrontmost"];
    [_origPrefs setValue:[NSNumber numberWithBool:_askForConfirmation] forKey:@"askForConfirmation"];
    [_origPrefs setValue:[NSNumber numberWithBool:_showMenu] forKey:@"showMenu"];
    [_origPrefs setValue:[NSNumber numberWithBool:_muteInsteadOfPause] forKey:@"muteInsteadOfPause"];
    [_origPrefs setValue:[NSNumber numberWithBool:_blockNextAndPreviousForRadio] forKey:@"blockNextAndPreviousForRadio"];
    [_origPrefs setValue:[NSNumber numberWithBool:_useMediaKeys] forKey:@"useMediaKeys"];
    [_origPrefs setValue:[NSNumber numberWithFloat:_plVolume] forKey:@"plVolume"];
    [_origPrefs setValue:[NSNumber numberWithInteger:[_PLNotificationCenter corner]] forKey:@"notificationCorner"];
    [_origPrefs setValue:[NSNumber numberWithFloat:[_PLNotificationCenter opacity]] forKey:@"notificationOpacity"];
    [_origPrefs setValue:[NSNumber numberWithBool:[_coverArtWindow artWindowEnabled]] forKey:@"artWindowEnabled"];
    [_origPrefs setValue:[NSNumber numberWithBool:[_coverArtWindow artWindowVisible]] forKey:@"artWindowVisible"];
    [_origPrefs setValue:[NSNumber numberWithInteger:[_coverArtWindow showSongDetailsSelection]] forKey:@"showSongDetailsSelection"];
    [_origPrefs setValue:[NSNumber numberWithInteger:[_coverArtWindow artWindowLevel]] forKey:@"artWindowLevel"];
    [_origPrefs setValue:[NSNumber numberWithBool:[_coverArtWindow showControlsOnMouseOver]] forKey:@"showControlsOnMouseOver"];
    [_origPrefs setValue:[NSNumber numberWithBool:[_coverArtWindow isLocked]] forKey:@"artWindowLocked"];
    [_origPrefs setValue:[NSNumber numberWithFloat:[_coverArtWindow opacity]] forKey:@"artWindowOpacity"];
    [_origPrefs setValue:[NSNumber numberWithInteger:_searchEngineChoiceIndex] forKey:@"searchEngine"];
    NSRect floaterPanel = [_bpmInspector frame];
    NSUInteger xPos = floaterPanel.origin.x;
    NSUInteger yPos = floaterPanel.origin.y;
    [_origPrefs setValue:[NSNumber numberWithInteger:xPos] forKey:@"floaterX"];
    [_origPrefs setValue:[NSNumber numberWithInteger:yPos] forKey:@"floaterY"];
}

- (void)restorePrefs {
    _radioButton = [[_origPrefs valueForKey:@"radioButton"] integerValue];
    BOOL oldStartOnLogin = [[_origPrefs valueForKey:@"startOnLogin"] boolValue];
    if (!(oldStartOnLogin == _startOnLogin)) {
        _startOnLogin = oldStartOnLogin;
        [_startOnLoginCheckBox setState:_startOnLogin];
        _triggerOnStart = [[_origPrefs valueForKey:@"triggerOnStart"] boolValue];
        [_triggerOnStartCheckBox setState:_triggerOnStart];
    }
    _showAlsoIfFrontmost = [[_origPrefs valueForKey:@"showAlsoIfFrontmost"] boolValue];
    [_showAlsoIfFrontmostCheckBox setState:_showAlsoIfFrontmost];
    _askForConfirmation = [[_origPrefs valueForKey:@"askForConfirmation"] boolValue];
    [_askForConfirmationCheckBox setState:_askForConfirmation];
    if (_askForConfirmation) {
        [[_favTableView removeButton] setTitle:@"Remove..."];
    } else {
        [[_favTableView removeButton] setTitle:@"Remove"];
    }
    _showMenu = [[_origPrefs valueForKey:@"showMenu"] boolValue];
    _plVolume = [[_origPrefs valueForKey:@"plVolume"] floatValue];
    [_showMenuCheckBox setState:_showMenu];
    if (_showMenu) [self showStatusMenu]; else [self hideStatusMenu];
    _muteInsteadOfPause = [[_origPrefs valueForKey:@"muteInsteadOfPause"] boolValue];
    [_muteInsteadOfPauseCheckBox setState:_muteInsteadOfPause];
    _blockNextAndPreviousForRadio = [[_origPrefs valueForKey:@"blockNextAndPreviousForRadio"] boolValue];
    [_blockNextAndPreviousForRadioCheckBox setState:_blockNextAndPreviousForRadio];
    _useMediaKeys = [[_origPrefs valueForKey:@"useMediaKeys"] boolValue];
    [_useMediaKeysCheckBox setState:_useMediaKeys];
    [_PLNotificationCenter setCorner:[[_origPrefs valueForKey:@"notificationCorner"] integerValue]];
    [self clickCornerButton:[_PLNotificationCenter corner]];
    [_PLNotificationCenter setOpacity:[[_origPrefs valueForKey:@"notificationOpacity"] floatValue]];
    [_opacitySlider setFloatValue:[_PLNotificationCenter opacity]];
    NSRect floaterPanel = [_bpmInspector frame];
    floaterPanel.origin.x = [[_origPrefs valueForKey:@"floaterX"] integerValue];
    floaterPanel.origin.y = [[_origPrefs valueForKey:@"floaterY"] integerValue];
    [_bpmInspector setFrame:floaterPanel display:YES];
    [_coverArtWindow setOpacity:[[_origPrefs valueForKey:@"artWindowOpacity"] floatValue]];
    if ([_coverArtWindow artWindowOnScreen]) {
        [_coverArtWindow updateWindowOpacity];
    }
    _searchEngineChoiceIndex = [[_origPrefs valueForKey:@"searchEngine"] integerValue];
    [_searchEnginePopUpButton selectItemAtIndex:_searchEngineChoiceIndex];
    [_coverArtWindow setIsLocked:![[_origPrefs valueForKey:@"artWindowLocked"] boolValue]];
    [_coverArtWindow toggleLockPosAndSize:self];
    NSInteger artWindowLevel = [[_origPrefs valueForKey:@"artWindowLevel"] integerValue];
    [_coverArtWindow artWindowLevelByNumber:artWindowLevel];
    if (artWindowLevel > 0) {
        artWindowLevel++;
        [_showSongDetailsOnMouseOverMenuItem setEnabled:YES];
    } else {
        [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    }
    [_artWindowLevelPopUpButton selectItemAtIndex:artWindowLevel];
    [_coverArtWindow setArtWindowEnabled:[[_origPrefs valueForKey:@"artWindowEnabled"] boolValue]];
    [_coverArtWindow setArtWindowVisible:[[_origPrefs valueForKey:@"artWindowVisible"] boolValue]];
    [_coverArtWindow setShowSongDetailsSelection:[[_origPrefs valueForKey:@"showSongDetailsSelection"] integerValue]];
    [_showSongDetailsPopUpButton selectItemAtIndex:[_coverArtWindow showSongDetailsSelection]];
    if ([_coverArtWindow showSongDetailsSelection] == 2) {
        [_coverArtWindow setBoxOpacity:0.0f];
    }
    [_coverArtWindow updateBoxOpacity];
}

- (void)savePrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:_radioButton forKey:@"radioButton"];
    [userDefaults setBool:_startOnLogin forKey:@"startOnLogin"];
    [userDefaults setBool:_triggerOnStart forKey:@"triggerOnStart"];
    [userDefaults setBool:_showAlsoIfFrontmost forKey:@"showAlsoIfFrontmost"];
    [userDefaults setBool:_askForConfirmation forKey:@"askForConfirmation"];
    [userDefaults setBool:_showMenu forKey:@"showMenu"];
    [userDefaults setBool:_muteInsteadOfPause forKey:@"muteInsteadOfPause"];
    [userDefaults setBool:_blockNextAndPreviousForRadio forKey:@"blockNextAndPreviousForRadio"];
    [userDefaults setBool:_useMediaKeys forKey:@"useMediaKeys"];
    [userDefaults setFloat:_plVolume forKey:@"plVolume"];
    [userDefaults setInteger:[_PLNotificationCenter corner] forKey:@"notificationCorner"];
    [userDefaults setFloat:[_PLNotificationCenter opacity] forKey:@"notificationOpacity"];
    [userDefaults setBool:[_coverArtWindow artWindowEnabled] forKey:@"artWindowEnabled"];
    [userDefaults setBool:[_coverArtWindow artWindowVisible] forKey:@"artWindowVisible"];
    [userDefaults setBool:[_coverArtWindow isLocked] forKey:@"artWindowLocked"];
    [userDefaults setBool:[_coverArtWindow showControlsOnMouseOver] forKey:@"showControlsOnMouseOver"];
    [userDefaults setInteger:[_coverArtWindow showSongDetailsSelection] forKey:@"showSongDetailsSelection"];
    [userDefaults setInteger:[_coverArtWindow artWindowLevel] forKey:@"artWindowLevel"];
    [userDefaults setFloat:[_coverArtWindow opacity] forKey:@"artWindowOpacity"];
    [userDefaults setInteger:_searchEngineChoiceIndex forKey:@"searchEngine"];
    [userDefaults setObject:NSStringFromRect([_coverArtWindow frame]) forKey:@"artWindowRect"];
    //NSArray *hotkeysForSaving = [_hotKeyCenter prepHotkeysForPropertyListStorage];
    //[userDefaults setObject:hotkeysForSaving forKey:@"hotKeys"];
    NSRect floaterPanel = [_bpmInspector frame];
    NSUInteger xPos = floaterPanel.origin.x;
    NSUInteger yPos = floaterPanel.origin.y;
    [userDefaults setInteger:xPos forKey:@"floaterX"];
    [userDefaults setInteger:yPos forKey:@"floaterY"];
    [userDefaults synchronize];
}

- (void)preparePrefsWindow {
    [self setWhatRadioButton:_radioButton];
    [_startOnLoginCheckBox setState:_startOnLogin];
    [_artWindowEnabledCheckBox setState:[_coverArtWindow artWindowEnabled]];
    [_triggerOnStartCheckBox setState:_triggerOnStart];
    [_showAlsoIfFrontmostCheckBox setState:_showAlsoIfFrontmost];
    [_askForConfirmationCheckBox setState:_askForConfirmation];
    [_showMenuCheckBox setState:_showMenu];
    [_muteInsteadOfPauseCheckBox setState:_muteInsteadOfPause];
    [_blockNextAndPreviousForRadioCheckBox setState:_blockNextAndPreviousForRadio];
    [_useMediaKeysCheckBox setState:_useMediaKeys];
    [_muteInsteadOfPauseCheckBox setState:_muteInsteadOfPause];
    [_searchEnginePopUpButton selectItemAtIndex:_searchEngineChoiceIndex];
}

- (void)continueConfigureNotificationSheet {
    [_window beginSheet:_notificationConfigSheet completionHandler:^(NSModalResponse returnCode) {
        [self->_PLNotificationCenter endPreview];
        if (returnCode != 0) {
            [self->_PLNotificationCenter setCorner:self->_oldCorner];
            [self->_PLNotificationCenter setOpacity:self->_oldOpacity];
            [self->_opacitySlider setFloatValue:self->_oldOpacity];
            [self clickCornerButton:self->_oldCorner];
        }
    }];
}

- (void)clickCornerButton:(NSInteger)corner {
    switch (corner) {
        case 0:
            [_bottomRight setState:YES];
            break;
            
        case 1:
            [_topRight setState:YES];
            break;
            
        case 2:
            [_topLeft setState:YES];
            break;
            
        case 3:
            [_bottomLeft setState:YES];
            break;
            
        default:
            [_bottomRight setState:YES];
    }
}

- (void)suppressDeletionWarnings:(BOOL)flag {
    _askForConfirmation = !flag;
    [_askForConfirmationCheckBox setState:_askForConfirmation];
    if (_askForConfirmation) {
        [[_favTableView removeButton] setTitle:@"Remove..."];
    } else {
        [[_favTableView removeButton] setTitle:@"Remove"];
    }
    [self savePrefs];
}

#pragma mark - Preferences related IBActions

- (IBAction)showControlsOnMouseOverCheckBoxClicked:(id)sender {
    [_coverArtWindow setShowControlsOnMouseOver:[sender state]];
    [_coverArtWindow updateControlsVisibility];
    _lastShowControlsOnMouseOverState = [sender state];
}

- (IBAction)lockArtWindowCheckBoxClicked:(id)sender {
    [_coverArtWindow setIsLocked:[sender state]];
    [_coverArtWindow reflectLockStatus];
    _lastLockArtWindowState = [sender state];
}

- (IBAction)showSongDetailsPopUpButtonChanged:(id)sender {
    [_coverArtWindow setShowSongDetailsSelection:[_showSongDetailsPopUpButton indexOfSelectedItem]];
    [_coverArtWindow updateBoxVisibility];
}

- (IBAction)showConfigureNotificationSheet:(id)sender {
    NSRect screen = [NSScreen mainScreen].frame;
    if (screen.size.width < screen.size.height) {
        [_notificationCornerBox setFrame:NSMakeRect(105, 100, 120, 180)];
    } else {
        [_notificationCornerBox setFrame:NSMakeRect(20, 100, 290, 180)];
    }
    [_bgImage setImage:[self getDesktopPicture]];
    _oldCorner = [_PLNotificationCenter corner];
    _oldOpacity = [_PLNotificationCenter opacity];
    [_opacitySlider setFloatValue:[_PLNotificationCenter opacity]];
    [_PLNotificationCenter initiatePreview];
    [self performSelector:@selector(continueConfigureNotificationSheet) withObject:nil afterDelay:0.1f];
}

- (IBAction)showConfigArtWindowSheet:(id)sender {
    __block NSInteger oldWindowLevel = [_coverArtWindow artWindowLevel];
    __block NSInteger oldShowSongDetails = [_coverArtWindow showSongDetailsSelection];
    __block CGFloat oldOpacity = [_coverArtWindow opacity];
    __block BOOL oldLockArt = [_coverArtWindow isLocked];
    __block BOOL oldShowControls = [_coverArtWindow showControlsOnMouseOver];
    __block BOOL oldState = !(oldWindowLevel == 0);
    __block NSInteger menuChoice = oldWindowLevel;
    _lastLockArtWindowState = oldLockArt;
    _lastShowControlsOnMouseOverState = oldShowControls;
    if (menuChoice > 0) menuChoice++;
    [_showSongDetailsPopUpButton selectItemAtIndex:oldShowSongDetails];
    [[_showSongDetailsPopUpButton menu] performActionForItemAtIndex:oldShowSongDetails];
    [_artWindowLevelPopUpButton selectItemAtIndex:menuChoice];
    [[_artWindowLevelPopUpButton menu] performActionForItemAtIndex:menuChoice];
    [_artWindowOpacitySlider setFloatValue:oldOpacity];
    if (!oldState) {
        [_lockArtWindowCheckBox setState:YES];
        [_showControlsOnMouseOverCheckBox setState:NO];
        [_lockArtWindowCheckBox setEnabled:NO];
        [_showControlsOnMouseOverCheckBox setEnabled:NO];
        [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    } else {
        [_lockArtWindowCheckBox setEnabled:YES];
        [_showControlsOnMouseOverCheckBox setEnabled:YES];
        [_showSongDetailsOnMouseOverMenuItem setEnabled:YES];
        [_lockArtWindowCheckBox setState:oldLockArt];
        [_showControlsOnMouseOverCheckBox setState:oldShowControls];
    }
    [_window setCollectionBehavior:NSWindowCollectionBehaviorMoveToActiveSpace];
    [_coverArtWindow beginDemoMode];
    [_window beginSheet:_artWindowConfigSheet completionHandler:^(NSModalResponse returnCode) {
        [self->_window setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces];
        if (returnCode != 0) {
            [self->_artWindowLevelPopUpButton selectItemAtIndex:menuChoice];
            [[self->_artWindowLevelPopUpButton menu] performActionForItemAtIndex:menuChoice];
            [self->_coverArtWindow setArtWindowLevel:oldWindowLevel];
            [self->_showSongDetailsPopUpButton selectItemAtIndex:oldShowSongDetails];
            [[self->_showSongDetailsPopUpButton menu] performActionForItemAtIndex:oldShowSongDetails];
            [self->_coverArtWindow setShowSongDetailsSelection:oldShowSongDetails];
            [self->_artWindowOpacitySlider setFloatValue:oldOpacity];
            [self->_coverArtWindow setOpacity:oldOpacity];
            if (oldWindowLevel == 0) {
                [self->_lockArtWindowCheckBox setState:YES];
                [self->_lockArtWindowCheckBox setEnabled:NO];
                [self->_showControlsOnMouseOverCheckBox setState:NO];
                [self->_lockArtWindowCheckBox setEnabled:NO];
            } else {
                [self->_lockArtWindowCheckBox setState:oldLockArt];
                [self->_lockArtWindowCheckBox setEnabled:YES];
                [self->_showControlsOnMouseOverCheckBox setState:oldShowControls];
                [self->_showControlsOnMouseOverCheckBox setEnabled:NO];
            }
            [self->_coverArtWindow setIsLocked:oldLockArt];
            [self->_coverArtWindow setShowControlsOnMouseOver:oldShowControls];
        } else {
            NSInteger newArtWindowLevel = [self->_artWindowLevelPopUpButton indexOfSelectedItem];
            if (newArtWindowLevel > 0) newArtWindowLevel--;
            [self->_coverArtWindow setArtWindowLevel:newArtWindowLevel];
            [self->_coverArtWindow setShowSongDetailsSelection:[self->_showSongDetailsPopUpButton indexOfSelectedItem]];
            if ([self->_lockArtWindowCheckBox isEnabled] == NO) {
                [self->_coverArtWindow setIsLocked:self->_lastLockArtWindowState];
                [self->_coverArtWindow setShowControlsOnMouseOver:self->_lastShowControlsOnMouseOverState];
            } else {
                [self->_coverArtWindow setIsLocked:[self->_lockArtWindowCheckBox state]];
                [self->_coverArtWindow setShowControlsOnMouseOver:[self->_showControlsOnMouseOverCheckBox state]];
            }
        }
        [self->_coverArtWindow endDemoMode];
    }];
}

- (IBAction)artWindowConfigSheetButtonPressed:(id)sender {
    NSInteger response = [[sender identifier] integerValue];
    [_window endSheet:_artWindowConfigSheet returnCode:response];
}

- (IBAction)notificationConfigSheetButtonPressed:(id)sender {
    NSInteger response = [[sender identifier] integerValue];
    [_window endSheet:_notificationConfigSheet returnCode:response];
}

- (IBAction)cornerSelectionRadioButtonClicked:(id)sender {
    NSInteger newlySelectedCorner = [[sender identifier] integerValue];
    [_PLNotificationCenter setCorner:newlySelectedCorner];
    [_PLNotificationCenter updatePreviewLocation];
}

- (IBAction)opacitySliderMoved:(id)sender {
    [_PLNotificationCenter setOpacity:[_opacitySlider floatValue]];
    [_PLNotificationCenter updatePreviewOpacity];
}

- (IBAction)toggleCoverArtWindow:(id)sender {
    if ([_coverArtWindow artWindowVisible]) {
        [_coverArtWindow setArtWindowVisible:NO];
        [_miArtWindow setTitle:@"Show Art Panel"];
        [_coverArtWindow fadeOut];
    } else {
        [_coverArtWindow setArtWindowVisible:YES];
        [_miArtWindow setTitle:@"Hide Art Panel"];
        [_coverArtWindow fadeIn];
    }
}

- (IBAction)glueToDesktopSelected:(id)sender {
    [_coverArtWindow setArtWindowLevel:0];
    if ([_lockArtWindowCheckBox isEnabled] == YES) {
        _lastLockArtWindowState = [_lockArtWindowCheckBox state];
        _lastShowControlsOnMouseOverState = [_showControlsOnMouseOverCheckBox state];
    }
    [_lockArtWindowCheckBox setState:YES];
    [_lockArtWindowCheckBox setEnabled:NO];
    [_showControlsOnMouseOverCheckBox setState:NO];
    [_showControlsOnMouseOverCheckBox setEnabled:NO];
    if ([_showSongDetailsPopUpButton indexOfSelectedItem] == 1) {
        [_showSongDetailsPopUpButton selectItemAtIndex:2];
    }
    [_showSongDetailsOnMouseOverMenuItem setEnabled:NO];
    _lastArtWindowLevelMenuSelectionWasTopItem = YES;
}

- (IBAction)glueToDesktopNotSelected:(id)sender {
    [_coverArtWindow setArtWindowLevel:[_artWindowLevelPopUpButton indexOfSelectedItem] - 1];
    if (!_lastArtWindowLevelMenuSelectionWasTopItem) return;
    [_lockArtWindowCheckBox setEnabled:YES];
    [_lockArtWindowCheckBox setState:_lastLockArtWindowState];
    [_showControlsOnMouseOverCheckBox setEnabled:YES];
    [_showControlsOnMouseOverCheckBox setState:_lastShowControlsOnMouseOverState];
    [_showSongDetailsOnMouseOverMenuItem setEnabled:YES];
    _lastArtWindowLevelMenuSelectionWasTopItem = NO;
}

- (IBAction)artWindowOpacitySliderChanged:(id)sender {
    [_coverArtWindow setOpacity:[sender floatValue]];
    [_coverArtWindow updateWindowOpacity];
}

- (IBAction)enableCoverArtWindowCheckBoxClicked:(id)sender {
    BOOL flag = [_artWindowEnabledCheckBox state];
    if ([_coverArtWindow artWindowEnabled]) {
        if ([_coverArtWindow artWindowOnScreen]) {
            [_coverArtWindow fadeOut];
        }
    }
    [_coverArtWindow setArtWindowEnabled:flag];
    [_artWindowConfigWindowButton setEnabled:[_coverArtWindow artWindowEnabled]];
    [_miArtWindow setHidden:![_coverArtWindow artWindowEnabled]];
    if (flag) {
        [_miArtWindow setHidden:NO];
        if ([_miBPMInspector isHidden]) {
            [_miBPMInspector setHidden:NO];
        }
        if ([_coverArtWindow artWindowVisible]) {
            [_miArtWindow setTitle:@"Hide Art Panel"];
        } else {
            [_miArtWindow setTitle:@"Show Art Panel"];
        }
        [_coverArtWindow fadeIn];
    } else {
        [_miArtWindow setHidden:YES];
        if (![_miBPMInspector isHidden]) {
            [_miBPMInspector setHidden:YES];
        }
    }
}


#pragma mark - Notification Methods

- (void)iTunesUpdateTrackInfo:(NSNotification *)notification {
    _playingApplication = @"iTunes";
    NSString *pState = [[notification userInfo] valueForKey:@"Player State"];
    if ([pState isEqualToString:@"Paused"]) return;
    _currentPlayerStatus = [pState copy];
    [_coverArtWindow receiveStatus:pState];
    BOOL fadeOutOK = YES;
    if ([pState isEqualToString:@"Stopped"]) {
        [self voidTrackInfoForMenu];
        if ([_Spotify isRunning]) {
            if (SpotifyEPlSStopped != [_Spotify playerState]) {
                _playingApplication = @"Spotify";
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if ([_Swinsian isRunning]) {
            if (SwinsianPlayerStateStopped != [_Swinsian playerState]) {
                _playingApplication = @"Swinsian";
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if (_isPlaying || _isPaused) {
            _playingApplication = @"Plectrum";
            fadeOutOK = NO;
        }
        if ([_coverArtWindow artWindowOnScreen] && fadeOutOK) {
            [_coverArtWindow fadeOut];
        }
        return;
    }
    [_coverArtWindow setCloseArtWindowEventReceived:NO];
    switch (_radioButton) {
        case 0:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromiTunes];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) [self collectTrackInfoForMenu];
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showNCNotification];
            }
            break;
            
        case 1:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromiTunes];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showGrowlNotification];
            }
            break;
            
        case 2:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromiTunes];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showPlectrumNotification];
            }
            break;
            
        case 3:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromiTunes];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
            }
            break;
            
        default:
            break;
    }
    [self setArtWindowDataAndDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
    if ([_trackInfoSource isEqualToString:@"iTunesRadio"]) {
        [_miSearchWeb setEnabled:YES];
        [_miSearchBC setEnabled:YES];
    }
    [self installPick];
}

- (void)iTunesTrackInfoChanged:(NSNotification *)notification {
    if ([_playingApplication isEqualToString:@"iTunes"]) {
        [self iTunesUpdateTrackInfo:notification];
    }
}

- (void)musicUpdateTrackInfo:(NSNotification *)notification {
    _playingApplication = @"Music";
    NSString *pState = [[notification userInfo] valueForKey:@"Player State"];
    if ([pState isEqualToString:@"Paused"]) return;
    _currentPlayerStatus = [pState copy];
    [_coverArtWindow receiveStatus:pState];
    BOOL fadeOutOK = YES;
    if ([pState isEqualToString:@"Stopped"]) {
        [self voidTrackInfoForMenu];
        if ([_Swinsian isRunning]) {
            if (SwinsianPlayerStateStopped != [_Swinsian playerState]) {
                _playingApplication = @"Swinsian";
                [self getTrackInfoFromSwinsian];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if ([_Spotify isRunning]) {
            if (SpotifyEPlSStopped != [_Spotify playerState]) {
                _playingApplication = @"Spotify";
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if (_isPlaying || _isPaused) {
            _playingApplication = @"Plectrum";
            fadeOutOK = NO;
        }
        if ([_coverArtWindow artWindowOnScreen] && fadeOutOK) {
            [_coverArtWindow fadeOut];
        }
        return;
    }
    [_coverArtWindow setCloseArtWindowEventReceived:NO];
    switch (_radioButton) {
        case 0:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromMusic];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showNCNotification];
            }
            break;
            
        case 1:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromMusic];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showGrowlNotification];
            }
            break;
            
        case 2:
            if (_isPlaying || _isPaused) {
                [self stop:self];
            }
            if ([_Swinsian isRunning]) {
                if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                    [_Swinsian pause];
                }
            }
            if ([pState isEqualToString:@"Playing"]) {
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromMusic];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showPlectrumNotification];
            }
            break;
            
        case 3:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromMusic];
                NSImage *artwork = _trackArt;
                if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                    artwork = [NSImage imageNamed:@"stream"];
                }
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
                        [_miTrackView setView:_riv];
                    } else {
                        [_miTrackView setView:_tiv];
                    }
                }
            }
            break;
            
        default:
            break;
    }
    [self setArtWindowDataAndDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
    if ([_trackInfoSource isEqualToString:@"MusicRadio"]) {
        [_miSearchWeb setEnabled:YES];
        [_miSearchBC setEnabled:YES];
    }
    if ([[_statusItem image] isNotEqualTo:[NSImage imageNamed:@"pick"]]) {
        [self installPick];
    }
}

- (void)musicTrackInfoChanged:(NSNotification *)notification {
    if ([_playingApplication isEqualToString:@"Music"]) {
        [self musicUpdateTrackInfo:notification];
    }
}

- (void)swinsianUpdateTrackInfo:(NSNotification *)notification {
    if (_swinsianNotificationDelayTimer) {
        [_swinsianNotificationDelayTimer invalidate];
        _swinsianNotificationDelayTimer = nil;
    }
    _swinsianNotification = notification;
    _swinsianNotificationDelayTimer = [NSTimer timerWithTimeInterval:0.10f target:self selector:@selector(continueSwinsianUpdateTrackInfo) userInfo:nil repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:_swinsianNotificationDelayTimer forMode:NSRunLoopCommonModes];
}

- (void)continueSwinsianUpdateTrackInfo {
    NSString *pState = [[[_swinsianNotification name] componentsSeparatedByString:@"-"] lastObject];
    _playingApplication = @"Swinsian";
    if ([pState isEqualToString:@"Paused"]) return;
    pState = [[pState componentsSeparatedByString:@"-"] lastObject];
    _currentPlayerStatus = [pState copy];
    [_coverArtWindow receiveStatus:pState];
    BOOL fadeOutOK = YES;
    if ([pState isEqualToString:@"Stopped"]) {
        [self voidTrackInfoForMenu];
        if ([_iTunes isRunning]) {
            if (iTunesEPlSStopped != [_iTunes playerState]) {
                _playingApplication = @"iTunes";
                [self getTrackInfoFromiTunes];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if ([_Music isRunning]) {
            if (MusicEPlSStopped != [_Music playerState]) {
                _playingApplication = @"Music";
                [self getTrackInfoFromMusic];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if ([_Spotify isRunning]) {
            if (SpotifyEPlSStopped != [_Spotify playerState]) {
                _playingApplication = @"Spotify";
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                fadeOutOK = NO;
            }
        }
        if (_isPlaying || _isPaused) {
            _playingApplication = @"Plectrum";
            fadeOutOK = NO;
        }
        if ([_coverArtWindow artWindowOnScreen] && fadeOutOK) {
            [_coverArtWindow fadeOut];
        }
        return;
    }
    [_coverArtWindow setCloseArtWindowEventReceived:NO];
    switch (_radioButton) {
        case 0:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromSwinsian];
                NSImage *artwork = _trackArt;
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) [self collectTrackInfoForMenu];
                if ((!_showAlsoIfFrontmost) && ([_Swinsian frontmost])) return;
                [self showNCNotification];
            }
            break;
            
        case 1:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromSwinsian];
                NSImage *artwork = _trackArt;
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showGrowlNotification];
            }
            break;
            
        case 2:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromSwinsian];
                NSImage *artwork = _trackArt;
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
                if ((!_showAlsoIfFrontmost) && ([_Swinsian frontmost])) return;
                [self showPlectrumNotification];
            }
            break;
            
        case 3:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Spotify isRunning]) {
                    if (SpotifyEPlSPlaying == [_Spotify playerState]) {
                        [_Spotify pause];
                    }
                }
                [self getTrackInfoFromSwinsian];
                NSImage *artwork = _trackArt;
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:artwork];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
            }
            break;
            
        default:
            break;
    }
    [self setArtWindowDataAndDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
    [self installPick];
}


- (void)spotifyUpdateTrackInfo:(NSNotification *)notification {
    NSString *pState = [[notification userInfo] valueForKey:@"Player State"];
    if ([pState isEqualToString:@"Stopped"] || [pState isEqualToString:@"Paused"]) return;
    _playingApplication = @"Spotify";
    _currentPlayerStatus = [pState copy];
    [_coverArtWindow receiveStatus:pState];
    [_coverArtWindow setCloseArtWindowEventReceived:NO];
    switch (_radioButton) {
        case 0:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showNCNotification];
            }
            break;
            
        case 1:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showGrowlNotification];
            }
            break;
            
        case 2:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showPlectrumNotification];
            }
            break;
            
        case 3:
            if ([pState isEqualToString:@"Playing"]) {
                if (_isPlaying || _isPaused) {
                    [self stop:self];
                }
                if ([_iTunes isRunning]) {
                    if (iTunesEPlSPlaying == [_iTunes playerState]) {
                        [_iTunes pause];
                    }
                }
                if ([_Music isRunning]) {
                    if (MusicEPlSPlaying == [_Music playerState]) {
                        [_Music pause];
                    }
                }
                if ([_Swinsian isRunning]) {
                    if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
                        [_Swinsian pause];
                    }
                }
                 [self getTrackInfoFromSpotify];
                [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
                [_coverArtWindow receiveStatus:[self getStatus]];
                [self getTrackInfo];
                if (_menuIsOpen) {
                    [self collectTrackInfoForMenu];
                    [_miTrackView setView:_tiv];
                }
            }
            break;
            
        default:
            break;
    }
    [self setArtWindowDataAndDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
    if ([[_statusItem image] isNotEqualTo:[NSImage imageNamed:@"pick"]]) {
        [self installPick];
    }
    _trackInfoSource = @"Spotify";
}

- (void)plectrumPreupdateTrackInfo:(NSNotification *)notification {
    _delayedNotification = notification;
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    if (_firstNotificationCatchTimer) {
        [_firstNotificationCatchTimer invalidate];
        _firstNotificationCatchTimer = nil;
    }
    if (_playingFromFav && _fetchesArtwork) {
        if (_notificationDelayTimer) {
            [_notificationDelayTimer invalidate];
            _notificationDelayTimer = nil;
        }
        _notificationDelayTimer = [NSTimer timerWithTimeInterval:1.0f target:self selector:@selector(plectrumUpdateTrackInfo) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:_notificationDelayTimer forMode:NSRunLoopCommonModes];
    } else {
        [self plectrumUpdateTrackInfo];
    }
}

- (void)plectrumUpdateTrackInfo {
    if ([_iTunes isRunning]) {
        if (iTunesEPlSPlaying == [_iTunes playerState]) {
            [_iTunes pause];
        }
    }
    if ([_Music isRunning]) {
        if (MusicEPlSPlaying == [_Music playerState]) {
            [_Music pause];
        }
    }
    if ([_Swinsian isRunning]) {
        if (SwinsianPlayerStatePlaying == [_Swinsian playerState]) {
            [_Swinsian pause];
        }
    }
    if ([_Spotify isRunning]) {
        if (SpotifyEPlSPlaying == [_Spotify playerState]) {
            [_Spotify pause];
        }
    }
    NSNotification *notification = _delayedNotification;
    _playingApplication = @"Plectrum";
    if ([[notification object] isEqual:[PLLeftTableViewController class]]) {
        [_favTableView invalidateTimer];
        return;
    }
    _trackInfoSource = @"Plectrum";
    NSString *titleStuff;
    NSDictionary *metaData = [notification userInfo];
    @try { //this is a bit hackish but eh, it solves a long standing bug that I never even realized existed.
        titleStuff = [metaData valueForKeyPath:@"metadata.StreamTitle"];
        if ([[metaData valueForKeyPath:@"metadata.SimpleNotification"] boolValue]) {
            titleStuff = [metaData valueForKeyPath:@"Internet radio or relay"];
        }
        if (!titleStuff || [titleStuff isEqualToString: @""]) titleStuff = [metaData valueForKeyPath:@"Internet radio or relay"];
    }
    @catch (NSException *e) {
        if (!titleStuff || [titleStuff isEqualToString: @""]) titleStuff = [metaData valueForKeyPath:@"Internet radio or relay"];
    }
    if (_playingFromFav) {
        if ([[_currentFav altinfo] isNotEqualTo:@""]) titleStuff = [self getTrackTitleFromAlternativeSource:[_currentFav altinfo]];
        //NSLog(@"titlestuff:%@", titleStuff);
            if (!titleStuff || [titleStuff isEqualToString:@""]) {
                NSLog(@"Altinfo malformed?");
                titleStuff = [metaData valueForKeyPath:@"metadata.StreamTitle"];
            }
        } else {
            titleStuff = [metaData valueForKeyPath:@"metadata.StreamTitle"];
        }
    
    if (_playingFromFav && [_currentFav ignoreDoubleNotifications] && [_currentFav fetchArt]) {
        if ([titleStuff isEqualToString:_trackTitle]) return;
    }
    _trackTitle = titleStuff;
    _trackArtist = _alternativeTitle;
    if (!_trackTitle || [_trackTitle isEqualToString:@""]) {
        _trackTitle = @"Internet radio or relay";
    } else {
        if (_isRecording) {
            [self writeAudacityLabel];
        }
    }

    if (!_trackArtist) {
        if (_lastFavoriteMenuSelection) {
            _trackArtist = [_lastFavoriteMenuSelection title];
        } else {
            _trackArtist = @"Plectrum Radio";
        }
    }
    if ([_trackArtist isEqualToString:@""]) {
        if (_playingFromURL) {
            _trackArtist = @"URL Stream";
        } else {
            _trackArtist = @"Unknown Station";
        }
    }

    if ([_alternativeTitle isNotEqualTo:@""]) _trackArtist = _alternativeTitle;
    if ([_trackTitle isEqualToString:@""] || [_trackTitle isEqualToString:@"-"] || [_trackTitle isEqualToString:@" - "]) _trackTitle = @"Internet radio or relay";
    _trackAlbum = _radioController.url.absoluteString;
    if (!_trackAlbum) _trackAlbum = @"--";
    _plLastStationName = [_trackArtist copy];
    _plLastStreamTitle = [_trackTitle copy];
    _plLastDisplayedURL = [_trackAlbum copy];
    _trackLength = @"";
    _trackArt = _plDisplayArt;
    [_coverArtWindow setCloseArtWindowEventReceived:NO];
    if (_lastFavoriteMenuSelection) {
        [_lastFavoriteMenuSelection setState:NSOnState];
    }
    [self getTrackInfoFromPlectrum];
    _trackArt = _plDisplayArt;
    if (![_coverArtWindow demoMode]) {
        if (_playingFromFav && _fetchesArtwork) {
            if ([_currentFav deferNotification]) _defermentRetryCount = 0;
            [self updateRadioArtworkAsynchronouslyAndContinueWithSelector:@selector(reupdateRadioArtwork)];
        } else {
            [self continuePlectrumUpdateTrackInfo];
        }
    } else {
        if (_playingFromFav && _fetchesArtwork) {
            [self updateRadioArtworkAsynchronouslyAndContinueWithSelector:@selector(refetchRadioArtwork)];
        } else {
            [self continuePlectrumUpdateTrackInfo];
        }
    }
}

- (void)continuePlectrumUpdateTrackInfo {
    if (![_coverArtWindow demoMode]) {
        _notificationDeferred = NO;
        [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
    } else {
        _notificationDeferred = YES;
    }
    _currentPlayerStatus = @"Playing";
    [_coverArtWindow receiveStatus:[self getStatus]];
    [self getTrackInfo];
    if (_menuIsOpen) {
        [self collectTrackInfoForMenu];
        [_miTrackView setView:_siv];
    }
    if (![_favTableView isTryingOut]) {
        if (!_isMuted) {
            switch (_radioButton) {
                case 0:
                    [self showNCNotification];
                    break;
                case 1:
                    [self showGrowlNotification];
                    break;
                case 2:
                    [self showPlectrumNotification];
                    break;
                default:
                    break;
            }
        }
    }
    [self setArtWindowDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
    if ([[_statusItem image] isNotEqualTo:[NSImage imageNamed:@"ray-dee-yo"]]) {
        [self installRaydio];
    }
    [_miSearchWeb setEnabled:YES];
    [_miSearchBC setEnabled:YES];
}

- (void)endCoverArtDeferment {
    _notificationDeferred = NO;
    [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
}

- (void)restoreRadioArtwork {
    if (_playingFromFav && _fetchesArtwork) {
        [self fetchRadioArtworkAsynchronouslyWithCompletionHandler:^(NSImage *image) {
            if (image) {
                self->_trackArt = image;
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [self->_coverArtWindow setTrackTitle:self->_trackTitle trackArtist:self->_trackArtist trackArt:self->_trackArt];
                });
            }
        }];
    }
}

- (NSString *)getTrackTitleFromAlternativeSource:(NSString *)theSource {
    NSURL *theURL = [NSURL URLWithString:theSource];
    if (!theURL) {
        NSLog(@"PLECTRUM: Could not fetch alternative title info - URL appears to be invalid.");
        return @"";
    }
    NSError *error;
    NSString *theString = [NSString stringWithContentsOfURL:theURL encoding:NSUTF8StringEncoding error:&error];
    if (!theString || [theString isEqualToString:@""]) {
        NSLog(@"PLECTRUM: Could not fetch alternative title info.");
        NSLog(@"PLECTRUM: Error info: %ld - %@", (long)[error code], [error localizedDescription]);
        return @"";
    }
    if ([theString length] > 128) theString = [NSString stringWithFormat:@"%@...", [theString substringWithRange:NSMakeRange(0, 128)]];
    return theString;
}

- (void)showNowPlayingInfo {
    NSString *whichIsRunning = [self getPlayingApplication];
    if ([whichIsRunning isEqualToString:@"iTunes"]) {
        switch (_radioButton) {
            case 0:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showNCNotification];
                break;
                
            case 1:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showGrowlNowPlayingNotification];
                break;
                
            case 2:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_iTunes frontmost])) return;
                [self showPlectrumNowPlayingNotification];
                break;
                
            case 3:
                [self getTrackInfo];
                break;
                
            default:
                break;
        }
        return;
    } else if ([whichIsRunning isEqualToString:@"Music"]) {
        switch (_radioButton) {
            case 0:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showNCNotification];
                break;
                
            case 1:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showGrowlNowPlayingNotification];
                break;
                
            case 2:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Music frontmost])) return;
                [self showPlectrumNowPlayingNotification];
                break;
                
            case 3:
                [self getTrackInfo];
                break;
                
            default:
                break;
        }
        return;
    } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
        switch (_radioButton) {
            case 0:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Swinsian frontmost])) return;
                [self showNCNotification];
                break;
                
            case 1:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Swinsian frontmost])) return;
                [self showGrowlNowPlayingNotification];
                break;
                
            case 2:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Swinsian frontmost])) return;
                [self showPlectrumNowPlayingNotification];
                break;
                
            case 3:
                [self getTrackInfo];
                break;
                
            default:
                break;
        }
        return;
    } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
        switch (_radioButton) {
            case 0:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showNCNotification];
                break;
                
            case 1:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showGrowlNowPlayingNotification];
                break;
                
            case 2:
                [self getTrackInfo];
                if ((!_showAlsoIfFrontmost) && ([_Spotify frontmost])) return;
                [self showPlectrumNowPlayingNotification];
                break;
                
            case 3:
                [self getTrackInfo];
                break;
                
            default:
                break;
        }
    } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
        _trackLength = @"";
        switch (_radioButton) {
            case 0:
                [self getTrackInfo];
                [self showNCNotification];
                break;
                
            case 1:
                [self getTrackInfo];
                [self showGrowlNowPlayingNotification];
                break;
                
            case 2:
                [self getTrackInfo];
                [self showPlectrumNowPlayingNotification];
                break;
                
            case 3:
                [self getTrackInfo];
                break;
                
            default:
                break;
        }
    } else {
        return;
    }
    [self setArtWindowDataAndDisplayIfNeeded];
    [_coverArtWindow receiveStatus:[self getStatus]];
}

- (void)showNCNotification {
    if (_trackTitle == nil) return;
    if (_notification) {
        if ([[_notification identifier] hasPrefix:@"TRANSIENT-"]) {
            [_notificationCenter removeDeliveredNotification:_notification];
        }
     }
    _notification = [[NSUserNotification alloc] init];
    if (![_trackLength isEqualToString:@""] || [_trackInfoSource isEqualToString:@"Swinsian"]) {
        [_notification setTitle:[NSString stringWithFormat:@"%@ (%@)", _trackTitle, _trackLength]];
    } else {
        if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
            if (_playingFromFav && _fetchesArtwork) {
                if (![_coverArtWindow demoMode]) {
                    _trackArt = [[_coverArtWindow coverArt] image];
                }
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = [NSImage imageNamed:@"radio"];
        }
        [_notification setTitle:_trackTitle];
    }
    NSImage *dispArt;
    if ([_trackArt isEqualTo:[NSImage imageNamed:@"stream"]]) {
        dispArt = [NSImage imageNamed:@"radio"];
    } else {
        dispArt = [_trackArt copy];
    }
    [_notification setSubtitle:_trackArtist];
    [_notification setInformativeText:_trackAlbum];
    if (_trackArt) [_notification setContentImage:dispArt];
    [_sivArt setImage:dispArt];
    [_notification setSoundName:nil];
    [_notification setIdentifier:[PLUtilities createUUID]];
    [_notificationCenter deliverNotification:_notification];
}

- (void)showGrowlNotification {
    NSString *trackDescription;
    if (_trackTitle == nil) return;
    if (![_trackLength isEqualToString:@""] || [_trackInfoSource isEqualToString:@"Swinsian"]) {
        trackDescription = [NSString stringWithFormat:@"%@\n%@\n%@\n%@", _trackArtist, _trackAlbum, _trackLength, _trackRating];
        if (_trackLoved) {
            trackDescription = [NSString stringWithFormat:@"%@  |  ❤︎", trackDescription];
        }
    } else {
        if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
            if (_playingFromFav && _fetchesArtwork) {
                if (![_coverArtWindow demoMode]) {
                    _trackArt = [[_coverArtWindow coverArt] image];
                }
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = [NSImage imageNamed:@"radio"];
        }
        [_sivArt setImage:_trackArt];
        trackDescription = [NSString stringWithFormat:@"%@\n%@", _trackArtist, _trackAlbum];
    }
    NSSize iconSize = NSMakeSize(64, 64);
    NSImage *oldIcon;
    if ([_trackArt isEqualTo:[NSImage imageNamed:@"stream"]]) {
        oldIcon = [NSImage imageNamed:@"radio"];
    } else {
        oldIcon = [_trackArt copy];
    }
    NSImage *newIcon = [[NSImage alloc] initWithSize:iconSize];
    [newIcon lockFocus];
    [oldIcon setSize:iconSize];
    [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
    [oldIcon drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, iconSize.width, iconSize.height) operation:NSCompositeCopy fraction:1.0];
    [newIcon unlockFocus];
    [GrowlApplicationBridge notifyWithTitle:_trackTitle description:trackDescription notificationName:@"Song Changed" iconData:[newIcon TIFFRepresentation] priority:0 isSticky:NO clickContext:@"context" identifier:@"PiKk"];
}

- (void)showGrowlNowPlayingNotification {
    if (_trackTitle == nil) return;
    NSString *trackDescription;
    if (![_trackLength isEqualToString:@""] || [_trackInfoSource isEqualToString:@"Swinsian"]) {
        trackDescription = [NSString stringWithFormat:@"%@\n%@\n%@ (currently at %@)\n%@", _trackArtist, _trackAlbum, _trackLength, _trackElapsed, _trackRating];
        if (_trackLoved) {
            trackDescription = [NSString stringWithFormat:@"%@  |  ❤︎", trackDescription];
        }
    } else {
        if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
            if (_playingFromFav && _fetchesArtwork) {
                if (![_coverArtWindow demoMode]) {
                    _trackArt = [[_coverArtWindow coverArt] image];
                }
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = [NSImage imageNamed:@"radio"];
        }
        [_sivArt setImage:_trackArt];
        trackDescription = [NSString stringWithFormat:@"%@\n%@", _trackArtist, _trackAlbum];
    }
    NSSize iconSize = NSMakeSize(64, 64);
    NSImage *oldIcon;
    if ([_trackArt isEqualTo:[NSImage imageNamed:@"stream"]]) {
        oldIcon = [NSImage imageNamed:@"radio"];
    } else {
        oldIcon = [_trackArt copy];
    }
    NSImage *newIcon = [[NSImage alloc] initWithSize:iconSize];
    [newIcon lockFocus];
    [oldIcon setSize:iconSize];
    [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
    [oldIcon drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, iconSize.width, iconSize.height) operation:NSCompositeCopy fraction:1.0];
    [newIcon unlockFocus];
    [GrowlApplicationBridge notifyWithTitle:_trackTitle description:trackDescription notificationName:@"Now Playing" iconData:[newIcon TIFFRepresentation] priority:0 isSticky:NO clickContext:@"context" identifier:@"PiKk"];
}

- (void)showPlectrumNotification {
    NSImage *trackArt;
    if (_trackTitle == nil) return;
    if (![_trackLength isEqualToString:@""] || [_trackInfoSource isEqualToString:@"Swinsian"]) {
        if (_trackArt) {
            trackArt = _trackArt;
        } else {
            trackArt = [NSImage imageNamed:@"appicon"];
        }
        NSString *newTrackRating = _trackRating;
        if (_trackLoved) {
            newTrackRating = [NSString stringWithFormat:@"%@  |  ❤︎", newTrackRating];
        }
        [_PLNotificationCenter showNotificationWithTitle:_trackTitle artwork:trackArt artist:_trackArtist album:_trackAlbum time:_trackLength rating:newTrackRating];
        return;
    } else {
        if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
            if (_playingFromFav && _fetchesArtwork) {
                if (![_coverArtWindow demoMode]) {
                    _trackArt = [[_coverArtWindow coverArt] image];
                }
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = [NSImage imageNamed:@"radio"];
        }
        NSImage *image = _trackArt;
        if ([image isEqualTo:[NSImage imageNamed:@"stream"]]) image = [NSImage imageNamed:@"radio"];
        [_PLNotificationCenter showRadioNotificationWithStation:_trackTitle nowPlaying:_trackArtist stationURL:_trackAlbum stationArt:image];
        [_sivArt setImage:image];
    }
}

- (void)showPlectrumNowPlayingNotification {
    NSImage *trackArt;
    if (_trackTitle == nil) return;
    if (_trackArt) {
        trackArt = _trackArt;
    } else {
        trackArt = [NSImage imageNamed:@"appicon"];
    }
    if (![_trackLength isEqualToString:@""] || [_trackInfoSource isEqualToString:@"Swinsian"]) {
        NSString *timeString = [NSString stringWithFormat:@"%@ (currently at %@)", _trackLength, _trackElapsed];
        NSString *newTrackRating = _trackRating;
        if (_trackLoved) {
            newTrackRating = [NSString stringWithFormat:@"%@  |  ❤︎", newTrackRating];
        }
        [_PLNotificationCenter showNotificationWithTitle:_trackTitle artwork:trackArt artist:_trackArtist album:_trackAlbum time:timeString rating:newTrackRating];
    } else {
        if ([_trackInfoSource isEqualToString:@"Plectrum"]) {
            if (_playingFromFav && _fetchesArtwork) {
                if (![_coverArtWindow demoMode]) {
                    _trackArt = [[_coverArtWindow coverArt] image];
                }
            } else {
                _trackArt = _plDisplayArt;
            }
        } else {
            _trackArt = [NSImage imageNamed:@"radio"];
        }
        NSImage *image = _trackArt;
        if ([image isEqualTo:[NSImage imageNamed:@"stream"]]) image = [NSImage imageNamed:@"radio"];
        [_PLNotificationCenter showRadioNotificationWithStation:_trackTitle nowPlaying:_trackArtist stationURL:_trackAlbum stationArt:image];
        [_sivArt setImage:image];
    }
}

- (void)setArtWindowDataAndDisplayIfNeeded {
    [_coverArtWindow setTrackTitle:_trackTitle trackArtist:_trackArtist trackArt:_trackArt];
    if ([_coverArtWindow artWindowEnabled] && [_coverArtWindow artWindowVisible]) {
        if (![_coverArtWindow artWindowOnScreen]) {
            [_coverArtWindow fadeIn];
        }
    }
}

- (void)setArtWindowDisplayIfNeeded {
    if ([_coverArtWindow artWindowEnabled] && [_coverArtWindow artWindowVisible]) {
        if (![_coverArtWindow artWindowOnScreen]) {
            [_coverArtWindow fadeIn];
        }
    }
}


#pragma mark - Collecting Track Info

- (void)getTrackInfo {
    NSString *whichIsRunning = _playingApplication;
    if ([whichIsRunning isEqualToString:@"iTunes"]) {
        [self getTrackInfoFromiTunes];
        return;
    } else if ([whichIsRunning isEqualToString:@"Music"]) {
        [self getTrackInfoFromMusic];
        return;
    } else if ([whichIsRunning isEqualToString:@"Swinsian"]) {
        [self getTrackInfoFromSwinsian];
        return;
    } else if ([whichIsRunning isEqualToString:@"Spotify"]) {
        [self getTrackInfoFromSpotify];
         return;
    } else if ([whichIsRunning isEqualToString:@"Plectrum"]) {
        [self getTrackInfoFromPlectrum];
    }
}

- (void)getTrackInfoFromiTunes {
    if (![_iTunes isRunning]) return;
    iTunesTrack *t = [_iTunes currentTrack];
    if ([[t kind] isEqualToString:@"Internetaudiostream"]) {
        _trackInfoSource = @"iTunesRadio";
        _trackTitle = [t name];
        if ([_trackTitle isEqualToString:@""] || !_trackTitle) _trackTitle = @"Radio";
        _trackArtist = [_iTunes currentStreamTitle];
        if ([_trackArtist isEqualToString:@""] || !_trackArtist) _trackArtist = @"Internet broadcast or relay";
        _trackAlbum = [_iTunes currentStreamURL];
        if ([_trackAlbum isEqualToString:@""] || !_trackAlbum) _trackAlbum = @"--";
        _trackLength = @"";
        _trackElapsed = @"";
        _trackArt = [NSImage imageNamed:@"stream"];
    } else {
        _trackTitle = [t name];
        if ([_trackTitle isEqualToString:@""]) _trackTitle = @"Unknown Track";
        _trackArtist = [t artist];
        if ([_trackArtist isEqualToString:@""]) _trackArtist = @"Unknown Artist";
        _trackAlbum = [t album];
        if ([_trackAlbum isEqualToString:@""]) _trackAlbum = @"Unknown Album";
        _trackLength = [t time];
        NSInteger position = [_iTunes playerPosition];
        NSInteger hr = (NSInteger)position / 3600;
        NSInteger mn = (NSInteger)(position - (hr * 60)) / 60;
        NSInteger sc = (NSInteger)position % 60;
        if (hr > 0) {
            _trackElapsed = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
        } else {
            _trackElapsed = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
        }
        NSInteger p = [[_iTunes currentTrack] rating] / 10;
        if (!_appleAppHalfStarsEnabled) p -= p % 2;
        _trackRating = [_tuneRating objectAtIndex:p];
        [self setStars:p];
        BOOL newLoved = [[_iTunes currentTrack] loved];
        if (!(_trackLoved == newLoved)) {
            [self setLovedness:newLoved];
        }
        _trackLoved = newLoved;
        iTunesArtwork *newTrackArt = [[t artworks] objectAtIndex:0];
        _trackArt = [newTrackArt data];
        if (![_trackArt isKindOfClass:[NSImage class]]) _trackArt = [[NSImage alloc] initWithData:[newTrackArt rawData]];
        _trackInfoSource = @"iTunes";
    }
}

- (void)getTrackInfoFromMusic {
    if (![_Music isRunning]) return;
    NSString *k = [self getStringFromMusicWithParameter:@"kind of current track"];
    if ([k hasPrefix:@"I"]) {
        _trackInfoSource = @"MusicRadio";
        _trackTitle = [self getStringFromMusicWithParameter:@"name of current track"];
        if ([_trackTitle isEqualToString:@""] || !_trackTitle) _trackTitle = @"Radio";
        _trackArtist = [_Music currentStreamTitle];
        if ([_trackArtist isEqualToString:@""] || !_trackArtist) _trackArtist = @"Internet broadcast or relay";
        _trackAlbum = [_Music currentStreamURL];
        if ([_trackAlbum isEqualToString:@""] || !_trackAlbum) _trackAlbum = @"--";
        _trackLength = @"";
        _trackElapsed = @"";
        _trackArt = [NSImage imageNamed:@"stream"];
    } else {
        _trackTitle = [self getStringFromMusicWithParameter:@"name of current track"];
        if ([_trackTitle isEqualToString:@""]) _trackTitle = @"Unknown Track";
        _trackArtist = [self getStringFromMusicWithParameter:@"artist of current track"];
        if ([_trackArtist isEqualToString:@""]) _trackArtist = @"Unknown Artist";
        _trackAlbum = [self getStringFromMusicWithParameter:@"album of current track"];
        if ([_trackAlbum isEqualToString:@""]) _trackAlbum = @"Unknown Album";
        _trackLength = [self getStringFromMusicWithParameter:@"time of current track"];
        NSInteger position = [self getFloatFromMusicWithParameter:@"player position"];
        NSInteger hr = (NSInteger)position / 3600;
        NSInteger mn = (NSInteger)(position - (hr * 60)) / 60;
        NSInteger sc = (NSInteger)position % 60;
        if (hr > 0) {
            _trackElapsed = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
        } else {
            _trackElapsed = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
        }
        NSInteger p = [self getIntegerFromMusicWithParameter:@"rating of current track"] / 10;
        if (!_appleAppHalfStarsEnabled) p -= p % 2;
        _trackRating = [_tuneRating objectAtIndex:p];
        [self setStars:p];
        BOOL newLoved = [self getBoolFromMusicWithParameter:@"loved of current track"];
        if (!(_trackLoved == newLoved)) {
            [self setLovedness:newLoved];
        }
        _trackLoved = newLoved;
        NSImage *newTrackArt = [self getImageFromMusic];
        if (!newTrackArt) newTrackArt = [NSImage imageNamed:@"file"];
        _trackArt = newTrackArt;
        _trackInfoSource = @"Music";
    }
}

- (NSString *)getStringFromMusicWithParameter:(NSString *)string {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", string];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem retrieving string value from Music.");
        NSLog(@"PLECTRUM: Supplied parameter: %@", string);
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        return nil;
    }
    return [returnValue stringValue];
}

- (NSInteger)getIntegerFromMusicWithParameter:(NSString *)string {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", string];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem retrieving integer value from Music.");
        NSLog(@"PLECTRUM: Supplied parameter: %@", string);
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        return 0;
    }
    return [[returnValue stringValue] integerValue];
}

- (float)getFloatFromMusicWithParameter:(NSString *)string {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", string];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem retrieving float value from Music.");
        NSLog(@"PLECTRUM: Supplied parameter: %@", string);
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        return 0.0f;
    }
    return [[returnValue stringValue] floatValue];
}

- (BOOL)getBoolFromMusicWithParameter:(NSString *)string {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", string];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem retrieving boolean value from Music.");
        NSLog(@"PLECTRUM: Supplied parameter: %@", string);
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        return NO;
    }
    return [returnValue booleanValue];
}

- (NSImage *)getImageFromMusic {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", @"raw data of artwork 1 of artworks of current track"];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem retrieving image from Music.");
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        return nil;
    }
    if ([returnValue data]) {
        return [[NSImage alloc] initWithData:[returnValue data]];
    } else {
        return nil;
    }
}

- (void)executeAppleScriptCommandInMusic:(NSString *)string {
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.Music\"\n%@\nend tell\n", string];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if (errorDict) {
        NSLog(@"PLECTRUM: Problem executing AppleScript in Music.");
        NSLog(@"PLECTRUM: Supplied command: %@", string);
        NSLog(@"PLECTRUM: Error: %@", errorDict);
        NSLog(@"PLECTRUM: Returned value: %@", returnValue);
    }
}

- (void)getTrackInfoFromSwinsian {
    if (![_Swinsian isRunning]) return;
    NSImage *currentTrackArt = [_trackArt copy];
    SwinsianTrack *t = [_Swinsian currentTrack];
    _trackTitle = [t name];
    if ([_trackTitle isEqualToString:@""]) _trackTitle = @"Unknown Track";
    _trackArtist = [t artist];
    if ([_trackArtist isEqualToString:@""]) _trackArtist = @"Unknown Artist";
    _trackAlbum = [t album];
    if ([_trackAlbum isEqualToString:@""]) _trackAlbum = @"Unknown Album";
    NSInteger duration = (NSInteger)[t duration];
    NSInteger hr = (NSInteger)duration / 3600;
    NSInteger mn = (NSInteger)(duration - (hr * 60)) / 60;
    NSInteger sc = (NSInteger)duration % 60;
    if (hr > 0) {
        _trackLength = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
    } else {
        _trackLength = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
    }
    NSInteger position = [_Swinsian playerPosition];
    hr = (NSInteger)position / 3600;
    mn = (NSInteger)(position - (hr * 60)) / 60;
    sc = (NSInteger)position % 60;
    if (hr > 0) {
        _trackElapsed = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
    } else {
        _trackElapsed = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
    }
    NSInteger p = [[t rating] floatValue] * 2;
    _trackRating = [_tuneRating objectAtIndex:p];
    [self setStars:p];
    _trackLoved = NO;
    NSString *swinsianUUID = [t id];
    if (!_swinsianUUID) _swinsianUUID = swinsianUUID;
    id artRef = [t albumArt];
    if ([[artRef class] isEqual:[NSAppleEventDescriptor class]]) {
        NSAppleEventDescriptor *artEvent = (NSAppleEventDescriptor *)artRef;
        NSData *artData = [artEvent data];
        _trackArt = [[NSImage alloc] initWithData:artData];
        _swinsianUUID = swinsianUUID;
    } else if ([[artRef class] isEqual:[NSImage class]]) {
        _trackArt = artRef;
        _swinsianUUID = swinsianUUID;
    } else {
        if (![swinsianUUID isEqualToString:_swinsianUUID]) {
            _trackArt = [NSImage imageNamed:@"file"];
            _swinsianUUID = swinsianUUID;
        } else {
            _trackArt = currentTrackArt;
        }
    }
    _trackInfoSource = @"Swinsian";
}

- (void)getTrackInfoFromSpotify {
    if (![_Spotify isRunning]) return;
    SpotifyTrack *t = [_Spotify currentTrack];
    _trackTitle = [t name];
    if ([_trackTitle isEqualToString:@""]) _trackTitle = @"Unknown Track";
    _trackArtist = [t artist];
    if ([_trackArtist isEqualToString:@""]) _trackArtist = @"Unknown Artist";
    _trackAlbum = [t album];
    if ([_trackAlbum isEqualToString:@""]) _trackAlbum = @"Unknown Album";
    //Spotify has removed identification of advertisements, leaving the code in, it does not harm and it might start working again some day.
    if ([_trackTitle isEqualToString:@"Spotify"] || [_trackTitle isEqualToString:@"Advertisement"]) {
        _trackTitle = @"Advertisement";
        _trackArtist = @"We'll be right back...";
        _trackAlbum = @"after the break";
    }
    NSInteger duration = [[_Spotify currentTrack] duration] / 1000;
    NSInteger hr = (NSInteger)duration / 3600;
    NSInteger mn = (NSInteger)(duration - (hr * 60)) / 60;
    NSInteger sc = (NSInteger)duration % 60;
    if (hr > 0) {
        _trackLength = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
    } else {
        _trackLength = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
    }
    NSInteger position = [_Spotify playerPosition];
    hr = (NSInteger)position / 3600;
    mn = (NSInteger)(position - (hr * 60)) / 60;
    sc = (NSInteger)position % 60;
    if (hr > 0) {
        _trackElapsed = [NSString stringWithFormat:@"%0d:%02d:%02d", (int)hr, (int)mn, (int)sc];
    } else {
        _trackElapsed = [NSString stringWithFormat:@"%0d:%02d", (int)mn, (int)sc];
    }
    NSInteger p = [[_Spotify currentTrack] popularity] / 20;
    _trackRating = [_spotRating objectAtIndex:p];
    _trackLoved = NO;
    _trackArt = [[NSImage alloc] initWithContentsOfURL:[NSURL URLWithString:[[_Spotify currentTrack] artworkUrl]]];
    _trackInfoSource = @"Spotify";
}

 - (void)getTrackInfoFromPlectrum {
     _trackTitle = [_plLastStreamTitle copy];
     _trackArtist = [_plLastStationName copy];
     _trackAlbum = [_plLastDisplayedURL copy];
     _trackInfoSource = @"Plectrum";
     _currentPlayerStatus = @"Playing";
}


#pragma mark - Login Items

- (BOOL)addToStartupItems:(BOOL)addOrNotFlag {
    return (SMLoginItemSetEnabled((__bridge CFStringRef)@"nl.kinoko-house.Plectrum-Helper", addOrNotFlag));
}


#pragma mark - BPM Inspector methods

- (void)changeThePics {
    [self changeButtonPicOnly];
    if (_picToggle == YES) {
        [_leftLamp setImage:_lampOnImage];
        [_rightLamp setImage: _lampOffImage];
        _picToggle = NO;
    } else {
        [_leftLamp setImage:_lampOffImage];
        [_rightLamp setImage:_lampOnImage];
        _picToggle = YES;
    }
}

- (void)changeButtonPicOnly {
    [_leftLamp setImage:_lampOffImage];
    [_rightLamp setImage:_lampOffImage];
    if ([[_theBigButton image] isEqual:_altDrumImage]) {
        [_theBigButton setImage: _regDrumImage];
        _picToggle = YES;
    } else {
        [_theBigButton setImage: _altDrumImage];
        _picToggle = NO;
    }
}

- (void)increaseBPM {
    if (_beatsPerMinute < 999) {
        _beatsPerMinute++;
        [_bpmMeter setStringValue: [NSString stringWithFormat:@"%u bpm", _beatsPerMinute]];
    }
}

- (void)decreaseBPM {
    if (_beatsPerMinute > 1) {
        _beatsPerMinute--;
        [_bpmMeter setStringValue: [NSString stringWithFormat:@"%u bpm", _beatsPerMinute]];
    }
}

- (void)applyNewBPM {
    _timeInterval = 60.0 / _beatsPerMinute;
    [self initBlinkTimer:_timeInterval];
}

- (void)buildColorTable {
    int i;
    NSArray *theLiteTable = @[@[@163, @163, @163],
                              @[@153, @153, @153],
                              @[@128, @128, @128],
                              @[@102, @102, @102],
                              @[@77,  @77,  @77 ],
                              @[@51,  @51,  @51 ],
                              @[@26,  @26,  @26 ],
                              @[@0,   @0,   @0  ],
                              @[@0,   @2,   @26 ],
                              @[@0,   @4,   @51 ],
                              @[@0,   @6,   @76 ],
                              @[@0,   @8,   @102],
                              @[@0,   @11,  @128],
                              @[@0,   @13,  @153],
                              @[@0,   @15,  @178],
                              @[@0,   @17,  @204],
                              ];
    NSArray *theDarkTable = @[@[@51,  @51,  @51 ],
                              @[@77,  @77,  @77 ],
                              @[@102, @102, @102],
                              @[@128, @128, @128],
                              @[@153, @153, @153],
                              @[@163, @163, @163],
                              @[@188, @188, @188],
                              @[@236, @236, @236],
                              @[@236, @236, @184],
                              @[@241, @241, @163],
                              @[@246, @246, @153],
                              @[@251, @251, @128],
                              @[@255, @255, @102],
                              @[@255, @255, @77 ],
                              @[@255, @255, @51 ],
                              @[@255, @255, @26 ],
                              ];
    unsigned long tableSize = [theLiteTable count];
    for (i = 0; i < tableSize; i++) {
        NSArray *theLiteColor = [theLiteTable objectAtIndex:i];
        NSArray *theDarkColor = [theDarkTable objectAtIndex:i];
        double theLiteRed = [(NSNumber *)[theLiteColor objectAtIndex:0] floatValue] / 256.0;
        double theLiteGreen = [(NSNumber *)[theLiteColor objectAtIndex:1] floatValue] / 256.0;
        double theLiteBlue = [(NSNumber *)[theLiteColor objectAtIndex:2] floatValue] / 256.0;
        double theDarkRed = [(NSNumber *)[theDarkColor objectAtIndex:0] floatValue] / 256.0;
        double theDarkGreen = [(NSNumber *)[theDarkColor objectAtIndex:1] floatValue] / 256.0;
        double theDarkBlue = [(NSNumber *)[theDarkColor objectAtIndex:2] floatValue] / 256.0;
        NSColor *theLiteNSColor = [NSColor colorWithCalibratedRed:theLiteRed green:theLiteGreen blue:theLiteBlue alpha:1.0];
        NSColor *theDarkNSColor = [NSColor colorWithCalibratedRed:theDarkRed green:theDarkGreen blue:theDarkBlue alpha:1.0];
        [_theLiteColorTable addObject:theLiteNSColor];
        [_theDarkColorTable addObject:theDarkNSColor];
        
    }
}

- (void)initBlinkTimer:(NSTimeInterval)interval {
    if (interval == 0) interval = _timeInterval;
    if (_blinkTimer != nil) [self stopBlinkTimer];
    _picToggle = NO;
    _timeInterval = interval;
    _blinkTimer = [NSTimer scheduledTimerWithTimeInterval:_timeInterval target:self selector:@selector(changeThePics) userInfo:nil repeats:YES];
}

- (void)initDelayTimer:(NSTimeInterval)interval {
    if (_delayTimer != nil) [self stopDelayTimer];
    interval = interval * 2;
    if (interval > 2.5) interval = 2.5;
    _delayTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(fireDelayTimer) userInfo:nil repeats:YES];
}

- (void)stopBlinkTimer {
    if ([_blinkTimer isValid]) [_blinkTimer invalidate];
    _blinkTimer = nil;
}

- (void)stopDelayTimer {
    if ([_delayTimer isValid]) [_delayTimer invalidate];
    _delayTimer = nil;
}

- (void)fireDelayTimer {
    [self stopDelayTimer];
    [self initBlinkTimer:_currentAverage];
    _lastInterval = 0;
}

- (NSPoint)getDefaultFloaterPosition {
    NSScreen *screen = [NSScreen mainScreen];
    NSRect screenRect = [screen visibleFrame];
    float x = (screenRect.size.width / 2) - 45;
    float y = (screenRect.size.height / 2) - 63;
    return NSMakePoint(x, y);
}

- (void)showInspector {
    [self initBlinkTimer:_timeInterval];
    [_bpmInspector makeKeyAndOrderFront:nil];
    [_miBPMInspector setTitle:@"Hide BPM Inspector"];
    [_iTunes activate];
}

- (void)hideInspector {
    if (_blinkTimer != nil) [self stopBlinkTimer];
    if (_delayTimer != nil) [self stopDelayTimer];
    [_bpmInspector orderOut:nil];
    [_miBPMInspector setTitle:@"Show BPM Inspector"];
    if (![_iTunes isRunning]) {
        [_miBPMInspector setHidden:YES];
        if ([_miArtWindow isHidden]) {
            [_miBPMSeparator setHidden:YES];
        } else {
            [_miBPMSeparator setHidden:NO];
        }
    }
}


#pragma mark - BPM Inspector IBActions

- (IBAction)toggleBPMInspector:(id)sender {
    if ([_bpmInspector isVisible]) {
        [self hideInspector];
    } else {
        [self showInspector];
    }
}

- (IBAction)gotClicked:(id)sender {
    double myInterval = [NSDate timeIntervalSinceReferenceDate];
    if (_lastInterval == 0) {
        [self changeButtonPicOnly];
        [_timeList removeAllObjects];
        _lastInterval = myInterval;
        _colorIndex = 0;
        [self initDelayTimer:1.0];
    } else {
        if (_blinkTimer != nil) [self stopBlinkTimer];
        if (_delayTimer != nil) [self stopDelayTimer];
        [self changeButtonPicOnly];
        double trueInterval = myInterval - _lastInterval;
        [_timeList addObject:[NSNumber numberWithFloat:trueInterval]];
        if ([_timeList count] > _maxTimeList) [_timeList removeObjectAtIndex:0];
        double av = 0.0;
        for(int i = 0; i < [_timeList count]; i++) {
            double currentValue = [(NSNumber *)[_timeList objectAtIndex:i] floatValue];
            av = av + currentValue;
        }
        _currentAverage = av / [_timeList count];
        _beatsPerMinute = 60.0 / _currentAverage;
        double myPrecisionSize = _precisionSize;
        if ((trueInterval > 0.2) && (trueInterval < 0.3)) myPrecisionSize = myPrecisionSize * 1.5;
        if (trueInterval <= 0.2) myPrecisionSize = myPrecisionSize * 2.0;
        double belowValue = _currentAverage - (_currentAverage * myPrecisionSize);
        double aboveValue = _currentAverage + (_currentAverage * myPrecisionSize);
        double lowerValue = _currentAverage - (_currentAverage * (myPrecisionSize * 2));
        double higherValue = _currentAverage + (_currentAverage * (myPrecisionSize * 2));
        double evenLowerValue = _currentAverage - (_currentAverage * (myPrecisionSize * 4));
        double evenHigherValue = _currentAverage + (_currentAverage * (myPrecisionSize * 4));
        if ((belowValue < trueInterval) && (trueInterval < aboveValue)) {
            if (_colorIndex < 15) _colorIndex++;
        }
        if (((lowerValue < trueInterval) && (trueInterval < belowValue)) || ((higherValue > trueInterval) && (trueInterval > aboveValue))) {
            if (_colorIndex > 0) _colorIndex--;
        }
        if ((trueInterval < lowerValue) || (trueInterval > higherValue)) {
            if (_colorIndex > 1) _colorIndex = _colorIndex - 2;
        }
        if ((trueInterval < evenLowerValue) || (trueInterval > evenHigherValue)) {
            _colorIndex = 0;
            unsigned long c = [_timeList count];
            if (c > 2) [_timeList removeObjectsInRange: NSMakeRange(0, c - 2)];
        }
        [_bpmMeter setStringValue: [NSString stringWithFormat:@"%u bpm", _beatsPerMinute]];
        [_bpmMeter setTextColor:[_theColorTable objectAtIndex:_colorIndex]];
        _lastInterval = myInterval;
        [self initDelayTimer:trueInterval];
    }
}

- (IBAction)gotSet:(id)sender {
    NSString *appString = @"iTunes";
    if (@available(macOS 10.15, *)) appString = @"Music";
    NSString *scriptCode = [NSString stringWithFormat:@"tell application id \"com.apple.%@\"\nif selection is not {} then\nrepeat with i from 1 to (count of items in selection)\nset bpm of (item i of selection) to %u\nend repeat\nelse\nbeep\nend if\nend tell\n", appString, _beatsPerMinute];
    NSDictionary *errorDict;
    NSAppleEventDescriptor *returnValue = NULL;
    NSAppleScript *scriptObject = [[NSAppleScript alloc] initWithSource:scriptCode];
    returnValue = [scriptObject executeAndReturnError:&errorDict];
    if ([returnValue data].length > 0) {
        NSLog(@"PLECTRUM: Problem setting BPM of selected tracks in iTunes/Music; more info below:");
        NSLog(@"PLECTRUM: %@", errorDict);
    }
}


#pragma mark - Dark Mode Support

- (void)checkDark {
    if ([self darkModeIsActive]) {
        _theColorTable = [_theDarkColorTable copy];
        [_rewBox setHidden:NO];
        [_playBox setHidden:NO];
        [_ffBox setHidden:NO];
        if (_isRecording) {
            [self installRaydio];
        }
        if ([_helpPopUpButton indexOfSelectedItem] == 0) {
            NSBundle *myMainBundle = [NSBundle mainBundle];
            NSString * helpFilePath = [myMainBundle pathForResource:@"tipsandtricks-dm" ofType:@"rtfd"];
            NSRect scrollRect = [[_helpScrollView contentView] documentVisibleRect];
            [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
            [_helpTextView readRTFDFromFile:helpFilePath];
            [_helpScrollView scrollRectToVisible:scrollRect];
            [_helpTextView setNeedsDisplay:YES];
            [_helpScrollView setNeedsDisplay:YES];
        }
    } else {
        _theColorTable = [_theLiteColorTable copy];
        [_rewBox setHidden:YES];
        [_playBox setHidden:YES];
        [_ffBox setHidden:YES];
        if (_isRecording) {
            [self installRaydio];
        }
        if ([_helpPopUpButton indexOfSelectedItem] == 0) {
            NSBundle *myMainBundle = [NSBundle mainBundle];
            NSString * helpFilePath = [myMainBundle pathForResource:@"tipsandtricks-lm" ofType:@"rtfd"];
            NSRect scrollRect = [[_helpScrollView contentView] documentVisibleRect];
            [[_helpTextView layoutManager] setAllowsNonContiguousLayout:YES];
            [_helpTextView readRTFDFromFile:helpFilePath];
            [_helpScrollView scrollRectToVisible:scrollRect];
            [_helpTextView setNeedsDisplay:YES];
            [_helpScrollView setNeedsDisplay:YES];
        }
    }
    [_bpmMeter setTextColor:[_theColorTable objectAtIndex:_colorIndex]];
}

- (BOOL)darkModeIsActive {
    if (@available(macOS 10.14, *)) {
        NSString *osxMode = [[NSUserDefaults standardUserDefaults] stringForKey:@"AppleInterfaceStyle"];
        if (osxMode == nil) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return NO;
    }
}


#pragma mark - Radio Favorites support

- (void)prepareAppSupportFolder {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL isDir;
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
    if (error) {
        NSString *errorDescription = [NSString stringWithFormat:@"An error occurred while accessing the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", [error code],[error localizedDescription]];
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Error while accessing application data" informativeText:errorDescription];
        [[NSApplication sharedApplication] terminate:nil];
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSURL *URLToMyAppSupportDir = [NSURL fileURLWithPath:pathToMyAppSupportDir];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyAppSupportDir isDirectory:&isDir];
    if (folderExists && !isDir) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"File found where a folder was expected" informativeText:@"There is a file with the name \"Plectrum\" in the \"Library/Application Support\" folder of your home folder. Please move this file to a different location and launch the application again. The application will close after you press the OK button."];
        [[NSApplication sharedApplication] terminate:nil];
    }
    if (!folderExists) {
        BOOL directoryCreated = [fileManager createDirectoryAtURL:URLToMyAppSupportDir withIntermediateDirectories:NO attributes:nil error:&error];
        if (!directoryCreated) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            NSString *errorDescription = [NSString stringWithFormat:@"An error occurred while creating the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", [error code],[error localizedDescription]];
            [PLUtilities showAlertWithMessageText:@"Error while creating application support directory" informativeText:errorDescription];
            [[NSApplication sharedApplication] terminate:nil];
        }
    }
}

- (void)loadRadioFavorites {
    id theObject;
    NSError *error;
restart:
    [self prepareAppSupportFolder];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    if (error) {
        NSLog(@"PLECTRUM: Getting app support URL failed.");
        NSLog(@"PLECTRUM: Error: %@", [error localizedDescription]);
        NSLog(@"PLECTRUM: Error: %@", [error localizedFailureReason]);
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Radio.favorites"];
    @try {
        theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:pathToMyAppSupportFile];
    }
    @catch (NSException *e) {
        NSLog(@"PLECTRUM: Problem reading radio favorites?");
        NSLog(@"PLECTRUM: Load Path: %@", pathToMyAppSupportFile);
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Could not load favorites"];
        [alert setInformativeText:[NSString stringWithFormat:@"There was a problem retrieving your favorites from the settings.\n\nError: %@\nReason: %@\n\nYou can try one of the following options: restart Plectrum, try to reload the favorites, or as a last resort or if the problem persists, reset them to the default favorites. You can then later restore them from a backup if you wish. We are very sorry for the inconvenience.", [e name], [e reason]]];
        [alert addButtonWithTitle:@"Reset"];
        [alert addButtonWithTitle:@"Restart"];
        [alert addButtonWithTitle:@"Reload"];
        NSModalResponse response = [alert runModal];
        if (response == NSAlertFirstButtonReturn) {
            goto restart;
        } else if (response == NSAlertSecondButtonReturn) {
            [self restartNow:self];
        } else {
            theObject = nil;
        }
    }
    @try {
        if (!theObject) {
            theObject = nil;
        } else if ([theObject count] < 3) {
            theObject = nil;
        }
    }
    @catch (NSException *e) {
        NSLog(@"PLECTRUM: Problem reading radio favorites?");
        NSLog(@"PLECTRUM: Load Path: %@", pathToAppSupportDir);
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Could not load favorites"];
        [alert setInformativeText:[NSString stringWithFormat:@"There was a problem retrieving your favorites from the settings (possibly because of an incomplete or damaged favorite list).\n\nError: %@\nReason: %@\n\nYou can try one of the following options: restart Plectrum, try to reload the favorites, or as a last resort or if the problem persists, reset them to the default favorites. You can then later restore them from a backup if you wish. We are very sorry for the inconvenience.", [e name], [e reason]]];
        [alert addButtonWithTitle:@"Reset"];
        [alert addButtonWithTitle:@"Restart"];
        [alert addButtonWithTitle:@"Reload"];
        NSModalResponse response = [alert runModal];
        if (response == NSAlertFirstButtonReturn) {
            goto restart;
        } else if (response == NSAlertSecondButtonReturn) {
            [self restartNow:self];
        } else {
            theObject = nil;
        }
    }
    if (!theObject) {
        [self initializeRadioFavorites];
    } else {
        _favList = [theObject objectAtIndex:0];
        _menuList = [theObject objectAtIndex:1];
        _playingURL = [theObject objectAtIndex:2];
    }
}

- (void)sortFavList {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    _favList = [[_favList sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}

- (void)initializeRadioFavorites {
    NSImage *img0, *img1, *img2, *img3, *img4, *img5, *img6, *img7, *img8, *img9;
    NSString *path;
    NSBundle *bundle = [NSBundle mainBundle];
    path = [bundle pathForResource:@"deepdepot" ofType:@"jpg"];
    img0 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"edendeeply" ofType:@"jpg"];
    img1 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"jdvjazz" ofType:@"jpg"];
    img2 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"jdvchill" ofType:@"jpg"];
    img3 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"casiopea" ofType:@"jpg"];
    img4 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"limbik" ofType:@"jpg"];
    img5 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"nwp" ofType:@"jpg"];
    img6 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"capchill" ofType:@"jpg"];
    img7 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"rrchania" ofType:@"jpg"];
    img8 = [[NSImage alloc] initWithContentsOfFile:path];
    path = [bundle pathForResource:@"secretagent" ofType:@"jpg"];
    img9 = [[NSImage alloc] initWithContentsOfFile:path];
    _favList = [[NSMutableArray alloc] initWithObjects:
                [[PLRadioFavorite alloc] initWithName:@"Deep Depot"
                                                genre:@"Deep House"
                                              country:@"Germany"
                                                  url:@"https://stream.laut.fm/deep_depot"
                                              altinfo:@""
                                                image:img0
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@""],
                
                [[PLRadioFavorite alloc] initWithName:@"Edendeeply"
                                                genre:@"Chill"
                                              country:@"Germany"
                                                  url:@"https://stream.laut.fm/edendeeply"
                                              altinfo:@""
                                                image:img1
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@""],
                
                [[PLRadioFavorite alloc] initWithName:@"Jazz de Ville"
                                                genre:@"Jazz"
                                              country:@"Netherlands"
                                                  url:@"https://onair22.xdevel.com/proxy/xautocloud_kkyb_420?mp=/stream"
                                              altinfo:@""
                                                image:img2
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:YES
                                    textTransformType:0
                                               artUrl:@"https://jazzdeville.com/albumart/"
                                      secondaryString:@""
                                               prefix:@"jpg"
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"c55d8846-85e5-496a-a2b7-80228a9ef6c8"],
                
                [[PLRadioFavorite alloc] initWithName:@"Jazz de Ville Chill"
                                                genre:@"Jazz"
                                              country:@"Netherlands"
                                                  url:@"https://onair22.xdevel.com/proxy/xautocloud_td3e_421?mp=/stream"
                                              altinfo:@""
                                                image:img3
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:YES
                                    textTransformType:0
                                               artUrl:@"https://jazzdeville.com/albumart/"
                                      secondaryString:@""
                                               prefix:@"jpg"
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"b67cde9c-64d8-4b9d-ab30-268977330b50"],
                
                [[PLRadioFavorite alloc] initWithName:@"Limbik Frequencies"
                                                genre:@"EDM"
                                              country:@"USA"
                                                  url:@"https://limbikfreq.com:8443/128.mp3"
                                              altinfo:@""
                                                image:img5
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"24123698-dbae-4aa7-98a2-c7d78b7a6096"],
                
                [[PLRadioFavorite alloc] initWithName:@"Nightwave Plaza"
                                                genre:@"Vaporwave"
                                              country:@"USA"
                                                  url:@"http://radio.plaza.one/mp3"
                                              altinfo:@"https://api.plaza.one/status/track"
                                                image:img6
                                             fetchArt:YES
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@"https://api.plaza.one/status/"
                                      secondaryString:@"song.artwork_src"
                                               prefix:@""
                                    deferNotification:YES
                            ignoreDoubleNotifications:YES
                                          stationuuid:@"7cf1c860-9691-11e8-a767-52543be04c81"],
                
                [[PLRadioFavorite alloc] initWithName:@"Nonstop Casiopea"
                                                genre:@"Fusion Jazz"
                                              country:@"Japan"
                                                  url:@"http://hyades.shoutca.st:8551/;"
                                              altinfo:@""
                                                image:img4
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"2b969692-5178-4084-b9b9-fa122808a790"],
                
                [[PLRadioFavorite alloc] initWithName:@"Radio Caprice Chillwave"
                                                genre:@"Chillwave"
                                              country:@"Russia"
                                                  url:@"http://79.111.119.111:8002/chillwave"
                                              altinfo:@""
                                                image:img7
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@""],
                
                [[PLRadioFavorite alloc] initWithName:@"Radio Rebetika Chania"
                                                genre:@"Folk"
                                              country:@"Greece"
                                                  url:@"https://rebetiko.sealabs.net/radio"
                                              altinfo:@""
                                                image:img8
                                             fetchArt:YES
                                            fetchType:YES
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@"https://rebetiko.sealabs.net/radioajax.php"
                                      secondaryString:@"<img style=\"height:68px;vertical-align:middle;\" src=\"*\"/>"
                                               prefix:@"https://rebetiko.sealabs.net/"
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"e87fda89-97d8-4660-b049-0c0b8b84e6f3"],
                
                [[PLRadioFavorite alloc] initWithName:@"Soma Secret Agent"
                                                genre:@"Lounge"
                                              country:@"USA"
                                                  url:@"https://ice.somafm.com/secretagent"
                                              altinfo:@""
                                                image:img9
                                             fetchArt:NO
                                            fetchType:NO
                                        textTransform:NO
                                    textTransformType:0
                                               artUrl:@""
                                      secondaryString:@""
                                               prefix:@""
                                    deferNotification:NO
                            ignoreDoubleNotifications:NO
                                          stationuuid:@"960c7c81-0601-11e8-ae97-52543be04c81"],
                nil];
    [self sortFavList];
    _menuList = [[NSMutableArray alloc] initWithObjects:
                 [_favList objectAtIndex:6],
                 [_favList objectAtIndex:7],
                 [_favList objectAtIndex:1],
                 [_favList objectAtIndex:0],
                 [_favList objectAtIndex:3],
                 [_favList objectAtIndex:4],
                 [_favList objectAtIndex:9],
                 nil];
    _playingURL = [[_favList objectAtIndex:0] url];
    [self saveRadioFavorites];
}

- (void)saveRadioFavorites {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Radio.favorites"];
    NSMutableArray *export = [[NSMutableArray alloc] initWithObjects:
                              _favList,
                              _menuList,
                              _playingURL,
                              nil];
    BOOL success = [NSKeyedArchiver archiveRootObject:export toFile:pathToMyAppSupportFile];
    if (!success) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Could not save radio favorites" informativeText:@"There was an unspecified problem saving the radio favorites list. The previous version of the favorites list should still be in order. Please try to relauch the application, or restore an earlier backup of your favorites list if the problem persists, or contact the developer for further help. We are very sorry for the inconvenience."];
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"favorites"] forFile:pathToMyAppSupportFile options:0];
    }
}

- (void)backupRadioFavorites {
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH-mm-ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *exportName = [NSString stringWithFormat:@"Radio Favorites Backup %@.favorites", dateString];
    __block NSString *thePath;
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:@"Backup favorites"];
    [savePanel setMessage:@"Enter name and location for backup:"];
    [savePanel setNameFieldStringValue:exportName];
    [savePanel setAccessoryView:nil];
    [savePanel setDelegate:nil];
    if (_firstOpen) {
        [savePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    [savePanel beginSheetModalForWindow:_controlWindow completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseCancel) return;
        if ([savePanel URL] != nil) {
            self->_firstOpen = NO;
            thePath = [[savePanel URL] path];
        } else {
            return;
        }
        NSMutableArray *export = [[NSMutableArray alloc] initWithObjects:
                                  [self->_favTableView favList],
                                  [self->_menuTableView menuList],
                                  self->_playingURL,
                                  nil];
        BOOL success = [NSKeyedArchiver archiveRootObject:export toFile:thePath];
        if (!success) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            [PLUtilities showAlertWithMessageText:@"Could not backup radio favorites" informativeText:@"There was an unspecified problem backing up the radio favorites list. The previous version of the favorites list should still be in order. Please try to relauch the application, or restore an earlier backup of your favorites list if the problem persists, or contact the developer for further help. We are very sorry for the inconvenience."];
        } else {
            [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"favorites"] forFile:thePath options:0];
        }
    }];
}

- (void)restoreRadioFavorites {
    __block id theObject;
    __block NSString *thePath;
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setTitle:@"Restore Radio Favorites"];
    [openPanel setMessage:@"Select favorites file to restore:"];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setCanChooseFiles:YES];
    [openPanel setCanChooseDirectories:NO];
    [openPanel setCanCreateDirectories:NO];
    [openPanel setPrompt:@"Import"];
    [openPanel setAccessoryView:nil];
    [openPanel setDelegate:self];
    if (_firstOpen) {
        [openPanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    [openPanel beginSheetModalForWindow:_controlWindow completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseCancel) return;
        if ([openPanel URL] != nil) {
            self->_firstOpen = NO;
            thePath = [[openPanel URL] path];
        } else {
            return;
        }
        @try {
            theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:thePath];
        }
        @catch (NSException *e) {
            theObject = nil;
        }
        @try {
            if (!theObject) {
                theObject = nil;
            } else if ([theObject count] < 3) {
                theObject = nil;
            }
        }
        @catch (NSException *e) {
            theObject = nil;
        }
        if (!theObject) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            [PLUtilities showAlertWithMessageText:@"Could not read backup file" informativeText:@"There was a problem reading the favorites backup file. Your currently loaded favorites list should still be in order. Please ascertain yourself that the file in question is indeed a radio favorites file, or contact the developer for further help. We are very sorry for the inconvenience."];
        } else {
            if ([[self->_recordingTableView previewBox] isPlaying]) [[self->_recordingTableView previewBox] stop];
            if (self->_radioController) {
                if (self->_isPlaying) {
                    [self stop];
                }
            }
            @try {
                [self->_favTableView setFavList:[theObject objectAtIndex:0]];
                [self->_menuTableView setMenuList:[theObject objectAtIndex:1]];
                [self setPlayingURL:[theObject objectAtIndex:2]];
                if (self->_controlWindowIsAlreadyOpen) {
                    [self->_favTableView setHasBeenEdited:YES];
                    [self->_favTableView selectNothing];
                    [[self->_favTableView tableView] reloadData];
                    [self->_menuTableView selectNothing];
                    [[self->_menuTableView tableView] reloadData];
                }
            }
            @catch(NSException *e) {
                [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
                [PLUtilities showAlertWithMessageText:@"Could not read backup file" informativeText:@"There was a problem reading the favorites backup file. Your currently loaded favorites list should still be in order. Please ascertain yourself that the file in question is indeed a radio favorites file, or contact the developer for further help. We are very sorry for the inconvenience."];
            }
        }
    }];
}


#pragma mark - NSOpenSavePanelDelegate methods

- (BOOL)panel:(id)sender shouldEnableURL:(NSURL *)url {
    if ([url.absoluteString.pathExtension isEqualToString:@"favorites"] || ([url.absoluteString hasSuffix:@"/"] && ![url.absoluteString hasSuffix:@".app/"])) {
        return YES;
    }
    return NO;
}


#pragma mark - Track Info Window (Delegate) Support

- (BOOL)windowShouldClose:(NSWindow *)sender {
    _trackInfoWindowIsAlreadyOpen = NO;
    return YES;
}

- (void)loadTrackInfo {
    id theObject;
    NSError *error;
redo:
    [self prepareAppSupportFolder];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    if (error) {
        NSLog(@"PLECTRUM: Getting app support URL failed.");
        NSLog(@"PLECTRUM: Error: %@", [error localizedDescription]);
        NSLog(@"PLECTRUM: Error: %@", [error localizedFailureReason]);
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Track.info"];
    @try {
        theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:pathToMyAppSupportFile];
    }
    @catch (NSException *e) {
        NSLog(@"PLECTRUM: Problem reading track info?");
        NSLog(@"PLECTRUM: Load Path: %@", pathToMyAppSupportFile);
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Could not load recording bin"];
        [alert setInformativeText:[NSString stringWithFormat:@"There was a problem retrieving the track info list.\n\nError: %@\nReason: %@\n\nYou can try one of the following options: restart Plectrum, try to reload the recording bin, or as a last resort or if the problem persists, reset them to an empty list favorites. We are very sorry for the inconvenience.", [e name], [e reason]]];
        [alert addButtonWithTitle:@"Reset"];
        [alert addButtonWithTitle:@"Restart"];
        [alert addButtonWithTitle:@"Reload"];
        NSModalResponse response = [alert runModal];
        if (response == NSAlertFirstButtonReturn) {
            goto redo;
        } else if (response == NSAlertSecondButtonReturn) {
            [self restartNow:self];
        } else {
            theObject = nil;
        }
    }
    if (!theObject) {
        [self initializeTrackInfo];
        [_trackInfoTableView reloadData];
        return;
    }
    _trackInfoArray = [[NSMutableArray alloc] init];
    _trackInfoArray = [[theObject componentsSeparatedByString:@"\n"] mutableCopy];
    if ([_trackInfoArray containsObject:@""]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF != %@", @""];
        _trackInfoArray = [[_trackInfoArray filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    [_trackInfoTableView reloadData];
    [_trackInfoTableView scrollRowToVisible:[_trackInfoArray count] - 1];
}

- (void)initializeTrackInfo {
    _trackInfoArray = [[NSMutableArray alloc] init];
    [self saveTrackInfo];
}

- (void)saveTrackInfo {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Track.info"];
    if ([_trackInfoArray containsObject:@""]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF != %@", @""];
        _trackInfoArray = [[_trackInfoArray filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    NSMutableArray *export = [[_trackInfoArray componentsJoinedByString:@"\n"] copy];
    BOOL success = [NSKeyedArchiver archiveRootObject:export toFile:pathToMyAppSupportFile];
    if (!success) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Could not save collected track info" informativeText:@"There was an unspecified problem saving the track info. The previous version of the track info file should still be in order. Please try to relauch the application, restore an earlier backup of your collected track info if the problem persists, or contact the developer for further help. We are very sorry for the inconvenience."];
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"trackinfo"] forFile:pathToMyAppSupportFile options:0];
    }
}


#pragma mark - Track Info Table View Delegate and Data Source (Helper) Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_trackInfoArray count];
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *cellView = [_trackInfoTableView makeViewWithIdentifier:@"TrackInfoCellView" owner:self];
    [[cellView textField] setStringValue:[_trackInfoArray objectAtIndex:row]];
    return cellView;
}

- (IBAction)findSavedTrackOnWeb:(id)sender {
    NSInteger row = [_trackInfoTableView rowForView:[sender superview]];
    [self findSavedTrackOnWebForRow:row];
}

- (void)findSavedTrackOnWebForRow:(NSUInteger)row {
    NSString *searchTerm = [_trackInfoArray objectAtIndex:row];
    if (searchTerm == nil) searchTerm = @"";
    [[NSWorkspace sharedWorkspace] openURL:[self getSearchURLForCurrentlySelectedSearchEngine:searchTerm]];
}

- (IBAction)findSavedTrackOnBandcamp:(id)sender {
    NSInteger row = [_trackInfoTableView rowForView:[sender superview]];
    [self findSavedTrackOnBandcampForRow:row];
}

- (void)findSavedTrackOnBandcampForRow:(NSUInteger)row {
    NSString *searchTerm = [[_trackInfoArray objectAtIndex:row] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    searchTerm = [searchTerm stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    NSString *term = [NSString stringWithFormat:@"https://bandcamp.com/search?q=%@", searchTerm];
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:term]];
}

- (NSURL *)getSearchURLForCurrentlySelectedSearchEngine:(NSString *)searchTerms {
    NSString *searchString = (NSString *)[[[_searchEnginePopUpButton menu] itemAtIndex:_searchEngineChoiceIndex] representedObject];
    NSString *escapedSearchTerms = [searchTerms stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    escapedSearchTerms = [escapedSearchTerms stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
    NSString *urlAsString = [NSString stringWithFormat:@"%@%@", searchString, escapedSearchTerms];
    NSURL *url = [NSURL URLWithString:urlAsString];
    if (url == nil) {
        url = [NSURL URLWithString:@""]; // would this even work?
    }
    return url;
}

- (IBAction)searchEngineMenuChoiceChanged:(id)sender {
    _searchEngineChoiceIndex = [_searchEnginePopUpButton indexOfSelectedItem];
}


#pragma mark - Other External Track Info IBActions

- (IBAction)findTrackOnWeb:(id)sender {
    NSString *searchTerm;
    if (!_trackTitle) {
        searchTerm = @"";
    } else {
        searchTerm = _trackTitle;
    }
    [[NSWorkspace sharedWorkspace] openURL:[self getSearchURLForCurrentlySelectedSearchEngine:searchTerm]];
}

- (IBAction)findTrackOnBandcamp:(id)sender {
    NSString *searchTerm;
    if (!_trackTitle) {
        searchTerm = @"";
    } else {
        searchTerm = [_trackTitle stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    }
    NSString *term = [NSString stringWithFormat:@"https://bandcamp.com/search?q=%@", searchTerm];
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:term]];
}


#pragma mark - Recording List Support

- (void)loadRadioRecordings {
    id theObject;
    NSError *error;
    BOOL errorFlag = NO;
retry:
    [self prepareAppSupportFolder];
    [_recordingTableView prepareUserFolder];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:&error];
    if (error) {
        NSLog(@"PLECTRUM: Getting app support URL failed.");
        NSLog(@"PLECTRUM: Error: %@", [error localizedDescription]);
        NSLog(@"PLECTRUM: Error: %@", [error localizedFailureReason]);
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Radio.recordings"];
    @try {
        theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:pathToMyAppSupportFile];
    }
    @catch(NSException *e) {
        NSLog(@"PLECTRUM: Problem reading radio recordings?");
        NSLog(@"PLECTRUM: Load Path: %@", pathToMyAppSupportFile);
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Could not load recording bin"];
        [alert setInformativeText:[NSString stringWithFormat:@"There was a problem retrieving the listing of your radio recordings.\n\nError: %@\nReason: %@\n\nYou can try one of the following options: restart Plectrum, try to reload the recording bin, or as a last resort or if the problem persists, reset them to an empty list favorites. For what it is worth, your recordings themselves are safe; you can retrieve them from the /home/Music/Plectrum folder and store them elsewhere for safekeeping. We are very sorry for the inconvenience.", [e name], [e reason]]];
        [alert addButtonWithTitle:@"Reset"];
        [alert addButtonWithTitle:@"Restart"];
        [alert addButtonWithTitle:@"Reload"];
        NSModalResponse response = [alert runModal];
        if (response == NSAlertFirstButtonReturn) {
            goto retry;
        } else if (response == NSAlertSecondButtonReturn) {
            [self restartNow:self];
        } else {
            theObject = nil;
        }
    }
    if (!theObject) {
        theObject = nil;
    } else {
        @try {
            if ([theObject count] < 3) {
                theObject = nil;
                errorFlag = YES;
            } else {
                
            }
        }
        @catch (NSException *e) {
            theObject = nil;
            errorFlag = YES;
        }
    }
    if (!theObject) {
        [_recordingTableView setSortingKey:@"fileDate"];
        [_recordingTableView setAscending:YES];
        [_recordingTableView setRecordings:[@[] mutableCopy]];
        [self saveRadioRecordings];
    } else {
        [_recordingTableView setSortingKey:[theObject objectAtIndex:0]];
        [_recordingTableView setAscending:[[theObject objectAtIndex:1] boolValue]];
        [_recordingTableView setRecordings:[theObject objectAtIndex:2]];
    }
    if (errorFlag) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Error loading radio recording bin" informativeText:@"There was a problem loading the list of radio recordings. There may be inconsistencies between what recordings are registered in the list and what recordings are actually in the recording bin. The best course of action is to open the recordings folder and move the recordings there to a safe place, and contact the developer if the problem persists. We are very sorry for the inconvenience."];
    }
}

- (void)saveRadioRecordings {
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Plectrum"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"Radio.recordings"];
    NSMutableArray *export = [[NSMutableArray alloc] initWithObjects:
                              [_recordingTableView sortingKey],
                              [NSNumber numberWithBool:[_recordingTableView ascending]],
                              [_recordingTableView recordings],
                              nil];
    BOOL success = [NSKeyedArchiver archiveRootObject:export toFile:pathToMyAppSupportFile];
    if (!success) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"Could not save radio recording bin" informativeText:@"There was an unspecified problem saving the recording bin. There may be inconsistencies between what recordings are registered in the list and what recordings are actually in the recording bin. Please try to relauch the application, or contact the developer for further help. We are very sorry for the inconvenience."];
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"recordings"] forFile:pathToMyAppSupportFile options:0];
    }
}


#pragma mark - Media Key Trust and General Handling

- (void)handleTrust {
    if (_useMediaKeys) {
        Boolean isTrusted = AXIsProcessTrusted();
        if (isTrusted) {
            if (!_keyController) {
                _keyController = [PLMediaKeyController sharedController];
            }
        } else {
            [self acquireTrustForKeyController];
        }
    }
}

- (void)acquireTrustForKeyController {
    NSAlert *trust = [[NSAlert alloc] init];
    [trust setMessageText:@"Please allow access to media keys"];
    [trust setInformativeText:@"Plectrum does not currently have permission to control other applications through the use of the media keys. Please allow Plectrum to do so by using the following pop-up dialog which will guide you through the settings in System Preferences.\n\nPlease keep in mind that when you enable this ability, Plectrum will control its supported apps through these media keys, but application that are not controlled by Plectrum (for example, VLC) will lose access to these keys. Therefore, as an alternative, you can also opt to not grant access and leave things as they are. If you do allow access, Plectrum will restart.\n"];
    [trust setIcon:[NSImage imageNamed:@"appicon"]];
    [trust addButtonWithTitle:@"Allow access"];
    [trust addButtonWithTitle:@"Never mind"];
    [NSApp activateIgnoringOtherApps:YES];
    NSModalResponse response = [trust runModal];
    if (response != 1000) {
        [_useMediaKeysCheckBox setState:NO];
        _useMediaKeys = NO;
        [self savePrefs];
    } else {
        [_mediaKeyRestartWindow setLevel:NSSubmenuWindowLevel];
        [_mediaKeyRestartWindow makeKeyAndOrderFront:nil];
        _keyController = [PLMediaKeyController sharedController];
        NSURL *URL = [NSURL URLWithString:@"x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility"];
        [[NSWorkspace sharedWorkspace] openURL:URL];
    }
}

- (IBAction)restartNow:(id)sender {
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.Plectrum-Relauncher"];
    if ([appArray count] > 0) {
        [(NSRunningApplication *)[appArray objectAtIndex:0] terminate];
        sleep(1);
    }
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"Plectrum Relauncher" ofType:@"app"];
    [[NSWorkspace sharedWorkspace] launchApplication:path];
}

- (IBAction)restartLater:(id)sender {
    [_mediaKeyRestartWindow orderOut:nil];
}

@end
