//
//  PLNotificationCenter.h
//  Plectrum
//
//  Created by Petros Loukareas on 19/09/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


@interface PLNotificationCenter : NSObject

@property (weak) IBOutlet NSPanel *notification;
@property (weak) IBOutlet NSPanel *radioNotification;
@property (weak) IBOutlet NSBox *box;
@property (weak) IBOutlet NSBox *radioBox;

@property (assign) CGFloat opacity;
@property (assign) NSInteger corner;
@property (assign) BOOL notificationIsVisible;

- (void)showNotificationWithTitle:(NSString *)trackTitle artwork:(NSImage *)artwork artist:(NSString *)artistName album:(NSString *)albumName time:(NSString *)timeStamp rating:(NSString *)ratingString;
- (void)showRadioNotificationWithStation:(NSString *)stationName nowPlaying:(NSString *)nowPlaying stationURL:(NSString *)stationURL stationArt:(NSImage *)stationArt;
- (void)initiatePreview;
- (void)updatePreviewLocation;
- (void)updatePreviewOpacity;
- (void)endPreview;
- (IBAction)closeNotification:(id)sender;

@end


