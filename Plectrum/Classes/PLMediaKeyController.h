//
//  PLMediaKeyController.h
//  Plectrum
//
//  Created by Petros Loukareas on 22/07/2021.
//  Based on Gaurav Khanna's example at https://gist.github.com/gvkhna
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <IOKit/hidsystem/ev_keymap.h>


#define MAKE_SINGLETON(class_name, shared_method_name) \
+ (id)shared_method_name { \
static dispatch_once_t pred; \
static class_name * z ## class_name ## _ = nil; \
dispatch_once(&pred, ^{ \
z ## class_name ## _ = [[self alloc] init]; \
}); \
return z ## class_name ## _; \
} \
- (id)copy { \
return self; \
}


extern NSString * const MediaKeyPlayPauseNotification;
extern NSString * const MediaKeyNextNotification;
extern NSString * const MediaKeyPreviousNotification;
//extern NSString * const MediaKeyMuteNotification;
extern NSString * const MediaKeyVolumeDownNotification;
extern NSString * const MediaKeyVolumeUpNotification;


@interface PLMediaKeyController : NSObject

@property (nonatomic, assign, readonly) CFMachPortRef eventPort;
@property (assign) CFRunLoopSourceRef runLoopSource;

+ (id)sharedController;
- (void)releaseTap;

@end
