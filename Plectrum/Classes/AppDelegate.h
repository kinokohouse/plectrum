//
//  AppDelegate.h
//  Plectrum
//
//  Created by Petros Loukareas on 07-04-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <ServiceManagement/ServiceManagement.h>
#import <AudioToolbox/AudioFile.h>
#import <CoreMedia/CMTime.h>
#import <AVFoundation/AVAsset.h>
#import <Growl/Growl.h>

#import "PLHotKeyCenter.h"
#import "PLNotificationCenter.h"
#import "PLCoverArtWindow.h"
#import "PLRadioRecording.h"
#import "PLRadioFavorite.h"

#import "FSAudioController.h"



extern const float TIMEOUT_RANGE;
extern const float MAX_ARTWORK_DEFERMENT_RETRY_COUNT;

@interface AppDelegate : NSObject

@property (weak) IBOutlet NSMenuItem *miArtWindow;
@property (weak) IBOutlet NSPopUpButton *artWindowLevelPopUpButton;
@property (weak) IBOutlet NSTextField *openURLWarning;
@property (weak) IBOutlet NSTextField *urlTextField;
@property (weak) IBOutlet NSWindow *mediaKeyRestartWindow;
@property (weak) IBOutlet NSPanel *helpPanel;

@property (weak) IBOutlet NSWindow *hotKeySheet;
@property (weak) IBOutlet PLHotKeyCenter *hotKeyCenter;
@property (assign) IBOutlet PLHotKeyTextView *hkVolumeUp;
@property (assign) IBOutlet PLHotKeyTextView *hkVolumeDown;
@property (assign) IBOutlet PLHotKeyTextView *hkNowPlaying;
@property (assign) IBOutlet PLHotKeyTextView *hkPlayPause;
@property (assign) IBOutlet PLHotKeyTextView *hkNextTrack;
@property (assign) IBOutlet PLHotKeyTextView *hkPreviousTrack;
@property (assign) IBOutlet PLHotKeyTextView *hkActivate;
@property (assign) IBOutlet PLHotKeyTextView *hkCopy;
@property (weak) IBOutlet NSImageView *sivArt;

@property (strong) NSString *trackTitle;
@property (strong) NSString *trackArtist;
@property (strong) NSString *trackAlbum;
@property (strong) NSString *trackLength;
@property (strong) NSString *trackElapsed;
@property (strong) NSString *trackRating;
@property (assign) BOOL trackLoved;
@property (strong) NSImage *trackArt;
@property (strong) NSString *trackInfoSource;
@property (strong) NSMutableArray <NSString *> *trackInfoArray;
@property (strong) NSString *currentPlayerStatus;
@property (strong) PLRadioFavorite *currentFav;

@property (strong) NSUserNotificationCenter *notificationCenter;
@property (strong) NSUserNotification *notification;
@property (strong) id notifyingObject;

@property (assign) BOOL firstOpen;

//@property (nonatomic, readonly) PLRadioPlayer *radioController;
@property (nonatomic, readonly) FSAudioController *radioController;
@property (assign) BOOL isPlaying;
@property (assign) BOOL isPaused;
@property (assign) BOOL isStopped;
@property (assign) BOOL isMuted;
@property (assign) BOOL isStarting;
@property (assign) BOOL playingFromURL;
@property (assign) BOOL wasPlayingFromURL;
@property (assign) BOOL playingFromFav;
@property (assign) BOOL wasPlayingFromFav;
@property (assign) BOOL isRecording;
@property (assign) BOOL isGuardingAgainstRecordingModeChange;
@property (assign) BOOL hasPlayedBefore;
@property (strong) NSString *playingURL;
@property (strong) NSString * recordingUUID;
@property (strong) PLRadioRecording *currentRecording;
@property (assign) float plVolume;
@property (assign) float oldPlVolume;

@property (assign) BOOL useMediaKeys;

@property (strong) NSString *track;
@property (strong) NSString *artist;

@property (assign) BOOL askForConfirmation;
@property (assign) BOOL notificationDeferred;


- (IBAction)managingRecordingsDone:(id)sender;
- (IBAction)openURLOK:(id)sender;

- (void)endCoverArtDeferment;
- (void)restoreRadioArtwork;
- (void)respondToHotkey:(int)keyID;
- (NSString *)getStatus;
- (NSString *)getPlayingApplication;
- (void)suppressDeletionWarnings:(BOOL)flag;
- (void)suppressVolumeOfPlayingApplication;
- (void)restoreVolumeOfPlayingApplication;
- (void)checkToStopRecording;
- (void)stopRecording;
- (void)registerRecording;
- (void)saveRadioRecordings;
- (void)backupRadioFavorites;
- (void)restoreRadioFavorites;
- (void)performMenuSelectionByIdentifier:(int)identifier;
- (void)performNextFromMediaKeys;
- (void)performPreviousFromMediaKeys;
- (void)findSavedTrackOnWebForRow:(NSUInteger)row;
- (void)findSavedTrackOnBandcampForRow:(NSUInteger)row;
- (void)getTrackInfo;

- (void)increaseBPM;
- (void)decreaseBPM;
- (void)applyNewBPM;

- (void)hideInspector;

@end

