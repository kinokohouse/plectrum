//
//  PLRadioBrowserToolbox.h
//  Plectrum
//
//  Created by Petros Loukareas on 13/07/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Network/Network.h>
#import <netdb.h>

#import "PLLeftTableViewController.h"


NS_ASSUME_NONNULL_BEGIN

extern NSString *const radioBrowser;

@interface PLRadioBrowserToolbox : NSObject

@property (weak) IBOutlet NSWindow *browserSheet;
@property (weak) IBOutlet NSPopUpButton *choicePopUpButton;
@property (weak) IBOutlet NSPopUpButton *countryPopUpButton;
@property (weak) IBOutlet NSPopUpButton *popularityPopUpButton;
@property (weak) IBOutlet NSTextField *searchField;
@property (weak) IBOutlet NSTextField *filterField;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSTextField *errorField;
@property (weak) IBOutlet NSProgressIndicator *spinningWheel;
@property (weak) IBOutlet NSTableColumn *countryColumn;

@property (weak) IBOutlet PLLeftTableViewController *favTableView;

@property (weak) IBOutlet NSButton *searchButton;
@property (weak) IBOutlet NSButton *closeButton;
@property (weak) IBOutlet NSButton *addButton;
@property (weak) IBOutlet NSButton *playButton;

@property (strong) NSArray *tableContents;
@property (strong) NSArray *filteredTableContents;
@property (strong) NSString *userAgentString;
@property (strong) NSError *dataError;
@property (strong) NSString *baseURL;
@property (strong) id returnedData;
@property (assign) BOOL filteringInProgress;
@property (assign) BOOL lastClickWasSuccessful;
@property (assign) BOOL lastVoteWasSuccessful;


+ (NSArray <NSString *> *)IPv4AddressesForHostname:(nullable NSString *)host;
+ (NSArray *)hostnamesForIPv4Address:(NSString *)address;
+ (NSMutableArray <NSString *> *)dataToIPv4Addresses:(NSArray *)array;
+ (NSString *)buildUserAgentString;

- (void)showBrowserSheetOnWindow:(NSWindow *)window;
- (void)determineBaseURL;
- (void)sendRequest:(nonnull NSString *)requestParams withCallback:(SEL)selector onTarget:(nonnull id)target;
- (void)clickStationWithUUID:(nonnull NSString *)stationUUID;
- (void)voteForStationWithUUID:(nonnull NSString *)stationUUID;

- (IBAction)addToFavorites:(id)sender;
- (IBAction)justPlay:(id)sender;

@end

NS_ASSUME_NONNULL_END
