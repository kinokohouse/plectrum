//
//  main.m
//  Plectrum
//
//  Created by Petros Loukareas on 08-04-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
