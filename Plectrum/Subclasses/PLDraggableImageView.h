//
//  PLDraggableImageView.h
//  Plectrum
//
//  Created by Petros Loukareas on 01/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLDraggableImageView : NSImageView

@end

NS_ASSUME_NONNULL_END
