//
//  PLAudioPreviewBox.m
//  Plectrum
//
//  Created by Petros Loukareas on 16/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "PLAudioPreviewBox.h"
#import "AppDelegate.h"
#import "PLRecordingTableViewController.h"


@interface PLAudioPreviewBox() <AVAudioPlayerDelegate>

@property (weak) IBOutlet NSSlider *volumeKnob;
@property (weak) IBOutlet NSButton *playButton;
@property (weak) IBOutlet NSTextField *playLabel;
@property (weak) IBOutlet NSSlider *scrubSlider;
@property (weak) IBOutlet PLRecordingTableViewController *recordingTableView;

@property (strong) AVAudioPlayer *previewPlayer;
@property (strong) NSTimer *sliderUpdateTimer;
@property (strong) NSTimer *sliderResetTimer;
@property (assign) float previewVolume;

@property (assign) BOOL isScrubbing;
@property (strong) NSTimer *scrubTimer;
@property (assign) double lastTimeValue;

@end

@implementation PLAudioPreviewBox


#pragma mark - AwakeFromNib

- (void)awakeFromNib {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    _isPlaying = NO;
    _previewVolume = [appDelegate plVolume];
    if (_previewVolume == 0.0f) _previewVolume = 0.75f;
    [_volumeKnob setFloatValue:_previewVolume];
}


#pragma mark - AVAudioPlayerDelegate methods

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    _isPlaying = NO;
    _previewPlayer = nil;
    [_playButton setImage:[NSImage imageNamed:@"play"]];
    [_playLabel setStringValue:@"Play"];
    [_scrubSlider setEnabled:NO];
    [_scrubSlider setFloatValue:0.0f];
    [appDelegate restoreVolumeOfPlayingApplication];
}


#pragma mark

- (void)stop {
    if (!_isPlaying) return;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (_previewPlayer) {
        [_previewPlayer stop];
        [_previewPlayer setVolume:0.0f];
        _previewPlayer = nil;
    }
    [self invalidateScrubSliderTimer];
    [appDelegate restoreVolumeOfPlayingApplication];
    [self resetControls];
    _isPlaying = NO;

}

- (void)resetControls {
    [_scrubSlider setEnabled:NO];
    [_scrubSlider setFloatValue:0.0f];
    [_scrubSlider setMinValue:0.0f];
    [_scrubSlider setMaxValue:1000.0f];
    [_playButton setImage:[NSImage imageNamed:@"play"]];
    [_playLabel setStringValue:@"Play"];
}

- (void)disableControls {
    [_volumeKnob setEnabled:NO];
    [_scrubSlider setEnabled:NO];
    [_scrubSlider setFloatValue:0.0f];
    [_playButton setImage:[NSImage imageNamed:@"play"]];
    [_playButton setEnabled:NO];
    [_playLabel setStringValue:@"Play"];
    [_playLabel setEnabled:NO];
}

- (void)enableControls {
    [_volumeKnob setEnabled:YES];
    [_scrubSlider setEnabled:NO];
    [_playButton setImage:[NSImage imageNamed:@"play"]];
    [_playButton setEnabled:YES];
    [_playLabel setStringValue:@"Play"];
    [_playLabel setEnabled:YES];
}

- (void)continuePlaying {
    [_scrubSlider setEnabled:YES];
    [_playButton setImage:[NSImage imageNamed:@"stop"]];
    [_playButton setEnabled:YES];
    [_playLabel setStringValue:@"Stop"];
    [_playLabel setEnabled:YES];
}

- (IBAction)play:(id)sender {
    if (_isPlaying) {
        [self stop];
        return;
    }
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSError *error;
    error = nil;
    if (_previewPlayer) {
        [_previewPlayer stop];
        [_previewPlayer setVolume:0.0f];
        _previewPlayer = nil;
    }
    NSURL *theURL = [_recordingTableView playURLforSelection];
    _previewPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:theURL error:&error];
    if (error) {
        NSUserNotification *notification = [[NSUserNotification alloc] init];
        [notification setTitle:@"Could not start preview"];
        [notification setSubtitle:@"Problem with audio file"];
        [notification setInformativeText:@"Please check the format of the file."];
        [notification setSoundName:@"nil"];
        [[appDelegate notificationCenter] deliverNotification:notification];
        _previewPlayer = nil;
        _isPlaying = NO;
        return;
    }
    [_previewPlayer setDelegate:self];
    [_volumeKnob setFloatValue:_previewVolume];
    [_previewPlayer setVolume:_previewVolume];
    [_playButton setImage:[NSImage imageNamed:@"stop"]];
    [_playLabel setStringValue:@"Stop"];
    [_previewPlayer prepareToPlay];
    [_scrubSlider setMaxValue:(double)[_previewPlayer duration]];
    [_scrubSlider setMinValue:0.0f];
    [_scrubSlider setFloatValue:0.0f];
    [_scrubSlider setEnabled:YES];
    [appDelegate suppressVolumeOfPlayingApplication];
    [self startScrubSliderUpdate];
    [_previewPlayer play];
    _isPlaying = YES;
}

- (void)startScrubSliderUpdate {
    [self invalidateScrubSliderTimer];
    _sliderUpdateTimer = [NSTimer timerWithTimeInterval:0.05f target:self selector:@selector(updateScrubSlider) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_sliderUpdateTimer forMode:NSRunLoopCommonModes];
}

- (void)invalidateScrubSliderTimer {
    if (_sliderUpdateTimer) {
        [_sliderUpdateTimer invalidate];
        _sliderUpdateTimer = nil;
    }
}

- (void)updateScrubSlider {
    if (!_previewPlayer) return;
    [_scrubSlider setFloatValue:(float)[_previewPlayer currentTime]];
}

- (IBAction)volumeSliderChanged:(id)sender {
    _previewVolume = [_volumeKnob floatValue];
    if (_previewPlayer) {
        [_previewPlayer setVolume:_previewVolume];
    }
}

- (IBAction)scrubSliderChanged:(id)sender {
    if (_isScrubbing) return;
    _isScrubbing = YES;
    double newTime = [_scrubSlider doubleValue];
    if (newTime == _lastTimeValue) {
        _isScrubbing = NO;
        return;
    }
    if (_scrubTimer) {
        [_scrubTimer invalidate];
        _scrubTimer = nil;
    }
    _scrubTimer = [NSTimer timerWithTimeInterval:0.1f target:self selector:@selector(timedTimeAdjustment:) userInfo:[NSNumber numberWithDouble:newTime] repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:_scrubTimer forMode:NSRunLoopCommonModes];
    if (_previewPlayer) {
        [_previewPlayer setCurrentTime:(NSTimeInterval)[_scrubSlider doubleValue]];
    }
}

- (void)timedTimeAdjustment:(id)obj {
    double newTime = [[obj userInfo] doubleValue];
    if (_previewPlayer) [_previewPlayer setCurrentTime:(NSTimeInterval)[_scrubSlider doubleValue]];
    _lastTimeValue = newTime;
    if (_scrubTimer) {
        [_scrubTimer invalidate];
        _scrubTimer = nil;
    }
    _isScrubbing = NO;
}



@end
