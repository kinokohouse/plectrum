//
//  PLPlusMinusButton.h
//  Plectrum
//
//  Created by Petros Loukareas on 22/08/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>
#include "AppDelegate.h"


@interface PLPlusMinusButton : NSButton


@property (weak) IBOutlet NSWindow *bpmInspector;

@property (strong) NSTimer *repeatTimer;
@property (assign) NSUInteger buttonPosition;


- (void)mouseUp:(NSEvent *)event;
- (void)mouseDown:(NSEvent *)event;
- (void)setFirstTimer;
- (void)setNextTimer;

@end

