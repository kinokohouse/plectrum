//
//  PLDraggableImageView.m
//  Plectrum
//
//  Created by Petros Loukareas on 01/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLDraggableImageView.h"

@implementation PLDraggableImageView

- (BOOL)mouseDownCanMoveWindow {
    return YES;
}

@end
