//
//  PLAudioPreviewBox.h
//  Plectrum
//
//  Created by Petros Loukareas on 16/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLAudioPreviewBox : NSBox

@property (assign) BOOL isPlaying;

- (IBAction)play:(id)sender;
- (void)stop;
- (void)resetControls;
- (void)disableControls;
- (void)enableControls;
- (void)continuePlaying;

@end

NS_ASSUME_NONNULL_END
