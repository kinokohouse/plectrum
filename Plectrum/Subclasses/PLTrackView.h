//
//  PLTrackView.h
//  Plectrum
//
//  Created by Petros Loukareas on 13-05-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

@interface PLTrackView : NSView

@end
