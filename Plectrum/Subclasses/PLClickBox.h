//
//  PLClickBox.h
//  Plectrum
//
//  Created by Petros Loukareas on 17/07/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLClickBox : NSBox

@end

NS_ASSUME_NONNULL_END
