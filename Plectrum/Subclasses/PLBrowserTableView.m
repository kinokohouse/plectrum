//
//  PLBrowserTableView.m
//  Plectrum
//
//  Created by Petros Loukareas on 23/07/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLBrowserTableView.h"

@implementation PLBrowserTableView

- (NSMenu *)menuForEvent:(NSEvent *)event {
    NSPoint pt = NSMakePoint(0.0f, [self convertPoint:[event locationInWindow] toView:nil].y);
    NSInteger rowForMenu = [self rowAtPoint:pt];
    if ([_toolbox filteringInProgress]) {
        if (rowForMenu > [[_toolbox filteredTableContents] count]) return nil;
    } else {
        if (rowForMenu > [[_toolbox tableContents] count]) return nil;
    }
    NSMenu *menu = [[NSMenu alloc] init];
    NSIndexSet *selection = [self selectedRowIndexes];
    if ([selection count] == 0) return nil;
    NSMenuItem *addItem = [[NSMenuItem alloc] initWithTitle:@"Add to Favorites" action:@selector(addToFavorites:) keyEquivalent:@""];
    [addItem setTarget:_toolbox];
    [menu addItem:addItem];
    if ([selection count] == 1) {
        NSMenuItem *playItem = [[NSMenuItem alloc] initWithTitle:@"Play Now" action:@selector(justPlay:) keyEquivalent:@""];
        [playItem setTarget:_toolbox];
        [menu addItem:playItem];
    }
    return menu;
}

@end
