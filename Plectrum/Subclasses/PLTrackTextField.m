//
//  PLTrackTextField.m
//  Plectrum
//
//  Created by Petros Loukareas on 12/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLTrackTextField.h"
#import "PLCoverArtWindow.h"

@interface PLTrackTextField()

@property (weak) IBOutlet PLCoverArtWindow *coverArtWindow;
@property (weak) IBOutlet NSTextField *artistTextField;

@end

@implementation PLTrackTextField

- (void)setStringValue:(NSString *)stringValue {
    [super setStringValue:stringValue];
    if ([stringValue isEqualToString:@""]) {
        if ([[_artistTextField stringValue] isEqualToString:@""]) {
            if (![[_coverArtWindow trackBox] isHidden]) {
                [[_coverArtWindow trackBox] setHidden:YES];
            }
            return;
        }
    } else {
        if ([[_coverArtWindow trackBox] isHidden]) {
            [[_coverArtWindow trackBox] setHidden:NO];
        }
    }
}


@end
