//
//  PLCoverArtWindow.m
//  Plectrum
//
//  Created by Petros Loukareas on 27/09/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLCoverArtWindow.h"
#import "PLImageView.h"

#import "AppDelegate.h"



@interface PLCoverArtWindow()

@property (weak) IBOutlet NSButton *playButton;

@property (strong) NSTimer *windowAnimationTimer;
@property (strong) NSViewAnimation *windowAnimation;
@property (strong) NSTimer *boxAnimationTimer;
@property (strong) NSTrackingArea *trackingArea;
@property (assign) CGFloat fadeStep;
@property (assign) CGFloat currentOpacity;
@property (assign) BOOL isAnimatingIn;

@property (strong) NSString *trackName;
@property (strong) NSString *trackMusician;
@property (strong) NSImage *trackArt;

@end


@implementation PLCoverArtWindow


- (NSSize)aspectRatio {
    return NSMakeSize(1.0f, 1.0f);
}

- (NSSize)resizeIncrements {
    if (_demoMode) return NSMakeSize(1.0f, 1.0f);
    if (!_isLocked) {
        return NSMakeSize(1.0f, 1.0f);
    } else {
        return NSZeroSize;
    }
}

- (BOOL)isMovableByWindowBackground {
    if (_demoMode) {
        return YES;
    }
    return !_isLocked;
}

- (BOOL)isMovable {
    if (_demoMode) {
        return YES;
    }
    return !_isLocked;
}

- (void)fadeIn {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (_demoMode) {
        _trackName = [appDelegate trackTitle];
        _trackMusician = [appDelegate trackArtist];
        _trackArt = [appDelegate trackArt];
        return;
    }
    if (!_artWindowEnabled || !_artWindowVisible) {
        return;
    }
    if (![[appDelegate trackInfoSource] containsString:@"Spotify"] && [[appDelegate getPlayingApplication] isEqualToString:@"None"] && [[appDelegate currentPlayerStatus] isEqualToString:@"Paused"]) {
        return;
    }
    NSString *theStatus = [self getStatus];
    if ([theStatus isEqualToString:@"Stopped"]) {
        [_playButton setImage:[NSImage imageNamed:@"play"]];
        return;
    } else if ([theStatus isEqualToString:@"Playing"]) {
        [_playButton setImage:[NSImage imageNamed:@"pause"]];
    } else {
        [_playButton setImage:[NSImage imageNamed:@"play"]];
    }
    if (_trackingArea) {
        [[self contentView] removeTrackingArea:_trackingArea];
        _trackingArea = nil;
    }
    [self setTrackTitle:[appDelegate trackTitle] trackArtist:[appDelegate trackArtist] trackArt:[appDelegate trackArt]];
    _trackingArea = [[NSTrackingArea alloc] initWithRect:[[self contentView] bounds] options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[self contentView] addTrackingArea:_trackingArea];
    if (_showSongDetailsSelection > 0) {
        [_trackBox setAlphaValue:0.0f];
    } else {
        [_trackBox setAlphaValue:1.0f];
    }
    if (_isAnimating) {
        if (_isAnimatingIn) {
            return;
        } else {
            [self killWindowAnimation];
            _currentOpacity = [self alphaValue];
        }
    } else {
        _currentOpacity = 0.0f;
    }
    _fadeStep = _opacity / 10;
    [self setAlphaValue:0.0f];
    _windowAnimationTimer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueFadeIn) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_windowAnimationTimer forMode:NSRunLoopCommonModes];
    _isAnimating = YES;
    _isAnimatingIn = YES;
    _artWindowOnScreen = YES;
}

- (void)continueFadeIn {
    _currentOpacity += _fadeStep;
    if (_currentOpacity >= _opacity) {
        _currentOpacity = _opacity;
        [self killWindowAnimation];
        _isAnimating = NO;
        _isAnimatingIn = NO;
    }
    [self setAlphaValue:_currentOpacity];
}

- (void)fadeOut {
    if (_demoMode) {
        _closeArtWindowEventReceived = YES;
        return;
    }
    if (_isAnimating) {
        if (_isAnimatingIn) {
            return;
        } else {
            [self killWindowAnimation];
            _currentOpacity = [self alphaValue];
        }
    } else {
        _currentOpacity = _opacity;
    }
    _fadeStep = _opacity / 10;
    [self setAlphaValue:_currentOpacity];
    _windowAnimationTimer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueFadeOut) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_windowAnimationTimer forMode:NSRunLoopCommonModes];
    _isAnimating = YES;
    _isAnimatingIn = NO;
}

- (void)continueFadeOut {
    _currentOpacity -= _fadeStep;
    if (_currentOpacity <= 0.0f) {
        _currentOpacity = 0.0f;
        [self killWindowAnimation];
        _isAnimating = NO;
        _artWindowOnScreen = NO;
        if (_closeArtWindowEventReceived) {
            [self setAlphaValue:_currentOpacity];
            [self resetCoverArtWindowAfterFadeOut];
        }
    }
    [self setAlphaValue:_currentOpacity];
}

- (void)killWindowAnimation {
    if (_windowAnimationTimer) {
        [_windowAnimationTimer invalidate];
        _windowAnimationTimer = nil;
    }
}

- (void)setTrackTitle:(NSString *)track trackArtist:(NSString *)artist trackArt:(NSImage *)artwork {
     if (_demoMode) {
        if (track) {
            _trackName = track;
            _trackMusician = artist;
            _trackArt = artwork;
            return;
        }
    }
    if (track) {
        [_trackTitle setStringValue:track];
    } else {
        [_trackTitle setStringValue:@""];
    }
    if (artist) {
        [_trackArtist setStringValue:artist];
    } else {
        [_trackArtist setStringValue:@""];
    }
    if (artwork) {
        if ([artwork isEqual:[NSImage imageNamed:@"appicon"]]) {
            artwork = [NSImage imageNamed:@"file"];
        } else if ([artwork isEqual:[NSImage imageNamed:@"radio"]]) {
            artwork = [NSImage imageNamed:@"stream"];
        }
        [_coverArt setImage:artwork];
    } else {
        [_coverArt setImage:[NSImage imageNamed:@"file"]];
    }
}

- (void)setImageOnly:(NSImage *)image {
    if (_demoMode) return;
    if (image) {
        if ([image isEqual:[NSImage imageNamed:@"appicon"]]) {
            image = [NSImage imageNamed:@"file"];
        } else if ([image isEqual:[NSImage imageNamed:@"radio"]]) {
            image = [NSImage imageNamed:@"stream"];
        }
        [_coverArt setImage:image];
    } else {
        [_coverArt setImage:[NSImage imageNamed:@"file"]];
    }
}

- (IBAction)glueWindowToDesktop:(id)sender {
    [self setLevel:kCGDesktopIconWindowLevel - 1];
    _showSongDetailsSelection = 2;
    [self updateBoxVisibility];
    _artWindowLevel = 0;
    [self setCollectionBehavior:([self collectionBehavior] | NSWindowCollectionBehaviorStationary) & ~NSWindowCollectionBehaviorManaged];
}

- (IBAction)keepBehindOtherWindows:(id)sender {
    [self setLevel:kCGDesktopIconWindowLevel + 1];
    _artWindowLevel = 1;
    [self setCollectionBehavior:([self collectionBehavior] | NSWindowCollectionBehaviorStationary) & ~NSWindowCollectionBehaviorManaged];
}

- (IBAction)treatAsRegularWindow:(id)sender {
    [self setLevel:NSNormalWindowLevel];
    _artWindowLevel = 2;
    [self setCollectionBehavior:([self collectionBehavior] | NSWindowCollectionBehaviorManaged) & ~NSWindowCollectionBehaviorStationary];
}

- (IBAction)keepAboveOtherWindows:(id)sender {
    [self setLevel:NSSubmenuWindowLevel];
    _artWindowLevel = 3;
    [self setCollectionBehavior:([self collectionBehavior] | NSWindowCollectionBehaviorManaged) & ~NSWindowCollectionBehaviorStationary];
}

- (void)artWindowLevelByNumber:(NSInteger)levelNumber {
    switch (levelNumber) {
        case 0:
            [self glueWindowToDesktop:self];
            break;
        case 1:
            [self keepBehindOtherWindows:self];
            break;
        case 2:
            [self treatAsRegularWindow:self];
            break;
        case 3:
            [self keepAboveOtherWindows:self];
    }
}

- (void)moveArtWindowToTopLevelOnly {
    [self setLevel:NSSubmenuWindowLevel];
}

- (void)updateWindowLevel {
    [self artWindowLevelByNumber:_artWindowLevel];
}

- (IBAction)toggleLockPosAndSize:(id)sender {
    if (!_isLocked) {
        [[_coverArt lockMenuItem] setState:YES];
        self.styleMask &= ~NSWindowStyleMaskResizable;
    } else {
        [[_coverArt lockMenuItem] setState:NO];
        self.styleMask |= NSWindowStyleMaskResizable;
    }
    _isLocked = !_isLocked;
    [self setMovable:!_isLocked];
}

- (IBAction)playPause:(id)sender {
    if (_demoMode) return;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate respondToHotkey:3];
}

- (IBAction)nextTrack:(id)sender {
    if (_demoMode) return;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate performNextFromMediaKeys];
}

- (IBAction)prevTrack:(id)sender {
    if (_demoMode) return;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate performPreviousFromMediaKeys];
}

- (NSString *)getStatus {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    return [appDelegate getStatus];
}

- (void)setStatus {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSString *status = [appDelegate getStatus];
    if ([status isEqualToString:@"Playing"]) {
        [_playButton setImage:[NSImage imageNamed:@"pause"]];
    } else {
        [_playButton setImage:[NSImage imageNamed:@"play"]];
    }
}

- (void)windowDidResize:(NSNotification *)notification {
    if (_trackingArea) {
        [[self contentView] removeTrackingArea:_trackingArea];
        _trackingArea = nil;
    }
    _trackingArea = [[NSTrackingArea alloc] initWithRect:[[self contentView] bounds] options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[self contentView] addTrackingArea:_trackingArea];
}

- (void)mouseEntered:(NSEvent *)event {
    if (!_showControlsOnMouseOver && _showSongDetailsSelection != 1) return;
    [self boxFadeIn];
}

- (void)mouseExited:(NSEvent *)event {
    if (!_showControlsOnMouseOver && _showSongDetailsSelection != 1) return;
    [self boxFadeOut];
}

- (void)boxFadeIn {
    if ([[_trackTitle stringValue] isEqualToString:@""] && [[_trackArtist stringValue] isEqualToString:@""]) return;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (_demoMode && ([[appDelegate artWindowLevelPopUpButton] indexOfSelectedItem] == 0)) return;
    if (_showSongDetailsSelection == 1) {
        if (_boxAnimationTimer) {
            [_boxAnimationTimer invalidate];
            _boxAnimationTimer = nil;
        }
        if (_boxOpacity >= 1.0f) {
            _boxOpacity = 1.0f;
            return;
        }
        _boxIsAnimating = YES;
    }
    if (_boxAnimationTimer) {
        [_boxAnimationTimer invalidate];
        _boxAnimationTimer = nil;
    }
    _boxAnimationTimer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueBoxFadeIn) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_boxAnimationTimer forMode:NSRunLoopCommonModes];
}

- (void)continueBoxFadeIn {
    _boxOpacity += 0.10f;
    if (_boxOpacity >= 1.0f) {
        _boxOpacity = 1.0f;
        [_boxAnimationTimer invalidate];
        _boxAnimationTimer = nil;
        _boxIsAnimating = NO;
    }
    if (_showSongDetailsSelection == 1) {
        [_trackBox setAlphaValue:_boxOpacity];
    }
    if (_showControlsOnMouseOver) {
        [_buttonBox setAlphaValue:_boxOpacity];
    }
}

- (void)boxFadeOut {
    if (_boxAnimationTimer) {
        [_boxAnimationTimer invalidate];
        _boxAnimationTimer = nil;
    }
    if (_boxOpacity <= 0.0f) {
        _boxOpacity = 0.0f;
        return;
    }
    _boxIsAnimating = YES;
    if (_boxAnimationTimer) {
        [_boxAnimationTimer invalidate];
        _boxAnimationTimer = nil;
    }
    _boxAnimationTimer = [NSTimer timerWithTimeInterval:0.02f target:self selector:@selector(continueBoxFadeOut) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_boxAnimationTimer forMode:NSRunLoopCommonModes];
}

- (void)continueBoxFadeOut {
    _boxOpacity -= 0.10f;
    if (_boxOpacity <= 0.0f) {
        _boxOpacity = 0.0f;
        if (_boxAnimationTimer) {
            [_boxAnimationTimer invalidate];
            _boxAnimationTimer = nil;
            _boxIsAnimating = NO;
        }
    }
    if (_showControlsOnMouseOver) {
        [_buttonBox setAlphaValue:_boxOpacity];
    }
    if (_showSongDetailsSelection == 1) {
        [_trackBox setAlphaValue:_boxOpacity];
    }
}

- (void)updateWindowOpacity {
    [self setAlphaValue:_opacity];
}

- (void)updateBoxOpacity {
    [_trackBox setAlphaValue:_boxOpacity];
}

- (void)reflectLockStatus {
    if (_demoMode) {
        self.styleMask |= NSWindowStyleMaskResizable;
        [self setMovable:YES];
        return;
    }
    if (_isLocked) {
        [[_coverArt lockMenuItem] setState:YES];
        self.styleMask &= ~NSWindowStyleMaskResizable;
    } else {
        [[_coverArt lockMenuItem] setState:NO];
        self.styleMask |= NSWindowStyleMaskResizable;
    }
    [self setMovable:!_isLocked];
}

- (void)updateBoxVisibility {
    if (_showSongDetailsSelection == 0) {
        [_trackBox setAlphaValue:1.0f];
    } else {
        [_trackBox setAlphaValue:0.0f];
    }
}

- (void)updateControlsVisibility {
    if (_artWindowLevel == 0) {
        [_buttonBox setAlphaValue:0.0f];
    } else {
        if (!_showControlsOnMouseOver) {
            [_buttonBox setAlphaValue:0.0f];
        }
    }
}

- (void)beginDemoMode {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    _demoMode = YES;
    _trackName = [_trackTitle stringValue];
    _trackMusician = [_trackArtist stringValue];
    _trackArt = [_coverArt image];
    [_trackTitle setStringValue:@"Art Panel Preview"];
    [_trackArtist setStringValue:@"See what it will look like"];
    [_coverArt setImage:[NSImage imageNamed:@"kitteh-large"]];
    [self setCollectionBehavior:NSWindowCollectionBehaviorMoveToActiveSpace];
    [self artWindowLevelByNumber:3];
    _artWindowVisible = YES;
    [self setAlphaValue:_opacity];
    [self updateWindowOpacity];
    [[appDelegate miArtWindow] setTitle:@"Hide Art Panel"];
    [[appDelegate miArtWindow] setEnabled:NO];
    if (!_artWindowOnScreen) [self fadeIn];
    _isLocked = NO;
    [self reflectLockStatus];
    if ([[self getStatus] isEqualToString:@"Stopped"]) {
        _closeArtWindowEventReceived = YES;
    } else {
        _closeArtWindowEventReceived = NO;
    }
    [self renewTrackingArea];
}

- (void)endDemoMode {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    _artWindowVisible = YES;
    if (_closeArtWindowEventReceived && [[self getStatus] isEqualToString:@"Stopped"]) {
         [[appDelegate miArtWindow] setTitle:@"Show Art Panel"];
    } else {
        [_trackTitle setStringValue:_trackName];
        [_trackArtist setStringValue:_trackMusician];
        [_coverArt setImage:_trackArt];
        [[appDelegate miArtWindow] setTitle:@"Hide Art Panel"];
    }
    [[appDelegate miArtWindow] setEnabled:YES];
    [self setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces];
    [self renewTrackingArea];
    _demoMode = NO;
    [self reflectSettings];
    if (_closeArtWindowEventReceived) {
        [self fadeOut];
    } else {
        if ([appDelegate notificationDeferred]) {
            [appDelegate endCoverArtDeferment];
        } else {
            if ([appDelegate isPlaying] || [appDelegate isPaused]) {
                [appDelegate restoreRadioArtwork]; // a bit hackish, but eh
            }
        }
    }
}

- (void)reflectSettings {
    [self updateWindowOpacity];
    [self artWindowLevelByNumber:_artWindowLevel];
    [self reflectLockStatus];
    [self updateBoxVisibility];
    [self updateControlsVisibility];
}

- (void)prepareForFirstUse {
    [_buttonBox setAlphaValue:0.0f];
    [self setAlphaValue:0.0f];
    [self artWindowLevelByNumber:_artWindowLevel];
    [self reflectLockStatus];
    [self updateBoxVisibility];
    [self updateControlsVisibility];
}

- (void)resetCoverArtWindowAfterFadeOut {
    [_coverArt setImage:[NSImage imageNamed:@"appicon"]];
    [_trackTitle setStringValue:@""];
    [_trackArtist setStringValue:@""];
    _closeArtWindowEventReceived = NO;
}

- (void)receiveStatus:(NSString *)statusString {
    if ([statusString isEqualToString:@"Playing"]) {
        [_playButton setImage:[NSImage imageNamed:@"pause"]];
    } else {
        [_playButton setImage:[NSImage imageNamed:@"play"]];
    }
}

- (IBAction)setNewDepth:(id)sender {
    NSInteger selection = [[sender identifier] integerValue];
    _artWindowLevel = selection;
    [self reflectSettings];
}

- (void)renewTrackingArea {
    if (_trackingArea) {
        [[self contentView] removeTrackingArea:_trackingArea];
        _trackingArea = nil;
    }
    _trackingArea = [[NSTrackingArea alloc] initWithRect:[[self contentView] bounds] options:NSTrackingActiveAlways|NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    [[self contentView] addTrackingArea:_trackingArea];
}

@end
