//
//  PLArtistTextField.h
//  Plectrum
//
//  Created by Petros Loukareas on 12/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLArtistTextField : NSTextField

@end

NS_ASSUME_NONNULL_END
