//
//  PLBrowserTableView.h
//  Plectrum
//
//  Created by Petros Loukareas on 23/07/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PLRadioBrowserToolbox.h"
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface PLBrowserTableView : NSTableView

@property (weak) IBOutlet PLRadioBrowserToolbox *toolbox;

@end

NS_ASSUME_NONNULL_END
