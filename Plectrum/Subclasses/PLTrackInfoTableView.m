//
//  PLTrackInfoTableView.m
//  Plectrum
//
//  Created by Petros Loukareas on 26/09/2022.
//  Copyright © 2022 Kinoko House. All rights reserved.
//

#import "PLTrackInfoTableView.h"
#import "AppDelegate.h"

@implementation PLTrackInfoTableView

- (NSMenu *)menuForEvent:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSMenu *menu = [[NSMenu alloc] init];
    NSPoint pt = NSMakePoint(0.0f, [self convertPoint:[event locationInWindow] toView:nil].y);
    NSInteger rowForMenu = [self rowAtPoint:pt];
    NSInteger c = [[appDelegate trackInfoArray] count];
    if (rowForMenu > (c - 1) || c == 0) return nil;
    NSIndexSet *selection = [self selectedRowIndexes];
    if (![selection containsIndex:rowForMenu] || [selection count] == 0) {
        selection = [NSIndexSet indexSetWithIndex:rowForMenu];
        [self selectRowIndexes:selection byExtendingSelection:NO];
    }
    if ([selection count] > 1) {
        NSMenuItem *menuItem1a = [[NSMenuItem alloc] initWithTitle:@"Remove Selected Tracks" action:@selector(removeSelectedItems) keyEquivalent:@""];
        [menu addItem:menuItem1a];
    } else {
        NSMenuItem *menuItem1b = [[NSMenuItem alloc] initWithTitle:@"Remove Selected Track" action:@selector(removeSelectedItems) keyEquivalent:@""];
        [menu addItem:menuItem1b];
        [menu addItem:[NSMenuItem separatorItem]];
        NSMenuItem *menuItem3 = [[NSMenuItem alloc] initWithTitle:@"Search Web for Track" action:@selector(findTrackOnWeb) keyEquivalent:@""];
        NSImage *item3Image = [NSImage imageNamed:@"button-www"];
        [item3Image setSize:NSMakeSize(12.0f, 12.0f)];
        [menuItem3 setImage:item3Image];
        [menu addItem:menuItem3];
        NSMenuItem *menuItem4 = [[NSMenuItem alloc] initWithTitle:@"Search Bandcamp for Track" action:@selector(findTrackOnBC) keyEquivalent:@""];
        NSImage *item4Image = [NSImage imageNamed:@"button-bandcamp"];
        [item4Image setSize:NSMakeSize(12.0f, 12.0f)];
        [menuItem4 setImage:item4Image];
        [menu addItem:menuItem4];
    }
    return menu;
}

- (void)removeSelectedItems {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if ([appDelegate askForConfirmation]) {
        NSString *itemCount;
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Are you sure?"];
        if ([[self selectedRowIndexes] count] == 1) {
            itemCount = @"one track";
        } else {
            itemCount = [NSString stringWithFormat:@"%ld tracks", (long)itemCount];
        }
        [alert setInformativeText:[NSString stringWithFormat:@"You are about to remove %@. This operation cannot be undone. Are you sure you want to go ahead?", itemCount]];
        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Cancel"];
        [alert setAlertStyle:NSAlertStyleWarning];
        [alert beginSheetModalForWindow:[self window] completionHandler:^(NSModalResponse returnCode) {
            if (returnCode == NSAlertFirstButtonReturn) {
                [[appDelegate trackInfoArray] removeObjectsAtIndexes:[self selectedRowIndexes]];
                [self reloadData];
            }
        }];
    } else {
        [[appDelegate trackInfoArray] removeObjectsAtIndexes:[self selectedRowIndexes]];
        [self reloadData];
    }
}

- (void)findTrackOnWeb {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate findSavedTrackOnWebForRow:[[self selectedRowIndexes] firstIndex]];
}

- (void)findTrackOnBC {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate findSavedTrackOnBandcampForRow:[[self selectedRowIndexes] firstIndex]];
}

- (void)reloadData {
    [super reloadData];
    NSUInteger rows = [self numberOfRows];
    if (rows == 1) {
        [_countTextField setStringValue:@"1 track"];
    } else {
        [_countTextField setStringValue:[NSString stringWithFormat:@"%ld tracks", (long)rows]];
    }
}

@end
