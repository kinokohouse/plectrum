//
//  PLImageView.m
//  Plectrum
//
//  Created by Petros Loukareas on 01/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLImageView.h"

@implementation PLImageView

- (BOOL)mouseDownCanMoveWindow {
    return YES;
}

- (NSMenu *)menuForEvent:(NSEvent *)event {
    if (![[[self window] valueForKey:@"demoMode"] boolValue]) {
        NSInteger windowLevel = [[[self window] valueForKey:@"artWindowLevel"] integerValue] + 1;
        for (int i = 1; i < 5; i++) {
            if (i == windowLevel) {
                [[_contextualMenu itemAtIndex:i] setState:YES];
            } else {
                [[_contextualMenu itemAtIndex:i] setState:NO];
            }
        }
        return _contextualMenu;
    } else {
        return nil;
    }
}

- (void)setImage:(NSImage *)image {
    [super setImage:image];
    if ([image size].width == [image size].height) {
        [_theImageBlurred setImage:nil];
        return;
    }
    NSImage *blurImage = [image copy];
    [blurImage blurEffectWithRadius:10.0f];
    [_theImageBlurred setImage:blurImage];
}

@end
