//
//  PLClearableImageView.m
//  Plectrum
//
//  Created by Petros Loukareas on 22/04/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLClearableImageView.h"

@interface PLClearableImageView()  <NSDraggingSource, NSFilePromiseProviderDelegate>

@property (assign) BOOL optionKeyPressed;

@end


@implementation PLClearableImageView


- (void)awakeFromNib {
    _optionKeyPressed = NO;
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (void)keyDown:(NSEvent *)event {
    if ([event keyCode] == 51) {
        [self setImage:[NSImage imageNamed:@"stream"]];
    }
}

- (void)flagsChanged:(NSEvent *)event {
    if ([event modifierFlags] & NSEventModifierFlagOption) {
        _optionKeyPressed = YES;
    } else {
        _optionKeyPressed = NO;
    }
}

- (BOOL)acceptsFirstMouse:(NSEvent *)event {
    return YES;
}

- (void)mouseDown:(NSEvent*)event {
    NSString *fileType;
    if (_optionKeyPressed) {
        fileType = @"public.jpeg";
    } else {
        fileType = @"public.png";
    }
    NSFilePromiseProvider *fpp = [[NSFilePromiseProvider alloc] initWithFileType:fileType delegate:self];
    NSDraggingItem *di = [[NSDraggingItem alloc] initWithPasteboardWriter:fpp];
    NSRect frame = [self frame];
    frame.origin = NSMakePoint(0.0f, 0.0f);
    [di setDraggingFrame:frame contents:[self image]];
    [self beginDraggingSessionWithItems:@[di] event:event source:self];
}


#pragma mark - NSDraggingSource implementation

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
    return [NSImage canInitWithPasteboard: [sender draggingPasteboard]];
}

- (NSDragOperation)draggingSession:(nonnull NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context {
    if (context == NSDraggingContextOutsideApplication) {
        return NSDragOperationCopy;
    } else {
        return NSDragOperationNone;
    }
}


#pragma mark - NSFilePromiseProvider implementation

- (nonnull NSString *)filePromiseProvider:(nonnull NSFilePromiseProvider *)filePromiseProvider fileNameForType:(nonnull NSString *)fileType {
    if ([fileType isEqualToString:@"public.jpeg"]) {
        return [NSString stringWithFormat:@"%@.jpg", [[self window] title]];
    } else {
        return [NSString stringWithFormat:@"%@.png", [[self window] title]];
    }
}

- (void)filePromiseProvider:(nonnull NSFilePromiseProvider *)filePromiseProvider writePromiseToURL:(nonnull NSURL *)url completionHandler:(nonnull void (^)(NSError * _Nullable))completionHandler {
        NSBitmapImageFileType fileType;
    if ([[filePromiseProvider fileType] isEqualToString:@"public.jpeg"]) {
        fileType = NSJPEGFileType;
    } else {
        fileType = NSPNGFileType;
    }
    NSArray *representations;
    NSData *bitmapData;
    representations = [[self image] representations];
    bitmapData = [NSBitmapImageRep representationOfImageRepsInArray:representations usingType:fileType properties:@{}];
    [bitmapData writeToFile:[url path] atomically:YES];
    completionHandler(nil);
}


@end
