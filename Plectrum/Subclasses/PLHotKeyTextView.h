//
//  PLHotKeyTextView.h
//  Plectrum
//
//  Created by Petros Loukareas on 24-01-18.
//  Copyright (c) 2018 Kinoko House. MIT License.
//


# pragma mark Includes

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

# pragma mark Forward Class Declarations

@class PLHotKeyCenter;
@class PLHotKey;


@interface PLHotKeyTextView : NSTextView

@property __unsafe_unretained IBOutlet PLHotKeyCenter *hotKeyCenter;

@property __unsafe_unretained IBOutlet NSBox *outerBox;
@property __unsafe_unretained IBOutlet NSBox *innerBox;
@property __unsafe_unretained IBOutlet NSButton *clearButton;
@property __unsafe_unretained IBOutlet NSButton *cancelButton;

@property (assign) BOOL isSet;

@property (assign) unsigned long keyCode;
@property (assign) unsigned long modMask;
@property (strong) NSString *keyString;
@property (strong) NSDictionary *keyDict;


- (instancetype)initWithCoder:(NSCoder *)decoder;

- (IBAction)clearField:(id)sender;
- (IBAction)cancelNewKey:(id)sender;

- (void)keyDown:(NSEvent *)event;
- (void)mouseDown:(NSEvent *)event;

- (void)setHotKeyTextViewWithKeyCode:(unsigned long)keycode modMask:(unsigned long)modMask keyString:(NSString *)keyString;

@end
