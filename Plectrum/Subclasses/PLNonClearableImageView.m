//
//  PLNonClearableImageView.m
//  Plectrum
//
//  Created by Petros Loukareas on 22/04/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLNonClearableImageView.h"
#import "AppDelegate.h"
#import "PLRadioFavorite.h"

@interface PLNonClearableImageView()  <NSDraggingSource, NSFilePromiseProviderDelegate>

@end


@implementation PLNonClearableImageView


- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (BOOL)acceptsFirstMouse:(NSEvent *)event {
    return YES;
}

- (void)mouseDown:(NSEvent*)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([appDelegate playingFromURL]) return;
    NSUInteger flags = [[NSApp currentEvent] modifierFlags];
    if (!(flags & NSEventModifierFlagOption)) return;
    NSString *fileType = @"public.png";
    NSFilePromiseProvider *fpp = [[NSFilePromiseProvider alloc] initWithFileType:fileType delegate:self];
    NSDraggingItem *di = [[NSDraggingItem alloc] initWithPasteboardWriter:fpp];
    NSRect frame = [self frame];
    frame.origin = NSMakePoint(0.0f, 0.0f);
    [di setDraggingFrame:frame contents:[self image]];
    [self beginDraggingSessionWithItems:@[di] event:event source:self];
}


#pragma mark - NSDraggingSource implementation

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
    return [NSImage canInitWithPasteboard: [sender draggingPasteboard]];
}

- (NSDragOperation)draggingSession:(nonnull NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context {
    if (context == NSDraggingContextOutsideApplication) {
        return NSDragOperationCopy;
    } else {
        return NSDragOperationNone;
    }
}


#pragma mark - NSFilePromiseProvider implementation

- (nonnull NSString *)filePromiseProvider:(nonnull NSFilePromiseProvider *)filePromiseProvider fileNameForType:(nonnull NSString *)fileType {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSString *theName;
    if ([appDelegate isPlaying]) {
        theName = [appDelegate trackTitle];
        if (!theName) theName = @"Unknown";
        if ([theName isEqualToString:@"Internet radio or relay"]) {
            if ([appDelegate currentFav]) {
                theName = [[appDelegate currentFav] name];
            }
        }
    } else {
        theName = [NSString stringWithFormat:@"%@ - %@", [appDelegate trackArtist], [appDelegate trackTitle]];
        if ([theName containsString:@"(null)"]) theName = @"Unknown";
    }
    return [NSString stringWithFormat:@"%@.png", theName];
}

- (void)filePromiseProvider:(nonnull NSFilePromiseProvider *)filePromiseProvider writePromiseToURL:(nonnull NSURL *)url completionHandler:(nonnull void (^)(NSError * _Nullable))completionHandler {
        NSBitmapImageFileType fileType = NSPNGFileType;
    NSArray *representations;
    NSData *bitmapData;
    representations = [[self image] representations];
    bitmapData = [NSBitmapImageRep representationOfImageRepsInArray:representations usingType:fileType properties:@{}];
    [bitmapData writeToFile:[url path] atomically:YES];
    completionHandler(nil);
}


@end
