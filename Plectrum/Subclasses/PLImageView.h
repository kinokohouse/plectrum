//
//  PLImageView.h
//  Plectrum
//
//  Created by Petros Loukareas on 01/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import "PLCoverArtWindow.h"
#import "NSImage+Blur.h"

NS_ASSUME_NONNULL_BEGIN

@interface PLImageView : NSImageView

@property (weak) IBOutlet NSImageView *theImageBlurred;

@property (weak) IBOutlet NSMenu *contextualMenu;
@property (weak) IBOutlet NSMenuItem *lockMenuItem;

@end

NS_ASSUME_NONNULL_END
