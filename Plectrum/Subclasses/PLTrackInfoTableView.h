//
//  PLTrackInfoTableView.h
//  Plectrum
//
//  Created by Petros Loukareas on 26/09/2022.
//  Copyright © 2022 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLTrackInfoTableView : NSTableView

@property (weak) IBOutlet NSTextField *countTextField;

@end

NS_ASSUME_NONNULL_END
