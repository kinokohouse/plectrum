//
//  PLClearableImageView.h
//  Plectrum
//
//  Created by Petros Loukareas on 22/04/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLClearableImageView : NSImageView

@end

NS_ASSUME_NONNULL_END
