//
//  PLRadioFavoriteListView.h
//  Plectrum
//
//  Created by Petros Loukareas on 27/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLRadioFavoriteListView : NSTableCellView

@property (weak) IBOutlet NSTextField *stationGenre;
@property (weak) IBOutlet NSTextField *stationCountry;

@end

NS_ASSUME_NONNULL_END
