//
//  PLInspectorWindow.h
//  Plectrum
//
//  Created by Petros Loukareas on 25/08/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#include "AppDelegate.h"

@interface PLInspectorWindow : NSPanel

@end
