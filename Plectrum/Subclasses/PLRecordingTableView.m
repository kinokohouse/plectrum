//
//  PLRecordingTableView.m
//  Plectrum
//
//  Created by Petros Loukareas on 19/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRecordingTableView.h"
#import "PLRecordingTableViewController.h"
#import "PLAudioPreviewBox.h"
#import "AppDelegate.h"


@interface PLRecordingTableView()

@property (weak) IBOutlet PLRecordingTableViewController *recordingTableView;
@property (weak) IBOutlet PLAudioPreviewBox *previewBox;

@end


@implementation PLRecordingTableView

- (NSMenu *)menuForEvent:(NSEvent *)theEvent {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSPoint pt = NSMakePoint(0.0f, [self convertPoint:[theEvent locationInWindow] toView:nil].y);
    NSInteger rowForMenu = [self rowAtPoint:pt];
    if (rowForMenu > [[_recordingTableView recordings] count]) return nil;
    NSIndexSet *selection = [self selectedRowIndexes];
    if (![selection containsIndex:rowForMenu] || [selection count] == 0) {
        selection = [NSIndexSet indexSetWithIndex:rowForMenu];
        NSString *uuid = [[[_recordingTableView recordings] objectAtIndex:rowForMenu] uuid];
        if ([uuid isEqualToString:[appDelegate recordingUUID]]) return nil;
        [self selectRowIndexes:selection byExtendingSelection:NO];
    }
    NSMenuItem *item1, *item2, *item3, *item4;
    if ([selection count] == 1) {
        NSInteger index = [selection firstIndex];
        NSString *path = [[[_recordingTableView recordings] objectAtIndex:index] filePath];
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (!exists) {
            item1 = [[NSMenuItem alloc] initWithTitle:@"Remove From List" action:@selector(moveToTrash) keyEquivalent:@""];
            NSMenu *menu = [[NSMenu alloc] init];
            [menu addItem:item1];
            return menu;
        } else {
            item1 = [[NSMenuItem alloc] initWithTitle:@"Move To Trash" action:@selector(moveToTrash) keyEquivalent:@""];
            item2 = [[NSMenuItem alloc] initWithTitle:@"Reveal in Finder" action:@selector(revealInFinder) keyEquivalent:@""];
            item3 = [[NSMenuItem alloc] initWithTitle:@"Save To..." action:@selector(saveTo) keyEquivalent:@""];
            item4 = [[NSMenuItem alloc] initWithTitle:@"Preview" action:@selector(previewAudio) keyEquivalent:@""];
            NSMenu *menu = [[NSMenu alloc] init];
            [menu addItem:item1];
            [menu addItem:item2];
            [menu addItem:item3];
            [menu addItem:item4];
            return menu;
        }
    } else {
        item1 = [[NSMenuItem alloc] initWithTitle:@"Move To Trash" action:@selector(moveToTrash) keyEquivalent:@""];
        item2 = [[NSMenuItem alloc] initWithTitle:@"Open enclosing folder" action:@selector(openFolder) keyEquivalent:@""];
        item3 = [[NSMenuItem alloc] initWithTitle:@"Save To..." action:@selector(saveTo) keyEquivalent:@""];
        NSMenu *menu = [[NSMenu alloc] init];
        [menu addItem:item1];
        [menu addItem:item2];
        [menu addItem:item3];
        return menu;
    }
    return nil;
}

- (void)moveToTrash {
    [_recordingTableView moveToTrash:self];
}

- (void)revealInFinder {
    PLRadioRecording *selectedRecording = [[_recordingTableView recordings] objectAtIndex:[[_recordingTableView tableView] selectedRow]];
    [[NSWorkspace sharedWorkspace] selectFile:[selectedRecording filePath] inFileViewerRootedAtPath:[[selectedRecording filePath] stringByDeletingLastPathComponent]];
}

- (void)openFolder {
    [_recordingTableView openFolder:self];
}

- (void)saveTo {
    [_recordingTableView saveTo:self];
}

- (void)previewAudio {
    [_previewBox play:self];
}


@end
