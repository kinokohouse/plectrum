//
//  PLRightTableView.m
//  Plectrum
//
//  Created by Petros Loukareas on 08/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRightTableView.h"
#import "PLRightTableViewController.h"

@interface PLRightTableView()

@property (weak) IBOutlet PLRightTableViewController *menuTableView;

@end

@implementation PLRightTableView

- (NSMenu *)menuForEvent:(NSEvent *)theEvent {
    NSPoint pt = NSMakePoint(0.0f, [self convertPoint:[theEvent locationInWindow] toView:nil].y);
    NSInteger rowForMenu = [self rowAtPoint:pt];
    if (rowForMenu > [[_menuTableView menuList] count]) return nil;
    NSIndexSet *selection = [self selectedRowIndexes];
    if (![selection containsIndex:rowForMenu] || [selection count] == 0) {
        selection = [NSIndexSet indexSetWithIndex:rowForMenu];
        [self selectRowIndexes:selection byExtendingSelection:NO];
    }
    NSString *theTitle;
    if ([selection count] > 1) {
        theTitle = @"Remove these items";
    } else {
        theTitle = @"Remove this item";
    }
    NSMenu *theMenu = [[NSMenu alloc] init];
    NSMenuItem *theItem = [[NSMenuItem alloc] initWithTitle:theTitle action:@selector(removeItems:) keyEquivalent:@""];
    [theMenu addItem:theItem];
    return theMenu;
}

- (void)removeItems:(id)sender {
    [_menuTableView removeFromMenu:self];
}

@end
