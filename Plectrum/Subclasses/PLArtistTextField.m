//
//  PLArtistTextField.m
//  Plectrum
//
//  Created by Petros Loukareas on 12/10/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLArtistTextField.h"
#import "PLCoverArtWindow.h"

@interface PLArtistTextField()

@property (weak) IBOutlet PLCoverArtWindow *coverArtWindow;
@property (weak) IBOutlet NSTextField *trackTextField;

@end

@implementation PLArtistTextField

- (void)setStringValue:(NSString *)stringValue {
    [super setStringValue:stringValue];
    if ([stringValue isEqualToString:@""]) {
        if ([[_trackTextField stringValue] isEqualToString:@""]) {
            if (![[_coverArtWindow trackBox] isHidden]) {
                [[_coverArtWindow trackBox] setHidden:YES];
            }
            return;
        }
    } else {
        if ([[_coverArtWindow trackBox] isHidden]) {
            [[_coverArtWindow trackBox] setHidden:NO];
        }
    }
}

@end
