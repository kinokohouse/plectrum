//
//  PLLeftTableView.m
//  Plectrum
//
//  Created by Petros Loukareas on 08/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLLeftTableView.h"
#import "PLLeftTableViewController.h"
#import "PLRightTableViewController.h"
#import "AppDelegate.h"

@interface PLLeftTableView()

@property (weak) IBOutlet PLLeftTableViewController *favTableView;
@property (weak) IBOutlet PLRightTableViewController *menuTableView;

@end

@implementation PLLeftTableView

- (NSMenu *)menuForEvent:(NSEvent *)theEvent {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSPoint pt = NSMakePoint(0.0f, [self convertPoint:[theEvent locationInWindow] toView:nil].y);
    NSInteger rowForMenu = [self rowAtPoint:pt];
    if (rowForMenu > [[_favTableView favList] count]) return nil;
    NSIndexSet *selection = [self selectedRowIndexes];
    if (![selection containsIndex:rowForMenu] || [selection count] == 0) {
        NSString *uuid = [[[_favTableView favList] objectAtIndex:rowForMenu] uuid];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid==%@",uuid];
        NSArray *results = [[_menuTableView menuList] filteredArrayUsingPredicate:predicate];
        if ([results count] > 0) {
            NSMenu *miniMenu = [[NSMenu alloc] init];
            NSMenuItem *theItem = [[NSMenuItem alloc] initWithTitle:@"Add favorite..." action:@selector(addItem:) keyEquivalent:@""];
            [miniMenu addItem:theItem];
            return miniMenu;
        } else {
            selection = [NSIndexSet indexSetWithIndex:rowForMenu];
            [self selectRowIndexes:selection byExtendingSelection:NO];
        }
    }
    NSString *title1 = @"";
    NSString *title2 = @"";
    NSString *title3 = @"";
    if ([selection count] > 1) {
        title1 = @"Add favorites to menu";
        if ([appDelegate askForConfirmation]) {
            title3 = @"Remove favorites...";
        } else {
            title3 = @"Remove favorites";
        }
    } else {
        title1 = @"Add favorite to menu";
        title2 = @"Edit favorite...";
        if ([appDelegate askForConfirmation]) {
            title3 = @"Remove favorite...";
        } else {
            title3 = @"Remove favorite";
        }
    }
    NSMenu *theMenu = [[NSMenu alloc] init];
    NSMenuItem *item1 = [[NSMenuItem alloc] initWithTitle:title1 action:@selector(moveItems:) keyEquivalent:@""];
    [theMenu addItem:item1];
    if ([selection count] == 1) {
        NSMenuItem *item2 = [[NSMenuItem alloc] initWithTitle:title2 action:@selector(editItem:) keyEquivalent:@""];
        [theMenu addItem:item2];
    }
    NSMenuItem *item3 = [[NSMenuItem alloc] initWithTitle:title3 action:@selector(removeItems:) keyEquivalent:@""];
    [theMenu addItem:item3];
    [theMenu addItem:[NSMenuItem separatorItem]];
    NSMenuItem *item4 = [[NSMenuItem alloc] initWithTitle:@"New favorite..." action:@selector(addItem:) keyEquivalent:@""];
    [theMenu addItem:item4];
    return theMenu;
}

- (void)addItem:(id)sender {
    [_favTableView addFavorite:self];
}

- (void)moveItems:(id)sender {
    [_favTableView moveFavorites:self];
}

- (void)editItem:(id)sender {
    [_favTableView editFavorite:self];
}

- (void)removeItems:(id)sender {
    [_favTableView removeFavorites:self];
}

@end
