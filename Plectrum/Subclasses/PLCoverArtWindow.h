//
//  PLCoverArtWindow.h
//  Plectrum
//
//  Created by Petros Loukareas on 27/09/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@class PLImageView;

@interface PLCoverArtWindow : NSWindow

@property (weak) IBOutlet NSBox *trackBox;
@property (weak) IBOutlet NSBox *buttonBox;
@property (weak) IBOutlet NSTextField *trackTitle;
@property (weak) IBOutlet NSTextField *trackArtist;
@property (weak) IBOutlet PLImageView *coverArt;

@property (weak) IBOutlet NSButton *prevButton;
@property (weak) IBOutlet NSButton *nextButton;

@property (assign) BOOL artWindowEnabled;
@property (assign) BOOL artWindowVisible;
@property (assign) BOOL artWindowOnScreen;
@property (assign) NSInteger showSongDetailsSelection;
@property (assign) NSInteger artWindowLevel;
@property (assign) BOOL showControlsOnMouseOver;

@property (assign) CGFloat opacity;
@property (assign) CGFloat boxOpacity;
@property (assign) BOOL isLocked;
@property (assign) BOOL isAnimating;
@property (assign) BOOL boxIsAnimating;
@property (assign) BOOL closeArtWindowEventReceived;
@property (assign) BOOL demoMode;

- (IBAction)toggleLockPosAndSize:(id)sender;
- (IBAction)glueWindowToDesktop:(id)sender;
- (IBAction)keepBehindOtherWindows:(id)sender;
- (IBAction)treatAsRegularWindow:(id)sender;
- (IBAction)keepAboveOtherWindows:(id)sender;

- (void)fadeIn;
- (void)fadeOut;
- (void)setTrackTitle:(NSString *)track trackArtist:(NSString *)artist trackArt:(NSImage *)artwork;
- (void)setImageOnly:(NSImage *)image;
- (void)windowDidResize:(NSNotification *)notification;
- (void)setStatus;
- (void)receiveStatus:(NSString *)statusString;
- (void)updateWindowOpacity;
- (void)updateBoxOpacity;
- (void)artWindowLevelByNumber:(NSInteger)levelNumber;
- (void)updateWindowLevel;
- (void)reflectLockStatus;
- (void)updateBoxVisibility;
- (void)updateControlsVisibility;
- (void)beginDemoMode;
- (void)endDemoMode;
- (void)reflectSettings;
- (void)prepareForFirstUse;
- (void)renewTrackingArea;

@end

NS_ASSUME_NONNULL_END
