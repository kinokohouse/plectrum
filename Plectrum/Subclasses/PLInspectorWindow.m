//
//  PLInspectorWindow.m
//  Plectrum
//
//  Created by Petros Loukareas on 25/08/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLInspectorWindow.h"

@interface PLInspectorWindow() <NSWindowDelegate>

@end


@implementation PLInspectorWindow

- (BOOL)windowShouldClose:(NSNotification *)notification {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [appDelegate hideInspector];
    return NO;
}

@end
