//
//  PLRecordingListView.h
//  Plectrum
//
//  Created by Petros Loukareas on 13/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLRecordingListView : NSTableCellView

@property (weak) IBOutlet NSTextField *fileType;
@property (weak) IBOutlet NSTextField *fileSize;
@property (weak) IBOutlet NSTextField *fileTime;
@property (weak) IBOutlet NSTextField *fileURL;

@end

NS_ASSUME_NONNULL_END
