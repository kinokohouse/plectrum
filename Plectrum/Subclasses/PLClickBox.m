//
//  PLClickBox.m
//  Plectrum
//
//  Created by Petros Loukareas on 17/07/2021.
//  Copyright © 2021 Petros Loukareas. All rights reserved.
//

#import "PLClickBox.h"

@interface PLClickBox()

@property (strong) NSTrackingArea *trackingArea;
@property (strong) NSCursor *savedCursor;

@end


@implementation PLClickBox

- (void)updateTrackingAreas {
    [super updateTrackingAreas];
    [self ensureTrackingAreas];
    if (![[self trackingAreas] containsObject:_trackingArea]) {
        [self addTrackingArea:_trackingArea];
    }
}

- (void)ensureTrackingAreas {
    if (_trackingArea == nil) {
        self.trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:NSTrackingInVisibleRect | NSTrackingActiveAlways | NSTrackingMouseEnteredAndExited owner:self userInfo:nil];
    }
}

- (void)mouseDown:(NSEvent *)event {
    NSURL *url = [NSURL URLWithString:@"https://www.radio-browser.info/"];
    [[NSWorkspace sharedWorkspace] openURL:url];
}

- (void)mouseEntered:(NSEvent *)event {
    [[NSCursor currentCursor] push];
    [[NSCursor pointingHandCursor] set];
}

- (void)mouseExited:(NSEvent *)event {
    [NSCursor pop];
}
@end
