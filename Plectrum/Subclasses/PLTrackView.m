//
//  PLTrackView.m
//  Plectrum
//
//  Created by Petros Loukareas on 13-05-19.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLTrackView.h"
#import "AppDelegate.h"

@implementation PLTrackView

- (void)mouseUp:(NSEvent*) event {
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] performMenuSelectionByIdentifier:6];
}

@end
