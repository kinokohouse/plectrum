//
//  PLRecordingTableView.h
//  Plectrum
//
//  Created by Petros Loukareas on 19/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLRecordingTableView : NSTableView

@end

NS_ASSUME_NONNULL_END
