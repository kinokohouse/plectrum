//
//  PLPlusMinusButton.m
//  Plectrum
//
//  Created by Petros Loukareas on 22/08/2019.
//  Copyright (c) 2019-2021 Kinoko House. MIT License.
//

#import "PLPlusMinusButton.h"

@implementation PLPlusMinusButton

- (void)mouseDown:(NSEvent *)event {
    _buttonPosition = event.locationInWindow.x;
    [self setFirstTimer];
    [self highlight:YES];
}

- (void)mouseUp:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([_repeatTimer isValid]) {
        [_repeatTimer invalidate];
        _repeatTimer = nil;
    }
    if (_buttonPosition < 30) {
        [appDelegate decreaseBPM];
    } else {
        [appDelegate increaseBPM];
    }
    [self highlight:NO];
    [appDelegate applyNewBPM];
}

- (void)setFirstTimer {
    _repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setNextTimer) userInfo:nil repeats:NO];
}

- (void)setNextTimer {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if (_buttonPosition < 30) {
        [appDelegate decreaseBPM];
    } else {
        [appDelegate increaseBPM];
    }
    _repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(setNextTimer) userInfo:nil repeats:NO];
}

@end
