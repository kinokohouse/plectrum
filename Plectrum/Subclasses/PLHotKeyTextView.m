//
//  PLHotKeyTextView.m
//  Plectrum
//
//  Created by Petros Loukareas on 24-01-18.
//  Copyright (c) 2018 Kinoko House. MIT License.
//

#pragma mark - Includes

#import "PLHotKeyTextView.h"
#import "PLHotKeyCenter.h"
#import "AppDelegate.h"


#pragma mark - Constants

// if below const is NO, then at least either ctrl or cmd is mandatory
const BOOL ALLOW_NO_CMD_KEYS = NO;

@interface PLHotKeyTextView()

@property (assign) BOOL wasSet;
@property (assign) unsigned long prevKeyCode;
@property (assign) unsigned long prevModMask;
@property (strong) NSString *prevKeyString;

@property (assign) BOOL blocksFirstResponder;
@property (assign) BOOL isEditing;

@end


@implementation PLHotKeyTextView


#pragma mark - The Basics

- (void)awakeFromNib {
    [self setSelectedTextAttributes:@{}];
    if (@available(macOS 10.10, *)) {
        NSEdgeInsets insets = [(NSClipView *)[self superview] contentInsets];
        insets.right = 32.0f;
        [(NSClipView *)[self superview] setContentInsets:insets];
     }
    [self setAlignment:NSTextAlignmentLeft];
    _blocksFirstResponder = YES;
}

- (BOOL)becomeFirstResponder {
    _isEditing = YES;
    self.wasSet = self.isSet;
    self.prevKeyCode = self.keyCode;
    self.prevModMask = self.modMask;
    self.prevKeyString = self.keyString;
    PLHotKey *hotKey = [_hotKeyCenter getHotKeyWithKeyID:[[self identifier] intValue]];
    if (hotKey) [_hotKeyCenter unregisterHotKey:hotKey];
    [_hotKeyCenter setIsPaused:YES];
    [self setTextColor:[NSColor disabledControlTextColor]];
    [self setString:@"[enter key]"];
    [[self innerBox] setHidden:NO];
    return [[self window] makeFirstResponder:self];
}

- (BOOL)resignFirstResponder {
    _blocksFirstResponder = YES;
    if (_isEditing) {
        if ([self isSet]) {
            [self setString:[self keyString]];
            [self setTextColor:[NSColor controlTextColor]];
            [_hotKeyCenter registerHotKey:_keyCode withModMask:_modMask andKeyString:_keyString withKeyID:[[self identifier] intValue]];
        } else {
            [self setString:@"[not set]"];
            [self setTextColor:[NSColor disabledControlTextColor]];
        }
    }
    _isEditing = NO;
    [[self innerBox] setHidden:YES];
    [_hotKeyCenter setIsPaused:NO];
    return [[self window] resignFirstResponder];
}

- (BOOL)acceptsFirstResponder {
    return !_blocksFirstResponder;
}

- (void)mouseDown:(NSEvent *)event {
    _blocksFirstResponder = NO;
    [[self window] makeFirstResponder:self];
}

- (IBAction)clearField:(id)sender {
    int keyID = [[self identifier] intValue];
    PLHotKey *hotKey;
    _isEditing = NO;
    if ((self.keyString == nil) || ([self.keyString isEqualToString:@""])) {
        [[self window] makeFirstResponder:[self clearButton]];
        return;
    } else {
        self.font = [NSFont systemFontOfSize:13.0f];
        self.textColor = [NSColor disabledControlTextColor];
        [self setString:@"[not set]"];
        self.keyCode = 0;
        self.modMask = 0;
        self.keyString = @"";
        self.isSet = NO;
        [self setSelectionColor];
        [[self window] makeFirstResponder:[self clearButton]];
    }
    hotKey = [_hotKeyCenter getHotKeyWithKeyID:keyID];
    if (hotKey) {
        [_hotKeyCenter unregisterHotKey:hotKey];
    } else {
        NSLog(@"Unknown PLHotKeyTextView - Bug? In any event, doing nothing.");
    }
}

- (IBAction)cancelNewKey:(id)sender {
    if (!_isEditing) return;
    int keyID = [[self identifier] intValue];
    _isEditing = NO;
    if ((self.prevKeyString == nil) || ([self.prevKeyString isEqualToString:@""])) {
        self.textColor = [NSColor disabledControlTextColor];
        [self setString:@"[not set]"];
    } else {
        self.font = [NSFont systemFontOfSize:13.0f];
        if (self.wasSet) {
            self.keyCode = self.prevKeyCode;
            self.modMask = self.prevModMask;
            self.keyString = self.prevKeyString;
            self.isSet = YES;
            self.textColor = [NSColor controlTextColor];
            [self setString:self.keyString];
            [_hotKeyCenter registerHotKey:_keyCode withModMask:_modMask andKeyString:_keyString withKeyID:keyID];
        } else {
            self.keyCode = 0;
            self.modMask = 0;
            self.keyString = @"";
            self.isSet = NO;
            self.textColor = [NSColor disabledControlTextColor];
            [self setString:@"[not set]"];
        }
    }
    [[self window] makeFirstResponder:[self clearButton]];
}


#pragma mark - Instance Creation

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self getKeyDict];
    }
    return self;
}


#pragma mark - Key Events

- (void)keyDown:(NSEvent *)event {
    int keyID = [[self identifier] intValue];
    _isEditing = YES;
    unsigned long modflags = (unsigned long)event.modifierFlags;
    int keycode = event.keyCode;
    if ((modflags == self.modMask) && (keycode == self.keyCode)) {
        [self cancelNewKey:self];
        return;
    }
    if (!ALLOW_NO_CMD_KEYS) {
        // disallow unmodified tab nonetheless
        if (keycode == 0x30 && ((modflags & NSEventModifierFlagControl) == 0) && ((modflags & NSEventModifierFlagCommand) == 0)) {
            NSBeep();
            return;
        }
        // disallow unmodified esc too, make it work as cancel
        if (keycode == 0x35 && ((modflags & NSEventModifierFlagControl) == 0) && ((modflags & NSEventModifierFlagCommand) == 0) && ((modflags & NSEventModifierFlagOption) == 0)) {
            if (((modflags & NSEventModifierFlagShift) == 0) && ((modflags & NSEventModifierFlagOption) == 0)) {
                [self cancelNewKey:self];
            } else {
                NSBeep();
            }
            return;
        }
        if (((modflags & NSEventModifierFlagControl) == 0) && (((modflags & NSEventModifierFlagCommand) == 0))) {
            NSBeep();
            return;
        }
    }
    NSString *keycombo = @"";
    if ((modflags & NSEventModifierFlagControl) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌃", keycombo];
    }
    if ((modflags & NSEventModifierFlagOption) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌥", keycombo];
    }
    if ((modflags & NSEventModifierFlagShift) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⇧", keycombo];
    }
    if ((modflags & NSEventModifierFlagCommand) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌘", keycombo];
    }
    // check for 'forbidden' key combos (regular paste, select all, etc.)
    NSString *downKey = [self findSymbolForKey:keycode];
    if (downKey == nil) {
        NSBeep();
        [[self window] makeFirstResponder:[self clearButton]];
        _isEditing = NO;
        return;
    }
    keycombo = [NSString stringWithFormat:@"%@%@", keycombo, [self findSymbolForKey:keycode]];
    if ([@[@"⌘A",@"⌘C",@"⌘V",@"⌘X",@"⌘Z"] indexOfObject:keycombo] != NSNotFound) {
        NSBeep();
        [[self window] makeFirstResponder:[self clearButton]];
        _isEditing = NO;
        return;
    }
    if ([keycombo isEqualToString:self.keyString]) {
        [self cancelNewKey:self];
        return;
    }
    self.font = [NSFont systemFontOfSize:13.0f];
    self.textColor = [NSColor controlTextColor];
    [self setString:keycombo];
    self.keyCode = keycode;
    self.modMask = modflags;
    self.keyString = keycombo;
    self.prevKeyCode = 0;
    self.prevModMask = 0;
    self.prevKeyString = @"";
    self.isSet = YES;
    [self setSelectionColor];
    [_hotKeyCenter registerHotKey:keycode withModMask:_modMask andKeyString:keycombo withKeyID:keyID];
    [[self window] makeFirstResponder:[self clearButton]];
}


#pragma mark - Utilities

- (void)getKeyDict {
    if (!_keyDict) {
        _keyDict = [NSDictionary dictionaryWithObjectsAndKeys:
                    @0x00, @"A",
                    @0x0B, @"B",
                    @0x08, @"C",
                    @0x02, @"D",
                    @0x0E, @"E",
                    @0x03, @"F",
                    @0x05, @"G",
                    @0x04, @"H",
                    @0x22, @"I",
                    @0x26, @"J",
                    @0x28, @"K",
                    @0x25, @"L",
                    @0x2E, @"M",
                    @0x2D, @"N",
                    @0x1F, @"O",
                    @0x23, @"P",
                    @0x0C, @"Q",
                    @0x0F, @"R",
                    @0x01, @"S",
                    @0x11, @"T",
                    @0x20, @"U",
                    @0x09, @"V",
                    @0x0D, @"W",
                    @0x07, @"X",
                    @0x10, @"Y",
                    @0x06, @"Z",
                    @0x1D, @"0",
                    @0x12, @"1",
                    @0x13, @"2",
                    @0x14, @"3",
                    @0x15, @"4",
                    @0x17, @"5",
                    @0x16, @"6",
                    @0x1A, @"7",
                    @0x1C, @"8",
                    @0x19, @"9",
                    @0x32, @"`",
                    @0x0A, @"§",
                    @0x2B, @",",
                    @0x2F, @".",
                    @0x1B, @"-",
                    @0x18, @"=",
                    @0x2C, @"/",
                    @0x2A, @"\\",
                    @0x27, @"'",
                    @0x29, @";",
                    @0x1E, @"]",
                    @0x21, @"[",
                    @0x7A, @"F1",
                    @0x78, @"F2",
                    @0x63, @"F3",
                    @0x76, @"F4",
                    @0x60, @"F5",
                    @0x61, @"F6",
                    @0x62, @"F7",
                    @0x64, @"F8",
                    @0x65, @"F9",
                    @0x6D, @"F10",
                    @0x67, @"F11",
                    @0x6F, @"F12",
                    @0x69, @"F13",
                    @0x6B, @"F14",
                    @0x71, @"F15",
                    @0x35, @"⎋",
                    @0x31, @"Space",
                    @0x24, @"↩",
                    @0x4C, @"⌤",
                    @0x30, @"⇥",
                    @0x33, @"⌫",
                    @0x7B, @"←",
                    @0x7C, @"→",
                    @0x7D, @"↓",
                    @0x7E, @"↑",
                    nil];
    }
}

- (NSString *)findSymbolForKey:(int)keyCode {
    NSArray *keyArray = [_keyDict allKeysForObject:[NSNumber numberWithInt:keyCode]];
    if ([keyArray count] == 0) {
        return nil;
    } else {
        return [keyArray objectAtIndex:0];
    }
}

- (void)setHotKeyTextViewWithKeyCode:(unsigned long)keycode modMask:(unsigned long)modMask keyString:(NSString *)keyString {
    if ([keyString isEqualToString:@""]) {
        self.font = [NSFont systemFontOfSize:13.0f];
        self.textColor = [NSColor disabledControlTextColor];
        [self setString:@"[not set]"];
        self.keyString = @"";
        self.keyCode = 0;
        self.modMask = 0;
        self.isSet = NO;
    } else {
        self.font = [NSFont systemFontOfSize:13.0f];
        self.textColor = [NSColor controlTextColor];
        [self setString:keyString];
        self.keyString = keyString;
        self.keyCode = keycode;
        self.modMask = modMask;
        self.isSet = YES;
    }
}

- (void)setSelectionColor {
    NSColor *foregroundSelectionColor;
    if ([self isSet]) {
        foregroundSelectionColor = [NSColor controlTextColor];
    } else {
        foregroundSelectionColor = [NSColor disabledControlTextColor];
    }
    [self setSelectedTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[NSColor controlBackgroundColor], NSBackgroundColorAttributeName, foregroundSelectionColor, NSForegroundColorAttributeName, nil]];
}

@end
