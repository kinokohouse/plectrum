//
//  PLRadioRecording.m
//  Plectrum
//
//  Created by Petros Loukareas on 13/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRadioRecording.h"
#import "PLUtilities.h"


@interface PLRadioRecording() <NSCoding, NSCopying>

@end


@implementation PLRadioRecording


- (id)init {
    if (self = [super init]) {
        [self setFileName:@""];
        [self setUuid:[PLUtilities createUUID]];
        [self setFileDate:[PLUtilities sortableDateString]];
        [self setFilePath:@""];
        [self setFileType:@""];
        [self setFileSize:@""];
        [self setFileSizeInBytes:@"00000000000000000000000000000000"];
        [self setFileTime:@""];
        [self setOriginURL:@""];
        return self;
    } else {
        return nil;
    }
}

- (id)initWithName:(NSString *)fileName path:(NSString *)filePath type:(NSString *)fileType size:(NSString *)fileSize byteSize:(long long)fileSizeInBytes time:(NSString *)fileTime url:(NSString *)originURL {
    if (self = [super init]) {
        [self setFileName:fileName];
        [self setUuid:[PLUtilities createUUID]];
        [self setFileDate:[PLUtilities sortableDateString]];
        [self setFilePath:filePath];
        [self setFileType:fileType];
        [self setFileSize:fileSize];
        [self setFileSizeInBytes:[NSString stringWithFormat:@"%032lld", fileSizeInBytes]];
        [self setFileTime:fileTime];
        [self setOriginURL:originURL];
        return self;
    } else {
        return nil;
    }
}

- (id)copyWithZone:(NSZone * _Nullable)zone {
    PLRadioRecording *copiedRecording = [[[self class] allocWithZone:zone] init];
    if (copiedRecording) {
        [copiedRecording setFileName:[self fileName]];
        [copiedRecording setUuid:[self uuid]];
        [copiedRecording setFileDate:[self fileDate]];
        [copiedRecording setFilePath:[self filePath]];
        [copiedRecording setFileType:[self fileType]];
        [copiedRecording setFileSize:[self fileSize]];
        [copiedRecording setFileSizeInBytes:[self fileSizeInBytes]];
        [copiedRecording setFileTime:[self fileTime]];
        [copiedRecording setOriginURL:[self originURL]];
    }
    return copiedRecording;
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    if (self = [super init]) {
        [self setFileName:[aDecoder decodeObjectForKey:@"fileName"]];
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setFilePath:[aDecoder decodeObjectForKey:@"fileDate"]];
        [self setFilePath:[aDecoder decodeObjectForKey:@"filePath"]];
        [self setFileType:[aDecoder decodeObjectForKey:@"fileType"]];
        [self setFileSize:[aDecoder decodeObjectForKey:@"fileSize"]];
        [self setFileSizeInBytes:[aDecoder decodeObjectForKey:@"fileSizeInBytes"]];
        [self setFileTime:[aDecoder decodeObjectForKey:@"fileTime"]];
        [self setOriginURL:[aDecoder decodeObjectForKey:@"originURL"]];
        return self;
    } else {
        return nil;
    }
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    [aCoder encodeObject:[self fileName] forKey:@"fileName"];
    [aCoder encodeObject:[self uuid] forKey:@"uuid"];
    [aCoder encodeObject:[self fileDate] forKey:@"fileDate"];
    [aCoder encodeObject:[self filePath] forKey:@"filePath"];
    [aCoder encodeObject:[self fileType] forKey:@"fileType"];
    [aCoder encodeObject:[self fileSize] forKey:@"fileSize"];
    [aCoder encodeObject:[self fileSizeInBytes] forKey:@"fileSizeInBytes"];
    [aCoder encodeObject:[self fileTime] forKey:@"fileTime"];
    [aCoder encodeObject:[self originURL] forKey:@"originURL"];
}

@end
