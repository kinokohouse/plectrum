//
//  PLHotKey.m
//  StuffBox
//
//  Created by Petros Loukareas on 14/12/2022.
//  Copyright © 2022 Petros Loukareas. All rights reserved.
//

#import "PLHotKey.h"

@interface PLHotKey() <NSCoding, NSCopying>

@end


@implementation PLHotKey

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setKeyCode:0];
        [self setModMask:0];
        [self setKeyID:0];
        [self setKeyString:@""];
        [self setRefStruct:[NSValue valueWithPointer:nil]];
        [self setHtkStruct:[NSValue valueWithPointer:nil]];
        return self;
    }
    return nil;
}

- (instancetype)initWithKeyCode:(unsigned long)keyCode modMask:(unsigned long)modMask keyString:(NSString *)keyString {
    self = [super init];
    if (self) {
        [self setKeyCode:keyCode];
        [self setModMask:modMask];
        [self setKeyID:0];
        [self setKeyString:keyString];
        [self setRefStruct:[NSValue valueWithPointer:nil]];
        [self setHtkStruct:[NSValue valueWithPointer:nil]];
        return self;
    }
    return nil;
}

- (instancetype)initWithKeyCode:(unsigned long)keyCode modMask:(unsigned long)modMask keyString:(NSString *)keyString refStruct:(NSValue *)refStruct htkStruct:(NSValue *)htkStruct {
    self = [super init];
    if (self) {
        [self setKeyCode:keyCode];
        [self setModMask:modMask];
        [self setKeyID:0];
        [self setKeyString:keyString];
        [self setRefStruct:refStruct];
        [self setHtkStruct:htkStruct];
        return self;
    }
    return nil;
}


#pragma mark - NSCopying

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    PLHotKey *copiedHotKey = [[[self class] allocWithZone:zone] init];
    if (copiedHotKey) {
        [copiedHotKey setKeyCode:[self keyCode]];
        [copiedHotKey setModMask:[self modMask]];
        [copiedHotKey setKeyID:[self keyID]];
        [copiedHotKey setKeyString:[self keyString]];
        [copiedHotKey setRefStruct:[self refStruct]];
        [copiedHotKey setHtkStruct:[self htkStruct]];
        return copiedHotKey;
    } else {
        return nil;
    }
}


#pragma mark - NSCoding

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        unsigned long keyCode = [[aDecoder decodeObjectForKey:@"keyCode"] unsignedLongValue];
        [self setKeyCode:keyCode];
        unsigned long modMask = [[aDecoder decodeObjectForKey:@"modMask"] unsignedLongValue];
        [self setModMask:modMask];
        [self setKeyID:[aDecoder decodeIntForKey:@"keyID"]];
        [self setKeyString:[aDecoder decodeObjectForKey:@"keyString"]];
        [self setRefStruct:[NSValue valueWithPointer:nil]];
        [self setHtkStruct:[NSValue valueWithPointer:nil]];
        return self;
    }
    return nil;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    NSString *keyCode = [NSString stringWithFormat:@"%lu", [self keyCode]];
    [aCoder encodeObject:keyCode forKey:@"keyCode"];
    NSString *modMask = [NSString stringWithFormat:@"%lu", [self modMask]];
    [aCoder encodeObject:modMask forKey:@"modMask"];
    [aCoder encodeInt:[self keyID] forKey:@"keyID"];
    [aCoder encodeObject:[self keyString] forKey:@"keyString"];
}

@end
