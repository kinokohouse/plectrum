//
//  PLHotKey.h
//  StuffBox
//
//  Created by Petros Loukareas on 14/12/2022.
//  Copyright © 2022 Petros Loukareas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLUtilities.h"
#import "PLHotKeyTextView.h"

NS_ASSUME_NONNULL_BEGIN


@interface PLHotKey : NSObject

@property (assign) unsigned long keyCode;
@property (assign) unsigned long modMask;
@property (assign) int keyID;
@property (copy) NSString *keyString;
@property (strong) NSValue *refStruct;
@property (strong) NSValue *htkStruct;

- (instancetype)init;
- (instancetype)initWithKeyCode:(unsigned long)keyCode modMask:(unsigned long)modMask keyString:(NSString *)keyString;
- (instancetype)initWithKeyCode:(unsigned long)keyCode modMask:(unsigned long)modMask keyString:(NSString *)keyString refStruct:(NSValue *)refStruct htkStruct:(NSValue *)htkStruct;

@end


NS_ASSUME_NONNULL_END
