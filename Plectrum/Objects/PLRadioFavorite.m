//
//  PLRadioFavorite.m
//  Plectrum
//
//  Created by Petros Loukareas on 27/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRadioFavorite.h"
#import "PLUtilities.h"


@interface PLRadioFavorite() <NSCoding, NSCopying>

@end


@implementation PLRadioFavorite


#pragma mark - Object Methods

- (id)init {
    if (self = [super init]) {
        [self setName:@""];
        [self setUuid:[PLUtilities createUUID]];
        [self setGenre:@""];
        [self setCountry:@""];
        [self setUrl:@""];
        [self setAltinfo:@""];
        [self setImage:[NSImage imageNamed:@"stream"]];
        [self setFetchArt:NO];
        [self setFetchType:NO];
        [self setArtUrl:@""];
        [self setSecondaryString:@""];
        [self setPrefix:@""];
        [self setDeferNotification:NO];
        [self setIgnoreDoubleNotifications:NO];
        [self setStationuuid:@""];
        return self;
    } else {
        return nil;
    }
}

- (id)initWithName:(NSString *)name genre:(NSString *)genre country:(NSString *)country url:(NSString *)url altinfo:(NSString *)altinfo image:(NSImage *)image fetchArt:(BOOL)fetchArt fetchType:(BOOL)fetchType textTransform:(BOOL)textTransform textTransformType:(NSInteger)textTransformType artUrl:(nonnull NSString *)artUrl secondaryString:(nonnull NSString *)secondaryString prefix:(nonnull NSString *)prefix deferNotification:(BOOL)deferNotification ignoreDoubleNotifications:(BOOL)ignoreDoubleNotifications stationuuid:(NSString *)stationuuid {
    if (self = [super init]) {
        [self setName:name];
        [self setUuid:[PLUtilities createUUID]];
        [self setGenre:genre];
        [self setCountry:country];
        [self setUrl:url];
        [self setAltinfo:altinfo];
        if (![self altinfo]) [self setAltinfo:@""];
        [self setImage:image];
        [self setFetchArt:fetchArt];
        [self setFetchType:fetchType];
        [self setTextTransform:textTransform];
        [self setTextTransformType:textTransformType];
        [self setArtUrl:artUrl];
        if (![self artUrl]) [self setArtUrl:@""];
        [self setSecondaryString:secondaryString];
        if (![self secondaryString]) [self setSecondaryString:@""];
        [self setPrefix:prefix];
        if (![self prefix]) [self setPrefix:@""];
        [self setDeferNotification:deferNotification];
        if (![self deferNotification]) [self setDeferNotification:NO];
        [self setIgnoreDoubleNotifications:ignoreDoubleNotifications];
        if (![self ignoreDoubleNotifications]) [self setIgnoreDoubleNotifications:NO];
        [self setStationuuid:stationuuid];
        return self;
    } else {
        return nil;
    }
}

- (id)initDummy {
    if (self = [super init]) {
        [self setName:@""];
        [self setUuid:@""];
        [self setGenre:@""];
        [self setCountry:@""];
        [self setUrl:@""];
        [self setAltinfo:@""];
        [self setImage:[NSImage imageNamed:@"stream"]];
        [self setFetchArt:NO];
        [self setFetchType:NO];
        [self setTextTransform:NO];
        [self setTextTransformType:0];
        [self setArtUrl:@""];
        [self setSecondaryString:@""];
        [self setPrefix:@""];
        [self setDeferNotification:NO];
        [self setDeferNotification:NO];
        [self setStationuuid:@""];
        return self;
    } else {
        return nil;
    }
}


#pragma mark - NSCopying Implementation

- (id)copyWithZone:(NSZone * _Nullable)zone {
    PLRadioFavorite *copiedFav = [[[self class] allocWithZone:zone] init];
    if (copiedFav) {
        [copiedFav setName:[self name]];
        [copiedFav setUuid:[self uuid]];
        [copiedFav setGenre:[self genre]];
        [copiedFav setCountry:[self country]];
        [copiedFav setUrl:[self url]];
        [copiedFav setAltinfo:[self altinfo]];
        if (![copiedFav altinfo]) [copiedFav setAltinfo:@""];
        [copiedFav setImage:[self image]];
        [copiedFav setFetchArt:[self fetchArt]];
        [copiedFav setFetchType:[self fetchType]];
        [copiedFav setTextTransform:[self textTransform]];
        if (![copiedFav textTransform]) [copiedFav setTextTransform:NO];
        [copiedFav setTextTransformType:[self textTransformType]];
        if (![copiedFav textTransformType]) [copiedFav setTextTransformType:0];
        [copiedFav setArtUrl:[self artUrl]];
        if (![copiedFav artUrl]) [copiedFav setArtUrl:@""];
        [copiedFav setSecondaryString:[self secondaryString]];
        if (![copiedFav secondaryString]) [copiedFav setSecondaryString:@""];
        [copiedFav setPrefix:[self prefix]];
        if (![copiedFav prefix]) [copiedFav setPrefix:@""];
        [copiedFav setDeferNotification:[self deferNotification]];
        if (![copiedFav deferNotification]) [copiedFav setDeferNotification:NO];
        [copiedFav setIgnoreDoubleNotifications:[self ignoreDoubleNotifications]];
        if (![copiedFav ignoreDoubleNotifications]) [copiedFav setIgnoreDoubleNotifications:NO];
        [copiedFav setStationuuid:[self stationuuid]];
        if (![copiedFav stationuuid]) [copiedFav setStationuuid:@""];
        return copiedFav;
    } else {
        return nil;
    }
}


#pragma mark - NSCoding Implementation

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        [self setName:[aDecoder decodeObjectForKey:@"name"]];
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setGenre:[aDecoder decodeObjectForKey:@"genre"]];
        [self setCountry:[aDecoder decodeObjectForKey:@"country"]];
        [self setUrl:[aDecoder decodeObjectForKey:@"url"]];
        [self setAltinfo:[aDecoder decodeObjectForKey:@"altinfo"]];
        if (![self altinfo]) [self setAltinfo:@""];
        [self setImage:[aDecoder decodeObjectForKey:@"image"]];
        [self setFetchArt:[aDecoder decodeBoolForKey:@"fetchArt"]];
        [self setFetchType:[aDecoder decodeBoolForKey:@"fetchType"]];
        [self setTextTransform:[aDecoder decodeBoolForKey:@"textTransform"]];
        if (![self textTransform]) [self setTextTransform:NO];
        [self setTextTransformType:[aDecoder decodeIntegerForKey:@"textTransformType"]];
        if (![self textTransformType]) [self setTextTransformType:0];
        [self setArtUrl:[aDecoder decodeObjectForKey:@"artUrl"]];
        if (![self artUrl]) [self setArtUrl:@""];
        [self setSecondaryString:[aDecoder decodeObjectForKey:@"secondaryString"]];
        if (![self secondaryString]) [self setSecondaryString:@""];
        [self setPrefix:[aDecoder decodeObjectForKey:@"prefix"]];
        if (![self prefix]) [self setPrefix:@""];
        [self setDeferNotification:[aDecoder decodeBoolForKey:@"deferNotification"]];
        if (![self deferNotification]) [self setDeferNotification:NO];
        [self setIgnoreDoubleNotifications:[aDecoder decodeBoolForKey:@"ignoreDoubleNotifications"]];
        if (![self ignoreDoubleNotifications]) [self setIgnoreDoubleNotifications:NO];
        [self setStationuuid:[aDecoder decodeObjectForKey:@"stationuuid"]];
        if (![self stationuuid]) [self setStationuuid:@""];
        return self;
    } else {
        return nil;
    }
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self name] forKey:@"name"];
    [aCoder encodeObject:[self uuid] forKey:@"uuid"];
    [aCoder encodeObject:[self genre] forKey:@"genre"];
    [aCoder encodeObject:[self country] forKey:@"country"];
    [aCoder encodeObject:[self url] forKey:@"url"];
    [aCoder encodeObject:[self image] forKey:@"image"];
    [aCoder encodeBool:[self fetchArt] forKey:@"fetchArt"];
    [aCoder encodeBool:[self fetchType] forKey:@"fetchType"];
    if ([self textTransform]) {
        [aCoder encodeBool:[self textTransform] forKey:@"textTransform"];
    } else {
        [aCoder encodeBool:NO forKey:@"textTransform"];
    }
    if ([self textTransformType]) {
        [aCoder encodeInteger:[self textTransformType] forKey:@"textTransformType"];
    } else {
        [aCoder encodeInteger:0 forKey:@"textTransformType"];
    }
    if ([self altinfo]) {
        [aCoder encodeObject:[self altinfo] forKey:@"altinfo"];
    } else {
        [aCoder encodeObject:@"" forKey:@"altinfo"];
    }
    if ([self artUrl]) {
        [aCoder encodeObject:[self artUrl] forKey:@"artUrl"];
    } else {
        [aCoder encodeObject:@"" forKey:@"artUrl"];
    }
    if ([self secondaryString]) {
        [aCoder encodeObject:[self secondaryString] forKey:@"secondaryString"];
    } else {
        [aCoder encodeObject:@"" forKey:@"secondaryString"];
    }
    if ([self prefix]) {
        [aCoder encodeObject:[self prefix] forKey:@"prefix"];
    } else {
        [aCoder encodeObject:@"" forKey:@"prefix"];
    }
    if ([self deferNotification]) {
        [aCoder encodeBool:[self deferNotification] forKey:@"deferNotification"];
    } else {
        [aCoder encodeBool:NO forKey:@"deferNotification"];
    }
    if ([self ignoreDoubleNotifications]) {
        [aCoder encodeBool:[self ignoreDoubleNotifications] forKey:@"ignoreDoubleNotifications"];
    } else {
        [aCoder encodeBool:NO forKey:@"ignoreDoubleNotifications"];
    }
    if ([self stationuuid]) {
        [aCoder encodeObject:[self stationuuid] forKey:@"stationuuid"];
    } else {
        [aCoder encodeObject:@"" forKey:@"stationuuid"];
    }
}

@end
