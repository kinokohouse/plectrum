//
//  PLRadioFavorite.h
//  Plectrum
//
//  Created by Petros Loukareas on 27/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLRadioFavorite : NSObject

@property (copy) NSString *name;
@property (copy) NSString *uuid;
@property (copy) NSString *genre;
@property (copy) NSString *country;
@property (copy) NSString *url;
@property (copy) NSString *altinfo;
@property (copy) NSImage *image;

@property (assign) BOOL fetchArt;
@property (assign) BOOL fetchType; // NO = JSON (keyPath), YES = HTML (Regex);
@property (assign) BOOL textTransform;
@property (assign) NSInteger textTransformType;
@property (copy) NSString *artUrl;
@property (copy) NSString *secondaryString;
@property (copy) NSString *prefix;
@property (assign) BOOL deferNotification;
@property (assign) BOOL ignoreDoubleNotifications;
@property (copy) NSString *stationuuid;

- (id)init;
- (id)initWithName:(NSString *)name genre:(NSString *)genre country:(NSString *)country url:(NSString *)url altinfo:(NSString *)altinfo image:(NSImage *)image fetchArt:(BOOL)fetchArt fetchType:(BOOL)fetchType textTransform:(BOOL)textTransform textTransformType:(NSInteger)textTransformType artUrl:(nonnull NSString *)artUrl secondaryString:(nonnull NSString *)secondaryString prefix:(nonnull NSString *)prefix deferNotification:(BOOL)deferNotification ignoreDoubleNotifications:(BOOL)ignoreDoubleNotifications stationuuid:(NSString *)stationuuid;
- (id)initDummy;
- (id)copyWithZone:(NSZone * _Nullable)zone;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end

NS_ASSUME_NONNULL_END
