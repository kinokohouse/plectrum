//
//  PLRadioRecording.h
//  Plectrum
//
//  Created by Petros Loukareas on 13/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLRadioRecording : NSObject

@property (copy) NSString *fileName;
@property (copy) NSString *uuid;
@property (copy) NSString *fileDate;
@property (copy) NSString *filePath;
@property (copy) NSString *fileType;
@property (copy) NSString *fileSize;
@property (copy) NSString *fileSizeInBytes;
@property (copy) NSString *fileTime;
@property (copy) NSString *originURL;

- (id)init;
- (id)initWithName:(NSString *)fileName path:(NSString *)filePath type:(NSString *)fileType size:(NSString *)fileSize byteSize:(long long)fileSizeInBytes time:(NSString *)fileTime url:(NSString *)originURL;
- (id)copyWithZone:(NSZone * _Nullable)zone;
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;


@end

NS_ASSUME_NONNULL_END
