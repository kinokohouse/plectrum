//
//  PLLeftTableViewController.m
//  Plectrum
//
//  Created by Petros Loukareas on 28/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLLeftTableViewController.h"
#import "PLRightTableViewController.h"
#import "PLRadioFavoriteListView.h"
#import "PLAudioPreviewBox.h"


@interface PLLeftTableViewController () <NSTableViewDelegate, NSTableViewDataSource>

@property (weak) IBOutlet PLRightTableViewController *menuTableView;

@property (weak) IBOutlet NSTextField *favName;
@property (weak) IBOutlet NSTextField *favGenre;
@property (weak) IBOutlet NSTextField *favCountry;
@property (weak) IBOutlet NSTextField *favURL;
@property (weak) IBOutlet NSTextField *favAltInfo;
@property (weak) IBOutlet NSImageView *favImage;

@property (weak) IBOutlet NSTextField *urlTextField;
@property (weak) IBOutlet NSTextField *postFixTextField;
@property (weak) IBOutlet NSButton *artgrabCheckBox;
@property (weak) IBOutlet NSPopUpButton *artgrabPopUpButton;
@property (weak) IBOutlet NSBox *artgrabBox;
@property (weak) IBOutlet NSTextField *artgrabURL;
@property (weak) IBOutlet NSTextField *artgrabSecondaryStringLabel;
@property (weak) IBOutlet NSTextField *artgrabSecondaryString;
@property (weak) IBOutlet NSPopUpButton *artgrabTextTransformPopUpButton;
@property (weak) IBOutlet NSTextField *artgrabExplanationTextLabel;
@property (weak) IBOutlet NSTextField *artgrabPrefix;
@property (weak) IBOutlet NSButton *deferNotificationCheckBox;
@property (weak) IBOutlet NSButton *ignoreDoubleNotificationsCheckBox;

@property (weak) IBOutlet PLAudioPreviewBox *previewBox;

@property (weak) IBOutlet NSWindow *editSheet;
@property (weak) IBOutlet NSBox *browserBox;

@property (strong) NSString *uuidHeldOver;

@property (strong) NSTimer *timeOutTimer;

@property (assign) BOOL artgrabActive;
@property (assign) BOOL htmlSelected;
@property (assign) BOOL transformationSelected;

@end

@implementation PLLeftTableViewController


#pragma mark - Awake From Nib

- (void)awakeFromNib {
    [_tableView registerForDraggedTypes:@[@"nl.kinoko-house.PLMenuToFavesDragItem"]];
    _favsToMove = [[NSMutableArray alloc] init];
    [_tryButton setTitle:@"Try"];
    _isTryingOut = NO;
}


#pragma mark - NSTableViewDelegate methods

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    PLRadioFavorite *fav = [_favList objectAtIndex:row];
    NSString *name = [fav name];
    NSString *uuid = [fav uuid];
    NSString *genre = [fav genre];
    NSString *country= [fav country];
    NSImage *image = [fav image];
    if (!image) image = [NSImage imageNamed:@"radio"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid==%@",uuid];
    NSArray *results = [[_menuTableView menuList] filteredArrayUsingPredicate:predicate];
    NSString *theIdentifier;
    if ([results count] > 0) {
        theIdentifier = @"emptystation";
    } else {
        theIdentifier = @"happystation";
    }
    PLRadioFavoriteListView *listview = [tableView makeViewWithIdentifier:theIdentifier owner:self];
    [[listview textField] setStringValue:name];
    [[listview stationGenre] setStringValue:genre];
    [[listview stationCountry] setStringValue:country];
    [[listview imageView] setImage:image];
    return listview;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    NSString *uuid = [[_favList objectAtIndex:row] uuid];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid==%@",uuid];
    NSArray *results = [[_menuTableView menuList] filteredArrayUsingPredicate:predicate];
    if ([results count] > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)tableView:(NSTableView *)tableView shouldEditTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return NO;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSIndexSet *selection = [[self tableView] selectedRowIndexes];
    if ([selection count] == 0) {
        [_moveButton setEnabled:NO];
        [_editButton setEnabled:NO];
        [_removeButton setEnabled:NO];
    } else if ([selection count] == 1) {
        [_moveButton setEnabled:YES];
        [_editButton setEnabled:YES];
        [_removeButton setEnabled:YES];
    } else {
        [_moveButton setEnabled:YES];
        [_editButton setEnabled:NO];
        [_removeButton setEnabled:YES];
    }
}


#pragma mark - NSTableViewDataSource methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_favList count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return [_favList objectAtIndex:row];
}


#pragma mark - Drag and Drop methods

- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard {
    NSUInteger n = [rowIndexes firstIndex];
    BOOL noDrag = NO;
    do {
        if ([[[tableView viewAtColumn:0 row:n makeIfNecessary:NO] identifier] isEqualToString:@"emptystation"]) {
            noDrag = YES;
            break;
        } else {
            n = [rowIndexes indexGreaterThanIndex:n];
        }
    } while (n != NSIntegerMax && noDrag == NO);
    if (noDrag) return NO;
    [[NSCursor closedHandCursor] set];
    [pboard writeObjects:@[[NSString stringWithFormat:@"%@", rowIndexes]]];
    [_favsToMove removeAllObjects];
    _favsToMove = [[_favsToMove arrayByAddingObjectsFromArray:[_favList objectsAtIndexes:rowIndexes]] mutableCopy];
    [pboard declareTypes:@[@"nl.kinoko-house.PLFavesToMenuDragItem"] owner:nil];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation {
    NSString *theType = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] objectAtIndex:0];
    if ([theType isEqualToString:@"nl.kinoko-house.PLMenuToFavesDragItem"]) {
        [tableView setDropRow:row dropOperation:NSTableViewDropAbove];
        return NSDragOperationMove;
    } else {
        [[NSCursor arrowCursor] set];
        return NSDragOperationNone;
    }
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    [[NSCursor arrowCursor] set];
    NSArray <PLRadioFavorite *> *draggedFavs;
    BOOL ourObjectIsOnPasteBoard = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] containsObject:@"nl.kinoko-house.PLMenuToFavesDragItem"];
    if (ourObjectIsOnPasteBoard) {
        draggedFavs = [[_menuTableView menusToMove] mutableCopy];
    } else {
        return NO;
    }
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    for (PLRadioFavorite *d in draggedFavs) {
        for (int i = 0; i < [[_menuTableView menuList] count]; i++) {
            if ([[[[_menuTableView menuList] objectAtIndex:i] uuid] isEqualToString:[d uuid]]) {
                [indexSet addIndex:i];
                break;
            }
        }
    }
    [[_menuTableView tableView] beginUpdates];
    [[_menuTableView tableView] removeRowsAtIndexes:indexSet withAnimation:0x31];
    for (PLRadioFavorite *d in draggedFavs) {
        [_menuTableView removeItemWithUuid:[d uuid]];
    }
    [[_menuTableView tableView] endUpdates];
    [[_menuTableView tableView] reloadData];
    [tableView reloadData];
    [self selectNothing];
    [_menuTableView selectNothing];
    return YES;
}


#pragma mark - Utility Methods

- (void)addFavoritesFromRadioBrowser:(NSArray *)favorites {
    _uuidHeldOver = nil;
    if (!favorites || [favorites count] == 0) return;
    NSString *name, *url, *country, *tags, *genre, *stationuuid, *artwork, *path;
    NSBundle *bundle = [NSBundle mainBundle];
    NSImage *image;
    NSDictionary *dict;
    NSURL *artURL;
    for (int i = 0; i < [favorites count]; i++) {
        PLRadioFavorite *favorite = [PLRadioFavorite new];
        dict = [favorites objectAtIndex:i];
        name = [dict valueForKey:@"name"];
        [favorite setName:name];
        url = [dict valueForKey:@"url"];
        [favorite setUrl:url];
        country = [dict valueForKey:@"country"];
        if (country == nil || [country isEqualToString:@""]) country = @"Planet Earth";
        [favorite setCountry:country];
        tags = [dict valueForKey:@"tags"];
        if ([tags isNotEqualTo:@""]) {
            NSArray *split = [tags componentsSeparatedByString:@","];
            genre = [[split objectAtIndex:0] capitalizedString];
            if ([genre isEqualToString:@""]) genre = @"n/a";
        } else {
            genre = @"n/a";
        }
        [favorite setGenre:genre];
        stationuuid = [dict valueForKey:@"stationuuid"];
        if (!stationuuid) stationuuid = @"";
        if ([stationuuid length] > 1) {
            [favorite setStationuuid:stationuuid];
        } else {
            [favorite setStationuuid:@""];
        }
        [favorite setFetchArt:NO];
        [favorite setFetchType:NO];
        [favorite setArtUrl:@""];
        [favorite setSecondaryString:@""];
        [favorite setPrefix:@""];
        artwork = [dict valueForKey:@"favicon"];
        if (artwork && [artwork isNotEqualTo:@""]) {
            artURL = [NSURL URLWithString:artwork];
            image = [[NSImage alloc] initWithContentsOfURL:artURL];
            if (!image) {
                path = [bundle pathForResource:@"browser" ofType:@"jpg"];
                image = [[NSImage alloc] initWithContentsOfFile:path];
            }
        } else {
            path = [bundle pathForResource:@"browser" ofType:@"jpg"];
            image = [[NSImage alloc] initWithContentsOfFile:path];
        }
        [favorite setImage:image];
        [_favList addObject:favorite];
    }
    [self sortFavList];
    [_tableView reloadData];
    _hasBeenEdited = YES;
}

- (void)sortFavList {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    _favList = [[_favList sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}

- (void)selectObjectWithUuid:(NSString *)uuid {
    for (int i = 0; i < [_favList count]; i++) {
        if ([[[_favList objectAtIndex:i] uuid] isEqualToString:uuid]) {
            [[self tableView] selectColumnIndexes:[NSIndexSet indexSetWithIndex:i] byExtendingSelection:NO];
            break;
        }
    }
}

- (NSInteger)indexForObjectUUID:(NSString *)uuid {
    NSInteger ix = NSNotFound;
    for (int i = 0; i < [_favList count]; i++) {
        if ([[[_favList objectAtIndex:i] uuid] isEqualToString:uuid]) {
            ix = i;
            break;
        }
    }
    return ix;
}

- (void)selectNothing {
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
    [_removeButton setEnabled:NO];
    [_editButton setEnabled:NO];
    [_moveButton setEnabled:NO];
}


#pragma mark - IBActions

- (IBAction)addFavorite:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSString *menuId = [sender identifier];
    [_favName setStringValue:@""];
    [_favGenre setStringValue:@""];
    [_favCountry setStringValue:@""];
    [_favURL setEnabled:YES];
    [_favURL setEditable:YES];
    if ([menuId isEqualToString:@"URLAddedFromMenu"]) {
        [_favURL setStringValue:[appDelegate playingURL]];
    } else {
        [_favURL setStringValue:@"https://"];
    }
    [_favAltInfo setStringValue:@""];
    [_favImage setImage:[NSImage imageNamed:@"stream"]];
    [_artgrabCheckBox setState:NSOffState];
    [_browserBox setHidden:YES];
    _htmlSelected = NO;
    _transformationSelected = NO;
    _artgrabActive = NO;
    [self setArtgrabBoxInitialSettings];
    [_artgrabURL setStringValue:@""];
    [_artgrabSecondaryString setStringValue:@""];
    [_artgrabPrefix setStringValue:@""];
    [_deferNotificationCheckBox setState:NSControlStateValueOff];
    [_ignoreDoubleNotificationsCheckBox setState:NSControlStateValueOff];
    [_warningString setStringValue:@""];
    [_editSheet setTitle:@"Add Favorite"];
    [_controlWindow beginSheet:_editSheet completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseOK) {
            NSString *myURL = [self->_favURL stringValue];
            if (![myURL isEqualToString:@""] && ![myURL hasPrefix:@"http"]) {
                myURL = [NSString stringWithFormat:@"http://%@", myURL];
            }
            NSString *altURL = [self->_favAltInfo stringValue];
            if (![altURL isEqualToString:@""] && ![altURL hasPrefix:@"http"]) {
                altURL = [NSString stringWithFormat:@"https://%@", altURL];
            }
            NSString *artURL = [self->_artgrabURL stringValue];
            if (![artURL isEqualToString:@""] && ![artURL hasPrefix:@"http"]) {
                artURL = [NSString stringWithFormat:@"https://%@", artURL];
            }
            PLRadioFavorite *fav = [[PLRadioFavorite alloc] initWithName:[self->_favName stringValue] genre:[self->_favGenre stringValue] country:[self->_favCountry stringValue] url:myURL altinfo:altURL image:[self->_favImage image] fetchArt:self->_artgrabActive fetchType:self->_htmlSelected textTransform:self->_transformationSelected textTransformType:[self->_artgrabTextTransformPopUpButton indexOfSelectedItem] artUrl:artURL secondaryString:[self->_artgrabSecondaryString stringValue] prefix:[self->_artgrabPrefix stringValue] deferNotification:[self->_deferNotificationCheckBox state] ignoreDoubleNotifications:[self->_ignoreDoubleNotificationsCheckBox state] stationuuid:@""];
            [self->_favList addObject:[fav copy]];
            self->_uuidHeldOver = [[fav uuid] copy];
            [self sortFavList];
            [self->_tableView reloadData];
            [self selectObjectWithUuid:self->_uuidHeldOver];
            [self->_controlWindow endSheet:self->_editSheet];
            self->_hasBeenEdited = YES;
        }
    }];
}

- (IBAction)removeFavorites:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSString *theString;
    NSIndexSet *selection = [[self tableView] selectedRowIndexes];
    if ([appDelegate askForConfirmation]) {
        if ([selection count] > 1) {
            theString = [NSString stringWithFormat:@"%lu favorites", [selection count]];
        } else {
            theString = @"one favorite";
        }
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setAlertStyle:NSAlertStyleCritical];
        [alert setMessageText:@"Are you sure?"];
        [alert setInformativeText:[NSString stringWithFormat:@"You are about to delete %@. This cannot be undone. Are you sure you want to continue?", theString]];
        [alert addButtonWithTitle:@"Cancel"];
        [alert addButtonWithTitle:@"OK"];
        [alert setShowsSuppressionButton:YES];
        [[alert suppressionButton] setTitle:@"Do not show this warning again"];
        [alert beginSheetModalForWindow:_controlWindow completionHandler:^(NSModalResponse returnCode) {
            BOOL suppresswarnings = [[alert suppressionButton] state];
            [appDelegate suppressDeletionWarnings:suppresswarnings];
            if (returnCode == 1000) {
                return;
            } else {
                [self->_favList removeObjectsAtIndexes:selection];
                [[self tableView] selectColumnIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
                [[self->_menuTableView tableView] selectColumnIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
                [[self tableView] reloadData];
                [[self->_menuTableView tableView] reloadData];
                self->_hasBeenEdited = YES;
            }
        }];
    } else {
        [self->_favList removeObjectsAtIndexes:selection];
        [[self tableView] selectColumnIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
        [[self->_menuTableView tableView] selectColumnIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
        [[self tableView] reloadData];
        [[self->_menuTableView tableView] reloadData];
        self->_hasBeenEdited = YES;
    }
}

- (IBAction)editFavorite:(id)sender {
    NSInteger selection = [_tableView selectedRow];
    PLRadioFavorite *fav = [_favList objectAtIndex:selection];
    [_favName setStringValue:[fav name]];
    _uuidHeldOver = [fav uuid];
    [_favGenre setStringValue:[fav genre]];
    [_favCountry setStringValue:[fav country]];
    [_favURL setStringValue:[fav url]];
    [_favAltInfo setStringValue:[fav altinfo]];
    [_artgrabURL setStringValue:[fav artUrl]];
    [_artgrabSecondaryString setStringValue:[fav secondaryString]];
    [_artgrabPrefix setStringValue:[fav prefix]];
    [_artgrabTextTransformPopUpButton selectItemAtIndex:[fav textTransformType]];
    [_deferNotificationCheckBox setState:[fav deferNotification]];
    [_ignoreDoubleNotificationsCheckBox setState:[fav ignoreDoubleNotifications]];
    [_editSheet setTitle:[fav name]];
    if ([fav textTransform]) {
        [_artgrabPopUpButton selectItemAtIndex:2];
        [_artgrabTextTransformPopUpButton selectItemAtIndex:[fav textTransformType]];
        _htmlSelected = NO;
        _transformationSelected = YES;
    } else {
        if ([fav fetchType]) {
            [_artgrabPopUpButton selectItemAtIndex:1];
            _htmlSelected = YES;
            _transformationSelected = NO;
        } else {
            [_artgrabPopUpButton selectItemAtIndex:0];
            _htmlSelected = NO;
            _transformationSelected = NO;
        }
    }
    [self artgrabPopUpButtonSelectionChanged:_artgrabPopUpButton];
    _artgrabActive = [fav fetchArt];
    if (_artgrabActive) {
        [_artgrabCheckBox setState:NSOnState];
    } else {
        [_artgrabCheckBox setState:NSOffState];
    }
    if ([fav image]) {
        [_favImage setImage:[fav image]];
    } else {
        [_favImage setImage:[NSImage imageNamed:@"stream"]];
    }
    if ([fav stationuuid] && [[fav stationuuid] isNotEqualTo:@""]) {
        [_browserBox setHidden:NO];
        [_favURL setEnabled:YES];
        [_favURL setEditable:NO];
    } else {
        [_browserBox setHidden:YES];
        [_favURL setEnabled:YES];
    }
    [_warningString setStringValue:@""];
    [self artgrabCheckBoxClicked:_artgrabCheckBox];
    [self showOrHideArtgrabBox:NO];
    [_controlWindow beginSheet:_editSheet completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseOK) {
            [[self->_favList objectAtIndex:selection] setName:[self->_favName stringValue]];
            [[self->_favList objectAtIndex:selection] setUuid:self->_uuidHeldOver];
            [[self->_favList objectAtIndex:selection] setGenre:[self->_favGenre stringValue]];
            [[self->_favList objectAtIndex:selection] setCountry:[self->_favCountry stringValue]];
            if ([[self->_favImage image] isEqualTo:[NSImage imageNamed:@"stream"]]) {
                [self->_favImage setImage:nil];
            }
            [[self->_favList objectAtIndex:selection] setImage:[self->_favImage image]];
            [[self->_favList objectAtIndex:selection] setFetchArt:self->_artgrabActive];
            [[self->_favList objectAtIndex:selection] setFetchType:self->_htmlSelected];
            [[self->_favList objectAtIndex:selection] setTextTransform:self->_transformationSelected];
            [[self->_favList objectAtIndex:selection] setTextTransformType:[self->_artgrabTextTransformPopUpButton indexOfSelectedItem]];
            [[self->_favList objectAtIndex:selection] setSecondaryString:[self->_artgrabSecondaryString stringValue]];
            [[self->_favList objectAtIndex:selection] setPrefix :[self->_artgrabPrefix stringValue]];
            [[self->_favList objectAtIndex:selection] setDeferNotification:[self->_deferNotificationCheckBox state]];
            [[self->_favList objectAtIndex:selection] setIgnoreDoubleNotifications:[self->_ignoreDoubleNotificationsCheckBox state]];
            NSString *myURL = [self->_favURL stringValue];
            NSString *altURL = [self ->_favAltInfo stringValue];
            NSString *artURL = [self->_artgrabURL stringValue];
            if (![myURL isEqualToString:@""] && ![myURL hasPrefix:@"http"]) {
                myURL = [NSString stringWithFormat:@"https://%@", myURL];
            }
            if (![altURL isEqualToString:@""] && ![altURL hasPrefix:@"http"]) {
                altURL = [NSString stringWithFormat:@"https://%@", altURL];
            }
            if (![artURL isEqualToString:@""] && ![artURL hasPrefix:@"http"]) {
                artURL = [NSString stringWithFormat:@"https://%@", artURL];
            }
            [[self->_favList objectAtIndex:selection] setUrl:[myURL copy]];
            [[self->_favList objectAtIndex:selection] setAltinfo:[altURL copy]];
            [[self->_favList objectAtIndex:selection] setArtUrl:[artURL copy]];
            [self sortFavList];
            [self->_tableView reloadData];
            [self->_controlWindow endSheet:self->_editSheet];
            [self->_warningString setStringValue:@""];
            self->_hasBeenEdited = YES;
        }
    }];
}

- (IBAction)editFavoriteOnDoubleClick:(id)sender {
    NSInteger selection = [_tableView clickedRow];
    NSString *uuid = [[_favList objectAtIndex:selection] uuid];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid==%@",uuid];
    NSArray *results = [[_menuTableView menuList] filteredArrayUsingPredicate:predicate];
    if ([results count] > 0) return;
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
    PLRadioFavorite *fav = [_favList objectAtIndex:selection];
    [self continueEditingFav:fav atIndex:selection externalWrapUp:NO];
}

- (void)continueEditingFav:(PLRadioFavorite *)fav atIndex:(NSInteger)selection externalWrapUp:(BOOL)externalWrapUp {
    [_favName setStringValue:[fav name]];
    _uuidHeldOver = [fav uuid];
    [_favGenre setStringValue:[fav genre]];
    [_favCountry setStringValue:[fav country]];
    [_favURL setStringValue:[fav url]];
    [_favAltInfo setStringValue:[fav altinfo]];
    [_artgrabURL setStringValue:[fav artUrl]];
    [_artgrabSecondaryString setStringValue:[fav secondaryString]];
    [_artgrabPrefix setStringValue:[fav prefix]];
    [_artgrabTextTransformPopUpButton selectItemAtIndex:[fav textTransformType]];
    [_deferNotificationCheckBox setState:[fav deferNotification]];
    [_ignoreDoubleNotificationsCheckBox setState:[fav ignoreDoubleNotifications]];
    [_editSheet setTitle:[fav name]];
    if ([fav textTransform]) {
        [_artgrabPopUpButton selectItemAtIndex:2];
        [_artgrabTextTransformPopUpButton selectItemAtIndex:[fav textTransformType]];
        _htmlSelected = NO;
        _transformationSelected = YES;
    } else {
        if ([fav fetchType]) {
            [_artgrabPopUpButton selectItemAtIndex:1];
            _htmlSelected = YES;
            _transformationSelected = NO;
        } else {
            [_artgrabPopUpButton selectItemAtIndex:0];
            _htmlSelected = NO;
            _transformationSelected = NO;
        }
    }
    [self artgrabPopUpButtonSelectionChanged:_artgrabPopUpButton];
    _artgrabActive = [fav fetchArt];
    if (_artgrabActive) {
        [_artgrabCheckBox setState:NSOnState];
    } else {
        [_artgrabCheckBox setState:NSOffState];
    }
    if ([fav image]) {
        [_favImage setImage:[fav image]];
    } else {
        [_favImage setImage:[NSImage imageNamed:@"stream"]];
    }
    if ([fav stationuuid] && [[fav stationuuid] isNotEqualTo:@""]) {
        [_browserBox setHidden:NO];
        [_favURL setEnabled:YES];
        [_favURL setEditable:NO];
        [_favURL setToolTip:@"The URL cannot be changed because this station was added from www.radio-browser.info."];
    } else {
        [_browserBox setHidden:YES];
        [_favURL setEnabled:YES];
        [_favURL setToolTip:nil];
    }
    [_warningString setStringValue:@""];
    [self showOrHideArtgrabBox:NO];
    [_controlWindow beginSheet:_editSheet completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseOK) {
            [[self->_favList objectAtIndex:selection] setName:[self->_favName stringValue]];
            [[self->_favList objectAtIndex:selection] setUuid:self->_uuidHeldOver];
            [[self->_favList objectAtIndex:selection] setGenre:[self->_favGenre stringValue]];
            [[self->_favList objectAtIndex:selection] setCountry:[self->_favCountry stringValue]];
            if ([[self->_favImage image] isEqualTo:[NSImage imageNamed:@"stream"]]) {
                [self->_favImage setImage:nil];
            }
            [[self->_favList objectAtIndex:selection] setImage:[self->_favImage image]];
            NSString *myURL = [self->_favURL stringValue];
            NSString *altURL = [self->_favAltInfo stringValue];
            NSString *artURL = [self->_artgrabURL stringValue];
            if (![myURL isEqualToString:@""] && ![myURL hasPrefix:@"http"]) {
                myURL = [NSString stringWithFormat:@"https://%@", myURL];
            }
            if (![altURL isEqualToString:@""] && ![altURL hasPrefix:@"http"]) {
                altURL = [NSString stringWithFormat:@"https://%@", altURL];
            }
            if (![artURL isEqualToString:@""] && ![artURL hasPrefix:@"http"]) {
                artURL = [NSString stringWithFormat:@"https://%@", artURL];
            }
            [[self->_favList objectAtIndex:selection] setUrl:[myURL copy]];
            [[self->_favList objectAtIndex:selection] setAltinfo:[altURL copy]];
            [[self->_favList objectAtIndex:selection] setArtUrl:[artURL copy]];
            [[self->_favList objectAtIndex:selection] setImage:[self->_favImage image]];
            [[self->_favList objectAtIndex:selection] setFetchArt:self->_artgrabActive];
            [[self->_favList objectAtIndex:selection] setFetchType:self->_htmlSelected];
            [[self->_favList objectAtIndex:selection] setTextTransform:self->_transformationSelected];
            [[self->_favList objectAtIndex:selection] setTextTransformType:[self->_artgrabTextTransformPopUpButton indexOfSelectedItem]];
            [[self->_favList objectAtIndex:selection] setArtUrl:[self->_artgrabURL stringValue]];
            [[self->_favList objectAtIndex:selection] setSecondaryString:[self->_artgrabSecondaryString stringValue]];
            [[self->_favList objectAtIndex:selection] setPrefix :[self->_artgrabPrefix stringValue]];
            [[self->_favList objectAtIndex:selection] setDeferNotification:[self->_deferNotificationCheckBox state]];
            [[self->_favList objectAtIndex:selection] setIgnoreDoubleNotifications:[self->_ignoreDoubleNotificationsCheckBox state]];
            [self sortFavList];
            [self->_tableView reloadData];
            [self->_controlWindow endSheet:self->_editSheet];
            [self->_warningString setStringValue:@""];
            self->_hasBeenEdited = YES;
            if (externalWrapUp) [self->_menuTableView wrapUpEditing:[self->_favList objectAtIndex:selection]];
        }
    }];
}

- (IBAction)moveFavorites:(id)sender {
    NSIndexSet *selection = [[self tableView] selectedRowIndexes];
    NSMutableArray *menuList = [[_menuTableView menuList] mutableCopy];
    [menuList addObjectsFromArray:[_favList objectsAtIndexes:selection]];
    [_menuTableView setMenuList:[menuList mutableCopy]];
    [[_menuTableView tableView] reloadData];
    [[self tableView] reloadData];
    [self selectNothing];
    [[[_menuTableView tableView] window] makeFirstResponder:[_menuTableView tableView]];
    [[_menuTableView tableView] selectRowIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange([[_menuTableView menuList] count] - 1, [selection count])] byExtendingSelection:NO];
    [[_menuTableView removeButton] setEnabled:YES];
}

- (IBAction)addOrEditFavoriteOK:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if ([[_favName stringValue] isEqualToString:@""]) {
        [_warningString setStringValue:@"You must enter a name."];
        NSBeep();
        return;
    }
    NSURL *theURL = [NSURL URLWithString:[_favURL stringValue]];
    if (theURL && [theURL scheme] && [theURL host]) {
        if (_tryoutController) {
            [_tryoutController setVolume:0.0f];
            [_tryoutController stop];
            _tryoutController = nil;
            [appDelegate restoreVolumeOfPlayingApplication];
        }
        [_tryButton setTitle:@"Try"];
        _isTryingOut = NO;
        if ([_artgrabCheckBox state] == NSOnState) {
            NSURL *artURL = [NSURL URLWithString:[_artgrabURL stringValue]];
            if (!(artURL && [artURL scheme] && [artURL host])) {
                [_warningString setStringValue:@"Artwork URL not valid."];
                NSBeep();
                return;
            }
            if ([[_artgrabSecondaryString stringValue] isEqualToString:@""]) {
                if ([_artgrabPopUpButton indexOfSelectedItem] == 0) {
                    [_warningString setStringValue:@"Key path cannot be empty."];
                    NSBeep();
                    return;
                } else if ([_artgrabPopUpButton indexOfSelectedItem] == 1) {
                    [_warningString setStringValue:@"Search string cannot be empty."];
                    NSBeep();
                    return;
                }
            }
            if ([_artgrabPopUpButton indexOfSelectedItem] == 1) {
                NSArray *array = [[_artgrabSecondaryString stringValue] componentsSeparatedByString:@"*"];
                if ([array count] < 2) {
                    if (![[_artgrabSecondaryString stringValue] containsString:@"*"]) {
                        [_warningString setStringValue:@"No asterisk in search string."];
                    }
                    NSBeep();
                    return;
                 } else if ([array count] > 2) {
                    [_warningString setStringValue:@"Use one asterisk only."];
                    NSBeep();
                    return;
                } else if ([array count] == 2) {
                     if ([(NSString *)[array objectAtIndex:0] isEqualToString:@""] || [(NSString *)[array objectAtIndex:1] isEqualToString:@""]) {
                         [_warningString setStringValue:@"Imcomplete search string."];
                         NSBeep();
                         return;
                     }
                }
            }
            if (![[_artgrabPrefix stringValue] isEqualToString:@""]) {
                if ([_artgrabPopUpButton indexOfSelectedItem] != 2) {
                    NSURL *prefixURL = [NSURL URLWithString:[_artgrabPrefix stringValue]];
                    if (!(prefixURL && [prefixURL scheme] && [prefixURL host])) {
                        [_warningString setStringValue:@"Prefix must be a valid URL."];
                        NSBeep();
                        return;
                    }
                }
            }
        }
        [[appDelegate helpPanel] orderOut:nil];
        [_controlWindow endSheet:_editSheet returnCode:NSModalResponseOK];
    } else {
        [_warningString setStringValue:@"Stream URL is not valid."];
        NSBeep();
        return;
    }
}

- (IBAction)addOrEditFavoriteCancel:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [_warningString setStringValue:@""];
    if (_tryoutController) {
        [_tryoutController setVolume:0.0f];
        [_tryoutController stop];
        _tryoutController = nil;
        [appDelegate restoreVolumeOfPlayingApplication];
    }
    [_tryButton setTitle:@"Try"];
    _isTryingOut = NO;
    [[appDelegate helpPanel] orderOut:nil];
    [_controlWindow endSheet:_editSheet returnCode:NSModalResponseCancel];
}

- (IBAction)addOrEditFavoriteTryURL:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (!_isTryingOut) {
        NSURL *theURL = [NSURL URLWithString:[_favURL stringValue]];
        if (theURL && [theURL scheme] && [theURL host]) {
            if (!_tryoutController) {
                _tryoutController = [[FSAudioController alloc] initWithUrl:theURL];
            }
            [appDelegate suppressVolumeOfPlayingApplication];
            [_previewBox stop];
            [_previewBox disableControls];
            [_tryoutController play];
            float theVolume = 0;
            if ([appDelegate radioController]) {
                theVolume = [[appDelegate radioController] volume];
            }
            if (theVolume == 0) theVolume = 0.75;
            [_tryoutController setVolume:theVolume];
            [_tryoutController play];
            [_tryButton setTitle:@"Stop"];
            _isTryingOut = YES;
            if (_timeOutTimer) {
                [_timeOutTimer invalidate];
                _timeOutTimer = nil;
            }
            [appDelegate setNotifyingObject:self];
            _timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:(TIMEOUT_RANGE / 2) target:self selector:@selector(firstTimeOut) userInfo:nil repeats:NO];
        } else {
            [_warningString setStringValue:@"Stream URL is not valid."];
            NSBeep();
            return;
        }
    } else {
        [self resetAndRecover];
    }
}

- (void)erroredOut {
    [_warningString setStringValue:@"Error opening stream."];
    [self resetAndRecover];
}

- (void)resetAndRecover {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    [_tryoutController setVolume:0.0f];
    [_tryoutController stop];
    _tryoutController = nil;
    [appDelegate restoreVolumeOfPlayingApplication];
    [_tryButton setTitle:@"Try"];
    _isTryingOut = NO;
    [_previewBox enableControls];
}
- (IBAction)backupRadioFavorites:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate backupRadioFavorites];
}

- (IBAction)restoreRadioFavorites:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate restoreRadioFavorites];
}


#pragma mark - Testing Stream Timeout Related Methods

- (void)firstTimeOut {
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    [_warningString setStringValue:@"It's taking a bit longer..."];
    _timeOutTimer = [NSTimer scheduledTimerWithTimeInterval:(TIMEOUT_RANGE / 2) target:self selector:@selector(lastTimeOut) userInfo:nil repeats:NO];
}

- (void)lastTimeOut {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    if (_tryoutController) {
        [_tryoutController setVolume:0.0f];
        [_tryoutController stop];
        _tryoutController = nil;
    }
    [appDelegate restoreVolumeOfPlayingApplication];
    [_tryButton setTitle:@"Try"];
    _isTryingOut = NO;
    [_warningString setStringValue:@"Connection timed out."];
}

- (void)invalidateTimer {
    if (_timeOutTimer) {
        [_timeOutTimer invalidate];
        _timeOutTimer = nil;
    }
    [_warningString setStringValue:@""];
}

#pragma mark - Sheet Utilities

- (void)setArtgrabBoxInitialSettings {
    [self showOrHideArtgrabBox:NO];
    [_artgrabPopUpButton selectItemAtIndex:0];
    [self artgrabPopUpButtonSelectionChanged:_artgrabPopUpButton];
}

- (void)showOrHideArtgrabBox:(BOOL)animate {
    if (_artgrabActive) {
        NSRect rect = [_editSheet frame];
        rect.size.height = 530.0f;
        [_editSheet setFrame:rect display:YES animate:animate];
        [_artgrabBox setHidden:NO];
    } else {
        [_artgrabBox setHidden:YES];
        NSRect rect = [_editSheet frame];
        rect.size.height = 310.0f;
        [_editSheet setFrame:rect display:YES animate:animate];
    }
}

- (IBAction)artgrabPopUpButtonSelectionChanged:(id)sender {
    NSInteger selection = [sender indexOfSelectedItem];
    if (selection == 0) {
        [_urlTextField setStringValue:@"URL"];
        [_postFixTextField setStringValue:@"Prefix result with:"];
        [_artgrabSecondaryStringLabel setStringValue:@"Key Path:"];
        [_artgrabExplanationTextLabel setStringValue:@"Only use the dot '.' as separator between key path components."];
        [_artgrabSecondaryString setHidden:NO];
        [_artgrabTextTransformPopUpButton setHidden:YES];
        [_deferNotificationCheckBox setEnabled:YES];
        _htmlSelected = NO;
        _transformationSelected = NO;
    } else if (selection == 1) {
        [_urlTextField setStringValue:@"URL"];
        [_postFixTextField setStringValue:@"Prefix result with:"];
        [_artgrabSecondaryStringLabel setStringValue:@"Search phrase:"];
        [_artgrabExplanationTextLabel setStringValue:@"'*' indicates image URL, '?' indicates variable parts."];
        [_artgrabSecondaryString setHidden:NO];
        [_artgrabTextTransformPopUpButton setHidden:YES];
        [_deferNotificationCheckBox setEnabled:YES];
        _htmlSelected = YES;
        _transformationSelected = NO;
    } else {
        [_urlTextField setStringValue:@"Prefix URL:"];
        [_postFixTextField setStringValue:@"Extension:"];
        [_artgrabSecondaryStringLabel setStringValue:@"Transformation:"];
        [_artgrabExplanationTextLabel setStringValue:@"Currently, only the 'Jazz de Ville' transformation is available."];
        [_artgrabSecondaryString setHidden:YES];
        [_artgrabTextTransformPopUpButton setHidden:NO];
        [_deferNotificationCheckBox setState:NSControlStateValueOff];
        [_deferNotificationCheckBox setEnabled:NO];
        _htmlSelected = NO;
        _transformationSelected = YES;
    }
}

- (IBAction)artgrabCheckBoxClicked:(id)sender {
    _artgrabActive = [sender state];
    [self showOrHideArtgrabBox:YES];
}

- (IBAction)artGrabTextTransformPopUpButtonChanged:(id)sender {
    // for later extension
}


@end
