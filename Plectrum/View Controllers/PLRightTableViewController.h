//
//  PLRightTableViewController.h
//  Plectrum
//
//  Created by Petros Loukareas on 28/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "PLRadioFavorite.h"


NS_ASSUME_NONNULL_BEGIN

@interface PLRightTableViewController : NSObject

@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSButton *removeButton;

@property (strong) NSMutableArray <PLRadioFavorite *> *menuList;
@property (strong) NSMutableArray <PLRadioFavorite *> *menusToMove;


- (IBAction)removeFromMenu:(id)sender;

- (void)removeItemWithUuid:(NSString *)uuid;
- (void)selectNothing;

- (void)wrapUpEditing:(PLRadioFavorite *)fav;

@end

NS_ASSUME_NONNULL_END
