//
//  PLRecordingTableViewController.h
//  Plectrum
//
//  Created by Petros Loukareas on 17/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#import "PLRadioRecording.h"
#import "PLAudioPreviewBox.h"
#import "PLLeftTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PLRecordingTableViewController : NSObject


@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet PLAudioPreviewBox *previewBox;
@property (weak) IBOutlet PLLeftTableViewController *favTableView;
@property (weak) IBOutlet NSButton *moveToTrashButton;
@property (weak) IBOutlet NSButton *saveToButton;

@property (strong) NSMutableArray <PLRadioRecording *> *recordings;
@property (strong) NSMutableArray <NSString *> *savedSelection;
@property (strong) NSString *sortingKey;
@property (assign) BOOL ascending;
@property (assign) BOOL doNotDisturb;
@property (assign) BOOL doNotUpsetPreviewPlaying;


- (IBAction)moveToTrash:(id)sender;
- (IBAction)saveTo:(id)sender;
- (IBAction)openFolder:(id)sender;

- (NSURL *)playURLforSelection;
- (void)sortRecordings;
- (void)initializeForWindowShow;
- (void)prepareUserFolder;
- (NSString *)getUserFolderPath;
- (NSURL *)getUserFolderURL;
- (void)saveCurrentSelection;
- (void)restorePreviousSelection;
- (void)reloadCurrentlyRecordingRowOnly;
- (void)setTableViewHeaderFromSortingKey;

@end

NS_ASSUME_NONNULL_END
