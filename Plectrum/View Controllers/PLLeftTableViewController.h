//
//  PLLeftTableViewController.h
//  Plectrum
//
//  Created by Petros Loukareas on 28/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "PLRadioFavorite.h"
#import "FSAudioController.h"
#import "AppDelegate.h"


NS_ASSUME_NONNULL_BEGIN

@interface PLLeftTableViewController : NSObject

@property (weak) IBOutlet NSWindow *controlWindow;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSButton *moveButton;
@property (weak) IBOutlet NSButton *addButton;
@property (weak) IBOutlet NSButton *removeButton;
@property (weak) IBOutlet NSButton *editButton;
@property (weak) IBOutlet NSButton *tryButton;
@property (weak) IBOutlet NSTextField *warningString;

@property (strong) NSMutableArray <PLRadioFavorite *> *favList;
@property (strong) NSMutableArray <PLRadioFavorite *> *favsToMove;
@property (strong) FSAudioController *tryoutController;

@property (assign) BOOL hasBeenEdited;
@property (assign) BOOL isTryingOut;


- (IBAction)addFavorite:(id)sender;
- (IBAction)removeFavorites:(id)sender;
- (IBAction)moveFavorites:(id)sender;
- (IBAction)editFavorite:(id)sender;
- (IBAction)addOrEditFavoriteTryURL:(id)sender;

- (void)addFavoritesFromRadioBrowser:(NSArray *)favorites;
- (void)sortFavList;
- (void)selectObjectWithUuid:(NSString *)uuid;
- (NSInteger)indexForObjectUUID:(NSString *)uuid;
- (void)continueEditingFav:(PLRadioFavorite *)fav atIndex:(NSInteger)selection externalWrapUp:(BOOL)externalWrapUp;
- (void)selectNothing;
- (void)invalidateTimer;
- (void)erroredOut;

@end

NS_ASSUME_NONNULL_END
