//
//  PLRightTableViewController.m
//  Plectrum
//
//  Created by Petros Loukareas on 28/03/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRightTableViewController.h"
#import "PLLeftTableViewController.h"


@interface PLRightTableViewController () <NSTableViewDelegate, NSTableViewDataSource>

@property (weak) IBOutlet PLLeftTableViewController *favTableView;

@end


@implementation PLRightTableViewController

#pragma mark - Awake From Nib

- (void)awakeFromNib {
    [_tableView registerForDraggedTypes:@[@"nl.kinoko-house.PLMenuToFavesDragItem", @"nl.kinoko-house.PLFavesToMenuDragItem"]];
    _menusToMove = [[NSMutableArray alloc] initWithCapacity:10];
}


#pragma mark - NSTableViewDelegate Methods

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *view;
    view = [tableView makeViewWithIdentifier:@"regular" owner:self];
    NSMutableAttributedString *attribString = [[NSMutableAttributedString alloc] initWithString:[[_menuList objectAtIndex:row] name]];
    NSDictionary *boldAttrib = @{NSFontAttributeName:[NSFont boldSystemFontOfSize:[NSFont systemFontSize]]};
    NSDictionary *obliqAttrib = @{NSFontAttributeName:[NSFont systemFontOfSize:[NSFont systemFontSize]], NSObliquenessAttributeName:@0.25f};
    [[view textField] setStringValue:[[_menuList objectAtIndex:row] name]];
    if ([_menuList count] > 4 && row <= 2) {
        [attribString setAttributes:boldAttrib range:NSMakeRange(0, [[[_menuList objectAtIndex:row] name] length])];
    } else {
        [attribString setAttributes:obliqAttrib range:NSMakeRange(0, [[[_menuList objectAtIndex:row] name] length])];
    }
    [[view textField] setAttributedStringValue:attribString];
    return view;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    return YES;
}

- (BOOL)tableView:(NSTableView *)tableView shouldEditTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return NO;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    NSIndexSet *selection = [[self tableView] selectedRowIndexes];
    if ([selection count] > 0) {
        [_removeButton setEnabled:YES];
    } else {
        [_removeButton setEnabled:NO];
    }
}


#pragma mark - NSTableViewDataSource methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_menuList count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return [_menuList objectAtIndex:row];
}


#pragma mark - Drag and Drop methods

- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard {
    [[NSCursor closedHandCursor] set];
    [_menusToMove removeAllObjects];
    _menusToMove = [[_menusToMove arrayByAddingObjectsFromArray:[_menuList objectsAtIndexes:rowIndexes]] mutableCopy];
    [pboard writeObjects:@[[NSString stringWithFormat:@"%@", rowIndexes]]];
    [pboard declareTypes:@[@"nl.kinoko-house.PLMenuToFavesDragItem"] owner:nil];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation {
    NSString *theType = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] objectAtIndex:0];
    if ([theType isEqualToString:@"nl.kinoko-house.PLMenuToFavesDragItem"]) {
        [tableView setDropRow:row dropOperation:NSTableViewDropAbove];
        return NSDragOperationMove;
    } else if ([theType isEqualToString:@"nl.kinoko-house.PLFavesToMenuDragItem"]) {
        [tableView setDropRow:row dropOperation:NSTableViewDropAbove];
        return NSDragOperationCopy;
    } else {
        return NSDragOperationNone;
    }
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    int i, j;
    NSString *theType = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] objectAtIndex:0];
    if ([theType isEqualToString:@"nl.kinoko-house.PLMenuToFavesDragItem"]) {
        NSUInteger dropCount = 0;
        NSMutableArray *keepArray = [NSMutableArray arrayWithCapacity:[_menusToMove count]];
        PLRadioFavorite *dummyFav = [[PLRadioFavorite alloc] initDummy];
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
        for (i = 0; i < [_menuList count]; i++) {
            if ([_menusToMove count] > 0) {
                for (j = 0; j < [_menusToMove count]; j++) {
                    if ([[[_menuList objectAtIndex:i] uuid] isEqualToString:[[_menusToMove objectAtIndex:j] uuid]]) {
                        if (i < row) dropCount++;
                        [keepArray addObject:[_menusToMove objectAtIndex:j]];
                        [_menusToMove removeObjectAtIndex:j];
                        [_menuList replaceObjectAtIndex:i withObject:[dummyFav copy]];
                        [indexSet addIndex:i];
                    }
                }
            }
        }
        NSUInteger newDropRow = row - dropCount;
        [tableView beginUpdates];
        NSIndexSet *insertIdxs = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(newDropRow, [keepArray count])];
        [tableView removeRowsAtIndexes:indexSet withAnimation:0x31];
        [tableView insertRowsAtIndexes:insertIdxs withAnimation:0x32];
        for (i = (int)([_menuList count] - 1); i >= 0; i--) {
            if ([[[_menuList objectAtIndex:i] uuid] isEqualToString:@""]) [_menuList removeObjectAtIndex:i];
        }
        [_menuList insertObjects:[keepArray mutableCopy] atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(newDropRow, [keepArray count])]];
        [tableView endUpdates];
        NSInteger selRow = row;
        NSInteger rowCount = [indexSet count];
        NSIndexSet *indexset = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(selRow - dropCount, rowCount)];
        [tableView selectRowIndexes:indexset byExtendingSelection:NO];
        [tableView reloadData];
        [[NSCursor arrowCursor] set];
        return YES;
    } else if ([theType isEqualToString:@"nl.kinoko-house.PLFavesToMenuDragItem"]) {
        NSMutableArray *pasteObjects = [[_favTableView favsToMove] mutableCopy];
        NSIndexSet *newRowIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(row, [pasteObjects count])];
        [tableView beginUpdates];
        [tableView insertRowsAtIndexes:newRowIndexes withAnimation:0x32];
        [_menuList insertObjects:pasteObjects atIndexes:newRowIndexes];
        [tableView endUpdates];
        [tableView reloadData];
        [[tableView window] makeFirstResponder:tableView];
        [tableView selectRowIndexes:newRowIndexes byExtendingSelection:NO];
        [[_favTableView tableView] reloadData];
        [_favTableView selectNothing];
        [[NSCursor arrowCursor] set];
        return YES;
    } else {
        [[NSCursor arrowCursor] set];
        return NO;
    }
}


#pragma mark - Utility methods

- (void)removeItemWithUuid:(NSString *)uuid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uuid!=%@",uuid];
    _menuList = [[[_menuList filteredArrayUsingPredicate:predicate] mutableCopy] mutableCopy];
}

- (void)selectNothing {
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
    [_removeButton setEnabled:NO];
}

- (NSInteger)indexForObjectUUID:(NSString *)uuid {
    NSInteger ix = NSNotFound;
    for (int i = 0; i < [_menuList count]; i++) {
        if ([[[_menuList objectAtIndex:i] uuid] isEqualToString:uuid]) {
            ix = i;
            break;
        }
    }
    return ix;
}


#pragma mark - IBActions

- (IBAction)removeFromMenu:(id)sender {
    NSIndexSet *selection = [[self tableView] selectedRowIndexes];
    [_tableView beginUpdates];
    [_tableView removeRowsAtIndexes:selection withAnimation:0x30];
    [_menuList removeObjectsAtIndexes:selection];
    [_tableView endUpdates];
    [_tableView reloadData];
    [self selectNothing];
    [[_favTableView tableView] reloadData];
    [_favTableView selectNothing];
}

- (IBAction)editFavoriteOnDoubleClick:(id)sender {
    NSInteger selection = [_tableView clickedRow];
    NSString *uuid = [[_menuList objectAtIndex:selection] uuid];
    NSUInteger index = [_favTableView indexForObjectUUID:uuid];
    if (index == NSNotFound) return;
    PLRadioFavorite *fav = [[_favTableView favList] objectAtIndex:index];
    [_favTableView continueEditingFav:fav atIndex:index externalWrapUp:YES];
}

- (void)wrapUpEditing:(PLRadioFavorite *)fav {
    NSString *uuid = [fav uuid];
    NSInteger index = [self indexForObjectUUID:uuid];
    if (index == NSNotFound) return;
    [_menuList replaceObjectAtIndex:index withObject:fav];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
}


@end
