//
//  PLRecordingTableViewController.m
//  Plectrum
//
//  Created by Petros Loukareas on 17/04/2020.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "PLRecordingTableViewController.h"
#import "PLRecordingListView.h"
#import "PLUtilities.h"
#import "AppDelegate.h"


@interface PLRecordingTableViewController() <NSTableViewDelegate, NSTableViewDataSource, NSWindowDelegate>


@end


@implementation PLRecordingTableViewController


#pragma mark - Awake From Nib

- (void)awakeFromNib {
    [_tableView setDraggingSourceOperationMask:NSDragOperationCopy forLocal:NO];
    _doNotDisturb = NO;
    _doNotUpsetPreviewPlaying = NO;
}


#pragma mark - Backspace Key


#pragma mark - NSTableViewDelegate methods

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    PLRecordingListView *view;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    PLRadioRecording *recording = [_recordings objectAtIndex:row];
    NSMutableArray *array = [[[recording fileName] componentsSeparatedByString:@" "] mutableCopy];
    NSMutableArray *timeArray = [[[array lastObject] componentsSeparatedByString:@"-"] mutableCopy];
    NSString *newTime = [timeArray componentsJoinedByString:@":"];
    [array replaceObjectAtIndex:([array count] -1) withObject:newTime];
    NSString *newFileName = [array componentsJoinedByString:@" "];
    //NSLog(@"%@", [recording filePath]);
    if ([[recording uuid] isEqualToString:[appDelegate recordingUUID]]) {
        view = [tableView makeViewWithIdentifier:@"recording" owner:self];
        [[view textField] setStringValue:[NSString stringWithFormat:@"NOW RECORDING: %@", newFileName]];
    } else {
        BOOL fileExists = [fileManager fileExistsAtPath:[recording filePath]];
        if (fileExists) {
            view = [tableView makeViewWithIdentifier:@"regular" owner:self];
            [[view textField] setStringValue:newFileName];
        } else {
            view = [tableView makeViewWithIdentifier:@"missing" owner:self];
            [[view textField] setStringValue:[NSString stringWithFormat:@"*MISSING FILE* %@", newFileName]];
        }
    }
    NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFileType:[[recording fileType] lowercaseString]];
    [[view imageView] setImage:icon];
    [[view fileType] setStringValue:[recording fileType]];
    [[view fileSize] setStringValue:[recording fileSize]];
    [[view fileTime] setStringValue:[recording fileTime]];
    [[view fileURL] setStringValue:[recording originURL]];
    return view;
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    if ([_favTableView isTryingOut]) return NO;
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    PLRadioRecording *recording = [_recordings objectAtIndex:row];
    if ([appDelegate isRecording]) {
        if ([[recording uuid] isEqualToString:[appDelegate recordingUUID]]) {
            [tableView deselectAll:nil];
            return NO;
        }
    }
    return YES;
}

- (BOOL)tableView:(NSTableView *)tableView shouldEditTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return NO;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    if (!_doNotUpsetPreviewPlaying) [_previewBox stop];
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ([selection count] == 0) {
        [_moveToTrashButton setTitle:@"Move To Trash"];
        [_moveToTrashButton setEnabled:NO];
        [_saveToButton setEnabled:NO];
        [_previewBox disableControls];
    } else if ([selection count] > 1) {
        [_moveToTrashButton setTitle:@"Move To Trash"];
        [_moveToTrashButton setEnabled:YES];
        [_saveToButton setEnabled:YES];
        [_previewBox disableControls];
    } else {
        NSInteger index = [selection firstIndex];
        if ([[[_tableView viewAtColumn:0 row:index makeIfNecessary:NO] identifier] isEqualToString:@"missing"]) {
            [_moveToTrashButton setTitle:@"Remove"];
            [_moveToTrashButton setEnabled:YES];
            [_saveToButton setEnabled:NO];
            [_previewBox disableControls];
        } else {
            [_moveToTrashButton setTitle:@"Move To Trash"];
            [_moveToTrashButton setEnabled:YES];
            [_saveToButton setEnabled:YES];
            [_previewBox enableControls];
        }
    }
}

- (void)tableView:(NSTableView *)tableView mouseDownInHeaderOfTableColumn:(NSTableColumn *)tableColumn {
    _ascending = !_ascending;
    _ascending ? [self setAscendingSortIndicator] : [self setDescendingSortIndicator];
    [self sortRecordings];
}


#pragma mark - NSTableViewDataSource methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_recordings count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    if (tableColumn == 0) {
        return [_recordings objectAtIndex:row];
    } else {
        return nil;
    }
}

    
# pragma mark Utilities

- (NSURL *)playURLforSelection {
    if ([[_tableView selectedRowIndexes] count] != 1) {
        return nil;
    }
    NSInteger index = [[_tableView selectedRowIndexes] firstIndex];
    return [NSURL fileURLWithPath:[[_recordings objectAtIndex:index] filePath]];
}

- (void)sortRecordings {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    @try {
        _doNotDisturb = YES;
        _doNotUpsetPreviewPlaying = YES;
        if (!_sortingKey) _sortingKey = @"fileDate";
        [self saveCurrentSelection];
        NSMutableArray *sortDescriptorArray = [[NSMutableArray alloc] init];
        NSSortDescriptor *primarySortDescriptor = [NSSortDescriptor sortDescriptorWithKey:_sortingKey ascending:_ascending];
        [sortDescriptorArray addObject:primarySortDescriptor];
        if (![_sortingKey isEqualToString:@"fileName"]) {
            NSSortDescriptor *secondarySortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"fileName" ascending:_ascending];
            [sortDescriptorArray addObject:secondarySortDescriptor];
        }
        [_recordings sortUsingDescriptors:sortDescriptorArray];
        [_tableView reloadData];
        [self restorePreviousSelection];
        [appDelegate saveRadioRecordings];
        if ([_previewBox isPlaying]) {
            [_previewBox continuePlaying];
        }
        _doNotDisturb = NO;
        _doNotUpsetPreviewPlaying = NO;
    }
    @catch(NSException *e) {
        _doNotDisturb = NO;
        _doNotUpsetPreviewPlaying = NO;
    }
}

- (void)initializeForWindowShow {
    _ascending ? [self setAscendingSortIndicator] : [self setDescendingSortIndicator];
    [self sortRecordings];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
    [_moveToTrashButton setTitle:@"Move To Trash"];
    [_moveToTrashButton setEnabled:NO];
    [_saveToButton setEnabled:NO];
    [self setTableViewHeaderFromSortingKey];
    [_previewBox disableControls];
}

- (void)prepareUserFolder {
    NSError *error = nil;
    BOOL isDir;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *userFolder = [NSURL URLWithString:NSHomeDirectory()];
    NSString *pathToUserFolder = [userFolder path];
    NSString *pathToMyUserFolder = [pathToUserFolder stringByAppendingPathComponent:@"Music/Plectrum"];
    NSURL *urlToMyUserFolder = [NSURL fileURLWithPath:pathToMyUserFolder];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyUserFolder isDirectory:&isDir];
    if (folderExists && !isDir) {
        [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
        [PLUtilities showAlertWithMessageText:@"File found where a folder was expected" informativeText:@"There is a file with the name \"Plectrum\" inside the \"Music\" folder of your home folder. Please move this file to a different location and launch the application again. The application will close after you press the OK button."];
        [[NSApplication sharedApplication] terminate:nil];
    }
    if (!folderExists) {
        BOOL directoryCreated = [fileManager createDirectoryAtURL:urlToMyUserFolder withIntermediateDirectories:NO attributes:nil error:&error];
        if (!directoryCreated) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            NSString *errorDescription = [NSString stringWithFormat:@"An error occurred while creating the recordings directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", [error code],[error localizedDescription]];
            [PLUtilities showAlertWithMessageText:@"Error while creating application data directory" informativeText:errorDescription];
            [[NSApplication sharedApplication] terminate:nil];
        }
    }
}

- (NSString *)getUserFolderPath {
    NSURL *userFolder = [NSURL URLWithString:NSHomeDirectory()];
    NSString *pathToUserFolder = [userFolder path];
    NSString *pathToMyUserFolder = [pathToUserFolder stringByAppendingPathComponent:@"Music"];
    pathToMyUserFolder = [pathToMyUserFolder stringByAppendingPathComponent:@"Plectrum"];
    return pathToMyUserFolder;
}

- (NSURL *)getUserFolderURL {
    return [NSURL fileURLWithPath:[self getUserFolderPath]];
}

- (void)saveCurrentSelection {
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ([selection count] == 0) {
        _savedSelection = [@[] mutableCopy];
        return;
    }
    _savedSelection = [[NSMutableArray alloc] init];
    NSInteger index = [selection firstIndex];
    do {
        [_savedSelection addObject:[[_recordings objectAtIndex:index] uuid]];
        index = [selection indexGreaterThanIndex:index];
    } while (index != NSNotFound);
}

- (void)restorePreviousSelection {
    if (!_savedSelection || ([_savedSelection count] == 0)) return;
    NSMutableIndexSet *selection = [[NSMutableIndexSet alloc] init];
    for (int i = 0; i < [_savedSelection count]; i++) {
        for (int j = 0; j < [_recordings count]; j++) {
            if ([[_savedSelection objectAtIndex:i] isEqualToString:[[_recordings objectAtIndex:j] uuid]]) {
                [selection addIndex:j];
                break;
            }
        }
    }
    if ([selection count] == 0) return;
    [_tableView selectRowIndexes:selection byExtendingSelection:NO];
}

- (void)setAscendingSortIndicator {
    [_tableView setIndicatorImage:[NSImage imageNamed:@"NSAscendingSortIndicator"] inTableColumn:[[_tableView tableColumns] objectAtIndex:0]];
}

- (void)setDescendingSortIndicator {
    [_tableView setIndicatorImage:[NSImage imageNamed:@"NSDescendingSortIndicator"] inTableColumn:[[_tableView tableColumns] objectAtIndex:0]];
}


#pragma mark - IBActions

- (IBAction)moveToTrash:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ([selection count] == 0) return;
    if ([_previewBox isPlaying]) [_previewBox stop];
    NSInteger index = [selection firstIndex];
    do {
        PLRadioRecording *recording = [_recordings objectAtIndex:index];
        NSString *filePath = [recording filePath];
        NSString *labelPath = [NSString stringWithFormat:@"%@.txt", [filePath stringByDeletingPathExtension]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            [[NSFileManager defaultManager] trashItemAtURL:[NSURL fileURLWithPath:filePath] resultingItemURL:nil error:nil];
        }
        if ([[NSFileManager defaultManager] fileExistsAtPath:labelPath]) {
            [[NSFileManager defaultManager] trashItemAtURL:[NSURL fileURLWithPath:labelPath] resultingItemURL:nil error:nil];
        }
        index = [selection indexGreaterThanIndex:index];
    } while (index != NSNotFound);
    [_recordings removeObjectsAtIndexes:selection];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:-1] byExtendingSelection:NO];
    [_previewBox disableControls];
    [_moveToTrashButton setEnabled:NO];
    [_saveToButton setEnabled:NO];
    [appDelegate saveRadioRecordings];
}

- (IBAction)openFolder:(id)sender {
    BOOL isDir;
    if ([[NSFileManager defaultManager] fileExistsAtPath:[self getUserFolderPath] isDirectory:&isDir]) {
        if (isDir) {
            [[NSWorkspace sharedWorkspace] openURL: [self getUserFolderURL]];
        }
    }
    NSArray *finderApps = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"com.apple.Finder"];
    if ([finderApps count] > 0) {
        NSRunningApplication *finderApp = [finderApps objectAtIndex:0];
        [finderApp activateWithOptions:NSApplicationActivateIgnoringOtherApps];
    }
}

- (IBAction)saveTo:(id)sender {
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ([selection count] == 0) return;
    if ([selection count] > 1) {
        [self saveToMultiple:selection];
        return;
    }
    NSInteger index = [selection firstIndex];
    PLRadioRecording *recording = [_recordings objectAtIndex:index];
    NSString *originPath = [recording filePath];
    NSString *name = [recording fileName];
    NSString *ext = [[recording fileType] lowercaseString];
    NSString *exportName = [NSString stringWithFormat:@"%@.%@", name, ext];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    __block NSString *thePath;
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:@"Save recording"];
    [savePanel setMessage:@"Enter name and location for saving:"];
    [savePanel setNameFieldStringValue:exportName];
    [savePanel setAccessoryView:nil];
    [savePanel setDelegate:nil];
    if ([appDelegate firstOpen]) {
        [savePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    [savePanel beginSheetModalForWindow:[_tableView window] completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseCancel) return;
        if ([savePanel URL] != nil) {
            [appDelegate setFirstOpen:NO];
            thePath = [[savePanel URL] path];
        } else {
            return;
        }
        NSError *error;
        BOOL success = NO;
        if ( [[NSFileManager defaultManager] isReadableFileAtPath:originPath]) {
            success = [[NSFileManager defaultManager] copyItemAtPath:originPath toPath:thePath error:&error];
        }
        if (!success) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            [PLUtilities showAlertWithMessageText:@"Could not save recording" informativeText:[NSString stringWithFormat:@"A problem was encountered when saving the recording. This may be a problem with permissions; try saving the recording elsewhere. Your original recordings are safe and untouched. Please contact the developer if the problem persists. We are sorry for the inconvenience.\n\nError code: %ld\n\nError message: %@", (long)[error code], [error localizedDescription]]];
        }
    }];
}

- (void)saveToMultiple:(NSIndexSet *)indexes {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    __block NSString *thePath;
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setTitle:@"Save recordings"];
    [openPanel setMessage:@"Select directory for saving:"];
    [openPanel setAllowsMultipleSelection:NO];
    [openPanel setCanChooseFiles:NO];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanCreateDirectories:YES];
    [openPanel setPrompt:@"Save"];
    [openPanel setAccessoryView:nil];
    [openPanel setDelegate:nil];
    if ([appDelegate firstOpen]) {
        [openPanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    [openPanel beginSheetModalForWindow:[_tableView window] completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == NSModalResponseCancel) return;
        if ([openPanel URL] != nil) {
            [appDelegate setFirstOpen:NO];
            thePath = [[openPanel URL] path];
        } else {
            return;
        }
        BOOL success;
        BOOL errorFlag = NO;
        NSError *error;
        NSInteger index = [indexes firstIndex];
        do {
            PLRadioRecording *recording = [self->_recordings objectAtIndex:index];
            if ( [[NSFileManager defaultManager] isReadableFileAtPath:[recording filePath]]) {
                success = [[NSFileManager defaultManager] copyItemAtPath:[recording filePath] toPath:[NSString stringWithFormat:@"%@/%@.%@", thePath, [recording fileName], [[recording fileType] lowercaseString]] error:&error];
                if (!success) errorFlag = YES;
            }
            index = [indexes indexGreaterThanIndex:index];
        } while (index != NSNotFound);
        if (errorFlag) {
            [[NSApplication sharedApplication] activateIgnoringOtherApps:YES];
            [PLUtilities showAlertWithMessageText:@"Error while saving recordings" informativeText:[NSString stringWithFormat:@"Some errors were encountered when saving the recordings; Not all of them may have been saved correctly - however, your original recordings are still safe. This may be a problem with permissions; try saving the recordings elsewhere. Please contact the developer if the problem persists. We are sorry for the inconvenience.\n\nError code: %ld\n\nError message: %@", (long)[error code], [error localizedDescription]]];
        }
    }];
}

- (IBAction)sortingSetFromMenu:(id)sender {
    _sortingKey = [sender identifier];
    NSString *menuTitle = [sender title];
    NSString *lastComponent = [[menuTitle componentsSeparatedByString:@" "] lastObject];
    [[[_tableView tableColumns] objectAtIndex:0] setTitle:[NSString stringWithFormat:@"Recorded audio files sorted by %@", [lastComponent lowercaseString]]];
    [self sortRecordings];
}

- (void)setTableViewHeaderFromSortingKey {
    if (!_sortingKey) {
        _sortingKey = @"fileDate";
        _ascending = YES;
    }
    NSString *shortKey = [[_sortingKey substringFromIndex:4] lowercaseString];
    if ([shortKey isEqualToString:@"sizeinbytes"]) shortKey = @"size";
    [[[_tableView tableColumns] objectAtIndex:0] setTitle:[NSString stringWithFormat:@"Recorded audio files sorted by %@", shortKey]];
}

- (void)reloadCurrentlyRecordingRowOnly {
    _doNotDisturb = YES;
    @try {
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        NSString *uuid = [appDelegate recordingUUID];
        for (int i = 0; i < [_recordings count]; i++) {
            if ([[[_recordings objectAtIndex:i] uuid] isEqualToString:uuid]) {
                [_tableView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:i] columnIndexes:[NSIndexSet indexSetWithIndex:0]];
                break;
            }
        }
        _doNotDisturb = NO;
    }
    @catch(NSException *e) {
        _doNotDisturb = NO;
    }
}


#pragma mark - Window Delegate Methods

- (BOOL)windowShouldClose:(NSWindow *)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate managingRecordingsDone:self];
    return YES;
}


#pragma mark - Drag (not drop) Methods

- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)indexes toPasteboard:(NSPasteboard *)pasteboard {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [pasteboard declareTypes:@[NSFilesPromisePboardType] owner:self];
    NSMutableArray *setArray = [[NSMutableArray alloc] init];
    NSInteger i = [indexes firstIndex];
    do {
        if ([[_recordings objectAtIndex:i] uuid] == [appDelegate recordingUUID]) return NO;
        if (![fileManager fileExistsAtPath:[[_recordings objectAtIndex:i] filePath]]) return NO;
        [setArray addObject:[[[[_recordings objectAtIndex:i] fileType] lowercaseString] copy]];
        i = [indexes indexGreaterThanIndex:i];
    } while (i != NSNotFound);
    
    [pasteboard setPropertyList:setArray forType:NSFilesPromisePboardType];
    return YES;
}

- (NSArray <NSString *> *)tableView:(NSTableView *)tableView namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination forDraggedRowsWithIndexes:(NSIndexSet *)indexSet {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *draggedFilenames = [[NSMutableArray alloc] init];
    NSString *path = [dropDestination path];
    NSString *origPath;
    NSString *currentExt;
    NSString *currentPath;
    NSString *fullpath;
    NSInteger i = [indexSet firstIndex];
    do {
        PLRadioRecording *theItem = [_recordings objectAtIndex:i];
        origPath = [theItem filePath];
        fullpath = [NSString stringWithFormat:@"%@/%@.%@", path, [theItem fileName], [[theItem fileType] lowercaseString]];
        while ([fileManager fileExistsAtPath:fullpath]) {
            currentExt = [fullpath pathExtension];
            currentPath = [fullpath stringByDeletingPathExtension];
            fullpath = [PLUtilities getNameForCopy:currentPath];
            fullpath = [NSString stringWithFormat:@"%@.%@", fullpath, currentExt];
        }
        [draggedFilenames addObject:[fullpath lastPathComponent]];
        [fileManager copyItemAtPath:origPath toPath:fullpath error:nil];
        i = [indexSet indexGreaterThanIndex:i];
    } while (i != NSNotFound);
    return draggedFilenames;
}

@end
